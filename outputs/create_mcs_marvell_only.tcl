#
# Result of this script is mcs with FW for marvel PHY
# with header prepended. Header is used to by MB
# when reading FW from flash.
#
# Script is supposed to be placed in outputs directory.
# It is independent of opened project.
#

# Offset where in flash firmware shall be placed.
# Usually it must be after FPGA bitstream
set marvell_fw_flash_offset  0x800000

set origin_dir [file normalize [file dirname [info script]]]


# prepend header to marvell FW that will be used during read
# It creates file marvell_fw_hdr.bin in res folder
source $origin_dir/prepend_header_to_marvell_fw.tcl


# Interface SPIx4 or SPIx1 doesn't metter
# size is in megaBYTES so flash s25fl256 is 32MB long

write_cfgmem -force \
      -format mcs \
      -interface SPIx4  \
      -size 32 \
      -loaddata "up 0x00800000 $origin_dir/res/marvell_fw_hdr.bin" \
      -file "$origin_dir/res/marvell_only.mcs"

