# Copy all generated files necessary for final
# bitstream creation to src_data directory.
#
# Bitstream xenie_eth_example.bit and memory description file .mmi
# are copied from default raun/ impelemntation path.
# ELF of testing application for MB is copied from release
# result directory of default workspace and project.
#
# Script is supposed to be placed in outputs directory.
# It is independent of opened project.
#



set origin_dir [file normalize [file dirname [info script]]]
set src_data_dir [file normalize $origin_dir/src_data]
set run_dir [file normalize $origin_dir/../vivado/eth_example.runs/impl_1]

# Copy bitstream and memory description
file copy -force $run_dir/xenie_eth_example.bit $src_data_dir 
file copy -force $run_dir/xenie_eth_example.mmi $src_data_dir

# Copy elf

set elf_path [file normalize $origin_dir/../mb_fw/xenie_eth_test_womtd/Release/xenie_eth_test_womtd.elf]

file copy -force $elf_path $src_data_dir
