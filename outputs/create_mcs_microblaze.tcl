#
# Update content of block RAMs in bitstream.
# Elf for MB is loaded.
#
# Script is supposed to be in outputs directory.
# It is independent of opened project.
#



set origin_dir [file normalize [file dirname [info script]]]

set src_dir "$origin_dir/src_data"
set res_dir "$origin_dir/res"

exec updatemem -force \
   -meminfo $src_dir/xenie_eth_example.mmi \
   -data $src_dir/xenie_eth_test_womtd.elf \
   -bit $src_dir/xenie_eth_example.bit \
   -out $res_dir/xenie_eth_example_mb.bit \
   -proc "main_bd_inst/microblaze_0" 






# Interface SPIx4 or SPIx1 doesn't metter
# size is in megaBYTES so flash s25fl256 is 32MB long

write_cfgmem -force \
      -format mcs \
      -interface SPIx4  \
      -size 32 \
      -loadbit "up 0 $res_dir/xenie_eth_example_mb.bit" \
      -file $res_dir/xenie_eth_example_mb.mcs

