
# This script prepends header to marvell FW
# Header is needed during reading FW from flash
# It specifies mainly length of FW, but contains other
# information that are not necessary but convenient to have
#
# It takes the first file in src_data/marvell_fw as a FW, so only one
# file should be always in this folder
#
#
# Header is formatted like this:
# 4 byte magic number - binary form of ascii string "MVFW"
# 4 byte unsigned integer offset of FW in the flash (related to header start)
# 4 byte unsigned integer length of FW
# zero-ended string of creation date
# zero-ended string name of file with FW
#
# Script is supposed to be placed in outputs directory.
# It is independent of opened project.
#

set origin_dir [file normalize [file dirname [info script]]]
set fw_dir "$origin_dir/src_data/marvell_fw"


set fout [open $origin_dir/res/marvell_fw_hdr.bin w]
fconfigure $fout -translation binary

set potential_fw_list [glob -type f [concat $fw_dir/*] ]

set fw_full_filename [lindex $potential_fw_list 0]
set fw_filename [file tail $fw_full_filename]
set fw_length [file size $fw_full_filename]
set fw_filename_length [string length $fw_filename]

set creation_date [clock format [clock seconds] -format {%Y-%m-%d %H:%M:%S}]
set creation_date_length [string length $creation_date]

#calculate offset of FW
set fw_offset [expr 4 + 4 + 4 + $creation_date_length + 1 + \
      $fw_filename_length + 1]



# write file header
puts -nonewline $fout "MVFW"
puts -nonewline $fout [binary format i $fw_offset]
puts -nonewline $fout [binary format i $fw_length]
puts -nonewline $fout $creation_date
puts -nonewline $fout \000
puts -nonewline $fout $fw_filename
puts -nonewline $fout \000

# write FW payload
set fin [open $fw_full_filename]
fconfigure $fin -translation binary
fcopy $fin $fout
close $fin

# close output file
close $fout
