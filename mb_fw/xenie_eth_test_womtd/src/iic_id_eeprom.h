/******************************************************************************
**
** (C) Copyright 2013 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
*******************************************************************************
**
** This is driver for MAC EEPROM chip 24AA025E48.
**
*******************************************************************************
*/

#ifndef __IIC_ID_EEPROM_H__
#define __IIC_ID_EEPROM_H__

#include "iic_wrap.h"

struct iic_id_eeprom_dev {
	struct iic_wrap_dev *iic_wrap;
	u8 iic_addr;
	int iic_op_timeout;
};

#define ID_ADDR 0xFA
#define ID_EEPROM_MAX_SIZE 128

int iic_id_eeprom_init(struct iic_id_eeprom_dev *dev, struct iic_wrap_dev *iic_wrap, u8 iic_addr, int iic_op_timeout);
int iic_id_eeprom_getId(struct iic_id_eeprom_dev *dev, u8* id);
int iic_id_eeprom_read(struct iic_id_eeprom_dev *dev, u8 addr, u8* data, u8 len);
int iic_id_eeprom_write(struct iic_id_eeprom_dev *dev, u8 addr, u8* data, u8 len);

#endif /* __IIC_ID_EEPROM_H__ */

