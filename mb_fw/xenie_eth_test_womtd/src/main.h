/******************************************************************************
**
** (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
*******************************************************************************
*/


#ifndef __MAIN_H__
#define __MAIN_H__

/*
 * Structure describing revision info
 * that can be obtained from bitstream.
 */
struct bitstreamRev_s {
	u16	rev;
	u32 comp_date;
	u32 comp_time;
};

/*
 * Collection of system information that can be needed
 * on various places.
 */
struct sys_info_s {
	u16 hwRev;								/* Revision of board */
	u16 fwRev;								/* Revision of firmware */
	u8  uid[6];								/* Unique identifier (possible MAC)*/
	struct bitstreamRev_s bitRev;			/* Revision and built date of bitstream */

	struct {
		u32 ip;
		u32 netmask;
		u8  mac[6];
	} eth_settings;
	struct {
		int link_up;
		int speed;
	} eth_status;
};



#endif /*__MAIN_H__*/
