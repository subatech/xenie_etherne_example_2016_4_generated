/******************************************************************************
**
** (C) Copyright 2013 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
*******************************************************************************
**
** This is a wrapper for Xilinx IIC driver in interrupt mode.
**
*******************************************************************************
*/

#ifndef __IIC_WRAP_H__
#define __IIC_WRAP_H__
#include "xiic.h"

struct handler_info {
	int unprocessed_bytes;
	int error;		 /* set by interrupt in case of error */
	int finished; 	 /* marks end of operation - set by interrupt */
	int op_started;	 /* marks operation started */
	u32 end_tstmp; /* time when timeout should occur */

	int snd_star;
	int rcv_stat;
	int stat_stat;
};

struct iic_wrap_dev {
	XIic	iic;
	int started;		/* Xiic_start has been called already and no stop has beeen called */
	volatile struct handler_info hi;
};


int iic_wrap_init(struct iic_wrap_dev *dev, u16 IicDeviceId);
int iic_wrap_send_timeout(struct iic_wrap_dev *dev, u8 addr, u8 *tx_msg, int length, int timeout_ms);
int iic_wrap_recv_timeout(struct iic_wrap_dev *dev, u8 addr, u8 *rx_msg, int length, int timeout_ms);


#endif /* __IIC_WRAP_H__ */
