/***************************************************************************
 *
 * (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
 * Author: Marek Kvas (m.kvas@dspfpga.com)
 *
 ***************************************************************************
 *
 * This file is part of Xenia Ethernet Example project.
 * 
 * Xenia Ethernet Example project is free software: you can 
 * redistribute it and/or modify it under the terms of 
 * the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xenia Ethernet Example project is distributed in the hope that 
 * it will be useful, but WITHOUT ANY WARRANTY; without even 
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with Xenia Ethernet Example project.  If not, 
 * see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************
 */

#ifndef __GPIO_H__
#define __GPIO_H__

struct gpio_inst {
	u32 baseAddr;
	volatile u32 gpio_odata[2];
};

/* The first core IO 0*/
#define GPIO0_BANK	0
#define GPIO0_RXAUI_RESET		(1<<0)
#define GPIO0_ETH_PHY_RESET_N	(1<<1)

#define GPIO0_RXAUI_CLK156_LOCK	(1<<2)
#define GPIO0_RXAUI_QPLLLOCK	(1<<3)

#define GPIO0_RXAUI_DEBUG_SHIFT	4
#define GPIO0_RXAUI_DEBUG_MASK	(0x3f<<GPIO0_RXAUI_DEBUG_SHIFT)


#define GPIO0_ETH_PHY_CLK_SEL	(1<<10)
#define GPIO0_ALL_SRC_PG		(1<<11)

#define GPIO0_LED1				(1<<12)

#define GPIO0_FRAME_GEN_NRST	(1<<13)
#define GPIO0_XGMII_LOOPBACK	(1<<14)

/* The first core IO 1*/
#define GPIO1_BANK 1
#define GPIO1_PKT_GEN_IPG_SHIFT   		(0)
#define GPIO1_PKT_GEN_IPG_MASK    		(0xffff<<(GPIO1_PKT_GEN_IPG_SHIFT))
#define GPIO1_PKT_GEN_LEN_SHIFT   		(16)
#define GPIO1_PKT_GEN_LEN_MASK    		(0xffff<<(GPIO1_PKT_GEN_LEN_SHIFT))

/* The second core IO 0*/
#define GPIO2_BANK 2
/* The second core IO 1*/
#define GPIO3_BANK 3
#define VERSION_GPIO_0_BANK GPIO2_BANK
#define VERSION_GPIO_1_BANK GPIO3_BANK


/* The third core IO 0*/
#define GPIO4_BANK 4
#define GPIO4_HOST_MAC0_SHIFT   		(0)
#define GPIO4_HOST_MAC0_MASK    		(0xffffffff<<(GPIO4_HOST_MAC0_SHIFT))
#define GPIO4_DIR_HOST_MAC1_SHIFT   	(0)
#define GPIO4_DIR_HOST_MAC1_MASK    	(0xffff<<(GPIO4_DIR_HOST_MAC1_SHIFT))
#define GPIO4_DIR_LINK_SPEED_SHIFT   	(16)
#define GPIO4_DIR_LINK_SPEED_MASK    	(0x7<<(GPIO4_DIR_LINK_SPEED_SHIFT))
/* The third core IO 1*/
#define GPIO5_BANK 5
#define GPIO5_HOST_IP_SHIFT   			(0)
#define GPIO5_HOST_IP_MASK    			(0xffffffff<<(GPIO5_HOST_IP_SHIFT))
#define GPIO5_DIR_HOST_NETMASK_SHIFT   	(0)
#define GPIO5_DIR_HOST_NETMASK_MASK    	(0xffffffff<<(GPIO5_DIR_HOST_NETMASK_SHIFT))






void gpio_set_dir(int bank, u32 val);
void gpio_clear_dir(int bank, u32 val);
u32 gpio_get(int bank);
u32 gpio_get_dir(int bank);
void gpio_set_out(int bank, u32 val);
void gpio_clear_out(int bank, u32 val);
int gpio_wait_for_one_set(int bank, u32 gpioMask, u32 timeout_ms, void (*long_waiting_routine)(void* params), void *params);
void gpio_set_field(int bank, u32 mask, u32 shift, u32 val);
void gpio_set_dir_field(int bank, u32 mask, u32 shift, u32 val);
u32 gpio_get_field(int bank, u32 mask, u32 shift);
u32 gpio_get_dir_field(int bank, u32 mask, u32 shift);

void gpio_led(int led, int val);

int gpio_banks_available();


// Control functions for various gpio based regs
//void set_gpio_reg(u8 off, u32 value);
//u32 get_gpio_reg(u8 off);
void init_gpio_regs();


#endif /* __GPIO_H__ */

