/******************************************************************************
**
** (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
****************************************************************************
*/

#include "timers.h"


#define PERIODIC_TIMER_NO	0
#define ONE_SHOT_TIMER_NO   1

#define PERIODIC_TIMER_RST_VAL (0xffffffff - (TIMER_FREQ/1000)) /* ms */

/* internal clock with millisecond step */
static volatile u32 ms_time = 0;
static volatile int one_shot_flag = 0;

static XTmrCtr* TmrCtrInst;

u32 timers_ms_now()
{
	return ms_time;
}

u32 Timers_ms_elapsed(u32 start)
{
	u32 now;

	now = timers_ms_now();
	return timers_ms_diff(start, now);
}

u32 timers_ms_diff(u32 start, u32 stop)
{
	u32 elapsed;

	if (stop >= start)
		elapsed = (stop - start);
	else
		elapsed = 0xffffffff - (start - stop);

	return elapsed;
}

u32 timers_ms_elapsed_update(u32 *start)
{
	u32 now;
	u32 elapsed;

	now = timers_ms_now();
	elapsed = timers_ms_diff(now, *start);
	*start = now;
	return elapsed;
}

/* Timers handler */
void timers_Handler(void *CallBackRef, u8 TmrCtrNumber)
{
	XTmrCtr *InstancePtr = (XTmrCtr *)CallBackRef;

	/* service the periodic counter */
	if (XTmrCtr_IsExpired(InstancePtr, PERIODIC_TIMER_NO)) {
		ms_time++;
	}

}



/* Initialize timers (interrupts are already set */

int timers_init(XTmrCtr* TmrCtrInstancePtr, u16 DeviceId)
{
	int Status;

	TmrCtrInst = TmrCtrInstancePtr;

	/*
	 * Initialize the timer counter so that it's ready to use,
	 * specify the device ID that is generated in xparameters.h
	 */
	Status = XTmrCtr_Initialize(TmrCtrInst, DeviceId);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Setup the handler for both halfs of timer
	 */
	XTmrCtr_SetHandler(TmrCtrInst, timers_Handler,
			TmrCtrInst);

	/*
	 * Enable the interrupt
	 */

	/* Set up periodic counter */
	XTmrCtr_SetOptions(TmrCtrInst, PERIODIC_TIMER_NO,
				XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION);
	/* Set reset value determining timer period */
	XTmrCtr_SetResetValue(TmrCtrInst, PERIODIC_TIMER_NO, PERIODIC_TIMER_RST_VAL);


	/* Set up one-shot counter */
	XTmrCtr_SetOptions(TmrCtrInst, ONE_SHOT_TIMER_NO,
			XTC_DOWN_COUNT_OPTION/*XTC_INT_MODE_OPTION*/);


	/*
	 * Start the timer counter such that it's incrementing by default
	 */
	XTmrCtr_Start(TmrCtrInst, PERIODIC_TIMER_NO);

	return XST_SUCCESS;
}

void timers_udelay(u32 usDelay)
{
	u32 timer_preset;

	/* calculate preset value */
	timer_preset = (TIMER_FREQ/1000000) * usDelay; /* us */

	/* Set counter initial value */
	XTmrCtr_SetResetValue(TmrCtrInst, ONE_SHOT_TIMER_NO, timer_preset);

	/* Start timer */
	XTmrCtr_Start(TmrCtrInst, ONE_SHOT_TIMER_NO);

	/* Wait for timer to elapse */
	while (XTmrCtr_GetValue(TmrCtrInst, ONE_SHOT_TIMER_NO));

}
