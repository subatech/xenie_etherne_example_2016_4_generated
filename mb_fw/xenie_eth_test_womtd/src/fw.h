/***************************************************************************
 *
 * (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
 * Author: Marek Kvas (m.kvas@dspfpga.com)
 *
 ***************************************************************************
 *
 * This file is part of Xenia Ethernet Example project.
 * 
 * Xenia Ethernet Example project is free software: you can 
 * redistribute it and/or modify it under the terms of 
 * the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xenia Ethernet Example project is distributed in the hope that 
 * it will be useful, but WITHOUT ANY WARRANTY; without even 
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with Xenia Ethernet Example project.  If not, 
 * see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************
 */
#ifndef __FW_H__
#define __FW_H__

#include "spansion_flash.h"

/*
 * This structure describes FW header as
 * written in flash.
 */
struct fw_hdr {
	char magic[4]; /* Should be "MVFW" */
	unsigned int fw_offset; /* related to header start */
	unsigned int fw_length;
};

/*
 * This structure contains all information
 * for FW manipulation including pointer to
 * buffer containing FW is it has been already
 * read to memory.
 */
struct fw_hdr_ext {
	u32 fw_hdr_flash_off;
	struct fw_hdr fw_hdr;
	u8 *fw_meta_ptr; /*Always 1 byte larger than orig. meta-data to easy string handling*/
	u8 *fw_data_ptr;
};


void fw_free_hdr_ext_ptr(struct fw_hdr_ext **s);
int fw_find_in_flash(struct spansion_flash *sf, uint32_t fw_hdr_flash_offset, struct fw_hdr_ext **fw_hdr_ext_ptr);
void fw_print_info(struct fw_hdr_ext *fhe);
int fw_read_from_flash(struct spansion_flash *sf, struct fw_hdr_ext *fw_hdr_ext_ptr);

#endif /* __FW_H__ */
