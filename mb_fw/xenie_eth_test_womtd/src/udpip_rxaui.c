/******************************************************************************
**
** (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
****************************************************************************
*/

#include "sleep.h"
#include "gpio.h"
#include "udpip_rxaui.h"
#include "eth_phy.h"

void rxaui_core_reset()
{
	/* Assert reset */
	gpio_set_out(GPIO0_BANK, GPIO0_RXAUI_RESET);
	usleep(10*1000);
	/* Deassert */
	gpio_clear_out(GPIO0_BANK, GPIO0_RXAUI_RESET);
}

void udp_ip_core_reset(int rst)
{
	if (rst)
		gpio_clear_out(GPIO0_BANK, GPIO0_FRAME_GEN_NRST);
	else
		gpio_set_out(GPIO0_BANK, GPIO0_FRAME_GEN_NRST);
}

/*
 * Tell UDP/IP core what speed we are using.
 * If there is a mismatch between real link speed
 * and speed reported to UDP/IP core outgoing traffic
 * can be corrupted.
 *
 * Because of lack of synchronizers UDP/IP core should
 * be in reset during change
 *
 */

int udpip_core_set_speed(uint16_t phy_speed)
{
	enum udp_ip_core_speed spd;
	int ret = 0;

	switch(phy_speed) {
	case PHY_SPEED_10M_FD:
		spd = spd_10M;
		break;
	case PHY_SPEED_100M_FD:
		spd = spd_100M;
		break;
	case PHY_SPEED_1GIG_FD:
		spd = spd_1G;
		break;
	case PHY_SPEED_10GIG_FD:
		spd = spd_10G;
		break;
	case PHY_SPEED_2P5GIG_FD:
		spd = spd_2G5;
		break;
	case PHY_SPEED_5GIG_FD:
		spd = spd_5G;
		break;
	default:
		spd = spd_10M;
		ret = -1;
		break;
	}


	/* Set speed */
	gpio_set_dir_field(GPIO4_BANK,GPIO4_DIR_LINK_SPEED_MASK,
				GPIO4_DIR_LINK_SPEED_SHIFT, spd);

	return ret;
}

/*
 * Set host MAC address, IP address and netmask for
 * UDP/IP core.
 * Because of lack of synchronizers UDP/IP core should
 * be in reset during change
 */
void udpip_core_set_host_info(u8* mac, u32 ip, u32 nemask)
{
	/* MAC Address */
	gpio_set_field(GPIO4_BANK, GPIO4_HOST_MAC0_MASK,
			GPIO4_HOST_MAC0_SHIFT, mac[5] | (mac[4] << 8) |
			(mac[3] << 16) | (mac[2] << 24));
	gpio_set_dir_field(GPIO4_BANK, GPIO4_DIR_HOST_MAC1_MASK,
				GPIO4_DIR_HOST_MAC1_SHIFT, mac[1] | (mac[0] << 8));

	/* IP Address */
	gpio_set_field(GPIO5_BANK, GPIO5_HOST_IP_MASK, GPIO5_HOST_IP_SHIFT, ip);
	/* Netmask */
	gpio_set_dir_field(GPIO5_BANK, GPIO5_DIR_HOST_NETMASK_MASK,
			GPIO5_DIR_HOST_NETMASK_SHIFT, nemask);
}

