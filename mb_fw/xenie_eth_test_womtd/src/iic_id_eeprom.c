/******************************************************************************
**
** (C) Copyright 2013 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
**
*******************************************************************************
**
** This is driver for MAC EEPROM chip 24AA025E48.
**
*******************************************************************************
*/

#include "iic_id_eeprom.h"
#include "iic_wrap.h"

/*
 * Initialize context structure.
 * No communication with memory is done here.
 * Always succeeds - returns zero.
 */
int iic_id_eeprom_init(struct iic_id_eeprom_dev *dev,
		struct iic_wrap_dev *iic_wrap, u8 iic_addr, int iic_op_timeout)
{
	dev->iic_addr = iic_addr;
	dev->iic_wrap = iic_wrap;
	dev->iic_op_timeout = iic_op_timeout;
	return 0;
}


/*
 * Read continuous block of data to
 * buffer given by user.
 *
 * On success, zero is returned. On error,
 * negative number is returned.
 */
int iic_id_eeprom_read(struct iic_id_eeprom_dev *dev, u8 addr,
		u8* data, u8 len)
{
	unsigned res;

	if (len > ID_EEPROM_MAX_SIZE) {
		return -1;
	}

	/* Write command byte (address) */
	res = iic_wrap_send_timeout(dev->iic_wrap, dev->iic_addr,
			&addr, 1, dev->iic_op_timeout);
	if (res != 0) {
		return res;
	}

	/* Read data out */
	res = iic_wrap_recv_timeout(dev->iic_wrap, dev->iic_addr, data,
			len, dev->iic_op_timeout);

	return res;
}

/*
 * Write continuous block of data to
 * buffer given by user.
 *
 * On success, zero is returned. On error,
 * negative number is returned.
 */
int iic_id_eeprom_write(struct iic_id_eeprom_dev *dev, u8 addr, u8* data, u8 len)
{
	unsigned res;
	u8 buf[ID_EEPROM_MAX_SIZE + 1];

	if (len > ID_EEPROM_MAX_SIZE) {
		return -1;
	}

	buf[0] = addr;
	memcpy(buf + 1, data, len);

	/* Write command byte (address) and data*/
	res = iic_wrap_send_timeout(dev->iic_wrap, dev->iic_addr, buf, len + 1,
			dev->iic_op_timeout);
	return res;
}

/*
 * Read 48 bit long unique identifier to
 * buffer prepared by user.
 *
 * On success, zero is returned. On error,
 * negative number is returned.
 */
int iic_id_eeprom_getId(struct iic_id_eeprom_dev *dev, u8* id)
{
	return iic_id_eeprom_read(dev, ID_ADDR, id, 6);
}
