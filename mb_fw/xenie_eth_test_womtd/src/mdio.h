/******************************************************************************
**
** (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
****************************************************************************
*/

#ifndef __MDIO_H__
#define __MDIO_H__


#undef USE_ONE_WRITE_COMMANDING
#define USE_CMD_FIFO
#define CMD_FIFO_DEPTH		18


struct mdio_struct {
   unsigned int volatile divr; /* 8 bit divider */
   unsigned int volatile ctrl; /* control register (RUN, BUSY) */
   unsigned int volatile dout; /* data to be sent */
   unsigned int volatile din;  /* received data */
   unsigned int volatile opst; /* start sequence and operation */
   unsigned int volatile addr; /* register and phy address */
   unsigned int volatile occmd; /* one write cycle commanding(contains all info) - WO */
};


#define MDIO_RUN     (1<<1)
#define MDIO_BUSY    (1<<0)
#define MDIO_CMD_FIFO_FULL (1<<13)

#define MDIO_START_SEQ_MASK      (0x0300)
#define MDIO_START_SEQ_SHIFT     (8)

#define MDIO_OPERATION_MASK      (0x0003)
#define MDIO_OPEARTION_SHIFT     (0)

#define MDIO_PHYADDR_MASK        (0x1f00)
#define MDIO_PHYADDR_SHIFT       (8)

#define MDIO_PHYREG_MASK         (0x001f)
#define MDIO_PHYREG_SHIFT        (0)

#define MDIO_PREAMBLE_SUPPRESSION	(1<<8)

/* These are used when one write cycle commanding is used */
#define MDIO_OC_RUN					(1<<31)

#define MDIO_OC_START_SEQ_MASK      (0x30000000)
#define MDIO_OC_START_SEQ_SHIFT     (28)

#define MDIO_OC_OPERATION_MASK      (0x0c000000)
#define MDIO_OC_OPERATION_SHIFT     (26)

#define MDIO_OC_PHYADDR_MASK        (0x03e00000)
#define MDIO_OC_PHYADDR_SHIFT       (21)

#define MDIO_OC_PHYREG_MASK         (0x001f0000)
#define MDIO_OC_PHYREG_SHIFT        (16)

#define MDIO_OC_DATA_MASK           (0x0000ffff)
#define MDIO_OC_DATA_SHIFT          (0)


int mdio_send(volatile struct mdio_struct *dev, unsigned int start_seq,
              unsigned int operation, unsigned int phy_addr,
              unsigned int reg_addr, unsigned int data, int blocking);
int mdio_read(volatile struct mdio_struct *dev, unsigned int start_seq,
              unsigned int operation, unsigned int phy_addr,
              unsigned int reg_addr, unsigned int *data);

int mdio_set_options(volatile struct mdio_struct *dev, unsigned int div, int preamble_suppression);

int mdio_write_indirect(volatile struct mdio_struct *dev, uint16_t phyaddr,
		uint16_t devaddr, uint16_t regaddr, uint16_t data);
int mdio_write_indirect_nonblocking(volatile struct mdio_struct *dev, uint16_t phyaddr,
		uint16_t devaddr, uint16_t regaddr, uint16_t data);
int mdio_read_indirect(volatile struct mdio_struct *dev, uint16_t phyaddr,
		uint16_t devaddr, uint16_t regaddr, uint16_t *data);
int mdio_write_indirect_burst(volatile struct mdio_struct *dev, uint16_t phyaddr,
		uint16_t devaddr, uint16_t regaddr, uint8_t *data, int size);

#endif

