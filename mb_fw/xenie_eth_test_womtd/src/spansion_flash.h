/******************************************************************************
**
** (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
****************************************************************************
*/

#ifndef __SPANSION_FLASH_H__
#define __SPANSION_FLASH_H__

#include <xspi.h>

/*
 * Number of bytes per page in the flash device.
 */
#define SPANSION_FLASH_PAGE_SIZE		256
#define SPANSION_FLASH_BUF_OVERHEAD		8

struct spansion_flash {
	XSpi  *spi;
	uint32_t slave_select;
};


int spansion_flash_init (struct spansion_flash *sf, XSpi *spi, int slave_num);
int spansion_flash_quad_io_read(struct spansion_flash *sf, uint32_t flash_addr, uint8_t *buf, int len);
int spansion_flash_quad_mode(struct spansion_flash *sf);
int spansion_flash_extended_addressing(struct spansion_flash *sf, int onOff);
int spansion_flash_quad_io_read(struct spansion_flash *sf, uint32_t flash_addr, uint8_t *buf, int len);

#endif
