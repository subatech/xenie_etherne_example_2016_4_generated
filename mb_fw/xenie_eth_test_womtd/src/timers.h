/******************************************************************************
**
** (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
****************************************************************************
*/

#ifndef __TIMERS_H__
#define __TIMERS_H__

#include "xparameters.h"
#include "xtmrctr.h"


#define TIMER_FREQ			XPAR_AXI_TIMER_0_CLOCK_FREQ_HZ

int timers_init(XTmrCtr* TmrCtrInstancePtr, u16 DeviceId);
void timers_udelay(u32 usDelay);

u32 timers_ms_now();
u32 timers_ms_elapsed(u32 start);
u32 timers_ms_elapsed_update(u32 *start);
u32 timers_ms_diff(u32 start, u32 stop);

#endif /* __TIMERS_H__ */
