#ifndef __UPRINTF_H
#define __UPRINTF_H


void uprintf_init(void (*print)(void *inst, const char *buf, int len), void *inst);
void uprintf (char const *fmt0, ...);
int suprintf (char *buf, char const *fmt0, ...);

#endif

