/******************************************************************************
**
** (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
****************************************************************************
*/

#ifndef __UDPIP_RXAUI_H__
#define __UDPIP_RXAUI_H__

enum udp_ip_core_speed {
	spd_10G  = 0,
	spd_5G   = 1,
	spd_2G5  = 2,
	spd_1G   = 3,
	spd_100M = 4,
	spd_10M  = 5
};

void rxaui_core_reset();
void udp_ip_core_reset(int rst);
int udpip_core_set_speed(uint16_t phy_speed);
void udpip_core_set_host_info(u8* mac, u32 ip, u32 nemask);

#endif /* __UDPIP_RXAUI_H__ */
