/***************************************************************************
 *
 * (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
 * Author: Marek Kvas (m.kvas@dspfpga.com)
 *
 ***************************************************************************
 *
 * This file is part of Xenia Ethernet Example project.
 * 
 * Xenia Ethernet Example project is free software: you can 
 * redistribute it and/or modify it under the terms of 
 * the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xenia Ethernet Example project is distributed in the hope that 
 * it will be useful, but WITHOUT ANY WARRANTY; without even 
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with Xenia Ethernet Example project.  If not, 
 * see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************
 */

#include <stdio.h>
#include "xparameters.h"
#include "xgpio_l.h"
#include "gpio.h"

/*
 * List of available GPIO cores/banks
 */
static struct gpio_inst gpio_list[] =
		{{XPAR_AXI_GPIO_0_BASEADDR, {0,0}},
		 {XPAR_AXI_GPIO_1_VERSION_BASEADDR, {0,0}},
		 {XPAR_AXI_GPIO_2_NETINFO_BASEADDR, {0,0}},
		};

/*
 * return number of GPIO banks available.
 */
int gpio_banks_available()
{
	return (sizeof(gpio_list)/ sizeof(struct gpio_inst)) * 2;
}

/*
 * Set bits in direction register.
 * Bits that are 1 in val will be 1
 * in register too.
 */
void gpio_set_dir(int bank, u32 val)
{
	u32 data;
	int core_idx = bank/2;
	int port_idx = bank%2;
	u32 off = (port_idx == 0)?XGPIO_TRI_OFFSET:XGPIO_TRI2_OFFSET;
	u32 baseAddr = gpio_list[core_idx].baseAddr;

	data = XGpio_ReadReg(baseAddr, off);
	data |= val;
	XGpio_WriteReg(baseAddr, off, data);
}

/*
 * Clear bits in direction register.
 * Bits that are 1 in val will be 0
 * in register.
 */
void gpio_clear_dir(int bank, u32 val)
{
	u32 data;
	int core_idx = bank/2;
	int port_idx = bank%2;
	u32 off = (port_idx == 0)?XGPIO_TRI_OFFSET:XGPIO_TRI2_OFFSET;
	u32 baseAddr = gpio_list[core_idx].baseAddr;

	data = XGpio_ReadReg(baseAddr, off);
	data &= (~val);
	XGpio_WriteReg(baseAddr, off, data);
}

/*
 * Get current state of inputs
 * for specified bank.
 */
u32 gpio_get(int bank)
{
	int core_idx = bank/2;
	int port_idx = bank%2;
	u32 off = (port_idx == 0)?XGPIO_DATA_OFFSET:XGPIO_DATA2_OFFSET;
	u32 baseAddr = gpio_list[core_idx].baseAddr;
	return XGpio_ReadReg(baseAddr, off);
}

/*
 * Get current state of direction
 * register for specified bank.
 */
u32 gpio_get_dir(int bank)
{
	int core_idx = bank/2;
	int port_idx = bank%2;
	u32 off = (port_idx == 0)?XGPIO_TRI_OFFSET:XGPIO_TRI2_OFFSET;
	u32 baseAddr = gpio_list[core_idx].baseAddr;
	return XGpio_ReadReg(baseAddr, off);
}

/*
 * Set bits in output  register.
 * Bits that are 1 in val will be 1
 * in register too.
 */
void gpio_set_out(int bank, u32 val)
{
	int core_idx = bank/2;
	int port_idx = bank%2;
	u32 off = (port_idx == 0)?XGPIO_DATA_OFFSET:XGPIO_DATA2_OFFSET;
	u32 baseAddr = gpio_list[core_idx].baseAddr;
	gpio_list[core_idx].gpio_odata[port_idx] |= val;
	XGpio_WriteReg(baseAddr, off, gpio_list[core_idx].gpio_odata[port_idx]);
}

/*
 * Clear bits in output  register.
 * Bits that are 1 in val will be 0
 * in register.
 */
void gpio_clear_out(int bank, u32 val)
{
	int core_idx = bank/2;
	int port_idx = bank%2;
	u32 off = (port_idx == 0)?XGPIO_DATA_OFFSET:XGPIO_DATA2_OFFSET;
	u32 baseAddr = gpio_list[core_idx].baseAddr;
	gpio_list[core_idx].gpio_odata[port_idx] &= (~val);
	XGpio_WriteReg(baseAddr, off, gpio_list[core_idx].gpio_odata[port_idx]);
}

/*
 * Set bit field in output register to
 * specified value val.
 */
void gpio_set_field(int bank, u32 mask, u32 shift, u32 val)
{
	int core_idx = bank/2;
	int port_idx = bank%2;
	u32 off = (port_idx == 0)?XGPIO_DATA_OFFSET:XGPIO_DATA2_OFFSET;
	u32 baseAddr = gpio_list[core_idx].baseAddr;
	gpio_list[core_idx].gpio_odata[port_idx] &= ~mask;
	gpio_list[core_idx].gpio_odata[port_idx] |= (val << shift) & mask;
	XGpio_WriteReg(baseAddr, off, gpio_list[core_idx].gpio_odata[port_idx]);
}

/*
 * Set bit field in direction register to
 * specified value val.
 */
void gpio_set_dir_field(int bank, u32 mask, u32 shift, u32 val)
{
	u32 data;
	int core_idx = bank/2;
	int port_idx = bank%2;

	u32 off = (port_idx == 0)?XGPIO_TRI_OFFSET:XGPIO_TRI2_OFFSET;
	u32 baseAddr = gpio_list[core_idx].baseAddr;

	data = XGpio_ReadReg(baseAddr, off);
	data &= ~mask;
	data |= (val << shift) & mask;
	XGpio_WriteReg(baseAddr, off, data);
}

/*
 * Get value of bit field from inputs.
 */
u32 gpio_get_field(int bank, u32 mask, u32 shift)
{
	u32 data = gpio_get(bank);
	data &= mask;
	data >>= shift;
	return data;
}

/*
 * Get value of bit field from direction register.
 */
u32 gpio_get_dir_field(int bank, u32 mask, u32 shift)
{
	u32 data = gpio_get_dir(bank);
	data &= mask;
	data >>= shift;
	return data;
}

/*
 * Initialize GPIOs to default state.
 * By default all pins are set as inputs
 * (all ones in direction register) and
 * output register is cleared.
 *
 * This is done for all known GPIO banks.
 */
void init_gpio_regs()
{
	int i;
	for (i = 0; i < gpio_banks_available();i++) {
		gpio_clear_out(i, 0xffffffff);
		gpio_set_dir(i, 0xffffffff);
	}
}

