/******************************************************************************
**
** (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
** Author: Marek Kvas (m.kvas@dspfpga.com)
**
****************************************************************************
**
** This file is part of Xenia Ethernet Example project.
** 
** Xenia Ethernet Example project is free software: you can 
** redistribute it and/or modify it under the terms of 
** the GNU Lesser General Public License as published by the Free 
** Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** Xenia Ethernet Example project is distributed in the hope that 
** it will be useful, but WITHOUT ANY WARRANTY; without even 
** the implied warranty of MERCHANTABILITY or FITNESS FOR A 
** PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
** for more details.
** 
** You should have received a copy of the GNU Lesser General Public 
** License along with Xenia Ethernet Example project.  If not, 
** see <http://www.gnu.org/licenses/>.
****************************************************************************
*/

#ifndef __ETH_PHY_H__
#define __ETH_PHY_H__

/* Link speed constants used to specify what
 * speeds are advertized for autonegotiation or
 * what final resolved speed is. They can be
 * combined by ORring in the first case.
 */
#define PHY_ADV_NONE           0x0000 // No speeds to be advertised
#define PHY_SPEED_10M_HD       0x0001 // 10BT half-duplex
#define PHY_SPEED_10M_FD       0x0002 // 10BT full-duplex
#define PHY_SPEED_100M_HD      0x0004 // 100BASE-TX half-duplex
#define PHY_SPEED_100M_FD      0x0008 // 100BASE-TX full-duplex
#define PHY_SPEED_1GIG_HD      0x0010 // 1000BASE-T half-duplex
#define PHY_SPEED_1GIG_FD      0x0020 // 1000BASE-T full-duplex
#define PHY_SPEED_10GIG_FD     0x0040 // 10GBASE-T full-duplex
#define PHY_SPEED_2P5GIG_FD    0x0800 // 2.5GBASE-T full-duplex, 88X33X0/88E20X0 family only
#define PHY_SPEED_5GIG_FD      0x1000 // 5GBASE-T full-duplex, 88X33X0/88E20X0 family only

/*
 * Structure holding context of PHY
 * driver. User should never need its
 * contents.
 */
struct phy_dev;

/*
 * Typedefs for function pointers that point
 * to user-defined mdio access routines.
 */
typedef int (*mdio_read_fcn_type)(void *ctx, uint16_t phyaddr, uint16_t devaddr,
		uint16_t regaddr, uint16_t *data);
typedef int (*mdio_write_fcn_type)(void *ctx, uint16_t phyaddr, uint16_t devaddr,
		uint16_t regaddr, uint16_t data);
typedef int (*mdio_write_burst_fcn_type)(void *ctx, uint16_t phyaddr, uint16_t devaddr,
		uint16_t regaddr, uint8_t *data, int size);

/*
 * Initialize driver of PHY.
 * Space for struct phy_dev is dynamically
 * allocated inside the function - if it succeeds.
 *
 * On success, zero is returned. On error, -1 is returned.
 *
 * It fails if PHY is not accessible via MDIO - e.g. held in reset.
 */
int phy_init_drv(struct phy_dev **dev,
		uint16_t phy_addr,
		mdio_read_fcn_type mdio_read_fcn,
		mdio_write_fcn_type mdio_write_fcn,
		mdio_write_burst_fcn_type mdio_write_burst_fcn,
		void *mdio_ctx);

/*
 * Switch PHY to Xilinx RGMII core RXAUI mode.
 *
 * On success, zero is returned. On error, -1 is returned.
 */
int phy_configure_xilinx_rgmii(struct phy_dev *dev);

/*
 * Get revision of firmware that is currently running
 * by PHY's internal processor.
 *
 * On success, zero is returned. On error, -1 is returned.
 *
 * It fails if PHY is not accessible, or no firmware
 * is running. In case of fauilure all parts of version number
 * are set to 0.
 */
int phy_get_fw_rev (struct phy_dev *dev, uint8_t *fw_maj, uint8_t *fw_min,
		uint8_t *fw_inc, uint8_t *fw_test);
/*
 * Update and start new firmware on PHY's internal
 * processor.
 *
 * On success, zero is returned. On error, -1 is returned.
 *
 * If it succeeds firmware is up and running.
 */
int phy_update_fw(struct phy_dev *dev, void *fw_data, int fw_length);

/*
 * Set speeds that will be advertised for autonegotiation.
 * Autonegotiation is restarted.
 *
 * On success, zero is returned. On error, -1 is returned.
 *
 */
int phy_enable_speeds(struct phy_dev *dev, uint16_t speeds);

/*
 * Get link state and speed.
 *
 * On success, zero is returned. On error, -1 is returned.
 *
 * If function failed, speed and link values are invalid.
 */
int phy_is_baseT_up(struct phy_dev *dev, uint16_t *speed, int *link);

/*
 * Return string that contains version info of this library
 * and underlying Marvell MTD library.
 */
char *phy_get_version_string();

#endif /* __ETH_PHY_H__ */
