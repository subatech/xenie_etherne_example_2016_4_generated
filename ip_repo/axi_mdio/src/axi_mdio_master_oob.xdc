
# This constraints file is supposed to be used only during OOB synthesis run
# to specify default target clocks.

create_clock -period 10.000 -name S_AXI_ACLK_OOB -waveform {0.000 5.000} [get_ports {S_AXI_ACLK}]
