# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "Component_Name" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S_AXI_DATA_WIDTH" -parent ${Page_0}
  set g_add_synchronizers [ipgui::add_param $IPINST -name "g_add_synchronizers" -parent ${Page_0}]
  set_property tooltip {Adds two-stage synchronizer at MDIO_I if true} ${g_add_synchronizers}
  ipgui::add_param $IPINST -name "g_include_cmd_fifo" -parent ${Page_0}


}

proc update_PARAM_VALUE.g_add_synchronizers { PARAM_VALUE.g_add_synchronizers } {
	# Procedure called to update g_add_synchronizers when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_add_synchronizers { PARAM_VALUE.g_add_synchronizers } {
	# Procedure called to validate g_add_synchronizers
	return true
}

proc update_PARAM_VALUE.g_include_cmd_fifo { PARAM_VALUE.g_include_cmd_fifo } {
	# Procedure called to update g_include_cmd_fifo when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_include_cmd_fifo { PARAM_VALUE.g_include_cmd_fifo } {
	# Procedure called to validate g_include_cmd_fifo
	return true
}

proc update_PARAM_VALUE.C_S_AXI_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_ADDR_WIDTH } {
	# Procedure called to update C_S_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_S_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S_AXI_DATA_WIDTH { PARAM_VALUE.C_S_AXI_DATA_WIDTH } {
	# Procedure called to update C_S_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_DATA_WIDTH { PARAM_VALUE.C_S_AXI_DATA_WIDTH } {
	# Procedure called to validate C_S_AXI_DATA_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.g_add_synchronizers { MODELPARAM_VALUE.g_add_synchronizers PARAM_VALUE.g_add_synchronizers } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_add_synchronizers}] ${MODELPARAM_VALUE.g_add_synchronizers}
}

proc update_MODELPARAM_VALUE.C_S_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_S_AXI_DATA_WIDTH PARAM_VALUE.C_S_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_S_AXI_ADDR_WIDTH PARAM_VALUE.C_S_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.g_include_cmd_fifo { MODELPARAM_VALUE.g_include_cmd_fifo PARAM_VALUE.g_include_cmd_fifo } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_include_cmd_fifo}] ${MODELPARAM_VALUE.g_include_cmd_fifo}
}

