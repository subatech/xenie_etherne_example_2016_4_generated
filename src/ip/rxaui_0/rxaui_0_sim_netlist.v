// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
// Date        : Wed Mar 29 09:06:36 2017
// Host        : PCKVAS running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim
//               C:/projects/dfc/xenie/Eth_example/trunk/src/ip/rxaui_0/rxaui_0_sim_netlist.v
// Design      : rxaui_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7k70tfbg676-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* x_core_info = "rxaui_v4_3_7,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module rxaui_0
   (reset,
    dclk,
    clk156_out,
    clk156_lock,
    refclk_p,
    refclk_n,
    qplloutclk_out,
    qplllock_out,
    qplloutrefclk_out,
    refclk_out,
    xgmii_txd,
    xgmii_txc,
    xgmii_rxd,
    xgmii_rxc,
    rxaui_tx_l0_p,
    rxaui_tx_l0_n,
    rxaui_tx_l1_p,
    rxaui_tx_l1_n,
    rxaui_rx_l0_p,
    rxaui_rx_l0_n,
    rxaui_rx_l1_p,
    rxaui_rx_l1_n,
    signal_detect,
    debug,
    mdc,
    mdio_in,
    mdio_out,
    mdio_tri,
    prtad,
    type_sel);
  input reset;
  input dclk;
  output clk156_out;
  output clk156_lock;
  input refclk_p;
  input refclk_n;
  output qplloutclk_out;
  output qplllock_out;
  output qplloutrefclk_out;
  output refclk_out;
  input [63:0]xgmii_txd;
  input [7:0]xgmii_txc;
  output [63:0]xgmii_rxd;
  output [7:0]xgmii_rxc;
  output rxaui_tx_l0_p;
  output rxaui_tx_l0_n;
  output rxaui_tx_l1_p;
  output rxaui_tx_l1_n;
  input rxaui_rx_l0_p;
  input rxaui_rx_l0_n;
  input rxaui_rx_l1_p;
  input rxaui_rx_l1_n;
  input [1:0]signal_detect;
  output [5:0]debug;
  input mdc;
  input mdio_in;
  output mdio_out;
  output mdio_tri;
  input [4:0]prtad;
  input [1:0]type_sel;

  wire clk156_lock;
  wire clk156_out;
  wire dclk;
  wire [5:0]debug;
  wire mdc;
  wire mdio_in;
  wire mdio_out;
  wire mdio_tri;
  wire [4:0]prtad;
  wire qplllock_out;
  wire qplloutclk_out;
  wire qplloutrefclk_out;
  (* IBUF_LOW_PWR = 0 *) wire refclk_n;
  wire refclk_out;
  (* IBUF_LOW_PWR = 0 *) wire refclk_p;
  wire reset;
  wire rxaui_rx_l0_n;
  wire rxaui_rx_l0_p;
  wire rxaui_rx_l1_n;
  wire rxaui_rx_l1_p;
  wire rxaui_tx_l0_n;
  wire rxaui_tx_l0_p;
  wire rxaui_tx_l1_n;
  wire rxaui_tx_l1_p;
  wire [1:0]signal_detect;
  wire [1:0]type_sel;
  wire [7:0]xgmii_rxc;
  wire [63:0]xgmii_rxd;
  wire [7:0]xgmii_txc;
  wire [63:0]xgmii_txd;
  wire NLW_U0_gt0_drprdy_UNCONNECTED;
  wire NLW_U0_gt0_eyescandataerror_out_UNCONNECTED;
  wire NLW_U0_gt0_rxcommadet_out_UNCONNECTED;
  wire NLW_U0_gt0_rxprbserr_out_UNCONNECTED;
  wire NLW_U0_gt0_rxresetdone_out_UNCONNECTED;
  wire NLW_U0_gt0_txdlysresetdone_out_UNCONNECTED;
  wire NLW_U0_gt0_txphaligndone_out_UNCONNECTED;
  wire NLW_U0_gt0_txphinitdone_out_UNCONNECTED;
  wire NLW_U0_gt0_txresetdone_out_UNCONNECTED;
  wire NLW_U0_gt1_drprdy_UNCONNECTED;
  wire NLW_U0_gt1_eyescandataerror_out_UNCONNECTED;
  wire NLW_U0_gt1_rxcommadet_out_UNCONNECTED;
  wire NLW_U0_gt1_rxprbserr_out_UNCONNECTED;
  wire NLW_U0_gt1_rxresetdone_out_UNCONNECTED;
  wire NLW_U0_gt1_txdlysresetdone_out_UNCONNECTED;
  wire NLW_U0_gt1_txphaligndone_out_UNCONNECTED;
  wire NLW_U0_gt1_txphinitdone_out_UNCONNECTED;
  wire NLW_U0_gt1_txresetdone_out_UNCONNECTED;
  wire NLW_U0_gt_qplllock_out_UNCONNECTED;
  wire [7:0]NLW_U0_gt0_dmonitorout_out_UNCONNECTED;
  wire [15:0]NLW_U0_gt0_drpdo_UNCONNECTED;
  wire [2:0]NLW_U0_gt0_rxbufstatus_out_UNCONNECTED;
  wire [3:0]NLW_U0_gt0_rxdisperr_out_UNCONNECTED;
  wire [6:0]NLW_U0_gt0_rxmonitorout_out_UNCONNECTED;
  wire [3:0]NLW_U0_gt0_rxnotintable_out_UNCONNECTED;
  wire [7:0]NLW_U0_gt1_dmonitorout_out_UNCONNECTED;
  wire [15:0]NLW_U0_gt1_drpdo_UNCONNECTED;
  wire [2:0]NLW_U0_gt1_rxbufstatus_out_UNCONNECTED;
  wire [3:0]NLW_U0_gt1_rxdisperr_out_UNCONNECTED;
  wire [6:0]NLW_U0_gt1_rxmonitorout_out_UNCONNECTED;
  wire [3:0]NLW_U0_gt1_rxnotintable_out_UNCONNECTED;

  rxaui_0_rxaui_0_support U0
       (.clk156_lock(clk156_lock),
        .clk156_out(clk156_out),
        .dclk(dclk),
        .debug(debug),
        .gt0_dmonitorout_out(NLW_U0_gt0_dmonitorout_out_UNCONNECTED[7:0]),
        .gt0_drpaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gt0_drpdi({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gt0_drpdo(NLW_U0_gt0_drpdo_UNCONNECTED[15:0]),
        .gt0_drpen(1'b0),
        .gt0_drprdy(NLW_U0_gt0_drprdy_UNCONNECTED),
        .gt0_drpwe(1'b0),
        .gt0_eyescandataerror_out(NLW_U0_gt0_eyescandataerror_out_UNCONNECTED),
        .gt0_eyescanreset_in(1'b0),
        .gt0_eyescantrigger_in(1'b0),
        .gt0_loopback_in({1'b0,1'b0,1'b0}),
        .gt0_rxbufstatus_out(NLW_U0_gt0_rxbufstatus_out_UNCONNECTED[2:0]),
        .gt0_rxcdrhold_in(1'b0),
        .gt0_rxcommadet_out(NLW_U0_gt0_rxcommadet_out_UNCONNECTED),
        .gt0_rxdfelpmreset_in(1'b0),
        .gt0_rxdisperr_out(NLW_U0_gt0_rxdisperr_out_UNCONNECTED[3:0]),
        .gt0_rxlpmen_in(1'b1),
        .gt0_rxmonitorout_out(NLW_U0_gt0_rxmonitorout_out_UNCONNECTED[6:0]),
        .gt0_rxmonitorsel_in({1'b0,1'b0}),
        .gt0_rxnotintable_out(NLW_U0_gt0_rxnotintable_out_UNCONNECTED[3:0]),
        .gt0_rxpcsreset_in(1'b0),
        .gt0_rxpmareset_in(1'b0),
        .gt0_rxpolarity_in(1'b0),
        .gt0_rxprbscntreset_in(1'b0),
        .gt0_rxprbserr_out(NLW_U0_gt0_rxprbserr_out_UNCONNECTED),
        .gt0_rxprbssel_in({1'b0,1'b0,1'b0}),
        .gt0_rxrate_in({1'b0,1'b0,1'b0}),
        .gt0_rxresetdone_out(NLW_U0_gt0_rxresetdone_out_UNCONNECTED),
        .gt0_txdiffctrl_in({1'b1,1'b0,1'b1,1'b0}),
        .gt0_txdlysresetdone_out(NLW_U0_gt0_txdlysresetdone_out_UNCONNECTED),
        .gt0_txinhibit_in(1'b0),
        .gt0_txpcsreset_in(1'b0),
        .gt0_txphaligndone_out(NLW_U0_gt0_txphaligndone_out_UNCONNECTED),
        .gt0_txphinitdone_out(NLW_U0_gt0_txphinitdone_out_UNCONNECTED),
        .gt0_txpmareset_in(1'b0),
        .gt0_txpolarity_in(1'b0),
        .gt0_txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gt0_txprbsforceerr_in(1'b0),
        .gt0_txprbssel_in({1'b0,1'b0,1'b0}),
        .gt0_txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gt0_txresetdone_out(NLW_U0_gt0_txresetdone_out_UNCONNECTED),
        .gt1_dmonitorout_out(NLW_U0_gt1_dmonitorout_out_UNCONNECTED[7:0]),
        .gt1_drpaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gt1_drpdi({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gt1_drpdo(NLW_U0_gt1_drpdo_UNCONNECTED[15:0]),
        .gt1_drpen(1'b0),
        .gt1_drprdy(NLW_U0_gt1_drprdy_UNCONNECTED),
        .gt1_drpwe(1'b0),
        .gt1_eyescandataerror_out(NLW_U0_gt1_eyescandataerror_out_UNCONNECTED),
        .gt1_eyescanreset_in(1'b0),
        .gt1_eyescantrigger_in(1'b0),
        .gt1_loopback_in({1'b0,1'b0,1'b0}),
        .gt1_rxbufstatus_out(NLW_U0_gt1_rxbufstatus_out_UNCONNECTED[2:0]),
        .gt1_rxcdrhold_in(1'b0),
        .gt1_rxcommadet_out(NLW_U0_gt1_rxcommadet_out_UNCONNECTED),
        .gt1_rxdfelpmreset_in(1'b0),
        .gt1_rxdisperr_out(NLW_U0_gt1_rxdisperr_out_UNCONNECTED[3:0]),
        .gt1_rxlpmen_in(1'b1),
        .gt1_rxmonitorout_out(NLW_U0_gt1_rxmonitorout_out_UNCONNECTED[6:0]),
        .gt1_rxmonitorsel_in({1'b0,1'b0}),
        .gt1_rxnotintable_out(NLW_U0_gt1_rxnotintable_out_UNCONNECTED[3:0]),
        .gt1_rxpcsreset_in(1'b0),
        .gt1_rxpmareset_in(1'b0),
        .gt1_rxpolarity_in(1'b0),
        .gt1_rxprbscntreset_in(1'b0),
        .gt1_rxprbserr_out(NLW_U0_gt1_rxprbserr_out_UNCONNECTED),
        .gt1_rxprbssel_in({1'b0,1'b0,1'b0}),
        .gt1_rxrate_in({1'b0,1'b0,1'b0}),
        .gt1_rxresetdone_out(NLW_U0_gt1_rxresetdone_out_UNCONNECTED),
        .gt1_txdiffctrl_in({1'b1,1'b0,1'b1,1'b0}),
        .gt1_txdlysresetdone_out(NLW_U0_gt1_txdlysresetdone_out_UNCONNECTED),
        .gt1_txinhibit_in(1'b0),
        .gt1_txpcsreset_in(1'b0),
        .gt1_txphaligndone_out(NLW_U0_gt1_txphaligndone_out_UNCONNECTED),
        .gt1_txphinitdone_out(NLW_U0_gt1_txphinitdone_out_UNCONNECTED),
        .gt1_txpmareset_in(1'b0),
        .gt1_txpolarity_in(1'b0),
        .gt1_txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gt1_txprbsforceerr_in(1'b0),
        .gt1_txprbssel_in({1'b0,1'b0,1'b0}),
        .gt1_txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gt1_txresetdone_out(NLW_U0_gt1_txresetdone_out_UNCONNECTED),
        .gt_qplllock_out(NLW_U0_gt_qplllock_out_UNCONNECTED),
        .mdc(mdc),
        .mdio_in(mdio_in),
        .mdio_out(mdio_out),
        .mdio_tri(mdio_tri),
        .prtad(prtad),
        .qplllock_out(qplllock_out),
        .qplloutclk_out(qplloutclk_out),
        .qplloutrefclk_out(qplloutrefclk_out),
        .refclk_n(refclk_n),
        .refclk_out(refclk_out),
        .refclk_p(refclk_p),
        .reset(reset),
        .rxaui_rx_l0_n(rxaui_rx_l0_n),
        .rxaui_rx_l0_p(rxaui_rx_l0_p),
        .rxaui_rx_l1_n(rxaui_rx_l1_n),
        .rxaui_rx_l1_p(rxaui_rx_l1_p),
        .rxaui_tx_l0_n(rxaui_tx_l0_n),
        .rxaui_tx_l0_p(rxaui_tx_l0_p),
        .rxaui_tx_l1_n(rxaui_tx_l1_n),
        .rxaui_tx_l1_p(rxaui_tx_l1_p),
        .signal_detect(signal_detect),
        .type_sel(type_sel),
        .xgmii_rxc(xgmii_rxc),
        .xgmii_rxd(xgmii_rxd),
        .xgmii_txc(xgmii_txc),
        .xgmii_txd(xgmii_txd));
endmodule

(* ORIG_REF_NAME = "rxaui_0_block" *) 
module rxaui_0_rxaui_0_block
   (CLK,
    xgmii_rxd,
    xgmii_rxc,
    debug,
    mdio_out,
    mdio_tri,
    out,
    gt0_drprdy,
    gt0_eyescandataerror_out,
    rxaui_tx_l0_n,
    rxaui_tx_l0_p,
    gt0_rxcommadet_out,
    gt0_rxprbserr_out,
    gt0_rxresetdone_out,
    gt0_txdlysresetdone_out,
    gt0_txphaligndone_out,
    gt0_txphinitdone_out,
    D,
    gt0_drpdo,
    \uclk_mgt_rxbufstatus_reg_reg[5]_0 ,
    gt0_rxmonitorout_out,
    gt0_dmonitorout_out,
    \mgt_rxdisperr_reg_reg[7]_0 ,
    \mgt_rxnotintable_reg_reg[7]_0 ,
    gt1_drprdy,
    gt1_eyescandataerror_out,
    rxaui_tx_l1_n,
    rxaui_tx_l1_p,
    gt1_rxcommadet_out,
    gt1_rxprbserr_out,
    gt1_rxresetdone_out,
    gt1_txdlysresetdone_out,
    gt1_txphaligndone_out,
    gt1_txphinitdone_out,
    gt1_drpdo,
    gt1_rxmonitorout_out,
    gt1_dmonitorout_out,
    xgmii_txd,
    xgmii_txc,
    mdc,
    mdio_in,
    type_sel,
    prtad,
    dclk,
    gt0_drpen,
    gt0_drpwe,
    gt0_eyescanreset_in,
    gt0_eyescantrigger_in,
    rxaui_rx_l0_n,
    rxaui_rx_l0_p,
    qplloutclk_out,
    qplloutrefclk_out,
    gt0_rxcdrhold_in,
    gt0_rxdfelpmreset_in,
    gt0_rxlpmen_in,
    gt0_rxpcsreset_in,
    gt0_rxpolarity_in,
    gt0_rxprbscntreset_in,
    gt0_txinhibit_in,
    gt0_txpcsreset_in,
    gt0_txpmareset_in,
    gt0_txpolarity_in,
    gt0_txprbsforceerr_in,
    gt0_drpdi,
    gt0_rxmonitorsel_in,
    gt0_loopback_in,
    gt0_rxprbssel_in,
    gt0_rxrate_in,
    gt0_txprbssel_in,
    gt0_txdiffctrl_in,
    gt0_txpostcursor_in,
    gt0_txprecursor_in,
    gt0_drpaddr,
    gt1_drpen,
    gt1_drpwe,
    gt1_eyescanreset_in,
    gt1_eyescantrigger_in,
    rxaui_rx_l1_n,
    rxaui_rx_l1_p,
    gt1_rxcdrhold_in,
    gt1_rxdfelpmreset_in,
    gt1_rxlpmen_in,
    gt1_rxpcsreset_in,
    gt1_rxpolarity_in,
    gt1_rxprbscntreset_in,
    gt1_txinhibit_in,
    gt1_txpcsreset_in,
    gt1_txpmareset_in,
    gt1_txpolarity_in,
    gt1_txprbsforceerr_in,
    gt1_drpdi,
    gt1_rxmonitorsel_in,
    gt1_loopback_in,
    gt1_rxprbssel_in,
    gt1_rxrate_in,
    gt1_txprbssel_in,
    gt1_txdiffctrl_in,
    gt1_txpostcursor_in,
    gt1_txprecursor_in,
    gt1_drpaddr,
    reset,
    signal_detect,
    gt0_rxpmareset_in,
    gt1_rxpmareset_in,
    qplllock_out);
  output CLK;
  output [63:0]xgmii_rxd;
  output [7:0]xgmii_rxc;
  output [5:0]debug;
  output mdio_out;
  output mdio_tri;
  output [0:0]out;
  output gt0_drprdy;
  output gt0_eyescandataerror_out;
  output rxaui_tx_l0_n;
  output rxaui_tx_l0_p;
  output gt0_rxcommadet_out;
  output gt0_rxprbserr_out;
  output gt0_rxresetdone_out;
  output gt0_txdlysresetdone_out;
  output gt0_txphaligndone_out;
  output gt0_txphinitdone_out;
  output [1:0]D;
  output [15:0]gt0_drpdo;
  output [5:0]\uclk_mgt_rxbufstatus_reg_reg[5]_0 ;
  output [6:0]gt0_rxmonitorout_out;
  output [7:0]gt0_dmonitorout_out;
  output [7:0]\mgt_rxdisperr_reg_reg[7]_0 ;
  output [7:0]\mgt_rxnotintable_reg_reg[7]_0 ;
  output gt1_drprdy;
  output gt1_eyescandataerror_out;
  output rxaui_tx_l1_n;
  output rxaui_tx_l1_p;
  output gt1_rxcommadet_out;
  output gt1_rxprbserr_out;
  output gt1_rxresetdone_out;
  output gt1_txdlysresetdone_out;
  output gt1_txphaligndone_out;
  output gt1_txphinitdone_out;
  output [15:0]gt1_drpdo;
  output [6:0]gt1_rxmonitorout_out;
  output [7:0]gt1_dmonitorout_out;
  input [63:0]xgmii_txd;
  input [7:0]xgmii_txc;
  input mdc;
  input mdio_in;
  input [1:0]type_sel;
  input [4:0]prtad;
  input dclk;
  input gt0_drpen;
  input gt0_drpwe;
  input gt0_eyescanreset_in;
  input gt0_eyescantrigger_in;
  input rxaui_rx_l0_n;
  input rxaui_rx_l0_p;
  input qplloutclk_out;
  input qplloutrefclk_out;
  input gt0_rxcdrhold_in;
  input gt0_rxdfelpmreset_in;
  input gt0_rxlpmen_in;
  input gt0_rxpcsreset_in;
  input gt0_rxpolarity_in;
  input gt0_rxprbscntreset_in;
  input gt0_txinhibit_in;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  input gt0_txpolarity_in;
  input gt0_txprbsforceerr_in;
  input [15:0]gt0_drpdi;
  input [1:0]gt0_rxmonitorsel_in;
  input [2:0]gt0_loopback_in;
  input [2:0]gt0_rxprbssel_in;
  input [2:0]gt0_rxrate_in;
  input [2:0]gt0_txprbssel_in;
  input [3:0]gt0_txdiffctrl_in;
  input [4:0]gt0_txpostcursor_in;
  input [4:0]gt0_txprecursor_in;
  input [8:0]gt0_drpaddr;
  input gt1_drpen;
  input gt1_drpwe;
  input gt1_eyescanreset_in;
  input gt1_eyescantrigger_in;
  input rxaui_rx_l1_n;
  input rxaui_rx_l1_p;
  input gt1_rxcdrhold_in;
  input gt1_rxdfelpmreset_in;
  input gt1_rxlpmen_in;
  input gt1_rxpcsreset_in;
  input gt1_rxpolarity_in;
  input gt1_rxprbscntreset_in;
  input gt1_txinhibit_in;
  input gt1_txpcsreset_in;
  input gt1_txpmareset_in;
  input gt1_txpolarity_in;
  input gt1_txprbsforceerr_in;
  input [15:0]gt1_drpdi;
  input [1:0]gt1_rxmonitorsel_in;
  input [2:0]gt1_loopback_in;
  input [2:0]gt1_rxprbssel_in;
  input [2:0]gt1_rxrate_in;
  input [2:0]gt1_txprbssel_in;
  input [3:0]gt1_txdiffctrl_in;
  input [4:0]gt1_txpostcursor_in;
  input [4:0]gt1_txprecursor_in;
  input [8:0]gt1_drpaddr;
  input reset;
  input [1:0]signal_detect;
  input gt0_rxpmareset_in;
  input gt1_rxpmareset_in;
  input qplllock_out;

  wire CLK;
  wire [1:0]D;
  wire [2:0]bufStatus;
  wire data_out;
  wire dclk;
  wire [5:0]debug;
  wire done;
  wire [7:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr;
  wire [15:0]gt0_drpdi;
  wire [15:0]gt0_drpdo;
  wire gt0_drpen;
  wire gt0_drprdy;
  wire gt0_drpwe;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire [2:0]gt0_loopback_in;
  wire gt0_rxcdrhold_in;
  wire gt0_rxcommadet_out;
  wire gt0_rxdfelpmreset_in;
  wire gt0_rxlpmen_in;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire gt0_rxpcsreset_in;
  wire gt0_rxpmareset;
  wire gt0_rxpmareset_in;
  wire gt0_rxpolarity_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire [2:0]gt0_rxrate_in;
  wire gt0_rxresetdone_out;
  wire [3:0]gt0_txdiffctrl_in;
  wire gt0_txdlysresetdone_out;
  wire gt0_txinhibit_in;
  wire gt0_txoutclk_i;
  wire gt0_txpcsreset_in;
  wire gt0_txphaligndone_out;
  wire gt0_txphinitdone_out;
  wire gt0_txpmareset_in;
  wire gt0_txpolarity_in;
  wire [4:0]gt0_txpostcursor_in;
  wire gt0_txprbsforceerr_in;
  wire [2:0]gt0_txprbssel_in;
  wire [4:0]gt0_txprecursor_in;
  wire [7:0]gt1_dmonitorout_out;
  wire [8:0]gt1_drpaddr;
  wire [15:0]gt1_drpdi;
  wire [15:0]gt1_drpdo;
  wire gt1_drpen;
  wire gt1_drprdy;
  wire gt1_drpwe;
  wire gt1_eyescandataerror_out;
  wire gt1_eyescanreset_in;
  wire gt1_eyescantrigger_in;
  wire [2:0]gt1_loopback_in;
  wire gt1_rxcdrhold_in;
  wire [4:0]gt1_rxchbondo_i;
  wire gt1_rxcommadet_out;
  wire gt1_rxdfelpmreset_in;
  wire gt1_rxlpmen_in;
  wire [6:0]gt1_rxmonitorout_out;
  wire [1:0]gt1_rxmonitorsel_in;
  wire gt1_rxpcsreset_in;
  wire gt1_rxpmareset;
  wire gt1_rxpmareset_in;
  wire gt1_rxpolarity_in;
  wire gt1_rxprbscntreset_in;
  wire gt1_rxprbserr_out;
  wire [2:0]gt1_rxprbssel_in;
  wire [2:0]gt1_rxrate_in;
  wire gt1_rxresetdone_out;
  wire [3:0]gt1_txdiffctrl_in;
  wire gt1_txdlysresetdone_out;
  wire gt1_txinhibit_in;
  wire gt1_txpcsreset_in;
  wire gt1_txphaligndone_out;
  wire gt1_txphinitdone_out;
  wire gt1_txpmareset_in;
  wire gt1_txpolarity_in;
  wire [4:0]gt1_txpostcursor_in;
  wire gt1_txprbsforceerr_in;
  wire [2:0]gt1_txprbssel_in;
  wire [4:0]gt1_txprecursor_in;
  wire gt1_wrapper_i_n_35;
  wire gt1_wrapper_i_n_36;
  wire gt1_wrapper_i_n_37;
  wire gt1_wrapper_i_n_38;
  wire gt1_wrapper_i_n_39;
  wire gt1_wrapper_i_n_40;
  wire gt1_wrapper_i_n_41;
  wire gt1_wrapper_i_n_42;
  wire gt1_wrapper_i_n_43;
  wire gt1_wrapper_i_n_44;
  wire gt1_wrapper_i_n_45;
  wire gt1_wrapper_i_n_46;
  wire gt1_wrapper_i_n_47;
  wire gt1_wrapper_i_n_48;
  wire gt1_wrapper_i_n_49;
  wire gt1_wrapper_i_n_50;
  wire gt1_wrapper_i_n_51;
  wire gt1_wrapper_i_n_52;
  wire gt1_wrapper_i_n_53;
  wire gt1_wrapper_i_n_54;
  wire gt1_wrapper_i_n_55;
  wire gt1_wrapper_i_n_56;
  wire gt1_wrapper_i_n_57;
  wire gt1_wrapper_i_n_58;
  wire gt1_wrapper_i_n_59;
  wire gt1_wrapper_i_n_60;
  wire gt1_wrapper_i_n_61;
  wire gt1_wrapper_i_n_62;
  wire gt1_wrapper_i_n_63;
  wire gt1_wrapper_i_n_64;
  wire gt1_wrapper_i_n_65;
  wire gt1_wrapper_i_n_66;
  wire gt1_wrapper_i_n_82;
  wire gt1_wrapper_i_n_83;
  wire gt1_wrapper_i_n_84;
  wire gt1_wrapper_i_n_85;
  wire gt1_wrapper_i_n_86;
  wire gt1_wrapper_i_n_87;
  wire gt1_wrapper_i_n_88;
  wire gt1_wrapper_i_n_89;
  wire mdc;
  wire mdio_in;
  wire mdio_out;
  wire mdio_tri;
  wire [7:0]mgt_codecomma;
  wire [7:0]mgt_codevalid;
  wire mgt_enchansync;
  wire mgt_loopback;
  wire mgt_loopback_r;
  wire mgt_powerdown;
  wire mgt_powerdown_r;
  wire [1:0]mgt_rx_reset;
  wire \mgt_rxcharisk_reg_reg_n_0_[0] ;
  wire \mgt_rxcharisk_reg_reg_n_0_[1] ;
  wire \mgt_rxcharisk_reg_reg_n_0_[2] ;
  wire \mgt_rxcharisk_reg_reg_n_0_[3] ;
  wire \mgt_rxcharisk_reg_reg_n_0_[4] ;
  wire \mgt_rxcharisk_reg_reg_n_0_[5] ;
  wire \mgt_rxcharisk_reg_reg_n_0_[6] ;
  wire \mgt_rxcharisk_reg_reg_n_0_[7] ;
  wire \mgt_rxdata_reg_reg_n_0_[0] ;
  wire \mgt_rxdata_reg_reg_n_0_[10] ;
  wire \mgt_rxdata_reg_reg_n_0_[11] ;
  wire \mgt_rxdata_reg_reg_n_0_[12] ;
  wire \mgt_rxdata_reg_reg_n_0_[13] ;
  wire \mgt_rxdata_reg_reg_n_0_[14] ;
  wire \mgt_rxdata_reg_reg_n_0_[15] ;
  wire \mgt_rxdata_reg_reg_n_0_[16] ;
  wire \mgt_rxdata_reg_reg_n_0_[17] ;
  wire \mgt_rxdata_reg_reg_n_0_[18] ;
  wire \mgt_rxdata_reg_reg_n_0_[19] ;
  wire \mgt_rxdata_reg_reg_n_0_[1] ;
  wire \mgt_rxdata_reg_reg_n_0_[20] ;
  wire \mgt_rxdata_reg_reg_n_0_[21] ;
  wire \mgt_rxdata_reg_reg_n_0_[22] ;
  wire \mgt_rxdata_reg_reg_n_0_[23] ;
  wire \mgt_rxdata_reg_reg_n_0_[24] ;
  wire \mgt_rxdata_reg_reg_n_0_[25] ;
  wire \mgt_rxdata_reg_reg_n_0_[26] ;
  wire \mgt_rxdata_reg_reg_n_0_[27] ;
  wire \mgt_rxdata_reg_reg_n_0_[28] ;
  wire \mgt_rxdata_reg_reg_n_0_[29] ;
  wire \mgt_rxdata_reg_reg_n_0_[2] ;
  wire \mgt_rxdata_reg_reg_n_0_[30] ;
  wire \mgt_rxdata_reg_reg_n_0_[31] ;
  wire \mgt_rxdata_reg_reg_n_0_[32] ;
  wire \mgt_rxdata_reg_reg_n_0_[33] ;
  wire \mgt_rxdata_reg_reg_n_0_[34] ;
  wire \mgt_rxdata_reg_reg_n_0_[35] ;
  wire \mgt_rxdata_reg_reg_n_0_[36] ;
  wire \mgt_rxdata_reg_reg_n_0_[37] ;
  wire \mgt_rxdata_reg_reg_n_0_[38] ;
  wire \mgt_rxdata_reg_reg_n_0_[39] ;
  wire \mgt_rxdata_reg_reg_n_0_[3] ;
  wire \mgt_rxdata_reg_reg_n_0_[40] ;
  wire \mgt_rxdata_reg_reg_n_0_[41] ;
  wire \mgt_rxdata_reg_reg_n_0_[42] ;
  wire \mgt_rxdata_reg_reg_n_0_[43] ;
  wire \mgt_rxdata_reg_reg_n_0_[44] ;
  wire \mgt_rxdata_reg_reg_n_0_[45] ;
  wire \mgt_rxdata_reg_reg_n_0_[46] ;
  wire \mgt_rxdata_reg_reg_n_0_[47] ;
  wire \mgt_rxdata_reg_reg_n_0_[48] ;
  wire \mgt_rxdata_reg_reg_n_0_[49] ;
  wire \mgt_rxdata_reg_reg_n_0_[4] ;
  wire \mgt_rxdata_reg_reg_n_0_[50] ;
  wire \mgt_rxdata_reg_reg_n_0_[51] ;
  wire \mgt_rxdata_reg_reg_n_0_[52] ;
  wire \mgt_rxdata_reg_reg_n_0_[53] ;
  wire \mgt_rxdata_reg_reg_n_0_[54] ;
  wire \mgt_rxdata_reg_reg_n_0_[55] ;
  wire \mgt_rxdata_reg_reg_n_0_[56] ;
  wire \mgt_rxdata_reg_reg_n_0_[57] ;
  wire \mgt_rxdata_reg_reg_n_0_[58] ;
  wire \mgt_rxdata_reg_reg_n_0_[59] ;
  wire \mgt_rxdata_reg_reg_n_0_[5] ;
  wire \mgt_rxdata_reg_reg_n_0_[60] ;
  wire \mgt_rxdata_reg_reg_n_0_[61] ;
  wire \mgt_rxdata_reg_reg_n_0_[62] ;
  wire \mgt_rxdata_reg_reg_n_0_[63] ;
  wire \mgt_rxdata_reg_reg_n_0_[6] ;
  wire \mgt_rxdata_reg_reg_n_0_[7] ;
  wire \mgt_rxdata_reg_reg_n_0_[8] ;
  wire \mgt_rxdata_reg_reg_n_0_[9] ;
  wire [7:0]mgt_rxdisperr_reg;
  wire [7:0]\mgt_rxdisperr_reg_reg[7]_0 ;
  wire [7:0]mgt_rxnotintable_reg;
  wire [7:0]\mgt_rxnotintable_reg_reg[7]_0 ;
  wire [0:0]out;
  wire p_0_in;
  wire p_0_in4_in;
  wire [1:0]p_0_out;
  wire plllocked_sync_i_n_1;
  wire plllocked_sync_i_n_2;
  wire [4:0]prtad;
  wire qplllock_out;
  wire qplloutclk_out;
  wire qplloutrefclk_out;
  wire reset;
  wire reset_count_done_sync_i_n_0;
  wire rxaui_0_core_n_104;
  wire rxaui_0_core_n_105;
  wire rxaui_0_core_n_106;
  wire rxaui_0_core_n_107;
  wire rxaui_0_core_n_108;
  wire rxaui_0_core_n_109;
  wire rxaui_0_core_n_110;
  wire rxaui_0_core_n_111;
  wire rxaui_0_core_n_112;
  wire rxaui_0_core_n_113;
  wire rxaui_0_core_n_114;
  wire rxaui_0_core_n_115;
  wire rxaui_0_core_n_116;
  wire rxaui_0_core_n_117;
  wire rxaui_0_core_n_118;
  wire rxaui_0_core_n_119;
  wire rxaui_0_core_n_120;
  wire rxaui_0_core_n_121;
  wire rxaui_0_core_n_122;
  wire rxaui_0_core_n_123;
  wire rxaui_0_core_n_124;
  wire rxaui_0_core_n_125;
  wire rxaui_0_core_n_126;
  wire rxaui_0_core_n_127;
  wire rxaui_0_core_n_128;
  wire rxaui_0_core_n_129;
  wire rxaui_0_core_n_130;
  wire rxaui_0_core_n_131;
  wire rxaui_0_core_n_132;
  wire rxaui_0_core_n_133;
  wire rxaui_0_core_n_134;
  wire rxaui_0_core_n_135;
  wire rxaui_0_core_n_140;
  wire rxaui_0_core_n_141;
  wire rxaui_0_core_n_142;
  wire rxaui_0_core_n_143;
  wire rxaui_0_core_n_145;
  wire rxaui_rx_l0_n;
  wire rxaui_rx_l0_p;
  wire rxaui_rx_l1_n;
  wire rxaui_rx_l1_p;
  wire rxaui_tx_l0_n;
  wire rxaui_tx_l0_p;
  wire rxaui_tx_l1_n;
  wire rxaui_tx_l1_p;
  wire [3:0]rxchariscomma_out;
  wire [3:0]rxcharisk_out;
  wire [31:0]rxdata_out;
  wire rxmcommaalignen_in;
  wire rxprbs_in_use;
  wire rxprbs_in_use0__0;
  wire [1:0]signal_detect;
  wire signal_detect_1_sync_i_n_0;
  wire soft_reset;
  wire [3:0]txcharisk_in;
  wire [31:0]txdata_in;
  wire txdlysreset_in;
  wire txphalign_in;
  wire txphinit_in;
  wire txsync_i_n_1;
  wire txsync_i_n_2;
  wire txsync_i_n_4;
  wire txsync_i_n_5;
  wire txsync_i_n_7;
  wire [1:0]type_sel;
  wire uclk_chbond_counter;
  wire \uclk_chbond_counter[0]_i_3_n_0 ;
  wire \uclk_chbond_counter[0]_i_4_n_0 ;
  wire \uclk_chbond_counter[0]_i_5_n_0 ;
  wire \uclk_chbond_counter[0]_i_6_n_0 ;
  wire \uclk_chbond_counter[0]_i_7_n_0 ;
  wire \uclk_chbond_counter[12]_i_2_n_0 ;
  wire \uclk_chbond_counter[12]_i_3_n_0 ;
  wire \uclk_chbond_counter[12]_i_4_n_0 ;
  wire \uclk_chbond_counter[12]_i_5_n_0 ;
  wire \uclk_chbond_counter[4]_i_2_n_0 ;
  wire \uclk_chbond_counter[4]_i_3_n_0 ;
  wire \uclk_chbond_counter[4]_i_4_n_0 ;
  wire \uclk_chbond_counter[4]_i_5_n_0 ;
  wire \uclk_chbond_counter[8]_i_2_n_0 ;
  wire \uclk_chbond_counter[8]_i_3_n_0 ;
  wire \uclk_chbond_counter[8]_i_4_n_0 ;
  wire \uclk_chbond_counter[8]_i_5_n_0 ;
  wire \uclk_chbond_counter_reg[0]_i_2_n_0 ;
  wire \uclk_chbond_counter_reg[0]_i_2_n_1 ;
  wire \uclk_chbond_counter_reg[0]_i_2_n_2 ;
  wire \uclk_chbond_counter_reg[0]_i_2_n_3 ;
  wire \uclk_chbond_counter_reg[0]_i_2_n_4 ;
  wire \uclk_chbond_counter_reg[0]_i_2_n_5 ;
  wire \uclk_chbond_counter_reg[0]_i_2_n_6 ;
  wire \uclk_chbond_counter_reg[0]_i_2_n_7 ;
  wire \uclk_chbond_counter_reg[12]_i_1_n_1 ;
  wire \uclk_chbond_counter_reg[12]_i_1_n_2 ;
  wire \uclk_chbond_counter_reg[12]_i_1_n_3 ;
  wire \uclk_chbond_counter_reg[12]_i_1_n_4 ;
  wire \uclk_chbond_counter_reg[12]_i_1_n_5 ;
  wire \uclk_chbond_counter_reg[12]_i_1_n_6 ;
  wire \uclk_chbond_counter_reg[12]_i_1_n_7 ;
  wire \uclk_chbond_counter_reg[4]_i_1_n_0 ;
  wire \uclk_chbond_counter_reg[4]_i_1_n_1 ;
  wire \uclk_chbond_counter_reg[4]_i_1_n_2 ;
  wire \uclk_chbond_counter_reg[4]_i_1_n_3 ;
  wire \uclk_chbond_counter_reg[4]_i_1_n_4 ;
  wire \uclk_chbond_counter_reg[4]_i_1_n_5 ;
  wire \uclk_chbond_counter_reg[4]_i_1_n_6 ;
  wire \uclk_chbond_counter_reg[4]_i_1_n_7 ;
  wire \uclk_chbond_counter_reg[8]_i_1_n_0 ;
  wire \uclk_chbond_counter_reg[8]_i_1_n_1 ;
  wire \uclk_chbond_counter_reg[8]_i_1_n_2 ;
  wire \uclk_chbond_counter_reg[8]_i_1_n_3 ;
  wire \uclk_chbond_counter_reg[8]_i_1_n_4 ;
  wire \uclk_chbond_counter_reg[8]_i_1_n_5 ;
  wire \uclk_chbond_counter_reg[8]_i_1_n_6 ;
  wire \uclk_chbond_counter_reg[8]_i_1_n_7 ;
  wire \uclk_chbond_counter_reg_n_0_[0] ;
  wire \uclk_chbond_counter_reg_n_0_[10] ;
  wire \uclk_chbond_counter_reg_n_0_[11] ;
  wire \uclk_chbond_counter_reg_n_0_[12] ;
  wire \uclk_chbond_counter_reg_n_0_[13] ;
  wire \uclk_chbond_counter_reg_n_0_[14] ;
  wire \uclk_chbond_counter_reg_n_0_[1] ;
  wire \uclk_chbond_counter_reg_n_0_[2] ;
  wire \uclk_chbond_counter_reg_n_0_[3] ;
  wire \uclk_chbond_counter_reg_n_0_[4] ;
  wire \uclk_chbond_counter_reg_n_0_[5] ;
  wire \uclk_chbond_counter_reg_n_0_[6] ;
  wire \uclk_chbond_counter_reg_n_0_[7] ;
  wire \uclk_chbond_counter_reg_n_0_[8] ;
  wire \uclk_chbond_counter_reg_n_0_[9] ;
  wire uclk_mgt_loopback_falling;
  wire uclk_mgt_loopback_falling_i_1_n_0;
  wire uclk_mgt_powerdown_falling;
  wire uclk_mgt_powerdown_falling0;
  wire uclk_mgt_rx_reset;
  wire uclk_mgt_rx_reset0;
  wire uclk_mgt_rxbuf_reset0;
  wire \uclk_mgt_rxbuf_reset[0]_i_2_n_0 ;
  wire \uclk_mgt_rxbuf_reset_reg_n_0_[0] ;
  wire [5:0]\uclk_mgt_rxbufstatus_reg_reg[5]_0 ;
  wire \uclk_mgt_rxbufstatus_reg_reg_n_0_[0] ;
  wire \uclk_mgt_rxbufstatus_reg_reg_n_0_[1] ;
  wire \uclk_mgt_rxbufstatus_reg_reg_n_0_[2] ;
  wire uclk_mgt_tx_reset;
  wire uclk_mgt_tx_reset0;
  wire uclk_reset156;
  wire \uclk_sync_counter[0]_i_1_n_0 ;
  wire \uclk_sync_counter[0]_i_3_n_0 ;
  wire \uclk_sync_counter[0]_i_4_n_0 ;
  wire \uclk_sync_counter[0]_i_5_n_0 ;
  wire \uclk_sync_counter[0]_i_6_n_0 ;
  wire \uclk_sync_counter[12]_i_2_n_0 ;
  wire \uclk_sync_counter[12]_i_3_n_0 ;
  wire \uclk_sync_counter[12]_i_4_n_0 ;
  wire \uclk_sync_counter[12]_i_5_n_0 ;
  wire \uclk_sync_counter[4]_i_2_n_0 ;
  wire \uclk_sync_counter[4]_i_3_n_0 ;
  wire \uclk_sync_counter[4]_i_4_n_0 ;
  wire \uclk_sync_counter[4]_i_5_n_0 ;
  wire \uclk_sync_counter[8]_i_2_n_0 ;
  wire \uclk_sync_counter[8]_i_3_n_0 ;
  wire \uclk_sync_counter[8]_i_4_n_0 ;
  wire \uclk_sync_counter[8]_i_5_n_0 ;
  wire \uclk_sync_counter_reg[0]_i_2_n_0 ;
  wire \uclk_sync_counter_reg[0]_i_2_n_1 ;
  wire \uclk_sync_counter_reg[0]_i_2_n_2 ;
  wire \uclk_sync_counter_reg[0]_i_2_n_3 ;
  wire \uclk_sync_counter_reg[0]_i_2_n_4 ;
  wire \uclk_sync_counter_reg[0]_i_2_n_5 ;
  wire \uclk_sync_counter_reg[0]_i_2_n_6 ;
  wire \uclk_sync_counter_reg[0]_i_2_n_7 ;
  wire \uclk_sync_counter_reg[12]_i_1_n_1 ;
  wire \uclk_sync_counter_reg[12]_i_1_n_2 ;
  wire \uclk_sync_counter_reg[12]_i_1_n_3 ;
  wire \uclk_sync_counter_reg[12]_i_1_n_4 ;
  wire \uclk_sync_counter_reg[12]_i_1_n_5 ;
  wire \uclk_sync_counter_reg[12]_i_1_n_6 ;
  wire \uclk_sync_counter_reg[12]_i_1_n_7 ;
  wire \uclk_sync_counter_reg[4]_i_1_n_0 ;
  wire \uclk_sync_counter_reg[4]_i_1_n_1 ;
  wire \uclk_sync_counter_reg[4]_i_1_n_2 ;
  wire \uclk_sync_counter_reg[4]_i_1_n_3 ;
  wire \uclk_sync_counter_reg[4]_i_1_n_4 ;
  wire \uclk_sync_counter_reg[4]_i_1_n_5 ;
  wire \uclk_sync_counter_reg[4]_i_1_n_6 ;
  wire \uclk_sync_counter_reg[4]_i_1_n_7 ;
  wire \uclk_sync_counter_reg[8]_i_1_n_0 ;
  wire \uclk_sync_counter_reg[8]_i_1_n_1 ;
  wire \uclk_sync_counter_reg[8]_i_1_n_2 ;
  wire \uclk_sync_counter_reg[8]_i_1_n_3 ;
  wire \uclk_sync_counter_reg[8]_i_1_n_4 ;
  wire \uclk_sync_counter_reg[8]_i_1_n_5 ;
  wire \uclk_sync_counter_reg[8]_i_1_n_6 ;
  wire \uclk_sync_counter_reg[8]_i_1_n_7 ;
  wire \uclk_sync_counter_reg_n_0_[0] ;
  wire \uclk_sync_counter_reg_n_0_[10] ;
  wire \uclk_sync_counter_reg_n_0_[11] ;
  wire \uclk_sync_counter_reg_n_0_[12] ;
  wire \uclk_sync_counter_reg_n_0_[13] ;
  wire \uclk_sync_counter_reg_n_0_[14] ;
  wire \uclk_sync_counter_reg_n_0_[1] ;
  wire \uclk_sync_counter_reg_n_0_[2] ;
  wire \uclk_sync_counter_reg_n_0_[3] ;
  wire \uclk_sync_counter_reg_n_0_[4] ;
  wire \uclk_sync_counter_reg_n_0_[5] ;
  wire \uclk_sync_counter_reg_n_0_[6] ;
  wire \uclk_sync_counter_reg_n_0_[7] ;
  wire \uclk_sync_counter_reg_n_0_[8] ;
  wire \uclk_sync_counter_reg_n_0_[9] ;
  wire [1:0]uclk_txresetdone_reg;
  wire uclk_txsync_start_phase_align_reg_n_0;
  wire [7:0]xgmii_rxc;
  wire [63:0]xgmii_rxd;
  wire [7:0]xgmii_txc;
  wire [63:0]xgmii_txd;
  wire [7:0]NLW_rxaui_0_core_status_vector_UNCONNECTED;
  wire [3:3]\NLW_uclk_chbond_counter_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_uclk_sync_counter_reg[12]_i_1_CO_UNCONNECTED ;

  LUT1 #(
    .INIT(2'h1)) 
    \core_mgt_rx_reset[0]_i_1 
       (.I0(gt0_rxresetdone_out),
        .O(p_0_out[0]));
  LUT1 #(
    .INIT(2'h1)) 
    \core_mgt_rx_reset[1]_i_1 
       (.I0(gt1_rxresetdone_out),
        .O(p_0_out[1]));
  FDRE \core_mgt_rx_reset_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_out[0]),
        .Q(mgt_rx_reset[0]),
        .R(1'b0));
  FDRE \core_mgt_rx_reset_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_out[1]),
        .Q(mgt_rx_reset[1]),
        .R(1'b0));
  rxaui_0_rxaui_0_gt_wrapper_GT gt0_wrapper_i
       (.D(D[0]),
        .RXCHBONDO(gt1_rxchbondo_i),
        .SR(uclk_mgt_tx_reset),
        .\TXDLYEN_reg[0] (txsync_i_n_1),
        .\TXDLYSRESET_reg[0] (txsync_i_n_4),
        .\TXPHALIGN_reg[0] (txsync_i_n_7),
        .\TXPHINIT_reg[0] (txsync_i_n_5),
        .dclk(dclk),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr(gt0_drpaddr),
        .gt0_drpdi(gt0_drpdi),
        .gt0_drpdo(gt0_drpdo),
        .gt0_drpen(gt0_drpen),
        .gt0_drprdy(gt0_drprdy),
        .gt0_drpwe(gt0_drpwe),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_loopback_in(gt0_loopback_in),
        .gt0_rxcdrhold_in(gt0_rxcdrhold_in),
        .gt0_rxcommadet_out(gt0_rxcommadet_out),
        .gt0_rxdfelpmreset_in(gt0_rxdfelpmreset_in),
        .gt0_rxlpmen_in(gt0_rxlpmen_in),
        .gt0_rxmonitorout_out(gt0_rxmonitorout_out),
        .gt0_rxmonitorsel_in(gt0_rxmonitorsel_in),
        .gt0_rxpcsreset_in(gt0_rxpcsreset_in),
        .gt0_rxpolarity_in(gt0_rxpolarity_in),
        .gt0_rxprbscntreset_in(gt0_rxprbscntreset_in),
        .gt0_rxprbserr_out(gt0_rxprbserr_out),
        .gt0_rxprbssel_in(gt0_rxprbssel_in),
        .gt0_rxrate_in(gt0_rxrate_in),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_txdiffctrl_in(gt0_txdiffctrl_in),
        .gt0_txdlysresetdone_out(gt0_txdlysresetdone_out),
        .gt0_txinhibit_in(gt0_txinhibit_in),
        .gt0_txoutclk_i(gt0_txoutclk_i),
        .gt0_txpcsreset_in(gt0_txpcsreset_in),
        .gt0_txphaligndone_out(gt0_txphaligndone_out),
        .gt0_txphinitdone_out(gt0_txphinitdone_out),
        .gt0_txpmareset_in(gt0_txpmareset_in),
        .gt0_txpolarity_in(gt0_txpolarity_in),
        .gt0_txpostcursor_in(gt0_txpostcursor_in),
        .gt0_txprbsforceerr_in(gt0_txprbsforceerr_in),
        .gt0_txprbssel_in(gt0_txprbssel_in),
        .gt0_txprecursor_in(gt0_txprecursor_in),
        .\mgt_codecomma_reg_reg[3] (rxchariscomma_out),
        .mgt_enable_align(rxaui_0_core_n_145),
        .mgt_enchansync(mgt_enchansync),
        .mgt_loopback_r(mgt_loopback_r),
        .mgt_powerdown(mgt_powerdown),
        .mgt_powerdown_r(mgt_powerdown_r),
        .\mgt_rxcharisk_reg_reg[3] (rxcharisk_out),
        .\mgt_rxdata_reg_reg[31] (rxdata_out),
        .\mgt_rxdisperr_reg_reg[3] (\mgt_rxdisperr_reg_reg[7]_0 [3:0]),
        .\mgt_rxnotintable_reg_reg[3] (\mgt_rxnotintable_reg_reg[7]_0 [3:0]),
        .mgt_txcharisk({rxaui_0_core_n_140,rxaui_0_core_n_141,rxaui_0_core_n_142,rxaui_0_core_n_143}),
        .mgt_txdata({rxaui_0_core_n_104,rxaui_0_core_n_105,rxaui_0_core_n_106,rxaui_0_core_n_107,rxaui_0_core_n_108,rxaui_0_core_n_109,rxaui_0_core_n_110,rxaui_0_core_n_111,rxaui_0_core_n_112,rxaui_0_core_n_113,rxaui_0_core_n_114,rxaui_0_core_n_115,rxaui_0_core_n_116,rxaui_0_core_n_117,rxaui_0_core_n_118,rxaui_0_core_n_119,rxaui_0_core_n_120,rxaui_0_core_n_121,rxaui_0_core_n_122,rxaui_0_core_n_123,rxaui_0_core_n_124,rxaui_0_core_n_125,rxaui_0_core_n_126,rxaui_0_core_n_127,rxaui_0_core_n_128,rxaui_0_core_n_129,rxaui_0_core_n_130,rxaui_0_core_n_131,rxaui_0_core_n_132,rxaui_0_core_n_133,rxaui_0_core_n_134,rxaui_0_core_n_135}),
        .out(gt0_rxpmareset),
        .qplloutclk_out(qplloutclk_out),
        .qplloutrefclk_out(qplloutrefclk_out),
        .rxaui_rx_l0_n(rxaui_rx_l0_n),
        .rxaui_rx_l0_p(rxaui_rx_l0_p),
        .rxaui_tx_l0_n(rxaui_tx_l0_n),
        .rxaui_tx_l0_p(rxaui_tx_l0_p),
        .\sync_r_reg[4] (out),
        .uclk_mgt_rx_reset(uclk_mgt_rx_reset),
        .uclk_mgt_rx_reset_reg(CLK),
        .\uclk_mgt_rxbuf_reset_reg[0] (\uclk_mgt_rxbuf_reset_reg_n_0_[0] ),
        .\uclk_mgt_rxbufstatus_reg_reg[2] (\uclk_mgt_rxbufstatus_reg_reg[5]_0 [2:0]));
  rxaui_0_rxaui_0_gt_wrapper_GT_0 gt1_wrapper_i
       (.D(D[1]),
        .RXCHBONDO(gt1_rxchbondo_i),
        .SR(uclk_mgt_tx_reset),
        .dclk(dclk),
        .gt1_dmonitorout_out(gt1_dmonitorout_out),
        .gt1_drpaddr(gt1_drpaddr),
        .gt1_drpdi(gt1_drpdi),
        .gt1_drpdo(gt1_drpdo),
        .gt1_drpen(gt1_drpen),
        .gt1_drprdy(gt1_drprdy),
        .gt1_drpwe(gt1_drpwe),
        .gt1_eyescandataerror_out(gt1_eyescandataerror_out),
        .gt1_eyescanreset_in(gt1_eyescanreset_in),
        .gt1_eyescantrigger_in(gt1_eyescantrigger_in),
        .gt1_loopback_in(gt1_loopback_in),
        .gt1_rxcdrhold_in(gt1_rxcdrhold_in),
        .gt1_rxcommadet_out(gt1_rxcommadet_out),
        .gt1_rxdfelpmreset_in(gt1_rxdfelpmreset_in),
        .gt1_rxlpmen_in(gt1_rxlpmen_in),
        .gt1_rxmonitorout_out(gt1_rxmonitorout_out),
        .gt1_rxmonitorsel_in(gt1_rxmonitorsel_in),
        .gt1_rxpcsreset_in(gt1_rxpcsreset_in),
        .gt1_rxpolarity_in(gt1_rxpolarity_in),
        .gt1_rxprbscntreset_in(gt1_rxprbscntreset_in),
        .gt1_rxprbserr_out(gt1_rxprbserr_out),
        .gt1_rxprbssel_in(gt1_rxprbssel_in),
        .gt1_rxrate_in(gt1_rxrate_in),
        .gt1_rxresetdone_out(gt1_rxresetdone_out),
        .gt1_txdiffctrl_in(gt1_txdiffctrl_in),
        .gt1_txdlysresetdone_out(gt1_txdlysresetdone_out),
        .gt1_txinhibit_in(gt1_txinhibit_in),
        .gt1_txpcsreset_in(gt1_txpcsreset_in),
        .gt1_txphaligndone_out(gt1_txphaligndone_out),
        .gt1_txphinitdone_out(gt1_txphinitdone_out),
        .gt1_txpmareset_in(gt1_txpmareset_in),
        .gt1_txpolarity_in(gt1_txpolarity_in),
        .gt1_txpostcursor_in(gt1_txpostcursor_in),
        .gt1_txprbsforceerr_in(gt1_txprbsforceerr_in),
        .gt1_txprbssel_in(gt1_txprbssel_in),
        .gt1_txprecursor_in(gt1_txprecursor_in),
        .\mgt_codecomma_reg_reg[7] ({gt1_wrapper_i_n_82,gt1_wrapper_i_n_83,gt1_wrapper_i_n_84,gt1_wrapper_i_n_85}),
        .mgt_enable_align(rxmcommaalignen_in),
        .mgt_enchansync(mgt_enchansync),
        .mgt_loopback_r(mgt_loopback_r),
        .mgt_powerdown(mgt_powerdown),
        .mgt_powerdown_r(mgt_powerdown_r),
        .\mgt_rxcharisk_reg_reg[7] ({gt1_wrapper_i_n_86,gt1_wrapper_i_n_87,gt1_wrapper_i_n_88,gt1_wrapper_i_n_89}),
        .\mgt_rxdata_reg_reg[63] ({gt1_wrapper_i_n_35,gt1_wrapper_i_n_36,gt1_wrapper_i_n_37,gt1_wrapper_i_n_38,gt1_wrapper_i_n_39,gt1_wrapper_i_n_40,gt1_wrapper_i_n_41,gt1_wrapper_i_n_42,gt1_wrapper_i_n_43,gt1_wrapper_i_n_44,gt1_wrapper_i_n_45,gt1_wrapper_i_n_46,gt1_wrapper_i_n_47,gt1_wrapper_i_n_48,gt1_wrapper_i_n_49,gt1_wrapper_i_n_50,gt1_wrapper_i_n_51,gt1_wrapper_i_n_52,gt1_wrapper_i_n_53,gt1_wrapper_i_n_54,gt1_wrapper_i_n_55,gt1_wrapper_i_n_56,gt1_wrapper_i_n_57,gt1_wrapper_i_n_58,gt1_wrapper_i_n_59,gt1_wrapper_i_n_60,gt1_wrapper_i_n_61,gt1_wrapper_i_n_62,gt1_wrapper_i_n_63,gt1_wrapper_i_n_64,gt1_wrapper_i_n_65,gt1_wrapper_i_n_66}),
        .\mgt_rxdisperr_reg_reg[7] (\mgt_rxdisperr_reg_reg[7]_0 [7:4]),
        .\mgt_rxnotintable_reg_reg[7] (\mgt_rxnotintable_reg_reg[7]_0 [7:4]),
        .mgt_txcharisk(txcharisk_in),
        .mgt_txdata(txdata_in),
        .out(gt1_rxpmareset),
        .qplloutclk_out(qplloutclk_out),
        .qplloutrefclk_out(qplloutrefclk_out),
        .rxaui_rx_l1_n(rxaui_rx_l1_n),
        .rxaui_rx_l1_p(rxaui_rx_l1_p),
        .rxaui_tx_l1_n(rxaui_tx_l1_n),
        .rxaui_tx_l1_p(rxaui_tx_l1_p),
        .\sync_r_reg[4] (out),
        .txdlysreset_in(txdlysreset_in),
        .txphalign_in(txphalign_in),
        .txphinit_in(txphinit_in),
        .uclk_mgt_rx_reset(uclk_mgt_rx_reset),
        .uclk_mgt_rx_reset_reg(CLK),
        .\uclk_mgt_rxbuf_reset_reg[0] (\uclk_mgt_rxbuf_reset_reg_n_0_[0] ),
        .\uclk_mgt_rxbufstatus_reg_reg[5] (\uclk_mgt_rxbufstatus_reg_reg[5]_0 [5:3]));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_codecomma_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_out[0]),
        .Q(mgt_codecomma[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_codecomma_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_out[1]),
        .Q(mgt_codecomma[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_codecomma_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_out[2]),
        .Q(mgt_codecomma[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_codecomma_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_out[3]),
        .Q(mgt_codecomma[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_codecomma_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_85),
        .Q(mgt_codecomma[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_codecomma_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_84),
        .Q(mgt_codecomma[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_codecomma_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_83),
        .Q(mgt_codecomma[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_codecomma_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_82),
        .Q(mgt_codecomma[7]),
        .R(1'b0));
  FDRE mgt_loopback_r_reg
       (.C(CLK),
        .CE(1'b1),
        .D(mgt_loopback),
        .Q(mgt_loopback_r),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    mgt_powerdown_r_reg
       (.C(CLK),
        .CE(1'b1),
        .D(mgt_powerdown),
        .Q(mgt_powerdown_r),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxcharisk_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_out[0]),
        .Q(\mgt_rxcharisk_reg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxcharisk_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_out[1]),
        .Q(\mgt_rxcharisk_reg_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxcharisk_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_out[2]),
        .Q(\mgt_rxcharisk_reg_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxcharisk_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_out[3]),
        .Q(\mgt_rxcharisk_reg_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxcharisk_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_89),
        .Q(\mgt_rxcharisk_reg_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxcharisk_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_88),
        .Q(\mgt_rxcharisk_reg_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxcharisk_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_87),
        .Q(\mgt_rxcharisk_reg_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxcharisk_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_86),
        .Q(\mgt_rxcharisk_reg_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[0]),
        .Q(\mgt_rxdata_reg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[10]),
        .Q(\mgt_rxdata_reg_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[11]),
        .Q(\mgt_rxdata_reg_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[12]),
        .Q(\mgt_rxdata_reg_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[13]),
        .Q(\mgt_rxdata_reg_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[14]),
        .Q(\mgt_rxdata_reg_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[15]),
        .Q(\mgt_rxdata_reg_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[16]),
        .Q(\mgt_rxdata_reg_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[17] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[17]),
        .Q(\mgt_rxdata_reg_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[18] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[18]),
        .Q(\mgt_rxdata_reg_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[19] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[19]),
        .Q(\mgt_rxdata_reg_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[1]),
        .Q(\mgt_rxdata_reg_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[20] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[20]),
        .Q(\mgt_rxdata_reg_reg_n_0_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[21] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[21]),
        .Q(\mgt_rxdata_reg_reg_n_0_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[22] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[22]),
        .Q(\mgt_rxdata_reg_reg_n_0_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[23] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[23]),
        .Q(\mgt_rxdata_reg_reg_n_0_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[24] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[24]),
        .Q(\mgt_rxdata_reg_reg_n_0_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[25] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[25]),
        .Q(\mgt_rxdata_reg_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[26] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[26]),
        .Q(\mgt_rxdata_reg_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[27] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[27]),
        .Q(\mgt_rxdata_reg_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[28] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[28]),
        .Q(\mgt_rxdata_reg_reg_n_0_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[29] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[29]),
        .Q(\mgt_rxdata_reg_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[2]),
        .Q(\mgt_rxdata_reg_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[30] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[30]),
        .Q(\mgt_rxdata_reg_reg_n_0_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[31] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[31]),
        .Q(\mgt_rxdata_reg_reg_n_0_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[32] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_66),
        .Q(\mgt_rxdata_reg_reg_n_0_[32] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[33] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_65),
        .Q(\mgt_rxdata_reg_reg_n_0_[33] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[34] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_64),
        .Q(\mgt_rxdata_reg_reg_n_0_[34] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[35] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_63),
        .Q(\mgt_rxdata_reg_reg_n_0_[35] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[36] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_62),
        .Q(\mgt_rxdata_reg_reg_n_0_[36] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[37] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_61),
        .Q(\mgt_rxdata_reg_reg_n_0_[37] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[38] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_60),
        .Q(\mgt_rxdata_reg_reg_n_0_[38] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[39] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_59),
        .Q(\mgt_rxdata_reg_reg_n_0_[39] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[3]),
        .Q(\mgt_rxdata_reg_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[40] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_58),
        .Q(\mgt_rxdata_reg_reg_n_0_[40] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[41] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_57),
        .Q(\mgt_rxdata_reg_reg_n_0_[41] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[42] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_56),
        .Q(\mgt_rxdata_reg_reg_n_0_[42] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[43] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_55),
        .Q(\mgt_rxdata_reg_reg_n_0_[43] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[44] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_54),
        .Q(\mgt_rxdata_reg_reg_n_0_[44] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[45] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_53),
        .Q(\mgt_rxdata_reg_reg_n_0_[45] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[46] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_52),
        .Q(\mgt_rxdata_reg_reg_n_0_[46] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[47] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_51),
        .Q(\mgt_rxdata_reg_reg_n_0_[47] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[48] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_50),
        .Q(\mgt_rxdata_reg_reg_n_0_[48] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[49] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_49),
        .Q(\mgt_rxdata_reg_reg_n_0_[49] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[4]),
        .Q(\mgt_rxdata_reg_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[50] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_48),
        .Q(\mgt_rxdata_reg_reg_n_0_[50] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[51] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_47),
        .Q(\mgt_rxdata_reg_reg_n_0_[51] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[52] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_46),
        .Q(\mgt_rxdata_reg_reg_n_0_[52] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[53] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_45),
        .Q(\mgt_rxdata_reg_reg_n_0_[53] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[54] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_44),
        .Q(\mgt_rxdata_reg_reg_n_0_[54] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[55] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_43),
        .Q(\mgt_rxdata_reg_reg_n_0_[55] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[56] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_42),
        .Q(\mgt_rxdata_reg_reg_n_0_[56] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[57] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_41),
        .Q(\mgt_rxdata_reg_reg_n_0_[57] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[58] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_40),
        .Q(\mgt_rxdata_reg_reg_n_0_[58] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[59] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_39),
        .Q(\mgt_rxdata_reg_reg_n_0_[59] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[5]),
        .Q(\mgt_rxdata_reg_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[60] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_38),
        .Q(\mgt_rxdata_reg_reg_n_0_[60] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[61] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_37),
        .Q(\mgt_rxdata_reg_reg_n_0_[61] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[62] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_36),
        .Q(\mgt_rxdata_reg_reg_n_0_[62] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[63] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt1_wrapper_i_n_35),
        .Q(\mgt_rxdata_reg_reg_n_0_[63] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[6]),
        .Q(\mgt_rxdata_reg_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[7]),
        .Q(\mgt_rxdata_reg_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[8]),
        .Q(\mgt_rxdata_reg_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_out[9]),
        .Q(\mgt_rxdata_reg_reg_n_0_[9] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdisperr_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxdisperr_reg_reg[7]_0 [0]),
        .Q(mgt_rxdisperr_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdisperr_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxdisperr_reg_reg[7]_0 [1]),
        .Q(mgt_rxdisperr_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdisperr_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxdisperr_reg_reg[7]_0 [2]),
        .Q(mgt_rxdisperr_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdisperr_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxdisperr_reg_reg[7]_0 [3]),
        .Q(mgt_rxdisperr_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdisperr_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxdisperr_reg_reg[7]_0 [4]),
        .Q(mgt_rxdisperr_reg[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdisperr_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxdisperr_reg_reg[7]_0 [5]),
        .Q(mgt_rxdisperr_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdisperr_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxdisperr_reg_reg[7]_0 [6]),
        .Q(mgt_rxdisperr_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdisperr_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxdisperr_reg_reg[7]_0 [7]),
        .Q(mgt_rxdisperr_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxnotintable_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxnotintable_reg_reg[7]_0 [0]),
        .Q(mgt_rxnotintable_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxnotintable_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxnotintable_reg_reg[7]_0 [1]),
        .Q(mgt_rxnotintable_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxnotintable_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxnotintable_reg_reg[7]_0 [2]),
        .Q(mgt_rxnotintable_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxnotintable_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxnotintable_reg_reg[7]_0 [3]),
        .Q(mgt_rxnotintable_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxnotintable_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxnotintable_reg_reg[7]_0 [4]),
        .Q(mgt_rxnotintable_reg[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxnotintable_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxnotintable_reg_reg[7]_0 [5]),
        .Q(mgt_rxnotintable_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxnotintable_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxnotintable_reg_reg[7]_0 [6]),
        .Q(mgt_rxnotintable_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxnotintable_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\mgt_rxnotintable_reg_reg[7]_0 [7]),
        .Q(mgt_rxnotintable_reg[7]),
        .R(1'b0));
  rxaui_0_rxaui_0_ff_synchronizer plllocked_sync_i
       (.out(out),
        .qplllock_out(qplllock_out),
        .reset156_r1_reg(plllocked_sync_i_n_2),
        .reset156_r3_reg(uclk_reset156),
        .soft_reset(soft_reset),
        .uclk_mgt_powerdown_falling(uclk_mgt_powerdown_falling),
        .uclk_mgt_rx_reset_reg(plllocked_sync_i_n_1),
        .uclk_mgt_rx_reset_reg_0(CLK));
  rxaui_0_rxaui_0_ff_synchronizer_1 reset_count_done_sync_i
       (.Q(uclk_txresetdone_reg),
        .\count_reg[7] (done),
        .out(uclk_reset156),
        .p_0_in4_in(p_0_in4_in),
        .reset_reg_reg(plllocked_sync_i_n_1),
        .soft_reset(soft_reset),
        .\sync_r_reg[4]_0 (out),
        .uclk_mgt_loopback_falling(uclk_mgt_loopback_falling),
        .uclk_mgt_powerdown_falling(uclk_mgt_powerdown_falling),
        .uclk_mgt_rx_reset0(uclk_mgt_rx_reset0),
        .uclk_mgt_rx_reset_reg(CLK),
        .uclk_mgt_tx_reset0(uclk_mgt_tx_reset0),
        .uclk_txsync_start_phase_align_reg(reset_count_done_sync_i_n_0),
        .uclk_txsync_start_phase_align_reg_0(uclk_txsync_start_phase_align_reg_n_0));
  rxaui_0_rxaui_0_reset_counter reset_counter_i
       (.Q(done),
        .dclk(dclk));
  (* C_FAMILY = "kintex7" *) 
  (* C_HAS_MDIO = "TRUE" *) 
  (* C_RXAUI_MODE = "0" *) 
  (* c_rxdata_width = "64" *) 
  (* c_txdata_width = "64" *) 
  rxaui_0_rxaui_v4_3_7_top rxaui_0_core
       (.align_status(debug[5]),
        .configuration_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .mdc(mdc),
        .mdio_in(mdio_in),
        .mdio_out(mdio_out),
        .mdio_tri(mdio_tri),
        .mgt_codecomma(mgt_codecomma),
        .mgt_codevalid(mgt_codevalid),
        .mgt_enable_align({rxmcommaalignen_in,rxaui_0_core_n_145}),
        .mgt_enchansync(mgt_enchansync),
        .mgt_loopback(mgt_loopback),
        .mgt_powerdown(mgt_powerdown),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_rxcharisk({\mgt_rxcharisk_reg_reg_n_0_[7] ,\mgt_rxcharisk_reg_reg_n_0_[6] ,\mgt_rxcharisk_reg_reg_n_0_[5] ,\mgt_rxcharisk_reg_reg_n_0_[4] ,\mgt_rxcharisk_reg_reg_n_0_[3] ,\mgt_rxcharisk_reg_reg_n_0_[2] ,\mgt_rxcharisk_reg_reg_n_0_[1] ,\mgt_rxcharisk_reg_reg_n_0_[0] }),
        .mgt_rxdata({\mgt_rxdata_reg_reg_n_0_[63] ,\mgt_rxdata_reg_reg_n_0_[62] ,\mgt_rxdata_reg_reg_n_0_[61] ,\mgt_rxdata_reg_reg_n_0_[60] ,\mgt_rxdata_reg_reg_n_0_[59] ,\mgt_rxdata_reg_reg_n_0_[58] ,\mgt_rxdata_reg_reg_n_0_[57] ,\mgt_rxdata_reg_reg_n_0_[56] ,\mgt_rxdata_reg_reg_n_0_[55] ,\mgt_rxdata_reg_reg_n_0_[54] ,\mgt_rxdata_reg_reg_n_0_[53] ,\mgt_rxdata_reg_reg_n_0_[52] ,\mgt_rxdata_reg_reg_n_0_[51] ,\mgt_rxdata_reg_reg_n_0_[50] ,\mgt_rxdata_reg_reg_n_0_[49] ,\mgt_rxdata_reg_reg_n_0_[48] ,\mgt_rxdata_reg_reg_n_0_[47] ,\mgt_rxdata_reg_reg_n_0_[46] ,\mgt_rxdata_reg_reg_n_0_[45] ,\mgt_rxdata_reg_reg_n_0_[44] ,\mgt_rxdata_reg_reg_n_0_[43] ,\mgt_rxdata_reg_reg_n_0_[42] ,\mgt_rxdata_reg_reg_n_0_[41] ,\mgt_rxdata_reg_reg_n_0_[40] ,\mgt_rxdata_reg_reg_n_0_[39] ,\mgt_rxdata_reg_reg_n_0_[38] ,\mgt_rxdata_reg_reg_n_0_[37] ,\mgt_rxdata_reg_reg_n_0_[36] ,\mgt_rxdata_reg_reg_n_0_[35] ,\mgt_rxdata_reg_reg_n_0_[34] ,\mgt_rxdata_reg_reg_n_0_[33] ,\mgt_rxdata_reg_reg_n_0_[32] ,\mgt_rxdata_reg_reg_n_0_[31] ,\mgt_rxdata_reg_reg_n_0_[30] ,\mgt_rxdata_reg_reg_n_0_[29] ,\mgt_rxdata_reg_reg_n_0_[28] ,\mgt_rxdata_reg_reg_n_0_[27] ,\mgt_rxdata_reg_reg_n_0_[26] ,\mgt_rxdata_reg_reg_n_0_[25] ,\mgt_rxdata_reg_reg_n_0_[24] ,\mgt_rxdata_reg_reg_n_0_[23] ,\mgt_rxdata_reg_reg_n_0_[22] ,\mgt_rxdata_reg_reg_n_0_[21] ,\mgt_rxdata_reg_reg_n_0_[20] ,\mgt_rxdata_reg_reg_n_0_[19] ,\mgt_rxdata_reg_reg_n_0_[18] ,\mgt_rxdata_reg_reg_n_0_[17] ,\mgt_rxdata_reg_reg_n_0_[16] ,\mgt_rxdata_reg_reg_n_0_[15] ,\mgt_rxdata_reg_reg_n_0_[14] ,\mgt_rxdata_reg_reg_n_0_[13] ,\mgt_rxdata_reg_reg_n_0_[12] ,\mgt_rxdata_reg_reg_n_0_[11] ,\mgt_rxdata_reg_reg_n_0_[10] ,\mgt_rxdata_reg_reg_n_0_[9] ,\mgt_rxdata_reg_reg_n_0_[8] ,\mgt_rxdata_reg_reg_n_0_[7] ,\mgt_rxdata_reg_reg_n_0_[6] ,\mgt_rxdata_reg_reg_n_0_[5] ,\mgt_rxdata_reg_reg_n_0_[4] ,\mgt_rxdata_reg_reg_n_0_[3] ,\mgt_rxdata_reg_reg_n_0_[2] ,\mgt_rxdata_reg_reg_n_0_[1] ,\mgt_rxdata_reg_reg_n_0_[0] }),
        .mgt_rxlock({1'b0,out}),
        .mgt_tx_reset({txsync_i_n_2,1'b0}),
        .mgt_txcharisk({txcharisk_in,rxaui_0_core_n_140,rxaui_0_core_n_141,rxaui_0_core_n_142,rxaui_0_core_n_143}),
        .mgt_txdata({txdata_in,rxaui_0_core_n_104,rxaui_0_core_n_105,rxaui_0_core_n_106,rxaui_0_core_n_107,rxaui_0_core_n_108,rxaui_0_core_n_109,rxaui_0_core_n_110,rxaui_0_core_n_111,rxaui_0_core_n_112,rxaui_0_core_n_113,rxaui_0_core_n_114,rxaui_0_core_n_115,rxaui_0_core_n_116,rxaui_0_core_n_117,rxaui_0_core_n_118,rxaui_0_core_n_119,rxaui_0_core_n_120,rxaui_0_core_n_121,rxaui_0_core_n_122,rxaui_0_core_n_123,rxaui_0_core_n_124,rxaui_0_core_n_125,rxaui_0_core_n_126,rxaui_0_core_n_127,rxaui_0_core_n_128,rxaui_0_core_n_129,rxaui_0_core_n_130,rxaui_0_core_n_131,rxaui_0_core_n_132,rxaui_0_core_n_133,rxaui_0_core_n_134,rxaui_0_core_n_135}),
        .prtad(prtad),
        .reset(uclk_reset156),
        .rxclk(1'b0),
        .signal_detect({signal_detect_1_sync_i_n_0,data_out}),
        .soft_reset(soft_reset),
        .status_vector(NLW_rxaui_0_core_status_vector_UNCONNECTED[7:0]),
        .sync_status(debug[4:1]),
        .type_sel(type_sel),
        .usrclk(CLK),
        .xgmii_rxc(xgmii_rxc),
        .xgmii_rxd(xgmii_rxd),
        .xgmii_txc(xgmii_txc),
        .xgmii_txd(xgmii_txd));
  LUT2 #(
    .INIT(4'h1)) 
    rxaui_0_core_i_1
       (.I0(mgt_rxnotintable_reg[7]),
        .I1(mgt_rxdisperr_reg[7]),
        .O(mgt_codevalid[7]));
  LUT2 #(
    .INIT(4'h1)) 
    rxaui_0_core_i_2
       (.I0(mgt_rxnotintable_reg[6]),
        .I1(mgt_rxdisperr_reg[6]),
        .O(mgt_codevalid[6]));
  LUT2 #(
    .INIT(4'h1)) 
    rxaui_0_core_i_3
       (.I0(mgt_rxnotintable_reg[5]),
        .I1(mgt_rxdisperr_reg[5]),
        .O(mgt_codevalid[5]));
  LUT2 #(
    .INIT(4'h1)) 
    rxaui_0_core_i_4
       (.I0(mgt_rxnotintable_reg[4]),
        .I1(mgt_rxdisperr_reg[4]),
        .O(mgt_codevalid[4]));
  LUT2 #(
    .INIT(4'h1)) 
    rxaui_0_core_i_5
       (.I0(mgt_rxnotintable_reg[3]),
        .I1(mgt_rxdisperr_reg[3]),
        .O(mgt_codevalid[3]));
  LUT2 #(
    .INIT(4'h1)) 
    rxaui_0_core_i_6
       (.I0(mgt_rxnotintable_reg[2]),
        .I1(mgt_rxdisperr_reg[2]),
        .O(mgt_codevalid[2]));
  LUT2 #(
    .INIT(4'h1)) 
    rxaui_0_core_i_7
       (.I0(mgt_rxnotintable_reg[1]),
        .I1(mgt_rxdisperr_reg[1]),
        .O(mgt_codevalid[1]));
  LUT2 #(
    .INIT(4'h1)) 
    rxaui_0_core_i_8
       (.I0(mgt_rxnotintable_reg[0]),
        .I1(mgt_rxdisperr_reg[0]),
        .O(mgt_codevalid[0]));
  rxaui_0_rxaui_0_cl_clocking rxaui_cl_clocking_i
       (.clk156_out(CLK),
        .gt0_txoutclk_i(gt0_txoutclk_i));
  rxaui_0_rxaui_0_cl_resets rxaui_cl_resets_i
       (.out(uclk_reset156),
        .reset(reset),
        .\sync_r_reg[4] (plllocked_sync_i_n_2),
        .uclk_mgt_rx_reset_reg(CLK));
  rxaui_0_rxaui_0_ff_synchronizer_2 rxpmareset_sync0_i
       (.dclk(dclk),
        .gt0_rxpmareset_in(gt0_rxpmareset_in),
        .out(gt0_rxpmareset));
  rxaui_0_rxaui_0_ff_synchronizer_3 rxpmareset_sync1_i
       (.dclk(dclk),
        .gt1_rxpmareset_in(gt1_rxpmareset_in),
        .out(gt1_rxpmareset));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    rxprbs_in_use0
       (.I0(gt1_rxprbssel_in[2]),
        .I1(gt1_rxprbssel_in[1]),
        .I2(gt0_rxprbssel_in[1]),
        .I3(gt0_rxprbssel_in[0]),
        .I4(gt1_rxprbssel_in[0]),
        .I5(gt0_rxprbssel_in[2]),
        .O(rxprbs_in_use0__0));
  FDRE #(
    .INIT(1'b0)) 
    rxprbs_in_use_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxprbs_in_use0__0),
        .Q(rxprbs_in_use),
        .R(1'b0));
  rxaui_0_rxaui_0_ff_synchronizer_4 signal_detect_0_sync_i
       (.out(data_out),
        .signal_detect(signal_detect[0]),
        .uclk_mgt_rx_reset_reg(CLK));
  rxaui_0_rxaui_0_ff_synchronizer_5 signal_detect_1_sync_i
       (.out(signal_detect_1_sync_i_n_0),
        .signal_detect(signal_detect[1]),
        .uclk_mgt_rx_reset_reg(CLK));
  rxaui_0_rxaui_0_gt_wrapper_tx_sync_manual txsync_i
       (.SR(uclk_mgt_tx_reset),
        .\TXDLYEN_reg[0]_0 (txsync_i_n_1),
        .\TXDLYSRESET_reg[0]_0 (txsync_i_n_4),
        .\TXPHALIGN_reg[0]_0 (txsync_i_n_7),
        .\TXPHINIT_reg[0]_0 (txsync_i_n_5),
        .debug(debug[0]),
        .gt0_txdlysresetdone_out(gt0_txdlysresetdone_out),
        .gt0_txphaligndone_out(gt0_txphaligndone_out),
        .gt0_txphinitdone_out(gt0_txphinitdone_out),
        .gt1_txdlysresetdone_out(gt1_txdlysresetdone_out),
        .gt1_txphaligndone_out(gt1_txphaligndone_out),
        .gt1_txphinitdone_out(gt1_txphinitdone_out),
        .mgt_tx_reset(txsync_i_n_2),
        .txdlysreset_in(txdlysreset_in),
        .txphalign_in(txphalign_in),
        .txphinit_in(txphinit_in),
        .uclk_mgt_rx_reset_reg(CLK),
        .uclk_txsync_start_phase_align_reg(uclk_txsync_start_phase_align_reg_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \uclk_chbond_counter[0]_i_1 
       (.I0(\uclk_chbond_counter[0]_i_3_n_0 ),
        .I1(p_0_in),
        .I2(debug[5]),
        .I3(rxprbs_in_use),
        .O(uclk_chbond_counter));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \uclk_chbond_counter[0]_i_3 
       (.I0(debug[1]),
        .I1(debug[3]),
        .I2(debug[4]),
        .I3(debug[2]),
        .O(\uclk_chbond_counter[0]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[0]_i_4 
       (.I0(\uclk_chbond_counter_reg_n_0_[3] ),
        .O(\uclk_chbond_counter[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[0]_i_5 
       (.I0(\uclk_chbond_counter_reg_n_0_[2] ),
        .O(\uclk_chbond_counter[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[0]_i_6 
       (.I0(\uclk_chbond_counter_reg_n_0_[1] ),
        .O(\uclk_chbond_counter[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \uclk_chbond_counter[0]_i_7 
       (.I0(\uclk_chbond_counter_reg_n_0_[0] ),
        .O(\uclk_chbond_counter[0]_i_7_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[12]_i_2 
       (.I0(p_0_in),
        .O(\uclk_chbond_counter[12]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[12]_i_3 
       (.I0(\uclk_chbond_counter_reg_n_0_[14] ),
        .O(\uclk_chbond_counter[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[12]_i_4 
       (.I0(\uclk_chbond_counter_reg_n_0_[13] ),
        .O(\uclk_chbond_counter[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[12]_i_5 
       (.I0(\uclk_chbond_counter_reg_n_0_[12] ),
        .O(\uclk_chbond_counter[12]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[4]_i_2 
       (.I0(\uclk_chbond_counter_reg_n_0_[7] ),
        .O(\uclk_chbond_counter[4]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[4]_i_3 
       (.I0(\uclk_chbond_counter_reg_n_0_[6] ),
        .O(\uclk_chbond_counter[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[4]_i_4 
       (.I0(\uclk_chbond_counter_reg_n_0_[5] ),
        .O(\uclk_chbond_counter[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[4]_i_5 
       (.I0(\uclk_chbond_counter_reg_n_0_[4] ),
        .O(\uclk_chbond_counter[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[8]_i_2 
       (.I0(\uclk_chbond_counter_reg_n_0_[11] ),
        .O(\uclk_chbond_counter[8]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[8]_i_3 
       (.I0(\uclk_chbond_counter_reg_n_0_[10] ),
        .O(\uclk_chbond_counter[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[8]_i_4 
       (.I0(\uclk_chbond_counter_reg_n_0_[9] ),
        .O(\uclk_chbond_counter[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_chbond_counter[8]_i_5 
       (.I0(\uclk_chbond_counter_reg_n_0_[8] ),
        .O(\uclk_chbond_counter[8]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[0]_i_2_n_7 ),
        .Q(\uclk_chbond_counter_reg_n_0_[0] ),
        .R(uclk_chbond_counter));
  CARRY4 \uclk_chbond_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\uclk_chbond_counter_reg[0]_i_2_n_0 ,\uclk_chbond_counter_reg[0]_i_2_n_1 ,\uclk_chbond_counter_reg[0]_i_2_n_2 ,\uclk_chbond_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\uclk_chbond_counter_reg[0]_i_2_n_4 ,\uclk_chbond_counter_reg[0]_i_2_n_5 ,\uclk_chbond_counter_reg[0]_i_2_n_6 ,\uclk_chbond_counter_reg[0]_i_2_n_7 }),
        .S({\uclk_chbond_counter[0]_i_4_n_0 ,\uclk_chbond_counter[0]_i_5_n_0 ,\uclk_chbond_counter[0]_i_6_n_0 ,\uclk_chbond_counter[0]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[8]_i_1_n_5 ),
        .Q(\uclk_chbond_counter_reg_n_0_[10] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[8]_i_1_n_4 ),
        .Q(\uclk_chbond_counter_reg_n_0_[11] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[12]_i_1_n_7 ),
        .Q(\uclk_chbond_counter_reg_n_0_[12] ),
        .R(uclk_chbond_counter));
  CARRY4 \uclk_chbond_counter_reg[12]_i_1 
       (.CI(\uclk_chbond_counter_reg[8]_i_1_n_0 ),
        .CO({\NLW_uclk_chbond_counter_reg[12]_i_1_CO_UNCONNECTED [3],\uclk_chbond_counter_reg[12]_i_1_n_1 ,\uclk_chbond_counter_reg[12]_i_1_n_2 ,\uclk_chbond_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\uclk_chbond_counter_reg[12]_i_1_n_4 ,\uclk_chbond_counter_reg[12]_i_1_n_5 ,\uclk_chbond_counter_reg[12]_i_1_n_6 ,\uclk_chbond_counter_reg[12]_i_1_n_7 }),
        .S({\uclk_chbond_counter[12]_i_2_n_0 ,\uclk_chbond_counter[12]_i_3_n_0 ,\uclk_chbond_counter[12]_i_4_n_0 ,\uclk_chbond_counter[12]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[12]_i_1_n_6 ),
        .Q(\uclk_chbond_counter_reg_n_0_[13] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[12]_i_1_n_5 ),
        .Q(\uclk_chbond_counter_reg_n_0_[14] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[12]_i_1_n_4 ),
        .Q(p_0_in),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[0]_i_2_n_6 ),
        .Q(\uclk_chbond_counter_reg_n_0_[1] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[0]_i_2_n_5 ),
        .Q(\uclk_chbond_counter_reg_n_0_[2] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[0]_i_2_n_4 ),
        .Q(\uclk_chbond_counter_reg_n_0_[3] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[4]_i_1_n_7 ),
        .Q(\uclk_chbond_counter_reg_n_0_[4] ),
        .R(uclk_chbond_counter));
  CARRY4 \uclk_chbond_counter_reg[4]_i_1 
       (.CI(\uclk_chbond_counter_reg[0]_i_2_n_0 ),
        .CO({\uclk_chbond_counter_reg[4]_i_1_n_0 ,\uclk_chbond_counter_reg[4]_i_1_n_1 ,\uclk_chbond_counter_reg[4]_i_1_n_2 ,\uclk_chbond_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\uclk_chbond_counter_reg[4]_i_1_n_4 ,\uclk_chbond_counter_reg[4]_i_1_n_5 ,\uclk_chbond_counter_reg[4]_i_1_n_6 ,\uclk_chbond_counter_reg[4]_i_1_n_7 }),
        .S({\uclk_chbond_counter[4]_i_2_n_0 ,\uclk_chbond_counter[4]_i_3_n_0 ,\uclk_chbond_counter[4]_i_4_n_0 ,\uclk_chbond_counter[4]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[4]_i_1_n_6 ),
        .Q(\uclk_chbond_counter_reg_n_0_[5] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[4]_i_1_n_5 ),
        .Q(\uclk_chbond_counter_reg_n_0_[6] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[4]_i_1_n_4 ),
        .Q(\uclk_chbond_counter_reg_n_0_[7] ),
        .R(uclk_chbond_counter));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[8]_i_1_n_7 ),
        .Q(\uclk_chbond_counter_reg_n_0_[8] ),
        .R(uclk_chbond_counter));
  CARRY4 \uclk_chbond_counter_reg[8]_i_1 
       (.CI(\uclk_chbond_counter_reg[4]_i_1_n_0 ),
        .CO({\uclk_chbond_counter_reg[8]_i_1_n_0 ,\uclk_chbond_counter_reg[8]_i_1_n_1 ,\uclk_chbond_counter_reg[8]_i_1_n_2 ,\uclk_chbond_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\uclk_chbond_counter_reg[8]_i_1_n_4 ,\uclk_chbond_counter_reg[8]_i_1_n_5 ,\uclk_chbond_counter_reg[8]_i_1_n_6 ,\uclk_chbond_counter_reg[8]_i_1_n_7 }),
        .S({\uclk_chbond_counter[8]_i_2_n_0 ,\uclk_chbond_counter[8]_i_3_n_0 ,\uclk_chbond_counter[8]_i_4_n_0 ,\uclk_chbond_counter[8]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_chbond_counter_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_chbond_counter_reg[8]_i_1_n_6 ),
        .Q(\uclk_chbond_counter_reg_n_0_[9] ),
        .R(uclk_chbond_counter));
  LUT2 #(
    .INIT(4'h2)) 
    uclk_mgt_loopback_falling_i_1
       (.I0(mgt_loopback_r),
        .I1(mgt_loopback),
        .O(uclk_mgt_loopback_falling_i_1_n_0));
  FDRE uclk_mgt_loopback_falling_reg
       (.C(CLK),
        .CE(1'b1),
        .D(uclk_mgt_loopback_falling_i_1_n_0),
        .Q(uclk_mgt_loopback_falling),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    uclk_mgt_powerdown_falling_i_1
       (.I0(mgt_powerdown_r),
        .I1(mgt_powerdown),
        .O(uclk_mgt_powerdown_falling0));
  FDRE #(
    .INIT(1'b0)) 
    uclk_mgt_powerdown_falling_reg
       (.C(CLK),
        .CE(1'b1),
        .D(uclk_mgt_powerdown_falling0),
        .Q(uclk_mgt_powerdown_falling),
        .R(1'b0));
  FDRE uclk_mgt_rx_reset_reg
       (.C(CLK),
        .CE(1'b1),
        .D(uclk_mgt_rx_reset0),
        .Q(uclk_mgt_rx_reset),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFF60)) 
    \uclk_mgt_rxbuf_reset[0]_i_1 
       (.I0(bufStatus[1]),
        .I1(bufStatus[0]),
        .I2(bufStatus[2]),
        .I3(\uclk_mgt_rxbuf_reset[0]_i_2_n_0 ),
        .O(uclk_mgt_rxbuf_reset0));
  LUT4 #(
    .INIT(16'hBEAA)) 
    \uclk_mgt_rxbuf_reset[0]_i_2 
       (.I0(p_0_in),
        .I1(\uclk_mgt_rxbufstatus_reg_reg_n_0_[1] ),
        .I2(\uclk_mgt_rxbufstatus_reg_reg_n_0_[0] ),
        .I3(\uclk_mgt_rxbufstatus_reg_reg_n_0_[2] ),
        .O(\uclk_mgt_rxbuf_reset[0]_i_2_n_0 ));
  FDRE \uclk_mgt_rxbuf_reset_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(uclk_mgt_rxbuf_reset0),
        .Q(\uclk_mgt_rxbuf_reset_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_mgt_rxbufstatus_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_mgt_rxbufstatus_reg_reg[5]_0 [0]),
        .Q(\uclk_mgt_rxbufstatus_reg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_mgt_rxbufstatus_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_mgt_rxbufstatus_reg_reg[5]_0 [1]),
        .Q(\uclk_mgt_rxbufstatus_reg_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_mgt_rxbufstatus_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_mgt_rxbufstatus_reg_reg[5]_0 [2]),
        .Q(\uclk_mgt_rxbufstatus_reg_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_mgt_rxbufstatus_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_mgt_rxbufstatus_reg_reg[5]_0 [3]),
        .Q(bufStatus[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_mgt_rxbufstatus_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_mgt_rxbufstatus_reg_reg[5]_0 [4]),
        .Q(bufStatus[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_mgt_rxbufstatus_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_mgt_rxbufstatus_reg_reg[5]_0 [5]),
        .Q(bufStatus[2]),
        .R(1'b0));
  FDRE uclk_mgt_tx_reset_reg
       (.C(CLK),
        .CE(1'b1),
        .D(uclk_mgt_tx_reset0),
        .Q(uclk_mgt_tx_reset),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \uclk_sync_counter[0]_i_1 
       (.I0(\uclk_chbond_counter[0]_i_3_n_0 ),
        .I1(rxprbs_in_use),
        .I2(p_0_in4_in),
        .I3(mgt_powerdown),
        .O(\uclk_sync_counter[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[0]_i_3 
       (.I0(\uclk_sync_counter_reg_n_0_[3] ),
        .O(\uclk_sync_counter[0]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[0]_i_4 
       (.I0(\uclk_sync_counter_reg_n_0_[2] ),
        .O(\uclk_sync_counter[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[0]_i_5 
       (.I0(\uclk_sync_counter_reg_n_0_[1] ),
        .O(\uclk_sync_counter[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \uclk_sync_counter[0]_i_6 
       (.I0(\uclk_sync_counter_reg_n_0_[0] ),
        .O(\uclk_sync_counter[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[12]_i_2 
       (.I0(p_0_in4_in),
        .O(\uclk_sync_counter[12]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[12]_i_3 
       (.I0(\uclk_sync_counter_reg_n_0_[14] ),
        .O(\uclk_sync_counter[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[12]_i_4 
       (.I0(\uclk_sync_counter_reg_n_0_[13] ),
        .O(\uclk_sync_counter[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[12]_i_5 
       (.I0(\uclk_sync_counter_reg_n_0_[12] ),
        .O(\uclk_sync_counter[12]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[4]_i_2 
       (.I0(\uclk_sync_counter_reg_n_0_[7] ),
        .O(\uclk_sync_counter[4]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[4]_i_3 
       (.I0(\uclk_sync_counter_reg_n_0_[6] ),
        .O(\uclk_sync_counter[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[4]_i_4 
       (.I0(\uclk_sync_counter_reg_n_0_[5] ),
        .O(\uclk_sync_counter[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[4]_i_5 
       (.I0(\uclk_sync_counter_reg_n_0_[4] ),
        .O(\uclk_sync_counter[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[8]_i_2 
       (.I0(\uclk_sync_counter_reg_n_0_[11] ),
        .O(\uclk_sync_counter[8]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[8]_i_3 
       (.I0(\uclk_sync_counter_reg_n_0_[10] ),
        .O(\uclk_sync_counter[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[8]_i_4 
       (.I0(\uclk_sync_counter_reg_n_0_[9] ),
        .O(\uclk_sync_counter[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \uclk_sync_counter[8]_i_5 
       (.I0(\uclk_sync_counter_reg_n_0_[8] ),
        .O(\uclk_sync_counter[8]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[0]_i_2_n_7 ),
        .Q(\uclk_sync_counter_reg_n_0_[0] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  CARRY4 \uclk_sync_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\uclk_sync_counter_reg[0]_i_2_n_0 ,\uclk_sync_counter_reg[0]_i_2_n_1 ,\uclk_sync_counter_reg[0]_i_2_n_2 ,\uclk_sync_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\uclk_sync_counter_reg[0]_i_2_n_4 ,\uclk_sync_counter_reg[0]_i_2_n_5 ,\uclk_sync_counter_reg[0]_i_2_n_6 ,\uclk_sync_counter_reg[0]_i_2_n_7 }),
        .S({\uclk_sync_counter[0]_i_3_n_0 ,\uclk_sync_counter[0]_i_4_n_0 ,\uclk_sync_counter[0]_i_5_n_0 ,\uclk_sync_counter[0]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[8]_i_1_n_5 ),
        .Q(\uclk_sync_counter_reg_n_0_[10] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[8]_i_1_n_4 ),
        .Q(\uclk_sync_counter_reg_n_0_[11] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[12]_i_1_n_7 ),
        .Q(\uclk_sync_counter_reg_n_0_[12] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  CARRY4 \uclk_sync_counter_reg[12]_i_1 
       (.CI(\uclk_sync_counter_reg[8]_i_1_n_0 ),
        .CO({\NLW_uclk_sync_counter_reg[12]_i_1_CO_UNCONNECTED [3],\uclk_sync_counter_reg[12]_i_1_n_1 ,\uclk_sync_counter_reg[12]_i_1_n_2 ,\uclk_sync_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\uclk_sync_counter_reg[12]_i_1_n_4 ,\uclk_sync_counter_reg[12]_i_1_n_5 ,\uclk_sync_counter_reg[12]_i_1_n_6 ,\uclk_sync_counter_reg[12]_i_1_n_7 }),
        .S({\uclk_sync_counter[12]_i_2_n_0 ,\uclk_sync_counter[12]_i_3_n_0 ,\uclk_sync_counter[12]_i_4_n_0 ,\uclk_sync_counter[12]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[12]_i_1_n_6 ),
        .Q(\uclk_sync_counter_reg_n_0_[13] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[12]_i_1_n_5 ),
        .Q(\uclk_sync_counter_reg_n_0_[14] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[12]_i_1_n_4 ),
        .Q(p_0_in4_in),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[0]_i_2_n_6 ),
        .Q(\uclk_sync_counter_reg_n_0_[1] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[0]_i_2_n_5 ),
        .Q(\uclk_sync_counter_reg_n_0_[2] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[0]_i_2_n_4 ),
        .Q(\uclk_sync_counter_reg_n_0_[3] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[4]_i_1_n_7 ),
        .Q(\uclk_sync_counter_reg_n_0_[4] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  CARRY4 \uclk_sync_counter_reg[4]_i_1 
       (.CI(\uclk_sync_counter_reg[0]_i_2_n_0 ),
        .CO({\uclk_sync_counter_reg[4]_i_1_n_0 ,\uclk_sync_counter_reg[4]_i_1_n_1 ,\uclk_sync_counter_reg[4]_i_1_n_2 ,\uclk_sync_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\uclk_sync_counter_reg[4]_i_1_n_4 ,\uclk_sync_counter_reg[4]_i_1_n_5 ,\uclk_sync_counter_reg[4]_i_1_n_6 ,\uclk_sync_counter_reg[4]_i_1_n_7 }),
        .S({\uclk_sync_counter[4]_i_2_n_0 ,\uclk_sync_counter[4]_i_3_n_0 ,\uclk_sync_counter[4]_i_4_n_0 ,\uclk_sync_counter[4]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[4]_i_1_n_6 ),
        .Q(\uclk_sync_counter_reg_n_0_[5] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[4]_i_1_n_5 ),
        .Q(\uclk_sync_counter_reg_n_0_[6] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[4]_i_1_n_4 ),
        .Q(\uclk_sync_counter_reg_n_0_[7] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[8]_i_1_n_7 ),
        .Q(\uclk_sync_counter_reg_n_0_[8] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  CARRY4 \uclk_sync_counter_reg[8]_i_1 
       (.CI(\uclk_sync_counter_reg[4]_i_1_n_0 ),
        .CO({\uclk_sync_counter_reg[8]_i_1_n_0 ,\uclk_sync_counter_reg[8]_i_1_n_1 ,\uclk_sync_counter_reg[8]_i_1_n_2 ,\uclk_sync_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\uclk_sync_counter_reg[8]_i_1_n_4 ,\uclk_sync_counter_reg[8]_i_1_n_5 ,\uclk_sync_counter_reg[8]_i_1_n_6 ,\uclk_sync_counter_reg[8]_i_1_n_7 }),
        .S({\uclk_sync_counter[8]_i_2_n_0 ,\uclk_sync_counter[8]_i_3_n_0 ,\uclk_sync_counter[8]_i_4_n_0 ,\uclk_sync_counter[8]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_sync_counter_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\uclk_sync_counter_reg[8]_i_1_n_6 ),
        .Q(\uclk_sync_counter_reg_n_0_[9] ),
        .R(\uclk_sync_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_txresetdone_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(uclk_txresetdone_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uclk_txresetdone_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(uclk_txresetdone_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    uclk_txsync_start_phase_align_reg
       (.C(CLK),
        .CE(1'b1),
        .D(reset_count_done_sync_i_n_0),
        .Q(uclk_txsync_start_phase_align_reg_n_0),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_cl_clocking" *) 
module rxaui_0_rxaui_0_cl_clocking
   (clk156_out,
    gt0_txoutclk_i);
  output clk156_out;
  input gt0_txoutclk_i;

  wire clk156_out;
  wire gt0_txoutclk_i;

  (* box_type = "PRIMITIVE" *) 
  BUFG clk156_bufg_i
       (.I(gt0_txoutclk_i),
        .O(clk156_out));
endmodule

(* ORIG_REF_NAME = "rxaui_0_cl_resets" *) 
module rxaui_0_rxaui_0_cl_resets
   (out,
    reset,
    \sync_r_reg[4] ,
    uclk_mgt_rx_reset_reg);
  output out;
  input reset;
  input \sync_r_reg[4] ;
  input uclk_mgt_rx_reset_reg;

  wire reset;
  (* async_reg = "true" *) wire reset156_r1;
  (* async_reg = "true" *) wire reset156_r2;
  (* async_reg = "true" *) wire reset156_r3;
  wire \sync_r_reg[4] ;
  wire uclk_mgt_rx_reset_reg;

  assign out = reset156_r3;
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDSE reset156_r1_reg
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\sync_r_reg[4] ),
        .Q(reset156_r1),
        .S(reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDSE reset156_r2_reg
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(reset156_r1),
        .Q(reset156_r2),
        .S(reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDSE reset156_r3_reg
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(reset156_r2),
        .Q(reset156_r3),
        .S(reset));
endmodule

(* ORIG_REF_NAME = "rxaui_0_ff_synchronizer" *) 
module rxaui_0_rxaui_0_ff_synchronizer
   (out,
    uclk_mgt_rx_reset_reg,
    reset156_r1_reg,
    soft_reset,
    reset156_r3_reg,
    uclk_mgt_powerdown_falling,
    uclk_mgt_rx_reset_reg_0,
    qplllock_out);
  output [0:0]out;
  output uclk_mgt_rx_reset_reg;
  output reset156_r1_reg;
  input soft_reset;
  input reset156_r3_reg;
  input uclk_mgt_powerdown_falling;
  input uclk_mgt_rx_reset_reg_0;
  input qplllock_out;

  wire qplllock_out;
  wire reset156_r1_reg;
  wire reset156_r3_reg;
  wire soft_reset;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [4:0]sync_r;
  wire uclk_mgt_powerdown_falling;
  wire uclk_mgt_rx_reset_reg;
  wire uclk_mgt_rx_reset_reg_0;

  assign out[0] = sync_r[4];
  LUT1 #(
    .INIT(2'h1)) 
    reset156_r1_i_1
       (.I0(sync_r[4]),
        .O(reset156_r1_reg));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg_0),
        .CE(1'b1),
        .D(qplllock_out),
        .Q(sync_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg_0),
        .CE(1'b1),
        .D(sync_r[0]),
        .Q(sync_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg_0),
        .CE(1'b1),
        .D(sync_r[1]),
        .Q(sync_r[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[3] 
       (.C(uclk_mgt_rx_reset_reg_0),
        .CE(1'b1),
        .D(sync_r[2]),
        .Q(sync_r[3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[4] 
       (.C(uclk_mgt_rx_reset_reg_0),
        .CE(1'b1),
        .D(sync_r[3]),
        .Q(sync_r[4]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h0004)) 
    uclk_mgt_rx_reset_i_2
       (.I0(soft_reset),
        .I1(sync_r[4]),
        .I2(reset156_r3_reg),
        .I3(uclk_mgt_powerdown_falling),
        .O(uclk_mgt_rx_reset_reg));
endmodule

(* ORIG_REF_NAME = "rxaui_0_ff_synchronizer" *) 
module rxaui_0_rxaui_0_ff_synchronizer_1
   (uclk_txsync_start_phase_align_reg,
    uclk_mgt_rx_reset0,
    uclk_mgt_tx_reset0,
    uclk_txsync_start_phase_align_reg_0,
    Q,
    p_0_in4_in,
    uclk_mgt_loopback_falling,
    reset_reg_reg,
    uclk_mgt_powerdown_falling,
    out,
    \sync_r_reg[4]_0 ,
    soft_reset,
    uclk_mgt_rx_reset_reg,
    \count_reg[7] );
  output uclk_txsync_start_phase_align_reg;
  output uclk_mgt_rx_reset0;
  output uclk_mgt_tx_reset0;
  input uclk_txsync_start_phase_align_reg_0;
  input [1:0]Q;
  input p_0_in4_in;
  input uclk_mgt_loopback_falling;
  input reset_reg_reg;
  input uclk_mgt_powerdown_falling;
  input out;
  input [0:0]\sync_r_reg[4]_0 ;
  input soft_reset;
  input uclk_mgt_rx_reset_reg;
  input [0:0]\count_reg[7] ;

  wire [1:0]Q;
  wire [0:0]\count_reg[7] ;
  wire out;
  wire p_0_in4_in;
  wire reset_reg_reg;
  wire soft_reset;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [4:0]sync_r;
  wire [0:0]\sync_r_reg[4]_0 ;
  wire uclk_mgt_loopback_falling;
  wire uclk_mgt_powerdown_falling;
  wire uclk_mgt_rx_reset0;
  wire uclk_mgt_rx_reset_reg;
  wire uclk_mgt_tx_reset0;
  wire uclk_txsync_start_phase_align_reg;
  wire uclk_txsync_start_phase_align_reg_0;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\count_reg[7] ),
        .Q(sync_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[0]),
        .Q(sync_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[1]),
        .Q(sync_r[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[3] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[2]),
        .Q(sync_r[3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[4] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[3]),
        .Q(sync_r[4]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hA8AA)) 
    uclk_mgt_rx_reset_i_1
       (.I0(sync_r[4]),
        .I1(p_0_in4_in),
        .I2(uclk_mgt_loopback_falling),
        .I3(reset_reg_reg),
        .O(uclk_mgt_rx_reset0));
  LUT5 #(
    .INIT(32'hAAAAA8AA)) 
    uclk_mgt_tx_reset_i_1
       (.I0(sync_r[4]),
        .I1(uclk_mgt_powerdown_falling),
        .I2(out),
        .I3(\sync_r_reg[4]_0 ),
        .I4(soft_reset),
        .O(uclk_mgt_tx_reset0));
  LUT4 #(
    .INIT(16'hE222)) 
    uclk_txsync_start_phase_align_i_1
       (.I0(uclk_txsync_start_phase_align_reg_0),
        .I1(sync_r[4]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(uclk_txsync_start_phase_align_reg));
endmodule

(* ORIG_REF_NAME = "rxaui_0_ff_synchronizer" *) 
module rxaui_0_rxaui_0_ff_synchronizer_2
   (out,
    dclk,
    gt0_rxpmareset_in);
  output [0:0]out;
  input dclk;
  input gt0_rxpmareset_in;

  wire dclk;
  wire gt0_rxpmareset_in;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [4:0]sync_r;

  assign out[0] = sync_r[4];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[0] 
       (.C(dclk),
        .CE(1'b1),
        .D(gt0_rxpmareset_in),
        .Q(sync_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[1] 
       (.C(dclk),
        .CE(1'b1),
        .D(sync_r[0]),
        .Q(sync_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[2] 
       (.C(dclk),
        .CE(1'b1),
        .D(sync_r[1]),
        .Q(sync_r[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[3] 
       (.C(dclk),
        .CE(1'b1),
        .D(sync_r[2]),
        .Q(sync_r[3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[4] 
       (.C(dclk),
        .CE(1'b1),
        .D(sync_r[3]),
        .Q(sync_r[4]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_ff_synchronizer" *) 
module rxaui_0_rxaui_0_ff_synchronizer_3
   (out,
    dclk,
    gt1_rxpmareset_in);
  output [0:0]out;
  input dclk;
  input gt1_rxpmareset_in;

  wire dclk;
  wire gt1_rxpmareset_in;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [4:0]sync_r;

  assign out[0] = sync_r[4];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[0] 
       (.C(dclk),
        .CE(1'b1),
        .D(gt1_rxpmareset_in),
        .Q(sync_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[1] 
       (.C(dclk),
        .CE(1'b1),
        .D(sync_r[0]),
        .Q(sync_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[2] 
       (.C(dclk),
        .CE(1'b1),
        .D(sync_r[1]),
        .Q(sync_r[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[3] 
       (.C(dclk),
        .CE(1'b1),
        .D(sync_r[2]),
        .Q(sync_r[3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[4] 
       (.C(dclk),
        .CE(1'b1),
        .D(sync_r[3]),
        .Q(sync_r[4]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_ff_synchronizer" *) 
module rxaui_0_rxaui_0_ff_synchronizer_4
   (out,
    uclk_mgt_rx_reset_reg,
    signal_detect);
  output [0:0]out;
  input uclk_mgt_rx_reset_reg;
  input [0:0]signal_detect;

  wire [0:0]signal_detect;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [4:0]sync_r;
  wire uclk_mgt_rx_reset_reg;

  assign out[0] = sync_r[4];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(signal_detect),
        .Q(sync_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[0]),
        .Q(sync_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[1]),
        .Q(sync_r[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[3] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[2]),
        .Q(sync_r[3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[4] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[3]),
        .Q(sync_r[4]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_ff_synchronizer" *) 
module rxaui_0_rxaui_0_ff_synchronizer_5
   (out,
    uclk_mgt_rx_reset_reg,
    signal_detect);
  output [0:0]out;
  input uclk_mgt_rx_reset_reg;
  input [0:0]signal_detect;

  wire [0:0]signal_detect;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [4:0]sync_r;
  wire uclk_mgt_rx_reset_reg;

  assign out[0] = sync_r[4];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(signal_detect),
        .Q(sync_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[0]),
        .Q(sync_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[1]),
        .Q(sync_r[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[3] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[2]),
        .Q(sync_r[3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync_r_reg[4] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync_r[3]),
        .Q(sync_r[4]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_gt_common_wrapper" *) 
module rxaui_0_rxaui_0_gt_common_wrapper
   (qplllock_out,
    qplloutclk_out,
    qplloutrefclk_out,
    refclk_out,
    dclk,
    common_pll_reset_i);
  output qplllock_out;
  output qplloutclk_out;
  output qplloutrefclk_out;
  input refclk_out;
  input dclk;
  input common_pll_reset_i;

  wire common_pll_reset_i;
  wire dclk;
  wire gtxe2_common_0_i_n_5;
  wire qplllock_out;
  wire qplloutclk_out;
  wire qplloutrefclk_out;
  wire refclk_out;
  wire NLW_gtxe2_common_0_i_DRPRDY_UNCONNECTED;
  wire NLW_gtxe2_common_0_i_QPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_common_0_i_REFCLKOUTMONITOR_UNCONNECTED;
  wire [15:0]NLW_gtxe2_common_0_i_DRPDO_UNCONNECTED;
  wire [7:0]NLW_gtxe2_common_0_i_QPLLDMONITOR_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_COMMON #(
    .BIAS_CFG(64'h0000040000001000),
    .COMMON_CFG(32'h00000000),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_QPLLLOCKDETCLK_INVERTED(1'b0),
    .QPLL_CFG(27'h06801C1),
    .QPLL_CLKOUT_CFG(4'b0000),
    .QPLL_COARSE_FREQ_OVRD(6'b010000),
    .QPLL_COARSE_FREQ_OVRD_EN(1'b0),
    .QPLL_CP(10'b0000011111),
    .QPLL_CP_MONITOR_EN(1'b0),
    .QPLL_DMONITOR_SEL(1'b0),
    .QPLL_FBDIV(10'b0010000000),
    .QPLL_FBDIV_MONITOR_EN(1'b0),
    .QPLL_FBDIV_RATIO(1'b1),
    .QPLL_INIT_CFG(24'h000006),
    .QPLL_LOCK_CFG(16'h21E8),
    .QPLL_LPF(4'b1111),
    .QPLL_REFCLK_DIV(1),
    .SIM_QPLLREFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_VERSION("4.0")) 
    gtxe2_common_0_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO(NLW_gtxe2_common_0_i_DRPDO_UNCONNECTED[15:0]),
        .DRPEN(1'b0),
        .DRPRDY(NLW_gtxe2_common_0_i_DRPRDY_UNCONNECTED),
        .DRPWE(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(refclk_out),
        .GTREFCLK1(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLDMONITOR(NLW_gtxe2_common_0_i_QPLLDMONITOR_UNCONNECTED[7:0]),
        .QPLLFBCLKLOST(NLW_gtxe2_common_0_i_QPLLFBCLKLOST_UNCONNECTED),
        .QPLLLOCK(qplllock_out),
        .QPLLLOCKDETCLK(dclk),
        .QPLLLOCKEN(1'b1),
        .QPLLOUTCLK(qplloutclk_out),
        .QPLLOUTREFCLK(qplloutrefclk_out),
        .QPLLOUTRESET(1'b0),
        .QPLLPD(1'b0),
        .QPLLREFCLKLOST(gtxe2_common_0_i_n_5),
        .QPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .QPLLRESET(common_pll_reset_i),
        .QPLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLRSVD2({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR(NLW_gtxe2_common_0_i_REFCLKOUTMONITOR_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "rxaui_0_gt_wrapper_GT" *) 
module rxaui_0_rxaui_0_gt_wrapper_GT
   (gt0_drprdy,
    gt0_eyescandataerror_out,
    rxaui_tx_l0_n,
    rxaui_tx_l0_p,
    gt0_rxcommadet_out,
    gt0_rxprbserr_out,
    gt0_rxresetdone_out,
    gt0_txdlysresetdone_out,
    gt0_txoutclk_i,
    gt0_txphaligndone_out,
    gt0_txphinitdone_out,
    D,
    gt0_drpdo,
    \uclk_mgt_rxbufstatus_reg_reg[2] ,
    \mgt_rxdata_reg_reg[31] ,
    gt0_rxmonitorout_out,
    gt0_dmonitorout_out,
    \mgt_codecomma_reg_reg[3] ,
    \mgt_rxcharisk_reg_reg[3] ,
    \mgt_rxdisperr_reg_reg[3] ,
    \mgt_rxnotintable_reg_reg[3] ,
    dclk,
    gt0_drpen,
    gt0_drpwe,
    gt0_eyescanreset_in,
    gt0_eyescantrigger_in,
    uclk_mgt_rx_reset,
    SR,
    rxaui_rx_l0_n,
    rxaui_rx_l0_p,
    qplloutclk_out,
    qplloutrefclk_out,
    \uclk_mgt_rxbuf_reset_reg[0] ,
    gt0_rxcdrhold_in,
    mgt_enchansync,
    gt0_rxdfelpmreset_in,
    gt0_rxlpmen_in,
    mgt_enable_align,
    gt0_rxpcsreset_in,
    out,
    gt0_rxpolarity_in,
    gt0_rxprbscntreset_in,
    \sync_r_reg[4] ,
    uclk_mgt_rx_reset_reg,
    \TXDLYEN_reg[0] ,
    \TXDLYSRESET_reg[0] ,
    mgt_powerdown_r,
    gt0_txinhibit_in,
    gt0_txpcsreset_in,
    \TXPHALIGN_reg[0] ,
    \TXPHINIT_reg[0] ,
    gt0_txpmareset_in,
    gt0_txpolarity_in,
    gt0_txprbsforceerr_in,
    gt0_drpdi,
    gt0_rxmonitorsel_in,
    mgt_powerdown,
    gt0_loopback_in,
    gt0_rxprbssel_in,
    gt0_rxrate_in,
    gt0_txprbssel_in,
    gt0_txdiffctrl_in,
    RXCHBONDO,
    gt0_txpostcursor_in,
    gt0_txprecursor_in,
    mgt_txdata,
    mgt_txcharisk,
    gt0_drpaddr,
    mgt_loopback_r);
  output gt0_drprdy;
  output gt0_eyescandataerror_out;
  output rxaui_tx_l0_n;
  output rxaui_tx_l0_p;
  output gt0_rxcommadet_out;
  output gt0_rxprbserr_out;
  output gt0_rxresetdone_out;
  output gt0_txdlysresetdone_out;
  output gt0_txoutclk_i;
  output gt0_txphaligndone_out;
  output gt0_txphinitdone_out;
  output [0:0]D;
  output [15:0]gt0_drpdo;
  output [2:0]\uclk_mgt_rxbufstatus_reg_reg[2] ;
  output [31:0]\mgt_rxdata_reg_reg[31] ;
  output [6:0]gt0_rxmonitorout_out;
  output [7:0]gt0_dmonitorout_out;
  output [3:0]\mgt_codecomma_reg_reg[3] ;
  output [3:0]\mgt_rxcharisk_reg_reg[3] ;
  output [3:0]\mgt_rxdisperr_reg_reg[3] ;
  output [3:0]\mgt_rxnotintable_reg_reg[3] ;
  input dclk;
  input gt0_drpen;
  input gt0_drpwe;
  input gt0_eyescanreset_in;
  input gt0_eyescantrigger_in;
  input uclk_mgt_rx_reset;
  input [0:0]SR;
  input rxaui_rx_l0_n;
  input rxaui_rx_l0_p;
  input qplloutclk_out;
  input qplloutrefclk_out;
  input \uclk_mgt_rxbuf_reset_reg[0] ;
  input gt0_rxcdrhold_in;
  input mgt_enchansync;
  input gt0_rxdfelpmreset_in;
  input gt0_rxlpmen_in;
  input [0:0]mgt_enable_align;
  input gt0_rxpcsreset_in;
  input [0:0]out;
  input gt0_rxpolarity_in;
  input gt0_rxprbscntreset_in;
  input [0:0]\sync_r_reg[4] ;
  input uclk_mgt_rx_reset_reg;
  input \TXDLYEN_reg[0] ;
  input \TXDLYSRESET_reg[0] ;
  input mgt_powerdown_r;
  input gt0_txinhibit_in;
  input gt0_txpcsreset_in;
  input \TXPHALIGN_reg[0] ;
  input \TXPHINIT_reg[0] ;
  input gt0_txpmareset_in;
  input gt0_txpolarity_in;
  input gt0_txprbsforceerr_in;
  input [15:0]gt0_drpdi;
  input [1:0]gt0_rxmonitorsel_in;
  input mgt_powerdown;
  input [2:0]gt0_loopback_in;
  input [2:0]gt0_rxprbssel_in;
  input [2:0]gt0_rxrate_in;
  input [2:0]gt0_txprbssel_in;
  input [3:0]gt0_txdiffctrl_in;
  input [4:0]RXCHBONDO;
  input [4:0]gt0_txpostcursor_in;
  input [4:0]gt0_txprecursor_in;
  input [31:0]mgt_txdata;
  input [3:0]mgt_txcharisk;
  input [8:0]gt0_drpaddr;
  input mgt_loopback_r;

  wire [0:0]D;
  wire [4:0]RXCHBONDO;
  wire [0:0]SR;
  wire \TXDLYEN_reg[0] ;
  wire \TXDLYSRESET_reg[0] ;
  wire \TXPHALIGN_reg[0] ;
  wire \TXPHINIT_reg[0] ;
  wire dclk;
  wire [7:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr;
  wire [15:0]gt0_drpdi;
  wire [15:0]gt0_drpdo;
  wire gt0_drpen;
  wire gt0_drprdy;
  wire gt0_drpwe;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire [2:0]gt0_loopback_in;
  wire gt0_rxcdrhold_in;
  wire gt0_rxcommadet_out;
  wire gt0_rxdfelpmreset_in;
  wire gt0_rxlpmen_in;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire gt0_rxpcsreset_in;
  wire gt0_rxpolarity_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire [2:0]gt0_rxrate_in;
  wire gt0_rxresetdone_out;
  wire [3:0]gt0_txdiffctrl_in;
  wire gt0_txdlysresetdone_out;
  wire gt0_txinhibit_in;
  wire gt0_txoutclk_i;
  wire gt0_txpcsreset_in;
  wire gt0_txphaligndone_out;
  wire gt0_txphinitdone_out;
  wire gt0_txpmareset_in;
  wire gt0_txpolarity_in;
  wire [4:0]gt0_txpostcursor_in;
  wire gt0_txprbsforceerr_in;
  wire [2:0]gt0_txprbssel_in;
  wire [4:0]gt0_txprecursor_in;
  wire gtxe2_i_n_10;
  wire gtxe2_i_n_12;
  wire gtxe2_i_n_13;
  wire gtxe2_i_n_14;
  wire gtxe2_i_n_23;
  wire gtxe2_i_n_30;
  wire gtxe2_i_n_38;
  wire gtxe2_i_n_39;
  wire gtxe2_i_n_78;
  wire gtxe2_i_n_79;
  wire gtxe2_i_n_9;
  wire gtxe2_i_n_91;
  wire gtxe2_i_n_92;
  wire gtxe2_i_n_93;
  wire gtxe2_i_n_94;
  wire gtxe2_i_n_95;
  wire [1:1]loopback_in0_out;
  wire [3:0]\mgt_codecomma_reg_reg[3] ;
  wire [0:0]mgt_enable_align;
  wire mgt_enchansync;
  wire mgt_loopback_r;
  wire mgt_powerdown;
  wire mgt_powerdown_r;
  wire [3:0]\mgt_rxcharisk_reg_reg[3] ;
  wire [31:0]\mgt_rxdata_reg_reg[31] ;
  wire [3:0]\mgt_rxdisperr_reg_reg[3] ;
  wire [3:0]\mgt_rxnotintable_reg_reg[3] ;
  wire [3:0]mgt_txcharisk;
  wire [31:0]mgt_txdata;
  wire [0:0]out;
  wire qplloutclk_out;
  wire qplloutrefclk_out;
  wire rxaui_rx_l0_n;
  wire rxaui_rx_l0_p;
  wire rxaui_tx_l0_n;
  wire rxaui_tx_l0_p;
  wire [0:0]\sync_r_reg[4] ;
  wire uclk_mgt_rx_reset;
  wire uclk_mgt_rx_reset_reg;
  wire \uclk_mgt_rxbuf_reset_reg[0] ;
  wire [2:0]\uclk_mgt_rxbufstatus_reg_reg[2] ;
  wire NLW_gtxe2_i_CPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_i_CPLLLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_CPLLREFCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gtxe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXDATAVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_TXRATEDONE_UNCONNECTED;
  wire [15:0]NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:4]NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:4]NLW_gtxe2_i_RXCHARISK_UNCONNECTED;
  wire [63:32]NLW_gtxe2_i_RXDATA_UNCONNECTED;
  wire [7:4]NLW_gtxe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXHEADER_UNCONNECTED;
  wire [7:4]NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXSTATUS_UNCONNECTED;
  wire [9:0]NLW_gtxe2_i_TSTOUT_UNCONNECTED;
  wire [1:0]NLW_gtxe2_i_TXBUFSTATUS_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_CHANNEL #(
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b0001111111),
    .ALIGN_COMMA_WORD(1),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(7),
    .CHAN_BOND_SEQ_1_1(10'b0101111100),
    .CHAN_BOND_SEQ_1_2(10'b0101111100),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0100000000),
    .CHAN_BOND_SEQ_2_2(10'b0100000000),
    .CHAN_BOND_SEQ_2_3(10'b0100000000),
    .CHAN_BOND_SEQ_2_4(10'b0100000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(2),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(32),
    .CLK_COR_MIN_LAT(27),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0100011100),
    .CLK_COR_SEQ_1_2(10'b0100011100),
    .CLK_COR_SEQ_1_3(10'b0100000000),
    .CLK_COR_SEQ_1_4(10'b0100000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0100000000),
    .CLK_COR_SEQ_2_2(10'b0100000000),
    .CLK_COR_SEQ_2_3(10'b0100000000),
    .CLK_COR_SEQ_2_4(10'b0100000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("FALSE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG(24'hBC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("TRUE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000002),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'h00018480),
    .PMA_RSV2(16'h2050),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(32'h00000000),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(4),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(72'h03000023FF10400020),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00000011110000),
    .RXLPM_LF_CFG(14'b00000011110000),
    .RXOOB_CFG(7'b0000110),
    .RXOUT_DIV(1),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'h000000),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RX_BIAS_CFG(12'b000000000100),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(7),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(3'b010),
    .RX_DATA_WIDTH(40),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(12'b000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFE_GAIN_CFG(23'h020FEA),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011110000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_KL_CFG(13'b0000011111110),
    .RX_DFE_KL_CFG2(32'h3010D90C),
    .RX_DFE_LPM_CFG(16'h0904),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_UT_CFG(17'b10001111000000000),
    .RX_DFE_VP_CFG(17'b00011111100000011),
    .RX_DFE_XYD_CFG(13'b0000000000000),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(1),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("4.0"),
    .TERM_RCAL_CFG(5'b10000),
    .TERM_RCAL_OVRD(1'b0),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("FALSE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(1),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPMARESET_TIME(5'b00001),
    .TX_CLK25_DIV(7),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(40),
    .TX_DEEMPH0(5'b00000),
    .TX_DEEMPH1(5'b00000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(1),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXUSR"),
    .UCODEER_CLR(1'b0)) 
    gtxe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD({1'b0,1'b0,1'b0,1'b0}),
        .CPLLFBCLKLOST(NLW_gtxe2_i_CPLLFBCLKLOST_UNCONNECTED),
        .CPLLLOCK(NLW_gtxe2_i_CPLLLOCK_UNCONNECTED),
        .CPLLLOCKDETCLK(1'b0),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(1'b1),
        .CPLLREFCLKLOST(NLW_gtxe2_i_CPLLREFCLKLOST_UNCONNECTED),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(1'b0),
        .DMONITOROUT(gt0_dmonitorout_out),
        .DRPADDR(gt0_drpaddr),
        .DRPCLK(dclk),
        .DRPDI(gt0_drpdi),
        .DRPDO(gt0_drpdo),
        .DRPEN(gt0_drpen),
        .DRPRDY(gt0_drprdy),
        .DRPWE(gt0_drpwe),
        .EYESCANDATAERROR(gt0_eyescandataerror_out),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(gt0_eyescanreset_in),
        .EYESCANTRIGGER(gt0_eyescantrigger_in),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(1'b0),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(uclk_mgt_rx_reset),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(SR),
        .GTXRXN(rxaui_rx_l0_n),
        .GTXRXP(rxaui_rx_l0_p),
        .GTXTXN(rxaui_tx_l0_n),
        .GTXTXP(rxaui_tx_l0_p),
        .LOOPBACK({gt0_loopback_in[2],loopback_in0_out,gt0_loopback_in[0]}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtxe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(qplloutclk_out),
        .QPLLREFCLK(qplloutrefclk_out),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(\uclk_mgt_rxbuf_reset_reg[0] ),
        .RXBUFSTATUS(\uclk_mgt_rxbufstatus_reg_reg[2] ),
        .RXBYTEISALIGNED(gtxe2_i_n_9),
        .RXBYTEREALIGN(gtxe2_i_n_10),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(gt0_rxcdrhold_in),
        .RXCDRLOCK(NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(gtxe2_i_n_12),
        .RXCHANISALIGNED(gtxe2_i_n_13),
        .RXCHANREALIGN(gtxe2_i_n_14),
        .RXCHARISCOMMA({NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED[7:4],\mgt_codecomma_reg_reg[3] }),
        .RXCHARISK({NLW_gtxe2_i_RXCHARISK_UNCONNECTED[7:4],\mgt_rxcharisk_reg_reg[3] }),
        .RXCHBONDEN(mgt_enchansync),
        .RXCHBONDI(RXCHBONDO),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO({gtxe2_i_n_91,gtxe2_i_n_92,gtxe2_i_n_93,gtxe2_i_n_94,gtxe2_i_n_95}),
        .RXCHBONDSLAVE(1'b1),
        .RXCLKCORCNT({gtxe2_i_n_78,gtxe2_i_n_79}),
        .RXCOMINITDET(NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gt0_rxcommadet_out),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gtxe2_i_RXDATA_UNCONNECTED[63:32],\mgt_rxdata_reg_reg[31] }),
        .RXDATAVALID(NLW_gtxe2_i_RXDATAVALID_UNCONNECTED),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(gt0_rxdfelpmreset_in),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDFEXYDHOLD(1'b0),
        .RXDFEXYDOVRDEN(1'b0),
        .RXDISPERR({NLW_gtxe2_i_RXDISPERR_UNCONNECTED[7:4],\mgt_rxdisperr_reg_reg[3] }),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gtxe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtxe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMEN(gt0_rxlpmen_in),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(mgt_enable_align),
        .RXMONITOROUT(gt0_rxmonitorout_out),
        .RXMONITORSEL(gt0_rxmonitorsel_in),
        .RXNOTINTABLE({NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED[7:4],\mgt_rxnotintable_reg_reg[3] }),
        .RXOOBRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(gtxe2_i_n_23),
        .RXOUTCLKFABRIC(NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(mgt_enable_align),
        .RXPCSRESET(gt0_rxpcsreset_in),
        .RXPD({mgt_powerdown,mgt_powerdown}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(out),
        .RXPOLARITY(gt0_rxpolarity_in),
        .RXPRBSCNTRESET(gt0_rxprbscntreset_in),
        .RXPRBSERR(gt0_rxprbserr_out),
        .RXPRBSSEL(gt0_rxprbssel_in),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gtxe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gtxe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE(gt0_rxrate_in),
        .RXRATEDONE(gtxe2_i_n_30),
        .RXRESETDONE(gt0_rxresetdone_out),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED),
        .RXSTATUS(NLW_gtxe2_i_RXSTATUS_UNCONNECTED[2:0]),
        .RXSYSCLKSEL({1'b1,1'b1}),
        .RXUSERRDY(\sync_r_reg[4] ),
        .RXUSRCLK(uclk_mgt_rx_reset_reg),
        .RXUSRCLK2(uclk_mgt_rx_reset_reg),
        .RXVALID(NLW_gtxe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TSTOUT(NLW_gtxe2_i_TSTOUT_UNCONNECTED[9:0]),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS(NLW_gtxe2_i_TXBUFSTATUS_UNCONNECTED[1:0]),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,mgt_txcharisk}),
        .TXCOMFINISH(NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,mgt_txdata}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL(gt0_txdiffctrl_in),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b0),
        .TXDLYEN(\TXDLYEN_reg[0] ),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(\TXDLYSRESET_reg[0] ),
        .TXDLYSRESETDONE(gt0_txdlysresetdone_out),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(mgt_powerdown_r),
        .TXGEARBOXREADY(NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(gt0_txinhibit_in),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(gt0_txoutclk_i),
        .TXOUTCLKFABRIC(gtxe2_i_n_38),
        .TXOUTCLKPCS(gtxe2_i_n_39),
        .TXOUTCLKSEL({1'b0,1'b1,1'b1}),
        .TXPCSRESET(gt0_txpcsreset_in),
        .TXPD({mgt_powerdown,mgt_powerdown}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(\TXPHALIGN_reg[0] ),
        .TXPHALIGNDONE(gt0_txphaligndone_out),
        .TXPHALIGNEN(1'b1),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(\TXPHINIT_reg[0] ),
        .TXPHINITDONE(gt0_txphinitdone_out),
        .TXPHOVRDEN(1'b0),
        .TXPISOPD(1'b0),
        .TXPMARESET(gt0_txpmareset_in),
        .TXPOLARITY(gt0_txpolarity_in),
        .TXPOSTCURSOR(gt0_txpostcursor_in),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(gt0_txprbsforceerr_in),
        .TXPRBSSEL(gt0_txprbssel_in),
        .TXPRECURSOR(gt0_txprecursor_in),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gtxe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gtxe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtxe2_i_TXRATEDONE_UNCONNECTED),
        .TXRESETDONE(D),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYSCLKSEL({1'b1,1'b1}),
        .TXUSERRDY(\sync_r_reg[4] ),
        .TXUSRCLK(uclk_mgt_rx_reset_reg),
        .TXUSRCLK2(uclk_mgt_rx_reset_reg));
  LUT2 #(
    .INIT(4'hE)) 
    gtxe2_i_i_1
       (.I0(mgt_loopback_r),
        .I1(gt0_loopback_in[1]),
        .O(loopback_in0_out));
endmodule

(* ORIG_REF_NAME = "rxaui_0_gt_wrapper_GT" *) 
module rxaui_0_rxaui_0_gt_wrapper_GT_0
   (gt1_drprdy,
    gt1_eyescandataerror_out,
    rxaui_tx_l1_n,
    rxaui_tx_l1_p,
    gt1_rxcommadet_out,
    gt1_rxprbserr_out,
    gt1_rxresetdone_out,
    gt1_txdlysresetdone_out,
    gt1_txphaligndone_out,
    gt1_txphinitdone_out,
    D,
    gt1_drpdo,
    \uclk_mgt_rxbufstatus_reg_reg[5] ,
    RXCHBONDO,
    \mgt_rxdata_reg_reg[63] ,
    gt1_rxmonitorout_out,
    gt1_dmonitorout_out,
    \mgt_codecomma_reg_reg[7] ,
    \mgt_rxcharisk_reg_reg[7] ,
    \mgt_rxdisperr_reg_reg[7] ,
    \mgt_rxnotintable_reg_reg[7] ,
    dclk,
    gt1_drpen,
    gt1_drpwe,
    gt1_eyescanreset_in,
    gt1_eyescantrigger_in,
    uclk_mgt_rx_reset,
    SR,
    rxaui_rx_l1_n,
    rxaui_rx_l1_p,
    qplloutclk_out,
    qplloutrefclk_out,
    \uclk_mgt_rxbuf_reset_reg[0] ,
    gt1_rxcdrhold_in,
    mgt_enchansync,
    gt1_rxdfelpmreset_in,
    gt1_rxlpmen_in,
    mgt_enable_align,
    gt1_rxpcsreset_in,
    out,
    gt1_rxpolarity_in,
    gt1_rxprbscntreset_in,
    \sync_r_reg[4] ,
    uclk_mgt_rx_reset_reg,
    txdlysreset_in,
    mgt_powerdown_r,
    gt1_txinhibit_in,
    gt1_txpcsreset_in,
    txphalign_in,
    txphinit_in,
    gt1_txpmareset_in,
    gt1_txpolarity_in,
    gt1_txprbsforceerr_in,
    gt1_drpdi,
    gt1_rxmonitorsel_in,
    mgt_powerdown,
    gt1_loopback_in,
    gt1_rxprbssel_in,
    gt1_rxrate_in,
    gt1_txprbssel_in,
    gt1_txdiffctrl_in,
    gt1_txpostcursor_in,
    gt1_txprecursor_in,
    mgt_txdata,
    mgt_txcharisk,
    gt1_drpaddr,
    mgt_loopback_r);
  output gt1_drprdy;
  output gt1_eyescandataerror_out;
  output rxaui_tx_l1_n;
  output rxaui_tx_l1_p;
  output gt1_rxcommadet_out;
  output gt1_rxprbserr_out;
  output gt1_rxresetdone_out;
  output gt1_txdlysresetdone_out;
  output gt1_txphaligndone_out;
  output gt1_txphinitdone_out;
  output [0:0]D;
  output [15:0]gt1_drpdo;
  output [2:0]\uclk_mgt_rxbufstatus_reg_reg[5] ;
  output [4:0]RXCHBONDO;
  output [31:0]\mgt_rxdata_reg_reg[63] ;
  output [6:0]gt1_rxmonitorout_out;
  output [7:0]gt1_dmonitorout_out;
  output [3:0]\mgt_codecomma_reg_reg[7] ;
  output [3:0]\mgt_rxcharisk_reg_reg[7] ;
  output [3:0]\mgt_rxdisperr_reg_reg[7] ;
  output [3:0]\mgt_rxnotintable_reg_reg[7] ;
  input dclk;
  input gt1_drpen;
  input gt1_drpwe;
  input gt1_eyescanreset_in;
  input gt1_eyescantrigger_in;
  input uclk_mgt_rx_reset;
  input [0:0]SR;
  input rxaui_rx_l1_n;
  input rxaui_rx_l1_p;
  input qplloutclk_out;
  input qplloutrefclk_out;
  input \uclk_mgt_rxbuf_reset_reg[0] ;
  input gt1_rxcdrhold_in;
  input mgt_enchansync;
  input gt1_rxdfelpmreset_in;
  input gt1_rxlpmen_in;
  input [0:0]mgt_enable_align;
  input gt1_rxpcsreset_in;
  input [0:0]out;
  input gt1_rxpolarity_in;
  input gt1_rxprbscntreset_in;
  input [0:0]\sync_r_reg[4] ;
  input uclk_mgt_rx_reset_reg;
  input txdlysreset_in;
  input mgt_powerdown_r;
  input gt1_txinhibit_in;
  input gt1_txpcsreset_in;
  input txphalign_in;
  input txphinit_in;
  input gt1_txpmareset_in;
  input gt1_txpolarity_in;
  input gt1_txprbsforceerr_in;
  input [15:0]gt1_drpdi;
  input [1:0]gt1_rxmonitorsel_in;
  input mgt_powerdown;
  input [2:0]gt1_loopback_in;
  input [2:0]gt1_rxprbssel_in;
  input [2:0]gt1_rxrate_in;
  input [2:0]gt1_txprbssel_in;
  input [3:0]gt1_txdiffctrl_in;
  input [4:0]gt1_txpostcursor_in;
  input [4:0]gt1_txprecursor_in;
  input [31:0]mgt_txdata;
  input [3:0]mgt_txcharisk;
  input [8:0]gt1_drpaddr;
  input mgt_loopback_r;

  wire [0:0]D;
  wire [4:0]RXCHBONDO;
  wire [0:0]SR;
  wire dclk;
  wire [7:0]gt1_dmonitorout_out;
  wire [8:0]gt1_drpaddr;
  wire [15:0]gt1_drpdi;
  wire [15:0]gt1_drpdo;
  wire gt1_drpen;
  wire gt1_drprdy;
  wire gt1_drpwe;
  wire gt1_eyescandataerror_out;
  wire gt1_eyescanreset_in;
  wire gt1_eyescantrigger_in;
  wire [2:0]gt1_loopback_in;
  wire gt1_rxcdrhold_in;
  wire gt1_rxcommadet_out;
  wire gt1_rxdfelpmreset_in;
  wire gt1_rxlpmen_in;
  wire [6:0]gt1_rxmonitorout_out;
  wire [1:0]gt1_rxmonitorsel_in;
  wire gt1_rxpcsreset_in;
  wire gt1_rxpolarity_in;
  wire gt1_rxprbscntreset_in;
  wire gt1_rxprbserr_out;
  wire [2:0]gt1_rxprbssel_in;
  wire [2:0]gt1_rxrate_in;
  wire gt1_rxresetdone_out;
  wire [3:0]gt1_txdiffctrl_in;
  wire gt1_txdlysresetdone_out;
  wire gt1_txinhibit_in;
  wire gt1_txpcsreset_in;
  wire gt1_txphaligndone_out;
  wire gt1_txphinitdone_out;
  wire gt1_txpmareset_in;
  wire gt1_txpolarity_in;
  wire [4:0]gt1_txpostcursor_in;
  wire gt1_txprbsforceerr_in;
  wire [2:0]gt1_txprbssel_in;
  wire [4:0]gt1_txprecursor_in;
  wire gtxe2_i_n_10;
  wire gtxe2_i_n_12;
  wire gtxe2_i_n_13;
  wire gtxe2_i_n_14;
  wire gtxe2_i_n_23;
  wire gtxe2_i_n_30;
  wire gtxe2_i_n_37;
  wire gtxe2_i_n_38;
  wire gtxe2_i_n_39;
  wire gtxe2_i_n_78;
  wire gtxe2_i_n_79;
  wire gtxe2_i_n_9;
  wire [1:1]loopback_in;
  wire [3:0]\mgt_codecomma_reg_reg[7] ;
  wire [0:0]mgt_enable_align;
  wire mgt_enchansync;
  wire mgt_loopback_r;
  wire mgt_powerdown;
  wire mgt_powerdown_r;
  wire [3:0]\mgt_rxcharisk_reg_reg[7] ;
  wire [31:0]\mgt_rxdata_reg_reg[63] ;
  wire [3:0]\mgt_rxdisperr_reg_reg[7] ;
  wire [3:0]\mgt_rxnotintable_reg_reg[7] ;
  wire [3:0]mgt_txcharisk;
  wire [31:0]mgt_txdata;
  wire [0:0]out;
  wire qplloutclk_out;
  wire qplloutrefclk_out;
  wire rxaui_rx_l1_n;
  wire rxaui_rx_l1_p;
  wire rxaui_tx_l1_n;
  wire rxaui_tx_l1_p;
  wire [0:0]\sync_r_reg[4] ;
  wire txdlysreset_in;
  wire txphalign_in;
  wire txphinit_in;
  wire uclk_mgt_rx_reset;
  wire uclk_mgt_rx_reset_reg;
  wire \uclk_mgt_rxbuf_reset_reg[0] ;
  wire [2:0]\uclk_mgt_rxbufstatus_reg_reg[5] ;
  wire NLW_gtxe2_i_CPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_i_CPLLLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_CPLLREFCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gtxe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXDATAVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_TXRATEDONE_UNCONNECTED;
  wire [15:0]NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:4]NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:4]NLW_gtxe2_i_RXCHARISK_UNCONNECTED;
  wire [63:32]NLW_gtxe2_i_RXDATA_UNCONNECTED;
  wire [7:4]NLW_gtxe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXHEADER_UNCONNECTED;
  wire [7:4]NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXSTATUS_UNCONNECTED;
  wire [9:0]NLW_gtxe2_i_TSTOUT_UNCONNECTED;
  wire [1:0]NLW_gtxe2_i_TXBUFSTATUS_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_CHANNEL #(
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b0001111111),
    .ALIGN_COMMA_WORD(1),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(7),
    .CHAN_BOND_SEQ_1_1(10'b0101111100),
    .CHAN_BOND_SEQ_1_2(10'b0101111100),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0100000000),
    .CHAN_BOND_SEQ_2_2(10'b0100000000),
    .CHAN_BOND_SEQ_2_3(10'b0100000000),
    .CHAN_BOND_SEQ_2_4(10'b0100000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(2),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(32),
    .CLK_COR_MIN_LAT(27),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0100011100),
    .CLK_COR_SEQ_1_2(10'b0100011100),
    .CLK_COR_SEQ_1_3(10'b0100000000),
    .CLK_COR_SEQ_1_4(10'b0100000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0100000000),
    .CLK_COR_SEQ_2_2(10'b0100000000),
    .CLK_COR_SEQ_2_3(10'b0100000000),
    .CLK_COR_SEQ_2_4(10'b0100000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("FALSE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG(24'hBC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("TRUE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000002),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'h00018480),
    .PMA_RSV2(16'h2050),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(32'h00000000),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(4),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(72'h03000023FF10400020),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00000011110000),
    .RXLPM_LF_CFG(14'b00000011110000),
    .RXOOB_CFG(7'b0000110),
    .RXOUT_DIV(1),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'h000000),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RX_BIAS_CFG(12'b000000000100),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(7),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(3'b010),
    .RX_DATA_WIDTH(40),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(12'b000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFE_GAIN_CFG(23'h020FEA),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011110000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_KL_CFG(13'b0000011111110),
    .RX_DFE_KL_CFG2(32'h3010D90C),
    .RX_DFE_LPM_CFG(16'h0904),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_UT_CFG(17'b10001111000000000),
    .RX_DFE_VP_CFG(17'b00011111100000011),
    .RX_DFE_XYD_CFG(13'b0000000000000),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(1),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("4.0"),
    .TERM_RCAL_CFG(5'b10000),
    .TERM_RCAL_OVRD(1'b0),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("FALSE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(1),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPMARESET_TIME(5'b00001),
    .TX_CLK25_DIV(7),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(40),
    .TX_DEEMPH0(5'b00000),
    .TX_DEEMPH1(5'b00000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(1),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXUSR"),
    .UCODEER_CLR(1'b0)) 
    gtxe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD({1'b0,1'b0,1'b0,1'b0}),
        .CPLLFBCLKLOST(NLW_gtxe2_i_CPLLFBCLKLOST_UNCONNECTED),
        .CPLLLOCK(NLW_gtxe2_i_CPLLLOCK_UNCONNECTED),
        .CPLLLOCKDETCLK(1'b0),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(1'b1),
        .CPLLREFCLKLOST(NLW_gtxe2_i_CPLLREFCLKLOST_UNCONNECTED),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(1'b0),
        .DMONITOROUT(gt1_dmonitorout_out),
        .DRPADDR(gt1_drpaddr),
        .DRPCLK(dclk),
        .DRPDI(gt1_drpdi),
        .DRPDO(gt1_drpdo),
        .DRPEN(gt1_drpen),
        .DRPRDY(gt1_drprdy),
        .DRPWE(gt1_drpwe),
        .EYESCANDATAERROR(gt1_eyescandataerror_out),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(gt1_eyescanreset_in),
        .EYESCANTRIGGER(gt1_eyescantrigger_in),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(1'b0),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(uclk_mgt_rx_reset),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(SR),
        .GTXRXN(rxaui_rx_l1_n),
        .GTXRXP(rxaui_rx_l1_p),
        .GTXTXN(rxaui_tx_l1_n),
        .GTXTXP(rxaui_tx_l1_p),
        .LOOPBACK({gt1_loopback_in[2],loopback_in,gt1_loopback_in[0]}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtxe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(qplloutclk_out),
        .QPLLREFCLK(qplloutrefclk_out),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(\uclk_mgt_rxbuf_reset_reg[0] ),
        .RXBUFSTATUS(\uclk_mgt_rxbufstatus_reg_reg[5] ),
        .RXBYTEISALIGNED(gtxe2_i_n_9),
        .RXBYTEREALIGN(gtxe2_i_n_10),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(gt1_rxcdrhold_in),
        .RXCDRLOCK(NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(gtxe2_i_n_12),
        .RXCHANISALIGNED(gtxe2_i_n_13),
        .RXCHANREALIGN(gtxe2_i_n_14),
        .RXCHARISCOMMA({NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED[7:4],\mgt_codecomma_reg_reg[7] }),
        .RXCHARISK({NLW_gtxe2_i_RXCHARISK_UNCONNECTED[7:4],\mgt_rxcharisk_reg_reg[7] }),
        .RXCHBONDEN(mgt_enchansync),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b1}),
        .RXCHBONDMASTER(1'b1),
        .RXCHBONDO(RXCHBONDO),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT({gtxe2_i_n_78,gtxe2_i_n_79}),
        .RXCOMINITDET(NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gt1_rxcommadet_out),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gtxe2_i_RXDATA_UNCONNECTED[63:32],\mgt_rxdata_reg_reg[63] }),
        .RXDATAVALID(NLW_gtxe2_i_RXDATAVALID_UNCONNECTED),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(gt1_rxdfelpmreset_in),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDFEXYDHOLD(1'b0),
        .RXDFEXYDOVRDEN(1'b0),
        .RXDISPERR({NLW_gtxe2_i_RXDISPERR_UNCONNECTED[7:4],\mgt_rxdisperr_reg_reg[7] }),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gtxe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtxe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMEN(gt1_rxlpmen_in),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(mgt_enable_align),
        .RXMONITOROUT(gt1_rxmonitorout_out),
        .RXMONITORSEL(gt1_rxmonitorsel_in),
        .RXNOTINTABLE({NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED[7:4],\mgt_rxnotintable_reg_reg[7] }),
        .RXOOBRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(gtxe2_i_n_23),
        .RXOUTCLKFABRIC(NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(mgt_enable_align),
        .RXPCSRESET(gt1_rxpcsreset_in),
        .RXPD({mgt_powerdown,mgt_powerdown}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(out),
        .RXPOLARITY(gt1_rxpolarity_in),
        .RXPRBSCNTRESET(gt1_rxprbscntreset_in),
        .RXPRBSERR(gt1_rxprbserr_out),
        .RXPRBSSEL(gt1_rxprbssel_in),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gtxe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gtxe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE(gt1_rxrate_in),
        .RXRATEDONE(gtxe2_i_n_30),
        .RXRESETDONE(gt1_rxresetdone_out),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED),
        .RXSTATUS(NLW_gtxe2_i_RXSTATUS_UNCONNECTED[2:0]),
        .RXSYSCLKSEL({1'b1,1'b1}),
        .RXUSERRDY(\sync_r_reg[4] ),
        .RXUSRCLK(uclk_mgt_rx_reset_reg),
        .RXUSRCLK2(uclk_mgt_rx_reset_reg),
        .RXVALID(NLW_gtxe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TSTOUT(NLW_gtxe2_i_TSTOUT_UNCONNECTED[9:0]),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS(NLW_gtxe2_i_TXBUFSTATUS_UNCONNECTED[1:0]),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,mgt_txcharisk}),
        .TXCOMFINISH(NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,mgt_txdata}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL(gt1_txdiffctrl_in),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b0),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(txdlysreset_in),
        .TXDLYSRESETDONE(gt1_txdlysresetdone_out),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(mgt_powerdown_r),
        .TXGEARBOXREADY(NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(gt1_txinhibit_in),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(gtxe2_i_n_37),
        .TXOUTCLKFABRIC(gtxe2_i_n_38),
        .TXOUTCLKPCS(gtxe2_i_n_39),
        .TXOUTCLKSEL({1'b0,1'b1,1'b1}),
        .TXPCSRESET(gt1_txpcsreset_in),
        .TXPD({mgt_powerdown,mgt_powerdown}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(txphalign_in),
        .TXPHALIGNDONE(gt1_txphaligndone_out),
        .TXPHALIGNEN(1'b1),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(txphinit_in),
        .TXPHINITDONE(gt1_txphinitdone_out),
        .TXPHOVRDEN(1'b0),
        .TXPISOPD(1'b0),
        .TXPMARESET(gt1_txpmareset_in),
        .TXPOLARITY(gt1_txpolarity_in),
        .TXPOSTCURSOR(gt1_txpostcursor_in),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(gt1_txprbsforceerr_in),
        .TXPRBSSEL(gt1_txprbssel_in),
        .TXPRECURSOR(gt1_txprecursor_in),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gtxe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gtxe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtxe2_i_TXRATEDONE_UNCONNECTED),
        .TXRESETDONE(D),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYSCLKSEL({1'b1,1'b1}),
        .TXUSERRDY(\sync_r_reg[4] ),
        .TXUSRCLK(uclk_mgt_rx_reset_reg),
        .TXUSRCLK2(uclk_mgt_rx_reset_reg));
  LUT2 #(
    .INIT(4'hE)) 
    gtxe2_i_i_1__0
       (.I0(mgt_loopback_r),
        .I1(gt1_loopback_in[1]),
        .O(loopback_in));
endmodule

(* ORIG_REF_NAME = "rxaui_0_gt_wrapper_tx_sync_manual" *) 
module rxaui_0_rxaui_0_gt_wrapper_tx_sync_manual
   (debug,
    \TXDLYEN_reg[0]_0 ,
    mgt_tx_reset,
    txdlysreset_in,
    \TXDLYSRESET_reg[0]_0 ,
    \TXPHINIT_reg[0]_0 ,
    txphinit_in,
    \TXPHALIGN_reg[0]_0 ,
    txphalign_in,
    uclk_mgt_rx_reset_reg,
    SR,
    uclk_txsync_start_phase_align_reg,
    gt0_txphaligndone_out,
    gt0_txdlysresetdone_out,
    gt1_txphaligndone_out,
    gt1_txdlysresetdone_out,
    gt0_txphinitdone_out,
    gt1_txphinitdone_out);
  output [0:0]debug;
  output \TXDLYEN_reg[0]_0 ;
  output [0:0]mgt_tx_reset;
  output txdlysreset_in;
  output \TXDLYSRESET_reg[0]_0 ;
  output \TXPHINIT_reg[0]_0 ;
  output txphinit_in;
  output \TXPHALIGN_reg[0]_0 ;
  output txphalign_in;
  input uclk_mgt_rx_reset_reg;
  input [0:0]SR;
  input uclk_txsync_start_phase_align_reg;
  input gt0_txphaligndone_out;
  input gt0_txdlysresetdone_out;
  input gt1_txphaligndone_out;
  input gt1_txdlysresetdone_out;
  input gt0_txphinitdone_out;
  input gt1_txphinitdone_out;

  wire \FSM_onehot_tx_phalign_manual_state[8]_i_2_n_0 ;
  wire \FSM_onehot_tx_phalign_manual_state[8]_i_4_n_0 ;
  wire \FSM_onehot_tx_phalign_manual_state[8]_i_5_n_0 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_tx_phalign_manual_state_reg_n_0_[1] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_tx_phalign_manual_state_reg_n_0_[2] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_tx_phalign_manual_state_reg_n_0_[3] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_tx_phalign_manual_state_reg_n_0_[4] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_tx_phalign_manual_state_reg_n_0_[5] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_tx_phalign_manual_state_reg_n_0_[6] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_tx_phalign_manual_state_reg_n_0_[7] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_tx_phalign_manual_state_reg_n_0_[8] ;
  wire PHASE_ALIGNMENT_DONE_i_1_n_0;
  wire [0:0]SR;
  wire \TXDLYEN_reg[0]_0 ;
  wire \TXDLYSRESET[0]_i_1_n_0 ;
  wire \TXDLYSRESET[1]_i_1_n_0 ;
  wire \TXDLYSRESET_reg[0]_0 ;
  wire \TXPHALIGN[0]_i_1_n_0 ;
  wire \TXPHALIGN[1]_i_1_n_0 ;
  wire \TXPHALIGN_reg[0]_0 ;
  wire \TXPHINIT[0]_i_1_n_0 ;
  wire \TXPHINIT[1]_i_1_n_0 ;
  wire \TXPHINIT_reg[0]_0 ;
  wire \cdc[0].sync_TXPHALIGNDONE_n_0 ;
  wire \cdc[0].sync_TXPHALIGNDONE_n_2 ;
  wire \cdc[0].sync_TXPHINITDONE_n_1 ;
  wire data_out0_out;
  wire [0:0]debug;
  wire gt0_txdlysresetdone_out;
  wire gt0_txphaligndone_out;
  wire gt0_txphinitdone_out;
  wire gt1_txdlysresetdone_out;
  wire gt1_txphaligndone_out;
  wire gt1_txphinitdone_out;
  wire [0:0]mgt_tx_reset;
  wire txdlysreset_in;
  wire [1:0]txdlysresetdone_store;
  wire \txdlysresetdone_store[0]_i_1_n_0 ;
  wire \txdlysresetdone_store[1]_i_1_n_0 ;
  wire txdlysresetdone_sync_1;
  wire txdone_clear_i_1_n_0;
  wire txdone_clear_reg_n_0;
  wire txphalign_in;
  wire [1:0]txphaligndone_prev;
  wire [1:0]txphaligndone_store;
  wire \txphaligndone_store[0]_i_1_n_0 ;
  wire \txphaligndone_store[1]_i_1_n_0 ;
  wire txphaligndone_sync_0;
  wire txphaligndone_sync_1;
  wire txphinit_in;
  (* RTL_KEEP = "yes" *) wire txphinitdone_clear_slave;
  wire [1:0]txphinitdone_prev;
  wire [1:0]txphinitdone_store_edge;
  wire \txphinitdone_store_edge[0]_i_1_n_0 ;
  wire \txphinitdone_store_edge[1]_i_1_n_0 ;
  wire txphinitdone_sync_0;
  wire txphinitdone_sync_1;
  wire uclk_mgt_rx_reset_reg;
  wire uclk_txsync_start_phase_align_reg;

  LUT5 #(
    .INIT(32'hFF808080)) 
    \FSM_onehot_tx_phalign_manual_state[8]_i_2 
       (.I0(txdlysresetdone_store[0]),
        .I1(txdlysresetdone_store[1]),
        .I2(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[1] ),
        .I3(uclk_txsync_start_phase_align_reg),
        .I4(txphinitdone_clear_slave),
        .O(\FSM_onehot_tx_phalign_manual_state[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF80808080808080)) 
    \FSM_onehot_tx_phalign_manual_state[8]_i_4 
       (.I0(txphaligndone_store[0]),
        .I1(txphaligndone_store[1]),
        .I2(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[6] ),
        .I3(txphinitdone_store_edge[0]),
        .I4(txphinitdone_store_edge[1]),
        .I5(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[5] ),
        .O(\FSM_onehot_tx_phalign_manual_state[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_onehot_tx_phalign_manual_state[8]_i_5 
       (.I0(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[3] ),
        .I1(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[7] ),
        .O(\FSM_onehot_tx_phalign_manual_state[8]_i_5_n_0 ));
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_tx_phalign_manual_state_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .D(1'b0),
        .Q(txphinitdone_clear_slave),
        .S(SR));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_tx_phalign_manual_state_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .D(txphinitdone_clear_slave),
        .Q(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[1] ),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_tx_phalign_manual_state_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .D(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[1] ),
        .Q(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[2] ),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_tx_phalign_manual_state_reg[3] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .D(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[2] ),
        .Q(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[3] ),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_tx_phalign_manual_state_reg[4] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .D(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[3] ),
        .Q(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[4] ),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_tx_phalign_manual_state_reg[5] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .D(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[4] ),
        .Q(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[5] ),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_tx_phalign_manual_state_reg[6] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .D(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[5] ),
        .Q(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[6] ),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_tx_phalign_manual_state_reg[7] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .D(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[6] ),
        .Q(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[7] ),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_tx_phalign_manual_state_reg[8] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .D(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[7] ),
        .Q(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[8] ),
        .R(SR));
  LUT3 #(
    .INIT(8'hDC)) 
    PHASE_ALIGNMENT_DONE_i_1
       (.I0(txphinitdone_clear_slave),
        .I1(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[8] ),
        .I2(debug),
        .O(PHASE_ALIGNMENT_DONE_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    PHASE_ALIGNMENT_DONE_reg
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(PHASE_ALIGNMENT_DONE_i_1_n_0),
        .Q(debug),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \TXDLYEN_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\cdc[0].sync_TXPHALIGNDONE_n_0 ),
        .Q(\TXDLYEN_reg[0]_0 ),
        .R(SR));
  LUT5 #(
    .INIT(32'h8FFF8888)) 
    \TXDLYSRESET[0]_i_1 
       (.I0(uclk_txsync_start_phase_align_reg),
        .I1(txphinitdone_clear_slave),
        .I2(txdlysresetdone_store[0]),
        .I3(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[1] ),
        .I4(\TXDLYSRESET_reg[0]_0 ),
        .O(\TXDLYSRESET[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h8FFF8888)) 
    \TXDLYSRESET[1]_i_1 
       (.I0(uclk_txsync_start_phase_align_reg),
        .I1(txphinitdone_clear_slave),
        .I2(txdlysresetdone_store[1]),
        .I3(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[1] ),
        .I4(txdlysreset_in),
        .O(\TXDLYSRESET[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \TXDLYSRESET_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\TXDLYSRESET[0]_i_1_n_0 ),
        .Q(\TXDLYSRESET_reg[0]_0 ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \TXDLYSRESET_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\TXDLYSRESET[1]_i_1_n_0 ),
        .Q(txdlysreset_in),
        .R(SR));
  LUT5 #(
    .INIT(32'hB0BFB0B0)) 
    \TXPHALIGN[0]_i_1 
       (.I0(txphaligndone_prev[0]),
        .I1(txphaligndone_sync_0),
        .I2(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[3] ),
        .I3(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[6] ),
        .I4(\TXPHALIGN_reg[0]_0 ),
        .O(\TXPHALIGN[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \TXPHALIGN[1]_i_1 
       (.I0(txphaligndone_store[1]),
        .I1(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[6] ),
        .I2(txphalign_in),
        .O(\TXPHALIGN[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \TXPHALIGN_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\TXPHALIGN[0]_i_1_n_0 ),
        .Q(\TXPHALIGN_reg[0]_0 ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \TXPHALIGN_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\TXPHALIGN[1]_i_1_n_0 ),
        .Q(txphalign_in),
        .R(SR));
  LUT5 #(
    .INIT(32'hB0BFB0B0)) 
    \TXPHINIT[0]_i_1 
       (.I0(txphinitdone_prev[0]),
        .I1(txphinitdone_sync_0),
        .I2(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[2] ),
        .I3(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[5] ),
        .I4(\TXPHINIT_reg[0]_0 ),
        .O(\TXPHINIT[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \TXPHINIT[1]_i_1 
       (.I0(txphinitdone_store_edge[1]),
        .I1(\FSM_onehot_tx_phalign_manual_state_reg_n_0_[5] ),
        .I2(txphinit_in),
        .O(\TXPHINIT[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \TXPHINIT_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\TXPHINIT[0]_i_1_n_0 ),
        .Q(\TXPHINIT_reg[0]_0 ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \TXPHINIT_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\TXPHINIT[1]_i_1_n_0 ),
        .Q(txphinit_in),
        .R(SR));
  rxaui_0_rxaui_0_tx_sync_sync_block \cdc[0].sync_TXDLYSRESETDONE 
       (.data_out(data_out0_out),
        .gt0_txdlysresetdone_out(gt0_txdlysresetdone_out),
        .uclk_mgt_rx_reset_reg(uclk_mgt_rx_reset_reg));
  rxaui_0_rxaui_0_tx_sync_sync_block_6 \cdc[0].sync_TXPHALIGNDONE 
       (.\FSM_onehot_tx_phalign_manual_state_reg[0] (\cdc[0].sync_TXPHALIGNDONE_n_2 ),
        .Q(txphaligndone_prev[0]),
        .\TXDLYEN_reg[0] (\cdc[0].sync_TXPHALIGNDONE_n_0 ),
        .\TXDLYEN_reg[0]_0 (\TXDLYEN_reg[0]_0 ),
        .data_out(txphaligndone_sync_0),
        .gt0_txphaligndone_out(gt0_txphaligndone_out),
        .out({\FSM_onehot_tx_phalign_manual_state_reg_n_0_[8] ,\FSM_onehot_tx_phalign_manual_state_reg_n_0_[7] ,\FSM_onehot_tx_phalign_manual_state_reg_n_0_[4] }),
        .uclk_mgt_rx_reset_reg(uclk_mgt_rx_reset_reg));
  rxaui_0_rxaui_0_tx_sync_sync_pulse \cdc[0].sync_TXPHINITDONE 
       (.D({\FSM_onehot_tx_phalign_manual_state_reg_n_0_[4] ,\FSM_onehot_tx_phalign_manual_state_reg_n_0_[2] }),
        .E(\cdc[0].sync_TXPHINITDONE_n_1 ),
        .\FSM_onehot_tx_phalign_manual_state_reg[3] (\FSM_onehot_tx_phalign_manual_state[8]_i_5_n_0 ),
        .Q(txphinitdone_prev[0]),
        .data_sync_reg6(\cdc[0].sync_TXPHALIGNDONE_n_2 ),
        .gt0_txphinitdone_out(gt0_txphinitdone_out),
        .\txdlysresetdone_store_reg[0] (\FSM_onehot_tx_phalign_manual_state[8]_i_2_n_0 ),
        .\txphaligndone_store_reg[0] (\FSM_onehot_tx_phalign_manual_state[8]_i_4_n_0 ),
        .txphinitdone_sync_0(txphinitdone_sync_0),
        .uclk_mgt_rx_reset_reg(uclk_mgt_rx_reset_reg));
  rxaui_0_rxaui_0_tx_sync_sync_block_7 \cdc[1].sync_TXDLYSRESETDONE 
       (.data_out(txdlysresetdone_sync_1),
        .gt1_txdlysresetdone_out(gt1_txdlysresetdone_out),
        .uclk_mgt_rx_reset_reg(uclk_mgt_rx_reset_reg));
  rxaui_0_rxaui_0_tx_sync_sync_block_8 \cdc[1].sync_TXPHALIGNDONE 
       (.data_out(txphaligndone_sync_1),
        .gt1_txphaligndone_out(gt1_txphaligndone_out),
        .uclk_mgt_rx_reset_reg(uclk_mgt_rx_reset_reg));
  rxaui_0_rxaui_0_tx_sync_sync_pulse_9 \cdc[1].sync_TXPHINITDONE 
       (.D(txphinitdone_sync_1),
        .gt1_txphinitdone_out(gt1_txphinitdone_out),
        .uclk_mgt_rx_reset_reg(uclk_mgt_rx_reset_reg));
  LUT1 #(
    .INIT(2'h1)) 
    rxaui_0_core_i_9
       (.I0(debug),
        .O(mgt_tx_reset));
  LUT2 #(
    .INIT(4'hE)) 
    \txdlysresetdone_store[0]_i_1 
       (.I0(data_out0_out),
        .I1(txdlysresetdone_store[0]),
        .O(\txdlysresetdone_store[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \txdlysresetdone_store[1]_i_1 
       (.I0(txdlysresetdone_sync_1),
        .I1(txdlysresetdone_store[1]),
        .O(\txdlysresetdone_store[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txdlysresetdone_store_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\txdlysresetdone_store[0]_i_1_n_0 ),
        .Q(txdlysresetdone_store[0]),
        .R(txdone_clear_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \txdlysresetdone_store_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\txdlysresetdone_store[1]_i_1_n_0 ),
        .Q(txdlysresetdone_store[1]),
        .R(txdone_clear_reg_n_0));
  LUT3 #(
    .INIT(8'h74)) 
    txdone_clear_i_1
       (.I0(uclk_txsync_start_phase_align_reg),
        .I1(txphinitdone_clear_slave),
        .I2(txdone_clear_reg_n_0),
        .O(txdone_clear_i_1_n_0));
  FDSE #(
    .INIT(1'b0)) 
    txdone_clear_reg
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(txdone_clear_i_1_n_0),
        .Q(txdone_clear_reg_n_0),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    \txphaligndone_prev_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(txphaligndone_sync_0),
        .Q(txphaligndone_prev[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txphaligndone_prev_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(txphaligndone_sync_1),
        .Q(txphaligndone_prev[1]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hF4)) 
    \txphaligndone_store[0]_i_1 
       (.I0(txphaligndone_prev[0]),
        .I1(txphaligndone_sync_0),
        .I2(txphaligndone_store[0]),
        .O(\txphaligndone_store[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hF4)) 
    \txphaligndone_store[1]_i_1 
       (.I0(txphaligndone_prev[1]),
        .I1(txphaligndone_sync_1),
        .I2(txphaligndone_store[1]),
        .O(\txphaligndone_store[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txphaligndone_store_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\txphaligndone_store[0]_i_1_n_0 ),
        .Q(txphaligndone_store[0]),
        .R(txdone_clear_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \txphaligndone_store_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\txphaligndone_store[1]_i_1_n_0 ),
        .Q(txphaligndone_store[1]),
        .R(txdone_clear_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \txphinitdone_prev_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(txphinitdone_sync_0),
        .Q(txphinitdone_prev[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txphinitdone_prev_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(txphinitdone_sync_1),
        .Q(txphinitdone_prev[1]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFFAE)) 
    \txphinitdone_store_edge[0]_i_1 
       (.I0(txphinitdone_store_edge[0]),
        .I1(txphinitdone_sync_0),
        .I2(txphinitdone_prev[0]),
        .I3(txdone_clear_reg_n_0),
        .O(\txphinitdone_store_edge[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hF4)) 
    \txphinitdone_store_edge[1]_i_1 
       (.I0(txphinitdone_prev[1]),
        .I1(txphinitdone_sync_1),
        .I2(txphinitdone_store_edge[1]),
        .O(\txphinitdone_store_edge[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txphinitdone_store_edge_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\txphinitdone_store_edge[0]_i_1_n_0 ),
        .Q(txphinitdone_store_edge[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txphinitdone_store_edge_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\txphinitdone_store_edge[1]_i_1_n_0 ),
        .Q(txphinitdone_store_edge[1]),
        .R(txdone_clear_reg_n_0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_reset_counter" *) 
module rxaui_0_rxaui_0_reset_counter
   (Q,
    dclk);
  output [0:0]Q;
  input dclk;

  wire [0:0]Q;
  wire \count[7]_i_1_n_0 ;
  wire \count[7]_i_3_n_0 ;
  wire \count_reg_n_0_[0] ;
  wire \count_reg_n_0_[1] ;
  wire \count_reg_n_0_[2] ;
  wire \count_reg_n_0_[3] ;
  wire \count_reg_n_0_[4] ;
  wire \count_reg_n_0_[5] ;
  wire \count_reg_n_0_[6] ;
  wire dclk;
  wire [7:0]plusOp;

  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count[1]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .I1(\count_reg_n_0_[1] ),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count[2]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .I1(\count_reg_n_0_[1] ),
        .I2(\count_reg_n_0_[2] ),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count[3]_i_1 
       (.I0(\count_reg_n_0_[1] ),
        .I1(\count_reg_n_0_[0] ),
        .I2(\count_reg_n_0_[2] ),
        .I3(\count_reg_n_0_[3] ),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count[4]_i_1 
       (.I0(\count_reg_n_0_[2] ),
        .I1(\count_reg_n_0_[0] ),
        .I2(\count_reg_n_0_[1] ),
        .I3(\count_reg_n_0_[3] ),
        .I4(\count_reg_n_0_[4] ),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count[5]_i_1 
       (.I0(\count_reg_n_0_[3] ),
        .I1(\count_reg_n_0_[1] ),
        .I2(\count_reg_n_0_[0] ),
        .I3(\count_reg_n_0_[2] ),
        .I4(\count_reg_n_0_[4] ),
        .I5(\count_reg_n_0_[5] ),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count[6]_i_1 
       (.I0(\count[7]_i_3_n_0 ),
        .I1(\count_reg_n_0_[6] ),
        .O(plusOp[6]));
  LUT1 #(
    .INIT(2'h1)) 
    \count[7]_i_1 
       (.I0(Q),
        .O(\count[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count[7]_i_2 
       (.I0(\count[7]_i_3_n_0 ),
        .I1(\count_reg_n_0_[6] ),
        .I2(Q),
        .O(plusOp[7]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \count[7]_i_3 
       (.I0(\count_reg_n_0_[5] ),
        .I1(\count_reg_n_0_[3] ),
        .I2(\count_reg_n_0_[1] ),
        .I3(\count_reg_n_0_[0] ),
        .I4(\count_reg_n_0_[2] ),
        .I5(\count_reg_n_0_[4] ),
        .O(\count[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(dclk),
        .CE(\count[7]_i_1_n_0 ),
        .D(plusOp[0]),
        .Q(\count_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(dclk),
        .CE(\count[7]_i_1_n_0 ),
        .D(plusOp[1]),
        .Q(\count_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(dclk),
        .CE(\count[7]_i_1_n_0 ),
        .D(plusOp[2]),
        .Q(\count_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(dclk),
        .CE(\count[7]_i_1_n_0 ),
        .D(plusOp[3]),
        .Q(\count_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(dclk),
        .CE(\count[7]_i_1_n_0 ),
        .D(plusOp[4]),
        .Q(\count_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(dclk),
        .CE(\count[7]_i_1_n_0 ),
        .D(plusOp[5]),
        .Q(\count_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(dclk),
        .CE(\count[7]_i_1_n_0 ),
        .D(plusOp[6]),
        .Q(\count_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(dclk),
        .CE(\count[7]_i_1_n_0 ),
        .D(plusOp[7]),
        .Q(Q),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_support" *) 
module rxaui_0_rxaui_0_support
   (reset,
    dclk,
    clk156_out,
    clk156_lock,
    qplloutclk_out,
    qplllock_out,
    qplloutrefclk_out,
    refclk_out,
    refclk_p,
    refclk_n,
    xgmii_txd,
    xgmii_txc,
    xgmii_rxd,
    xgmii_rxc,
    rxaui_tx_l0_p,
    rxaui_tx_l0_n,
    rxaui_tx_l1_p,
    rxaui_tx_l1_n,
    rxaui_rx_l0_p,
    rxaui_rx_l0_n,
    rxaui_rx_l1_p,
    rxaui_rx_l1_n,
    signal_detect,
    debug,
    gt0_drpaddr,
    gt0_drpen,
    gt0_drpdi,
    gt0_drpdo,
    gt0_drprdy,
    gt0_drpwe,
    gt0_txpmareset_in,
    gt0_txpcsreset_in,
    gt0_txresetdone_out,
    gt0_rxpmareset_in,
    gt0_rxpcsreset_in,
    gt0_rxresetdone_out,
    gt0_rxbufstatus_out,
    gt0_txphaligndone_out,
    gt0_txphinitdone_out,
    gt0_txdlysresetdone_out,
    gt_qplllock_out,
    gt0_eyescantrigger_in,
    gt0_eyescanreset_in,
    gt0_eyescandataerror_out,
    gt0_rxrate_in,
    gt0_loopback_in,
    gt0_rxpolarity_in,
    gt0_txpolarity_in,
    gt0_rxlpmen_in,
    gt0_rxdfelpmreset_in,
    gt0_rxmonitorsel_in,
    gt0_rxmonitorout_out,
    gt0_txpostcursor_in,
    gt0_txprecursor_in,
    gt0_txdiffctrl_in,
    gt0_txinhibit_in,
    gt0_rxprbscntreset_in,
    gt0_rxprbserr_out,
    gt0_rxprbssel_in,
    gt0_txprbssel_in,
    gt0_txprbsforceerr_in,
    gt0_rxcdrhold_in,
    gt0_dmonitorout_out,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    gt0_rxcommadet_out,
    gt1_drpaddr,
    gt1_drpen,
    gt1_drpdi,
    gt1_drpdo,
    gt1_drprdy,
    gt1_drpwe,
    gt1_txpmareset_in,
    gt1_txpcsreset_in,
    gt1_txresetdone_out,
    gt1_rxpmareset_in,
    gt1_rxpcsreset_in,
    gt1_rxresetdone_out,
    gt1_rxbufstatus_out,
    gt1_txphaligndone_out,
    gt1_txphinitdone_out,
    gt1_txdlysresetdone_out,
    gt1_eyescantrigger_in,
    gt1_eyescanreset_in,
    gt1_eyescandataerror_out,
    gt1_rxrate_in,
    gt1_loopback_in,
    gt1_rxpolarity_in,
    gt1_txpolarity_in,
    gt1_rxlpmen_in,
    gt1_rxdfelpmreset_in,
    gt1_rxmonitorsel_in,
    gt1_rxmonitorout_out,
    gt1_txpostcursor_in,
    gt1_txprecursor_in,
    gt1_txdiffctrl_in,
    gt1_txinhibit_in,
    gt1_rxprbscntreset_in,
    gt1_rxprbserr_out,
    gt1_rxprbssel_in,
    gt1_txprbssel_in,
    gt1_txprbsforceerr_in,
    gt1_rxcdrhold_in,
    gt1_dmonitorout_out,
    gt1_rxdisperr_out,
    gt1_rxnotintable_out,
    gt1_rxcommadet_out,
    mdc,
    mdio_in,
    mdio_out,
    mdio_tri,
    prtad,
    type_sel);
  input reset;
  input dclk;
  output clk156_out;
  output clk156_lock;
  output qplloutclk_out;
  output qplllock_out;
  output qplloutrefclk_out;
  output refclk_out;
  input refclk_p;
  input refclk_n;
  input [63:0]xgmii_txd;
  input [7:0]xgmii_txc;
  output [63:0]xgmii_rxd;
  output [7:0]xgmii_rxc;
  output rxaui_tx_l0_p;
  output rxaui_tx_l0_n;
  output rxaui_tx_l1_p;
  output rxaui_tx_l1_n;
  input rxaui_rx_l0_p;
  input rxaui_rx_l0_n;
  input rxaui_rx_l1_p;
  input rxaui_rx_l1_n;
  input [1:0]signal_detect;
  output [5:0]debug;
  input [8:0]gt0_drpaddr;
  input gt0_drpen;
  input [15:0]gt0_drpdi;
  output [15:0]gt0_drpdo;
  output gt0_drprdy;
  input gt0_drpwe;
  input gt0_txpmareset_in;
  input gt0_txpcsreset_in;
  output gt0_txresetdone_out;
  input gt0_rxpmareset_in;
  input gt0_rxpcsreset_in;
  output gt0_rxresetdone_out;
  output [2:0]gt0_rxbufstatus_out;
  output gt0_txphaligndone_out;
  output gt0_txphinitdone_out;
  output gt0_txdlysresetdone_out;
  output gt_qplllock_out;
  input gt0_eyescantrigger_in;
  input gt0_eyescanreset_in;
  output gt0_eyescandataerror_out;
  input [2:0]gt0_rxrate_in;
  input [2:0]gt0_loopback_in;
  input gt0_rxpolarity_in;
  input gt0_txpolarity_in;
  input gt0_rxlpmen_in;
  input gt0_rxdfelpmreset_in;
  input [1:0]gt0_rxmonitorsel_in;
  output [6:0]gt0_rxmonitorout_out;
  input [4:0]gt0_txpostcursor_in;
  input [4:0]gt0_txprecursor_in;
  input [3:0]gt0_txdiffctrl_in;
  input gt0_txinhibit_in;
  input gt0_rxprbscntreset_in;
  output gt0_rxprbserr_out;
  input [2:0]gt0_rxprbssel_in;
  input [2:0]gt0_txprbssel_in;
  input gt0_txprbsforceerr_in;
  input gt0_rxcdrhold_in;
  output [7:0]gt0_dmonitorout_out;
  output [3:0]gt0_rxdisperr_out;
  output [3:0]gt0_rxnotintable_out;
  output gt0_rxcommadet_out;
  input [8:0]gt1_drpaddr;
  input gt1_drpen;
  input [15:0]gt1_drpdi;
  output [15:0]gt1_drpdo;
  output gt1_drprdy;
  input gt1_drpwe;
  input gt1_txpmareset_in;
  input gt1_txpcsreset_in;
  output gt1_txresetdone_out;
  input gt1_rxpmareset_in;
  input gt1_rxpcsreset_in;
  output gt1_rxresetdone_out;
  output [2:0]gt1_rxbufstatus_out;
  output gt1_txphaligndone_out;
  output gt1_txphinitdone_out;
  output gt1_txdlysresetdone_out;
  input gt1_eyescantrigger_in;
  input gt1_eyescanreset_in;
  output gt1_eyescandataerror_out;
  input [2:0]gt1_rxrate_in;
  input [2:0]gt1_loopback_in;
  input gt1_rxpolarity_in;
  input gt1_txpolarity_in;
  input gt1_rxlpmen_in;
  input gt1_rxdfelpmreset_in;
  input [1:0]gt1_rxmonitorsel_in;
  output [6:0]gt1_rxmonitorout_out;
  input [4:0]gt1_txpostcursor_in;
  input [4:0]gt1_txprecursor_in;
  input [3:0]gt1_txdiffctrl_in;
  input gt1_txinhibit_in;
  input gt1_rxprbscntreset_in;
  output gt1_rxprbserr_out;
  input [2:0]gt1_rxprbssel_in;
  input [2:0]gt1_txprbssel_in;
  input gt1_txprbsforceerr_in;
  input gt1_rxcdrhold_in;
  output [7:0]gt1_dmonitorout_out;
  output [3:0]gt1_rxdisperr_out;
  output [3:0]gt1_rxnotintable_out;
  output gt1_rxcommadet_out;
  input mdc;
  input mdio_in;
  output mdio_out;
  output mdio_tri;
  input [4:0]prtad;
  input [1:0]type_sel;

  wire clk156_out;
  wire common_pll_reset_i;
  wire dclk;
  wire [5:0]debug;
  wire [7:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr;
  wire [15:0]gt0_drpdi;
  wire [15:0]gt0_drpdo;
  wire gt0_drpen;
  wire gt0_drprdy;
  wire gt0_drpwe;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire [2:0]gt0_loopback_in;
  wire [2:0]gt0_rxbufstatus_out;
  wire gt0_rxcdrhold_in;
  wire gt0_rxcommadet_out;
  wire gt0_rxdfelpmreset_in;
  wire [3:0]gt0_rxdisperr_out;
  wire gt0_rxlpmen_in;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire [3:0]gt0_rxnotintable_out;
  wire gt0_rxpcsreset_in;
  wire gt0_rxpmareset_in;
  wire gt0_rxpolarity_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire [2:0]gt0_rxrate_in;
  wire gt0_rxresetdone_out;
  wire [3:0]gt0_txdiffctrl_in;
  wire gt0_txdlysresetdone_out;
  wire gt0_txinhibit_in;
  wire gt0_txpcsreset_in;
  wire gt0_txphaligndone_out;
  wire gt0_txphinitdone_out;
  wire gt0_txpmareset_in;
  wire gt0_txpolarity_in;
  wire [4:0]gt0_txpostcursor_in;
  wire gt0_txprbsforceerr_in;
  wire [2:0]gt0_txprbssel_in;
  wire [4:0]gt0_txprecursor_in;
  wire gt0_txresetdone_out;
  wire [7:0]gt1_dmonitorout_out;
  wire [8:0]gt1_drpaddr;
  wire [15:0]gt1_drpdi;
  wire [15:0]gt1_drpdo;
  wire gt1_drpen;
  wire gt1_drprdy;
  wire gt1_drpwe;
  wire gt1_eyescandataerror_out;
  wire gt1_eyescanreset_in;
  wire gt1_eyescantrigger_in;
  wire [2:0]gt1_loopback_in;
  wire [2:0]gt1_rxbufstatus_out;
  wire gt1_rxcdrhold_in;
  wire gt1_rxcommadet_out;
  wire gt1_rxdfelpmreset_in;
  wire [3:0]gt1_rxdisperr_out;
  wire gt1_rxlpmen_in;
  wire [6:0]gt1_rxmonitorout_out;
  wire [1:0]gt1_rxmonitorsel_in;
  wire [3:0]gt1_rxnotintable_out;
  wire gt1_rxpcsreset_in;
  wire gt1_rxpmareset_in;
  wire gt1_rxpolarity_in;
  wire gt1_rxprbscntreset_in;
  wire gt1_rxprbserr_out;
  wire [2:0]gt1_rxprbssel_in;
  wire [2:0]gt1_rxrate_in;
  wire gt1_rxresetdone_out;
  wire [3:0]gt1_txdiffctrl_in;
  wire gt1_txdlysresetdone_out;
  wire gt1_txinhibit_in;
  wire gt1_txpcsreset_in;
  wire gt1_txphaligndone_out;
  wire gt1_txphinitdone_out;
  wire gt1_txpmareset_in;
  wire gt1_txpolarity_in;
  wire [4:0]gt1_txpostcursor_in;
  wire gt1_txprbsforceerr_in;
  wire [2:0]gt1_txprbssel_in;
  wire [4:0]gt1_txprecursor_in;
  wire gt1_txresetdone_out;
  wire gt_qplllock_out;
  wire mdc;
  wire mdio_in;
  wire mdio_out;
  wire mdio_tri;
  wire [4:0]prtad;
  wire qplllock_out;
  wire qplloutclk_out;
  wire qplloutrefclk_out;
  wire refclk_n;
  wire refclk_out;
  wire refclk_p;
  wire reset;
  wire rxaui_rx_l0_n;
  wire rxaui_rx_l0_p;
  wire rxaui_rx_l1_n;
  wire rxaui_rx_l1_p;
  wire rxaui_tx_l0_n;
  wire rxaui_tx_l0_p;
  wire rxaui_tx_l1_n;
  wire rxaui_tx_l1_p;
  wire [1:0]signal_detect;
  wire [1:0]type_sel;
  wire [7:0]xgmii_rxc;
  wire [63:0]xgmii_rxd;
  wire [7:0]xgmii_txc;
  wire [63:0]xgmii_txd;

  assign clk156_lock = gt_qplllock_out;
  rxaui_0_rxaui_0_block rxaui_block_i
       (.CLK(clk156_out),
        .D({gt1_txresetdone_out,gt0_txresetdone_out}),
        .dclk(dclk),
        .debug(debug),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr(gt0_drpaddr),
        .gt0_drpdi(gt0_drpdi),
        .gt0_drpdo(gt0_drpdo),
        .gt0_drpen(gt0_drpen),
        .gt0_drprdy(gt0_drprdy),
        .gt0_drpwe(gt0_drpwe),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_loopback_in(gt0_loopback_in),
        .gt0_rxcdrhold_in(gt0_rxcdrhold_in),
        .gt0_rxcommadet_out(gt0_rxcommadet_out),
        .gt0_rxdfelpmreset_in(gt0_rxdfelpmreset_in),
        .gt0_rxlpmen_in(gt0_rxlpmen_in),
        .gt0_rxmonitorout_out(gt0_rxmonitorout_out),
        .gt0_rxmonitorsel_in(gt0_rxmonitorsel_in),
        .gt0_rxpcsreset_in(gt0_rxpcsreset_in),
        .gt0_rxpmareset_in(gt0_rxpmareset_in),
        .gt0_rxpolarity_in(gt0_rxpolarity_in),
        .gt0_rxprbscntreset_in(gt0_rxprbscntreset_in),
        .gt0_rxprbserr_out(gt0_rxprbserr_out),
        .gt0_rxprbssel_in(gt0_rxprbssel_in),
        .gt0_rxrate_in(gt0_rxrate_in),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_txdiffctrl_in(gt0_txdiffctrl_in),
        .gt0_txdlysresetdone_out(gt0_txdlysresetdone_out),
        .gt0_txinhibit_in(gt0_txinhibit_in),
        .gt0_txpcsreset_in(gt0_txpcsreset_in),
        .gt0_txphaligndone_out(gt0_txphaligndone_out),
        .gt0_txphinitdone_out(gt0_txphinitdone_out),
        .gt0_txpmareset_in(gt0_txpmareset_in),
        .gt0_txpolarity_in(gt0_txpolarity_in),
        .gt0_txpostcursor_in(gt0_txpostcursor_in),
        .gt0_txprbsforceerr_in(gt0_txprbsforceerr_in),
        .gt0_txprbssel_in(gt0_txprbssel_in),
        .gt0_txprecursor_in(gt0_txprecursor_in),
        .gt1_dmonitorout_out(gt1_dmonitorout_out),
        .gt1_drpaddr(gt1_drpaddr),
        .gt1_drpdi(gt1_drpdi),
        .gt1_drpdo(gt1_drpdo),
        .gt1_drpen(gt1_drpen),
        .gt1_drprdy(gt1_drprdy),
        .gt1_drpwe(gt1_drpwe),
        .gt1_eyescandataerror_out(gt1_eyescandataerror_out),
        .gt1_eyescanreset_in(gt1_eyescanreset_in),
        .gt1_eyescantrigger_in(gt1_eyescantrigger_in),
        .gt1_loopback_in(gt1_loopback_in),
        .gt1_rxcdrhold_in(gt1_rxcdrhold_in),
        .gt1_rxcommadet_out(gt1_rxcommadet_out),
        .gt1_rxdfelpmreset_in(gt1_rxdfelpmreset_in),
        .gt1_rxlpmen_in(gt1_rxlpmen_in),
        .gt1_rxmonitorout_out(gt1_rxmonitorout_out),
        .gt1_rxmonitorsel_in(gt1_rxmonitorsel_in),
        .gt1_rxpcsreset_in(gt1_rxpcsreset_in),
        .gt1_rxpmareset_in(gt1_rxpmareset_in),
        .gt1_rxpolarity_in(gt1_rxpolarity_in),
        .gt1_rxprbscntreset_in(gt1_rxprbscntreset_in),
        .gt1_rxprbserr_out(gt1_rxprbserr_out),
        .gt1_rxprbssel_in(gt1_rxprbssel_in),
        .gt1_rxrate_in(gt1_rxrate_in),
        .gt1_rxresetdone_out(gt1_rxresetdone_out),
        .gt1_txdiffctrl_in(gt1_txdiffctrl_in),
        .gt1_txdlysresetdone_out(gt1_txdlysresetdone_out),
        .gt1_txinhibit_in(gt1_txinhibit_in),
        .gt1_txpcsreset_in(gt1_txpcsreset_in),
        .gt1_txphaligndone_out(gt1_txphaligndone_out),
        .gt1_txphinitdone_out(gt1_txphinitdone_out),
        .gt1_txpmareset_in(gt1_txpmareset_in),
        .gt1_txpolarity_in(gt1_txpolarity_in),
        .gt1_txpostcursor_in(gt1_txpostcursor_in),
        .gt1_txprbsforceerr_in(gt1_txprbsforceerr_in),
        .gt1_txprbssel_in(gt1_txprbssel_in),
        .gt1_txprecursor_in(gt1_txprecursor_in),
        .mdc(mdc),
        .mdio_in(mdio_in),
        .mdio_out(mdio_out),
        .mdio_tri(mdio_tri),
        .\mgt_rxdisperr_reg_reg[7]_0 ({gt1_rxdisperr_out,gt0_rxdisperr_out}),
        .\mgt_rxnotintable_reg_reg[7]_0 ({gt1_rxnotintable_out,gt0_rxnotintable_out}),
        .out(gt_qplllock_out),
        .prtad(prtad),
        .qplllock_out(qplllock_out),
        .qplloutclk_out(qplloutclk_out),
        .qplloutrefclk_out(qplloutrefclk_out),
        .reset(reset),
        .rxaui_rx_l0_n(rxaui_rx_l0_n),
        .rxaui_rx_l0_p(rxaui_rx_l0_p),
        .rxaui_rx_l1_n(rxaui_rx_l1_n),
        .rxaui_rx_l1_p(rxaui_rx_l1_p),
        .rxaui_tx_l0_n(rxaui_tx_l0_n),
        .rxaui_tx_l0_p(rxaui_tx_l0_p),
        .rxaui_tx_l1_n(rxaui_tx_l1_n),
        .rxaui_tx_l1_p(rxaui_tx_l1_p),
        .signal_detect(signal_detect),
        .type_sel(type_sel),
        .\uclk_mgt_rxbufstatus_reg_reg[5]_0 ({gt1_rxbufstatus_out,gt0_rxbufstatus_out}),
        .xgmii_rxc(xgmii_rxc),
        .xgmii_rxd(xgmii_rxd),
        .xgmii_txc(xgmii_txc),
        .xgmii_txd(xgmii_txd));
  rxaui_0_rxaui_0_gt_common_wrapper rxaui_gt_common_i
       (.common_pll_reset_i(common_pll_reset_i),
        .dclk(dclk),
        .qplllock_out(qplllock_out),
        .qplloutclk_out(qplloutclk_out),
        .qplloutrefclk_out(qplloutrefclk_out),
        .refclk_out(refclk_out));
  rxaui_0_rxaui_0_support_clocking rxaui_support_clocking_i
       (.refclk_n(refclk_n),
        .refclk_out(refclk_out),
        .refclk_p(refclk_p));
  rxaui_0_rxaui_0_support_resets rxaui_support_resets_i
       (.common_pll_reset_i(common_pll_reset_i),
        .dclk(dclk),
        .reset(reset));
endmodule

(* ORIG_REF_NAME = "rxaui_0_support_clocking" *) 
module rxaui_0_rxaui_0_support_clocking
   (refclk_out,
    refclk_p,
    refclk_n);
  output refclk_out;
  input refclk_p;
  input refclk_n;

  wire refclk_n;
  wire refclk_n_ibuf;
  wire refclk_out;
  wire refclk_p;
  wire refclk_p_ibuf;
  wire NLW_refclk_ibufds_ODIV2_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE2 #(
    .CLKCM_CFG("TRUE"),
    .CLKRCV_TRST("TRUE"),
    .CLKSWING_CFG(2'b11)) 
    refclk_ibufds
       (.CEB(1'b0),
        .I(refclk_p_ibuf),
        .IB(refclk_n_ibuf),
        .O(refclk_out),
        .ODIV2(NLW_refclk_ibufds_ODIV2_UNCONNECTED));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    refclk_n_ibuf_inst
       (.I(refclk_n),
        .O(refclk_n_ibuf));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    refclk_p_ibuf_inst
       (.I(refclk_p),
        .O(refclk_p_ibuf));
endmodule

(* ORIG_REF_NAME = "rxaui_0_support_resets" *) 
module rxaui_0_rxaui_0_support_resets
   (common_pll_reset_i,
    dclk,
    reset);
  output common_pll_reset_i;
  input dclk;
  input reset;

  wire common_pll_reset_i;
  wire count_d1;
  wire \counter[7]_i_3_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[3] ;
  wire \counter_reg_n_0_[4] ;
  wire \counter_reg_n_0_[5] ;
  wire \counter_reg_n_0_[6] ;
  wire dclk;
  wire initial_reset;
  wire initial_reset_i_1_n_0;
  wire [7:0]plusOp;
  wire reset;
  wire reset_count_done;
  wire sel;

  FDRE count_d1_reg
       (.C(dclk),
        .CE(1'b1),
        .D(reset_count_done),
        .Q(count_d1),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[2]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[3]_i_1 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_1 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_1 
       (.I0(\counter_reg_n_0_[3] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[2] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counter[6]_i_1 
       (.I0(\counter[7]_i_3_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .O(plusOp[6]));
  LUT1 #(
    .INIT(2'h1)) 
    \counter[7]_i_1 
       (.I0(reset_count_done),
        .O(sel));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[7]_i_2 
       (.I0(\counter[7]_i_3_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(reset_count_done),
        .O(plusOp[7]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter[7]_i_3 
       (.I0(\counter_reg_n_0_[5] ),
        .I1(\counter_reg_n_0_[3] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[2] ),
        .I5(\counter_reg_n_0_[4] ),
        .O(\counter[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[0] 
       (.C(dclk),
        .CE(sel),
        .D(plusOp[0]),
        .Q(\counter_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[1] 
       (.C(dclk),
        .CE(sel),
        .D(plusOp[1]),
        .Q(\counter_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[2] 
       (.C(dclk),
        .CE(sel),
        .D(plusOp[2]),
        .Q(\counter_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[3] 
       (.C(dclk),
        .CE(sel),
        .D(plusOp[3]),
        .Q(\counter_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[4] 
       (.C(dclk),
        .CE(sel),
        .D(plusOp[4]),
        .Q(\counter_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[5] 
       (.C(dclk),
        .CE(sel),
        .D(plusOp[5]),
        .Q(\counter_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[6] 
       (.C(dclk),
        .CE(sel),
        .D(plusOp[6]),
        .Q(\counter_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[7] 
       (.C(dclk),
        .CE(sel),
        .D(plusOp[7]),
        .Q(reset_count_done),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_common_0_i_i_1
       (.I0(initial_reset),
        .I1(reset_count_done),
        .I2(reset),
        .O(common_pll_reset_i));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT2 #(
    .INIT(4'h2)) 
    initial_reset_i_1
       (.I0(reset_count_done),
        .I1(count_d1),
        .O(initial_reset_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    initial_reset_reg
       (.C(dclk),
        .CE(1'b1),
        .D(initial_reset_i_1_n_0),
        .Q(initial_reset),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_tx_sync_sync_block" *) 
module rxaui_0_rxaui_0_tx_sync_sync_block
   (data_out,
    gt0_txdlysresetdone_out,
    uclk_mgt_rx_reset_reg);
  output data_out;
  input gt0_txdlysresetdone_out;
  input uclk_mgt_rx_reset_reg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_txdlysresetdone_out;
  wire uclk_mgt_rx_reset_reg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(gt0_txdlysresetdone_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_tx_sync_sync_block" *) 
module rxaui_0_rxaui_0_tx_sync_sync_block_6
   (\TXDLYEN_reg[0] ,
    data_out,
    \FSM_onehot_tx_phalign_manual_state_reg[0] ,
    Q,
    out,
    \TXDLYEN_reg[0]_0 ,
    gt0_txphaligndone_out,
    uclk_mgt_rx_reset_reg);
  output \TXDLYEN_reg[0] ;
  output data_out;
  output \FSM_onehot_tx_phalign_manual_state_reg[0] ;
  input [0:0]Q;
  input [2:0]out;
  input \TXDLYEN_reg[0]_0 ;
  input gt0_txphaligndone_out;
  input uclk_mgt_rx_reset_reg;

  wire \FSM_onehot_tx_phalign_manual_state_reg[0] ;
  wire [0:0]Q;
  wire \TXDLYEN_reg[0] ;
  wire \TXDLYEN_reg[0]_0 ;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_txphaligndone_out;
  wire [2:0]out;
  wire uclk_mgt_rx_reset_reg;

  LUT2 #(
    .INIT(4'h2)) 
    \FSM_onehot_tx_phalign_manual_state[8]_i_6 
       (.I0(data_out),
        .I1(Q),
        .O(\FSM_onehot_tx_phalign_manual_state_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFFFBFFFFFFFBF0)) 
    \TXDLYEN[0]_i_1 
       (.I0(Q),
        .I1(data_out),
        .I2(out[1]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(\TXDLYEN_reg[0]_0 ),
        .O(\TXDLYEN_reg[0] ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(gt0_txphaligndone_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_tx_sync_sync_block" *) 
module rxaui_0_rxaui_0_tx_sync_sync_block_7
   (data_out,
    gt1_txdlysresetdone_out,
    uclk_mgt_rx_reset_reg);
  output data_out;
  input gt1_txdlysresetdone_out;
  input uclk_mgt_rx_reset_reg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt1_txdlysresetdone_out;
  wire uclk_mgt_rx_reset_reg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(gt1_txdlysresetdone_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_tx_sync_sync_block" *) 
module rxaui_0_rxaui_0_tx_sync_sync_block_8
   (data_out,
    gt1_txphaligndone_out,
    uclk_mgt_rx_reset_reg);
  output data_out;
  input gt1_txphaligndone_out;
  input uclk_mgt_rx_reset_reg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt1_txphaligndone_out;
  wire uclk_mgt_rx_reset_reg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(gt1_txphaligndone_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_tx_sync_sync_pulse" *) 
module rxaui_0_rxaui_0_tx_sync_sync_pulse
   (txphinitdone_sync_0,
    E,
    uclk_mgt_rx_reset_reg,
    \txdlysresetdone_store_reg[0] ,
    D,
    \txphaligndone_store_reg[0] ,
    \FSM_onehot_tx_phalign_manual_state_reg[3] ,
    data_sync_reg6,
    Q,
    gt0_txphinitdone_out);
  output txphinitdone_sync_0;
  output [0:0]E;
  input uclk_mgt_rx_reset_reg;
  input \txdlysresetdone_store_reg[0] ;
  input [1:0]D;
  input \txphaligndone_store_reg[0] ;
  input \FSM_onehot_tx_phalign_manual_state_reg[3] ;
  input data_sync_reg6;
  input [0:0]Q;
  input gt0_txphinitdone_out;

  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_onehot_tx_phalign_manual_state[8]_i_3_n_0 ;
  wire \FSM_onehot_tx_phalign_manual_state_reg[3] ;
  wire [0:0]Q;
  wire USER_DONE_i_1_n_0;
  wire data_sync_reg6;
  wire gt0_txphinitdone_out;
  wire [2:0]stretch_r;
  wire \stretch_r[2]_i_1_n_0 ;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [2:0]sync1_r;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [2:0]sync2_r;
  wire \txdlysresetdone_store_reg[0] ;
  wire \txphaligndone_store_reg[0] ;
  wire txphinitdone_sync_0;
  wire uclk_mgt_rx_reset_reg;

  LUT6 #(
    .INIT(64'hFFFFFFFEFFEEFFEE)) 
    \FSM_onehot_tx_phalign_manual_state[8]_i_1 
       (.I0(\txdlysresetdone_store_reg[0] ),
        .I1(\FSM_onehot_tx_phalign_manual_state[8]_i_3_n_0 ),
        .I2(D[1]),
        .I3(\txphaligndone_store_reg[0] ),
        .I4(\FSM_onehot_tx_phalign_manual_state_reg[3] ),
        .I5(data_sync_reg6),
        .O(E));
  LUT3 #(
    .INIT(8'h20)) 
    \FSM_onehot_tx_phalign_manual_state[8]_i_3 
       (.I0(D[0]),
        .I1(Q),
        .I2(txphinitdone_sync_0),
        .O(\FSM_onehot_tx_phalign_manual_state[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    USER_DONE_i_1
       (.I0(sync1_r[0]),
        .I1(sync2_r[0]),
        .O(USER_DONE_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    USER_DONE_reg
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(USER_DONE_i_1_n_0),
        .Q(txphinitdone_sync_0),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \stretch_r[2]_i_1 
       (.I0(gt0_txphinitdone_out),
        .O(\stretch_r[2]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \stretch_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .CLR(\stretch_r[2]_i_1_n_0 ),
        .D(stretch_r[1]),
        .Q(stretch_r[0]));
  FDCE #(
    .INIT(1'b0)) 
    \stretch_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .CLR(\stretch_r[2]_i_1_n_0 ),
        .D(stretch_r[2]),
        .Q(stretch_r[1]));
  FDCE #(
    .INIT(1'b0)) 
    \stretch_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .CLR(\stretch_r[2]_i_1_n_0 ),
        .D(1'b1),
        .Q(stretch_r[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync1_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync1_r[1]),
        .Q(sync1_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync1_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync1_r[2]),
        .Q(sync1_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync1_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(stretch_r[0]),
        .Q(sync1_r[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync2_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync2_r[1]),
        .Q(sync2_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync2_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync2_r[2]),
        .Q(sync2_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync2_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(gt0_txphinitdone_out),
        .Q(sync2_r[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_0_tx_sync_sync_pulse" *) 
module rxaui_0_rxaui_0_tx_sync_sync_pulse_9
   (D,
    uclk_mgt_rx_reset_reg,
    gt1_txphinitdone_out);
  output [0:0]D;
  input uclk_mgt_rx_reset_reg;
  input gt1_txphinitdone_out;

  wire [0:0]D;
  wire USER_DONE_i_1__0_n_0;
  wire gt1_txphinitdone_out;
  wire \stretch_r[2]_i_1_n_0 ;
  wire \stretch_r_reg_n_0_[0] ;
  wire \stretch_r_reg_n_0_[1] ;
  wire \stretch_r_reg_n_0_[2] ;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [2:0]sync1_r;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [2:0]sync2_r;
  wire uclk_mgt_rx_reset_reg;

  LUT2 #(
    .INIT(4'h8)) 
    USER_DONE_i_1__0
       (.I0(sync1_r[0]),
        .I1(sync2_r[0]),
        .O(USER_DONE_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    USER_DONE_reg
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(USER_DONE_i_1__0_n_0),
        .Q(D),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \stretch_r[2]_i_1 
       (.I0(gt1_txphinitdone_out),
        .O(\stretch_r[2]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \stretch_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .CLR(\stretch_r[2]_i_1_n_0 ),
        .D(\stretch_r_reg_n_0_[1] ),
        .Q(\stretch_r_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \stretch_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .CLR(\stretch_r[2]_i_1_n_0 ),
        .D(\stretch_r_reg_n_0_[2] ),
        .Q(\stretch_r_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \stretch_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .CLR(\stretch_r[2]_i_1_n_0 ),
        .D(1'b1),
        .Q(\stretch_r_reg_n_0_[2] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync1_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync1_r[1]),
        .Q(sync1_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync1_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync1_r[2]),
        .Q(sync1_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync1_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(\stretch_r_reg_n_0_[0] ),
        .Q(sync1_r[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync2_r_reg[0] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync2_r[1]),
        .Q(sync2_r[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync2_r_reg[1] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(sync2_r[2]),
        .Q(sync2_r[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sync2_r_reg[2] 
       (.C(uclk_mgt_rx_reset_reg),
        .CE(1'b1),
        .D(gt1_txphinitdone_out),
        .Q(sync2_r[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "align_counter" *) 
module rxaui_0_align_counter
   (\count_reg[4]_0 ,
    Q,
    \state_reg[1][1] ,
    \state_reg[1][1]_0 ,
    \state_reg[0][0] ,
    \state_reg[1][0] ,
    \tx_is_idle_reg[0] ,
    \tx_is_q_reg[0] ,
    next_ifg_is_a_reg,
    \count_reg[4]_1 ,
    usrclk_reset,
    usrclk,
    D,
    E,
    \state_reg[0][0]_0 ,
    \state_reg[0][1] ,
    \state_reg[1][1]_1 ,
    \state_reg[1][0]_0 );
  output [3:0]\count_reg[4]_0 ;
  output [1:0]Q;
  output \state_reg[1][1] ;
  output \state_reg[1][1]_0 ;
  output \state_reg[0][0] ;
  input \state_reg[1][0] ;
  input [0:0]\tx_is_idle_reg[0] ;
  input [0:0]\tx_is_q_reg[0] ;
  input next_ifg_is_a_reg;
  input \count_reg[4]_1 ;
  input usrclk_reset;
  input usrclk;
  input [1:0]D;
  input [0:0]E;
  input [0:0]\state_reg[0][0]_0 ;
  input \state_reg[0][1] ;
  input \state_reg[1][1]_1 ;
  input [0:0]\state_reg[1][0]_0 ;

  wire [1:0]D;
  wire [0:0]E;
  wire [1:0]Q;
  wire [2:2]count;
  wire \count[2]_i_1_n_0 ;
  wire \count[3]_i_1_n_0 ;
  wire \count[3]_i_2_n_0 ;
  wire \count[4]_i_1_n_0 ;
  wire [3:0]\count_reg[4]_0 ;
  wire \count_reg[4]_1 ;
  wire extra_a;
  wire extra_a_i_1_n_0;
  wire next_ifg_is_a_reg;
  wire [0:0]p_0_out;
  wire [3:2]p_1_in;
  wire \prbs_reg_n_0_[5] ;
  wire \prbs_reg_n_0_[6] ;
  wire \prbs_reg_n_0_[7] ;
  wire \state_reg[0][0] ;
  wire [0:0]\state_reg[0][0]_0 ;
  wire \state_reg[0][1] ;
  wire \state_reg[1][0] ;
  wire [0:0]\state_reg[1][0]_0 ;
  wire \state_reg[1][1] ;
  wire \state_reg[1][1]_0 ;
  wire \state_reg[1][1]_1 ;
  wire [0:0]\tx_is_idle_reg[0] ;
  wire [0:0]\tx_is_q_reg[0] ;
  wire usrclk;
  wire usrclk_reset;

  LUT4 #(
    .INIT(16'h9F90)) 
    \count[2]_i_1 
       (.I0(count),
        .I1(\count_reg[4]_0 [1]),
        .I2(\state_reg[1][0] ),
        .I3(p_1_in[2]),
        .O(\count[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \count[3]_i_1 
       (.I0(\state_reg[1][0] ),
        .I1(\count_reg[4]_0 [2]),
        .I2(\count_reg[4]_0 [3]),
        .I3(\count_reg[4]_0 [1]),
        .I4(count),
        .O(\count[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA9FFA900)) 
    \count[3]_i_2 
       (.I0(\count_reg[4]_0 [2]),
        .I1(\count_reg[4]_0 [1]),
        .I2(count),
        .I3(\state_reg[1][0] ),
        .I4(p_1_in[3]),
        .O(\count[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hF0E0FFFF)) 
    \count[4]_i_1 
       (.I0(count),
        .I1(\count_reg[4]_0 [1]),
        .I2(\count_reg[4]_0 [3]),
        .I3(\count_reg[4]_0 [2]),
        .I4(\state_reg[1][0] ),
        .O(\count[4]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(usrclk),
        .CE(\count[3]_i_1_n_0 ),
        .D(D[0]),
        .Q(\count_reg[4]_0 [0]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(usrclk),
        .CE(\count[3]_i_1_n_0 ),
        .D(D[1]),
        .Q(\count_reg[4]_0 [1]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(usrclk),
        .CE(\count[3]_i_1_n_0 ),
        .D(\count[2]_i_1_n_0 ),
        .Q(count),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(usrclk),
        .CE(\count[3]_i_1_n_0 ),
        .D(\count[3]_i_2_n_0 ),
        .Q(\count_reg[4]_0 [2]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\count[4]_i_1_n_0 ),
        .Q(\count_reg[4]_0 [3]),
        .R(usrclk_reset));
  LUT6 #(
    .INIT(64'h00000000BABA00BA)) 
    extra_a_i_1
       (.I0(extra_a),
        .I1(\state_reg[0][0]_0 ),
        .I2(\state_reg[0][1] ),
        .I3(\state_reg[1][1]_1 ),
        .I4(\state_reg[1][0]_0 ),
        .I5(usrclk_reset),
        .O(extra_a_i_1_n_0));
  FDRE extra_a_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(extra_a_i_1_n_0),
        .Q(extra_a),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    \prbs[1]_i_1__0 
       (.I0(\prbs_reg_n_0_[6] ),
        .I1(\prbs_reg_n_0_[7] ),
        .O(p_0_out));
  FDSE #(
    .INIT(1'b1)) 
    \prbs_reg[1] 
       (.C(usrclk),
        .CE(E),
        .D(p_0_out),
        .Q(Q[0]),
        .S(usrclk_reset));
  FDSE #(
    .INIT(1'b1)) 
    \prbs_reg[2] 
       (.C(usrclk),
        .CE(E),
        .D(Q[0]),
        .Q(Q[1]),
        .S(usrclk_reset));
  FDSE #(
    .INIT(1'b1)) 
    \prbs_reg[3] 
       (.C(usrclk),
        .CE(E),
        .D(Q[1]),
        .Q(p_1_in[2]),
        .S(usrclk_reset));
  FDSE #(
    .INIT(1'b1)) 
    \prbs_reg[4] 
       (.C(usrclk),
        .CE(E),
        .D(p_1_in[2]),
        .Q(p_1_in[3]),
        .S(usrclk_reset));
  FDSE #(
    .INIT(1'b1)) 
    \prbs_reg[5] 
       (.C(usrclk),
        .CE(E),
        .D(p_1_in[3]),
        .Q(\prbs_reg_n_0_[5] ),
        .S(usrclk_reset));
  FDSE #(
    .INIT(1'b1)) 
    \prbs_reg[6] 
       (.C(usrclk),
        .CE(E),
        .D(\prbs_reg_n_0_[5] ),
        .Q(\prbs_reg_n_0_[6] ),
        .S(usrclk_reset));
  FDSE #(
    .INIT(1'b1)) 
    \prbs_reg[7] 
       (.C(usrclk),
        .CE(E),
        .D(\prbs_reg_n_0_[6] ),
        .Q(\prbs_reg_n_0_[7] ),
        .S(usrclk_reset));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF40)) 
    \state[1][0]_i_5 
       (.I0(extra_a),
        .I1(\count_reg[4]_0 [1]),
        .I2(\count_reg[4]_0 [0]),
        .I3(\count_reg[4]_0 [2]),
        .I4(\count_reg[4]_0 [3]),
        .I5(count),
        .O(\state_reg[0][0] ));
  LUT5 #(
    .INIT(32'h00105555)) 
    \state[1][0]_i_6 
       (.I0(\count_reg[4]_1 ),
        .I1(\count_reg[4]_0 [1]),
        .I2(extra_a),
        .I3(\count_reg[4]_0 [0]),
        .I4(count),
        .O(\state_reg[1][1]_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \state[1][1]_i_2 
       (.I0(\state_reg[1][1]_0 ),
        .I1(\tx_is_idle_reg[0] ),
        .I2(\tx_is_q_reg[0] ),
        .I3(next_ifg_is_a_reg),
        .O(\state_reg[1][1] ));
endmodule

(* ORIG_REF_NAME = "deskew_state_machine" *) 
module rxaui_0_deskew_state_machine
   (mgt_enchansync,
    \debug[5] ,
    local_fault,
    usrclk,
    sync_status,
    usrclk_reset,
    D,
    \mgt_rxdata_reg_reg[24] );
  output mgt_enchansync;
  output \debug[5] ;
  output local_fault;
  input usrclk;
  input sync_status;
  input usrclk_reset;
  input [1:0]D;
  input [1:0]\mgt_rxdata_reg_reg[24] ;

  wire [1:0]D;
  wire \debug[5] ;
  wire deskew_error;
  wire \deskew_error_reg_n_0_[0] ;
  wire enchansync_i;
  wire got_align;
  wire \got_align_reg_n_0_[0] ;
  wire local_fault;
  wire mgt_enchansync;
  wire [1:0]\mgt_rxdata_reg_reg[24] ;
  wire [2:0]next_state;
  wire \state[1][0]_i_1_n_0 ;
  wire \state[1][1]_i_1_n_0 ;
  wire \state[1][1]_i_2__0_n_0 ;
  wire \state[1][1]_i_3__0_n_0 ;
  wire \state[1][1]_i_4__0_n_0 ;
  wire \state[1][1]_i_5__0_n_0 ;
  wire \state[1][2]_i_1_n_0 ;
  wire \state[1][2]_i_3_n_0 ;
  wire \state_reg_n_0_[1][0] ;
  wire \state_reg_n_0_[1][1] ;
  wire \state_reg_n_0_[1][2] ;
  wire sync_status;
  wire usrclk;
  wire usrclk_reset;

  FDRE align_status_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg_n_0_[1][2] ),
        .Q(\debug[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \deskew_error_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [0]),
        .Q(\deskew_error_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \deskew_error_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [1]),
        .Q(deskew_error),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    enchansync_i_1
       (.I0(\state_reg_n_0_[1][1] ),
        .I1(\state_reg_n_0_[1][0] ),
        .I2(sync_status),
        .I3(\state_reg_n_0_[1][2] ),
        .O(enchansync_i));
  FDRE enchansync_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(enchansync_i),
        .Q(mgt_enchansync),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \got_align_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(D[0]),
        .Q(\got_align_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \got_align_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(D[1]),
        .Q(got_align),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \rxd_out[63]_i_1 
       (.I0(\debug[5] ),
        .O(local_fault));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \state[1][0]_i_1 
       (.I0(next_state[0]),
        .I1(sync_status),
        .I2(usrclk_reset),
        .O(\state[1][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF3C0000FDFC0000F)) 
    \state[1][0]_i_2__0 
       (.I0(sync_status),
        .I1(\state[1][1]_i_4__0_n_0 ),
        .I2(deskew_error),
        .I3(got_align),
        .I4(\state[1][1]_i_2__0_n_0 ),
        .I5(\state[1][2]_i_3_n_0 ),
        .O(next_state[0]));
  LUT6 #(
    .INIT(64'h000000003C07002D)) 
    \state[1][1]_i_1 
       (.I0(got_align),
        .I1(\state[1][1]_i_2__0_n_0 ),
        .I2(\state[1][1]_i_3__0_n_0 ),
        .I3(deskew_error),
        .I4(\state[1][1]_i_4__0_n_0 ),
        .I5(\state[1][1]_i_5__0_n_0 ),
        .O(\state[1][1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCCEFD3C3FFFFFFFF)) 
    \state[1][1]_i_2__0 
       (.I0(\state_reg_n_0_[1][1] ),
        .I1(\state_reg_n_0_[1][0] ),
        .I2(\got_align_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[1][2] ),
        .I4(\deskew_error_reg_n_0_[0] ),
        .I5(sync_status),
        .O(\state[1][1]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'hF55F48BFFFFFFFFF)) 
    \state[1][1]_i_3__0 
       (.I0(\state_reg_n_0_[1][2] ),
        .I1(\got_align_reg_n_0_[0] ),
        .I2(\state_reg_n_0_[1][0] ),
        .I3(\state_reg_n_0_[1][1] ),
        .I4(\deskew_error_reg_n_0_[0] ),
        .I5(sync_status),
        .O(\state[1][1]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h0808A88888888888)) 
    \state[1][1]_i_4__0 
       (.I0(sync_status),
        .I1(\state_reg_n_0_[1][2] ),
        .I2(\state_reg_n_0_[1][0] ),
        .I3(\got_align_reg_n_0_[0] ),
        .I4(\deskew_error_reg_n_0_[0] ),
        .I5(\state_reg_n_0_[1][1] ),
        .O(\state[1][1]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \state[1][1]_i_5__0 
       (.I0(usrclk_reset),
        .I1(sync_status),
        .O(\state[1][1]_i_5__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \state[1][2]_i_1 
       (.I0(next_state[2]),
        .I1(sync_status),
        .I2(usrclk_reset),
        .O(\state[1][2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAA0AEA0AA)) 
    \state[1][2]_i_2__0 
       (.I0(\state[1][1]_i_4__0_n_0 ),
        .I1(sync_status),
        .I2(\state[1][2]_i_3_n_0 ),
        .I3(deskew_error),
        .I4(got_align),
        .I5(\state[1][1]_i_2__0_n_0 ),
        .O(next_state[2]));
  LUT5 #(
    .INIT(32'h9793EBBB)) 
    \state[1][2]_i_3 
       (.I0(\deskew_error_reg_n_0_[0] ),
        .I1(\state_reg_n_0_[1][1] ),
        .I2(\state_reg_n_0_[1][0] ),
        .I3(\got_align_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1][2] ),
        .O(\state[1][2]_i_3_n_0 ));
  FDRE \state_reg[1][0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state[1][0]_i_1_n_0 ),
        .Q(\state_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \state_reg[1][1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state[1][1]_i_1_n_0 ),
        .Q(\state_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \state_reg[1][2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state[1][2]_i_1_n_0 ),
        .Q(\state_reg_n_0_[1][2] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "k_r_prbs" *) 
module rxaui_0_k_r_prbs
   (Q,
    usrclk_reset,
    usrclk);
  output [1:0]Q;
  input usrclk_reset;
  input usrclk;

  wire [1:0]Q;
  wire p_0_in;
  wire [1:0]p_2_out;
  wire \prbs_reg_n_0_[1] ;
  wire \prbs_reg_n_0_[2] ;
  wire \prbs_reg_n_0_[3] ;
  wire \prbs_reg_n_0_[4] ;
  wire \prbs_reg_n_0_[5] ;
  wire usrclk;
  wire usrclk_reset;

  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \prbs[1]_i_1 
       (.I0(p_0_in),
        .I1(\prbs_reg_n_0_[5] ),
        .O(p_2_out[0]));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \prbs[2]_i_1 
       (.I0(p_0_in),
        .I1(Q[0]),
        .O(p_2_out[1]));
  FDSE \prbs_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_2_out[0]),
        .Q(\prbs_reg_n_0_[1] ),
        .S(usrclk_reset));
  FDSE \prbs_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_2_out[1]),
        .Q(\prbs_reg_n_0_[2] ),
        .S(usrclk_reset));
  FDSE \prbs_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\prbs_reg_n_0_[1] ),
        .Q(\prbs_reg_n_0_[3] ),
        .S(usrclk_reset));
  FDSE \prbs_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\prbs_reg_n_0_[2] ),
        .Q(\prbs_reg_n_0_[4] ),
        .S(usrclk_reset));
  FDSE \prbs_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\prbs_reg_n_0_[3] ),
        .Q(\prbs_reg_n_0_[5] ),
        .S(usrclk_reset));
  FDSE \prbs_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\prbs_reg_n_0_[4] ),
        .Q(p_0_in),
        .S(usrclk_reset));
  FDSE \prbs_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\prbs_reg_n_0_[5] ),
        .Q(Q[0]),
        .S(usrclk_reset));
  FDSE \prbs_reg[8] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in),
        .Q(Q[1]),
        .S(usrclk_reset));
endmodule

(* ORIG_REF_NAME = "management" *) 
module rxaui_0_management
   (mdio_out,
    mdio_tri,
    clear_local_fault,
    clear_aligned,
    mgt_loopback_r_reg,
    mgt_powerdown_r_reg,
    test_pattern_en,
    \txd_out_reg[56] ,
    test_pattern_sel,
    \txc_out_reg[7] ,
    \txd_out_reg[59] ,
    clear_local_fault_edge_reg,
    clear_aligned_edge_reg,
    \txd_out_reg[5] ,
    \txd_out_reg[37] ,
    D,
    \txd_out_reg[27] ,
    reset_reg_reg_0,
    reset_int,
    \txd_out_reg[34] ,
    \txd_out_reg[59]_0 ,
    \txd_out_reg[62] ,
    \txd_out_reg[6] ,
    usrclk_reset_reg,
    usrclk,
    usrclk_reset,
    rx_local_fault,
    tx_local_fault,
    out,
    aligned_sticky,
    last_value,
    last_value_reg__0,
    \type_sel_reg_reg[0] ,
    p_0_in,
    Q,
    \state_reg[0][1] ,
    \state_reg[0][2] ,
    \state_reg[1][1] ,
    \state_reg[1][0] ,
    \state_reg[1][2] ,
    txc_filtered,
    \state_reg[0][0] ,
    \state_reg[0][0]_0 ,
    prtad,
    reset,
    \state_reg[1][2]_0 ,
    usrclk_reset_pipe,
    \signal_detect_int_reg[2] ,
    sync_status,
    mdc,
    mdio_in);
  output mdio_out;
  output mdio_tri;
  output clear_local_fault;
  output clear_aligned;
  output mgt_loopback_r_reg;
  output mgt_powerdown_r_reg;
  output test_pattern_en;
  output \txd_out_reg[56] ;
  output [1:0]test_pattern_sel;
  output \txc_out_reg[7] ;
  output \txd_out_reg[59] ;
  output clear_local_fault_edge_reg;
  output clear_aligned_edge_reg;
  output \txd_out_reg[5] ;
  output \txd_out_reg[37] ;
  output [4:0]D;
  output \txd_out_reg[27] ;
  output reset_reg_reg_0;
  output reset_int;
  output \txd_out_reg[34] ;
  output \txd_out_reg[59]_0 ;
  output \txd_out_reg[62] ;
  output \txd_out_reg[6] ;
  output usrclk_reset_reg;
  input usrclk;
  input usrclk_reset;
  input rx_local_fault;
  input tx_local_fault;
  input out;
  input aligned_sticky;
  input last_value;
  input last_value_reg__0;
  input \type_sel_reg_reg[0] ;
  input p_0_in;
  input [0:0]Q;
  input \state_reg[0][1] ;
  input \state_reg[0][2] ;
  input \state_reg[1][1] ;
  input [0:0]\state_reg[1][0] ;
  input \state_reg[1][2] ;
  input [4:0]txc_filtered;
  input \state_reg[0][0] ;
  input \state_reg[0][0]_0 ;
  input [4:0]prtad;
  input reset;
  input \state_reg[1][2]_0 ;
  input usrclk_reset_pipe;
  input [1:0]\signal_detect_int_reg[2] ;
  input [3:0]sync_status;
  input mdc;
  input mdio_in;

  wire [4:0]D;
  wire [0:0]Q;
  wire aligned_reg;
  wire aligned_sticky;
  wire aligned_sticky_reg;
  wire clear_aligned;
  wire clear_aligned0;
  wire clear_aligned_edge_reg;
  wire clear_local_fault;
  wire clear_local_fault0;
  wire clear_local_fault2;
  wire clear_local_fault2_carry__0_n_3;
  wire clear_local_fault2_carry_n_0;
  wire clear_local_fault2_carry_n_1;
  wire clear_local_fault2_carry_n_2;
  wire clear_local_fault2_carry_n_3;
  wire clear_local_fault_edge_reg;
  wire last_value;
  wire last_value_reg__0;
  wire mdc;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [2:0]mdc_reg;
  wire mdc_reg2;
  wire mdc_reg3;
  wire mdc_rising;
  wire mdc_rising_i_1_n_0;
  wire mdio_in;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [2:0]mdio_in_reg;
  wire mdio_in_reg2;
  wire mdio_in_reg3_reg_n_0;
  wire mdio_interface_1_n_10;
  wire mdio_interface_1_n_11;
  wire mdio_interface_1_n_12;
  wire mdio_interface_1_n_13;
  wire mdio_interface_1_n_14;
  wire mdio_interface_1_n_15;
  wire mdio_interface_1_n_16;
  wire mdio_interface_1_n_17;
  wire mdio_interface_1_n_18;
  wire mdio_interface_1_n_19;
  wire mdio_interface_1_n_2;
  wire mdio_interface_1_n_20;
  wire mdio_interface_1_n_21;
  wire mdio_interface_1_n_22;
  wire mdio_interface_1_n_23;
  wire mdio_interface_1_n_24;
  wire mdio_interface_1_n_25;
  wire mdio_interface_1_n_26;
  wire mdio_interface_1_n_27;
  wire mdio_interface_1_n_3;
  wire mdio_interface_1_n_4;
  wire mdio_interface_1_n_5;
  wire mdio_interface_1_n_8;
  wire mdio_interface_1_n_9;
  wire mdio_out;
  wire mdio_tri;
  wire mgt_loopback_r_reg;
  wire mgt_powerdown_r_reg;
  wire out;
  wire p_0_in;
  wire [4:0]prtad;
  wire reset;
  wire reset_int;
  wire reset_reg3;
  wire reset_reg3_carry__0_n_3;
  wire reset_reg3_carry_n_0;
  wire reset_reg3_carry_n_1;
  wire reset_reg3_carry_n_2;
  wire reset_reg3_carry_n_3;
  wire reset_reg_reg_0;
  wire rx_local_fault;
  wire rx_local_fault_reg;
  wire [1:0]\signal_detect_int_reg[2] ;
  wire [2:0]signal_detect_reg;
  wire \state_reg[0][0] ;
  wire \state_reg[0][0]_0 ;
  wire \state_reg[0][1] ;
  wire \state_reg[0][2] ;
  wire [0:0]\state_reg[1][0] ;
  wire \state_reg[1][1] ;
  wire \state_reg[1][2] ;
  wire \state_reg[1][2]_0 ;
  wire [3:0]sync_reg;
  wire [3:0]sync_status;
  wire test_en_reg3;
  wire test_en_reg3_carry__0_n_3;
  wire test_en_reg3_carry_n_0;
  wire test_en_reg3_carry_n_1;
  wire test_en_reg3_carry_n_2;
  wire test_en_reg3_carry_n_3;
  wire test_pattern_en;
  wire [1:0]test_pattern_sel;
  wire tx_local_fault;
  wire tx_local_fault_reg;
  wire [4:0]txc_filtered;
  wire \txc_out_reg[7] ;
  wire \txd_out_reg[27] ;
  wire \txd_out_reg[34] ;
  wire \txd_out_reg[37] ;
  wire \txd_out_reg[56] ;
  wire \txd_out_reg[59] ;
  wire \txd_out_reg[59]_0 ;
  wire \txd_out_reg[5] ;
  wire \txd_out_reg[62] ;
  wire \txd_out_reg[6] ;
  wire \type_sel_reg_reg[0] ;
  wire usrclk;
  wire usrclk_reset;
  wire usrclk_reset_pipe;
  wire usrclk_reset_reg;
  wire [3:0]NLW_clear_local_fault2_carry_O_UNCONNECTED;
  wire [3:2]NLW_clear_local_fault2_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_clear_local_fault2_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_reset_reg3_carry_O_UNCONNECTED;
  wire [3:2]NLW_reset_reg3_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_reset_reg3_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_test_en_reg3_carry_O_UNCONNECTED;
  wire [3:2]NLW_test_en_reg3_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_test_en_reg3_carry__0_O_UNCONNECTED;

  FDRE aligned_reg_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(out),
        .Q(aligned_reg),
        .R(usrclk_reset));
  FDRE aligned_sticky_reg_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(aligned_sticky),
        .Q(aligned_sticky_reg),
        .R(usrclk_reset));
  LUT2 #(
    .INIT(4'h2)) 
    clear_aligned_edge_i_1
       (.I0(clear_aligned),
        .I1(last_value_reg__0),
        .O(clear_aligned_edge_reg));
  FDRE clear_aligned_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(clear_aligned0),
        .Q(clear_aligned),
        .R(usrclk_reset));
  CARRY4 clear_local_fault2_carry
       (.CI(1'b0),
        .CO({clear_local_fault2_carry_n_0,clear_local_fault2_carry_n_1,clear_local_fault2_carry_n_2,clear_local_fault2_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_clear_local_fault2_carry_O_UNCONNECTED[3:0]),
        .S({mdio_interface_1_n_14,mdio_interface_1_n_15,mdio_interface_1_n_16,mdio_interface_1_n_17}));
  CARRY4 clear_local_fault2_carry__0
       (.CI(clear_local_fault2_carry_n_0),
        .CO({NLW_clear_local_fault2_carry__0_CO_UNCONNECTED[3:2],clear_local_fault2,clear_local_fault2_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_clear_local_fault2_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,mdio_interface_1_n_20,mdio_interface_1_n_21}));
  LUT2 #(
    .INIT(4'h2)) 
    clear_local_fault_edge_i_1
       (.I0(clear_local_fault),
        .I1(last_value),
        .O(clear_local_fault_edge_reg));
  FDRE clear_local_fault_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(clear_local_fault0),
        .Q(clear_local_fault),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    loopback_reg_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_interface_1_n_23),
        .Q(mgt_loopback_r_reg),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    mdc_reg2_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(mdc_reg[2]),
        .Q(mdc_reg2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    mdc_reg3_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(mdc_reg2),
        .Q(mdc_reg3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \mdc_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(mdc),
        .Q(mdc_reg[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \mdc_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(mdc_reg[0]),
        .Q(mdc_reg[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \mdc_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(mdc_reg[1]),
        .Q(mdc_reg[2]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    mdc_rising_i_1
       (.I0(mdc_reg2),
        .I1(mdc_reg3),
        .O(mdc_rising_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mdc_rising_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(mdc_rising_i_1_n_0),
        .Q(mdc_rising),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    mdio_in_reg2_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_in_reg[2]),
        .Q(mdio_in_reg2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    mdio_in_reg3_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_in_reg2),
        .Q(mdio_in_reg3_reg_n_0),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \mdio_in_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_in),
        .Q(mdio_in_reg[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \mdio_in_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_in_reg[0]),
        .Q(mdio_in_reg[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \mdio_in_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_in_reg[1]),
        .Q(mdio_in_reg[2]),
        .R(1'b0));
  rxaui_0_mdio_interface mdio_interface_1
       (.CO(clear_local_fault2),
        .Q({signal_detect_reg[2],signal_detect_reg[0]}),
        .S({mdio_interface_1_n_2,mdio_interface_1_n_3,mdio_interface_1_n_4,mdio_interface_1_n_5}),
        .\addr_int_reg[15]_0 (test_en_reg3),
        .\addr_pma_int_reg[15]_0 (reset_reg3),
        .aligned_reg(aligned_reg),
        .aligned_sticky_reg(aligned_sticky_reg),
        .clear_aligned0(clear_aligned0),
        .clear_local_fault0(clear_local_fault0),
        .clear_local_fault_reg({mdio_interface_1_n_14,mdio_interface_1_n_15,mdio_interface_1_n_16,mdio_interface_1_n_17}),
        .clear_local_fault_reg_0({mdio_interface_1_n_20,mdio_interface_1_n_21}),
        .loopback_reg_reg(mdio_interface_1_n_23),
        .loopback_reg_reg_0(mgt_loopback_r_reg),
        .mdc_rising(mdc_rising),
        .mdio_in_reg3_reg(mdio_in_reg3_reg_n_0),
        .mdio_out(mdio_out),
        .mdio_tri(mdio_tri),
        .p_0_in(p_0_in),
        .powerdown_reg_reg({mdio_interface_1_n_8,mdio_interface_1_n_9}),
        .powerdown_reg_reg_0(mdio_interface_1_n_24),
        .powerdown_reg_reg_1(mgt_powerdown_r_reg),
        .prtad(prtad),
        .reset_reg_reg(mdio_interface_1_n_25),
        .reset_reg_reg_0(reset_reg_reg_0),
        .rx_local_fault_reg(rx_local_fault_reg),
        .\sync_reg_reg[3] (sync_reg),
        .test_en_reg_reg({mdio_interface_1_n_10,mdio_interface_1_n_11,mdio_interface_1_n_12,mdio_interface_1_n_13}),
        .test_en_reg_reg_0({mdio_interface_1_n_18,mdio_interface_1_n_19}),
        .test_en_reg_reg_1(mdio_interface_1_n_22),
        .test_en_reg_reg_2(test_pattern_en),
        .\test_sel_reg_reg[0] (mdio_interface_1_n_26),
        .\test_sel_reg_reg[0]_0 (test_pattern_sel[0]),
        .\test_sel_reg_reg[1] (mdio_interface_1_n_27),
        .\test_sel_reg_reg[1]_0 (test_pattern_sel[1]),
        .tx_local_fault_reg(tx_local_fault_reg),
        .\type_sel_reg_reg[0] (\type_sel_reg_reg[0] ),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    powerdown_reg_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_interface_1_n_24),
        .Q(mgt_powerdown_r_reg),
        .R(usrclk_reset));
  CARRY4 reset_reg3_carry
       (.CI(1'b0),
        .CO({reset_reg3_carry_n_0,reset_reg3_carry_n_1,reset_reg3_carry_n_2,reset_reg3_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_reset_reg3_carry_O_UNCONNECTED[3:0]),
        .S({mdio_interface_1_n_2,mdio_interface_1_n_3,mdio_interface_1_n_4,mdio_interface_1_n_5}));
  CARRY4 reset_reg3_carry__0
       (.CI(reset_reg3_carry_n_0),
        .CO({NLW_reset_reg3_carry__0_CO_UNCONNECTED[3:2],reset_reg3,reset_reg3_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_reset_reg3_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,mdio_interface_1_n_8,mdio_interface_1_n_9}));
  FDRE #(
    .INIT(1'b0)) 
    reset_reg_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_interface_1_n_25),
        .Q(reset_reg_reg_0),
        .R(usrclk_reset));
  FDRE rx_local_fault_reg_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(rx_local_fault),
        .Q(rx_local_fault_reg),
        .R(usrclk_reset));
  FDRE \signal_detect_reg_reg[0] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\signal_detect_int_reg[2] [0]),
        .Q(signal_detect_reg[0]),
        .R(usrclk_reset));
  FDRE \signal_detect_reg_reg[2] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\signal_detect_int_reg[2] [1]),
        .Q(signal_detect_reg[2]),
        .R(usrclk_reset));
  FDRE \sync_reg_reg[0] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(sync_status[0]),
        .Q(sync_reg[0]),
        .R(usrclk_reset));
  FDRE \sync_reg_reg[1] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(sync_status[1]),
        .Q(sync_reg[1]),
        .R(usrclk_reset));
  FDRE \sync_reg_reg[2] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(sync_status[2]),
        .Q(sync_reg[2]),
        .R(usrclk_reset));
  FDRE \sync_reg_reg[3] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(sync_status[3]),
        .Q(sync_reg[3]),
        .R(usrclk_reset));
  CARRY4 test_en_reg3_carry
       (.CI(1'b0),
        .CO({test_en_reg3_carry_n_0,test_en_reg3_carry_n_1,test_en_reg3_carry_n_2,test_en_reg3_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_test_en_reg3_carry_O_UNCONNECTED[3:0]),
        .S({mdio_interface_1_n_10,mdio_interface_1_n_11,mdio_interface_1_n_12,mdio_interface_1_n_13}));
  CARRY4 test_en_reg3_carry__0
       (.CI(test_en_reg3_carry_n_0),
        .CO({NLW_test_en_reg3_carry__0_CO_UNCONNECTED[3:2],test_en_reg3,test_en_reg3_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_test_en_reg3_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,mdio_interface_1_n_18,mdio_interface_1_n_19}));
  FDRE #(
    .INIT(1'b0)) 
    test_en_reg_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_interface_1_n_22),
        .Q(test_pattern_en),
        .R(usrclk_reset));
  FDRE \test_sel_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_interface_1_n_26),
        .Q(test_pattern_sel[0]),
        .R(usrclk_reset));
  FDRE \test_sel_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(mdio_interface_1_n_27),
        .Q(test_pattern_sel[1]),
        .R(usrclk_reset));
  FDRE tx_local_fault_reg_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(tx_local_fault),
        .Q(tx_local_fault_reg),
        .R(usrclk_reset));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT5 #(
    .INIT(32'hCACCFAFF)) 
    \txc_out[0]_i_1 
       (.I0(test_pattern_sel[1]),
        .I1(txc_filtered[0]),
        .I2(test_pattern_sel[0]),
        .I3(test_pattern_en),
        .I4(\state_reg[0][0] ),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hC0AAC0C0CFAACFCF)) 
    \txc_out[1]_i_1 
       (.I0(test_pattern_sel[1]),
        .I1(txc_filtered[1]),
        .I2(\state_reg[0][0] ),
        .I3(test_pattern_sel[0]),
        .I4(test_pattern_en),
        .I5(\state_reg[0][0]_0 ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hC0AAC0C0CFAACFCF)) 
    \txc_out[2]_i_1 
       (.I0(test_pattern_sel[1]),
        .I1(txc_filtered[2]),
        .I2(\state_reg[0][0] ),
        .I3(test_pattern_sel[0]),
        .I4(test_pattern_en),
        .I5(\state_reg[0][0]_0 ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hC0AAC0C0CFAACFCF)) 
    \txc_out[3]_i_1 
       (.I0(test_pattern_sel[1]),
        .I1(txc_filtered[3]),
        .I2(\state_reg[0][0] ),
        .I3(test_pattern_sel[0]),
        .I4(test_pattern_en),
        .I5(\state_reg[0][0]_0 ),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT5 #(
    .INIT(32'hCACCFAFF)) 
    \txc_out[4]_i_1 
       (.I0(test_pattern_sel[1]),
        .I1(txc_filtered[4]),
        .I2(test_pattern_sel[0]),
        .I3(test_pattern_en),
        .I4(\state_reg[1][2]_0 ),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \txc_out[7]_i_1 
       (.I0(test_pattern_sel[1]),
        .I1(test_pattern_en),
        .I2(test_pattern_sel[0]),
        .O(\txc_out_reg[7] ));
  LUT6 #(
    .INIT(64'hD500D5000000D500)) 
    \txd_out[27]_i_3 
       (.I0(test_pattern_en),
        .I1(test_pattern_sel[0]),
        .I2(test_pattern_sel[1]),
        .I3(Q),
        .I4(\state_reg[0][2] ),
        .I5(\state_reg[0][1] ),
        .O(\txd_out_reg[27] ));
  LUT6 #(
    .INIT(64'h70FFFF7070FF70FF)) 
    \txd_out[29]_i_1 
       (.I0(test_pattern_sel[1]),
        .I1(test_pattern_sel[0]),
        .I2(test_pattern_en),
        .I3(Q),
        .I4(\state_reg[0][1] ),
        .I5(\state_reg[0][2] ),
        .O(\txd_out_reg[5] ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'h28)) 
    \txd_out[56]_i_1 
       (.I0(test_pattern_en),
        .I1(test_pattern_sel[0]),
        .I2(test_pattern_sel[1]),
        .O(\txd_out_reg[56] ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \txd_out[59]_i_2 
       (.I0(test_pattern_sel[0]),
        .I1(test_pattern_sel[1]),
        .I2(test_pattern_en),
        .O(\txd_out_reg[59] ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \txd_out[59]_i_3 
       (.I0(test_pattern_sel[1]),
        .I1(test_pattern_sel[0]),
        .I2(test_pattern_en),
        .O(\txd_out_reg[59]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'h2A)) 
    \txd_out[60]_i_1 
       (.I0(test_pattern_en),
        .I1(test_pattern_sel[0]),
        .I2(test_pattern_sel[1]),
        .O(\txd_out_reg[34] ));
  LUT6 #(
    .INIT(64'h70FFFF707070FFFF)) 
    \txd_out[61]_i_1 
       (.I0(test_pattern_sel[1]),
        .I1(test_pattern_sel[0]),
        .I2(test_pattern_en),
        .I3(\state_reg[1][1] ),
        .I4(\state_reg[1][0] ),
        .I5(\state_reg[1][2] ),
        .O(\txd_out_reg[37] ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \txd_out[62]_i_1 
       (.I0(test_pattern_en),
        .I1(test_pattern_sel[0]),
        .O(\txd_out_reg[6] ));
  LUT2 #(
    .INIT(4'h2)) 
    \txd_out[62]_i_3 
       (.I0(test_pattern_en),
        .I1(test_pattern_sel[1]),
        .O(\txd_out_reg[62] ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    usrclk_reset_i_1
       (.I0(usrclk_reset_pipe),
        .I1(reset_reg_reg_0),
        .I2(reset),
        .O(usrclk_reset_reg));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT2 #(
    .INIT(4'hE)) 
    usrclk_reset_pipe_i_1
       (.I0(reset),
        .I1(reset_reg_reg_0),
        .O(reset_int));
endmodule

(* ORIG_REF_NAME = "mdio_interface" *) 
module rxaui_0_mdio_interface
   (mdio_out,
    mdio_tri,
    S,
    clear_local_fault0,
    clear_aligned0,
    powerdown_reg_reg,
    test_en_reg_reg,
    clear_local_fault_reg,
    test_en_reg_reg_0,
    clear_local_fault_reg_0,
    test_en_reg_reg_1,
    loopback_reg_reg,
    powerdown_reg_reg_0,
    reset_reg_reg,
    \test_sel_reg_reg[0] ,
    \test_sel_reg_reg[1] ,
    usrclk_reset,
    mdc_rising,
    mdio_in_reg3_reg,
    usrclk,
    \type_sel_reg_reg[0] ,
    p_0_in,
    CO,
    prtad,
    \addr_pma_int_reg[15]_0 ,
    Q,
    powerdown_reg_reg_1,
    reset_reg_reg_0,
    tx_local_fault_reg,
    \sync_reg_reg[3] ,
    rx_local_fault_reg,
    loopback_reg_reg_0,
    aligned_reg,
    aligned_sticky_reg,
    test_en_reg_reg_2,
    \test_sel_reg_reg[0]_0 ,
    \test_sel_reg_reg[1]_0 ,
    \addr_int_reg[15]_0 );
  output mdio_out;
  output mdio_tri;
  output [3:0]S;
  output clear_local_fault0;
  output clear_aligned0;
  output [1:0]powerdown_reg_reg;
  output [3:0]test_en_reg_reg;
  output [3:0]clear_local_fault_reg;
  output [1:0]test_en_reg_reg_0;
  output [1:0]clear_local_fault_reg_0;
  output test_en_reg_reg_1;
  output loopback_reg_reg;
  output powerdown_reg_reg_0;
  output reset_reg_reg;
  output \test_sel_reg_reg[0] ;
  output \test_sel_reg_reg[1] ;
  input usrclk_reset;
  input mdc_rising;
  input mdio_in_reg3_reg;
  input usrclk;
  input \type_sel_reg_reg[0] ;
  input p_0_in;
  input [0:0]CO;
  input [4:0]prtad;
  input [0:0]\addr_pma_int_reg[15]_0 ;
  input [1:0]Q;
  input powerdown_reg_reg_1;
  input reset_reg_reg_0;
  input tx_local_fault_reg;
  input [3:0]\sync_reg_reg[3] ;
  input rx_local_fault_reg;
  input loopback_reg_reg_0;
  input aligned_reg;
  input aligned_sticky_reg;
  input test_en_reg_reg_2;
  input \test_sel_reg_reg[0]_0 ;
  input \test_sel_reg_reg[1]_0 ;
  input [0:0]\addr_int_reg[15]_0 ;

  wire [0:0]CO;
  wire \FSM_sequential_state[0]_i_1_n_0 ;
  wire \FSM_sequential_state[0]_i_2_n_0 ;
  wire \FSM_sequential_state[0]_i_3_n_0 ;
  wire \FSM_sequential_state[1]_i_1_n_0 ;
  wire \FSM_sequential_state[1]_i_2_n_0 ;
  wire \FSM_sequential_state[1]_i_3_n_0 ;
  wire \FSM_sequential_state[2]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_2_n_0 ;
  wire \FSM_sequential_state[3]_i_10_n_0 ;
  wire \FSM_sequential_state[3]_i_11_n_0 ;
  wire \FSM_sequential_state[3]_i_12_n_0 ;
  wire \FSM_sequential_state[3]_i_1_n_0 ;
  wire \FSM_sequential_state[3]_i_2_n_0 ;
  wire \FSM_sequential_state[3]_i_3_n_0 ;
  wire \FSM_sequential_state[3]_i_5_n_0 ;
  wire \FSM_sequential_state[3]_i_6_n_0 ;
  wire \FSM_sequential_state[3]_i_7_n_0 ;
  wire \FSM_sequential_state[3]_i_8_n_0 ;
  wire \FSM_sequential_state[3]_i_9_n_0 ;
  wire [1:0]Q;
  wire [3:0]S;
  wire [15:0]addr;
  wire \addr_int[0]_i_1_n_0 ;
  wire \addr_int[10]_i_1_n_0 ;
  wire \addr_int[11]_i_1_n_0 ;
  wire \addr_int[12]_i_1_n_0 ;
  wire \addr_int[12]_i_3_n_0 ;
  wire \addr_int[12]_i_4_n_0 ;
  wire \addr_int[12]_i_5_n_0 ;
  wire \addr_int[12]_i_6_n_0 ;
  wire \addr_int[13]_i_1_n_0 ;
  wire \addr_int[14]_i_1_n_0 ;
  wire \addr_int[15]_i_10_n_0 ;
  wire \addr_int[15]_i_1_n_0 ;
  wire \addr_int[15]_i_2_n_0 ;
  wire \addr_int[15]_i_3_n_0 ;
  wire \addr_int[15]_i_5_n_0 ;
  wire \addr_int[15]_i_6_n_0 ;
  wire \addr_int[15]_i_7_n_0 ;
  wire \addr_int[15]_i_8_n_0 ;
  wire \addr_int[15]_i_9_n_0 ;
  wire \addr_int[1]_i_1_n_0 ;
  wire \addr_int[2]_i_1_n_0 ;
  wire \addr_int[3]_i_1_n_0 ;
  wire \addr_int[4]_i_1_n_0 ;
  wire \addr_int[4]_i_3_n_0 ;
  wire \addr_int[4]_i_4_n_0 ;
  wire \addr_int[4]_i_5_n_0 ;
  wire \addr_int[4]_i_6_n_0 ;
  wire \addr_int[5]_i_1_n_0 ;
  wire \addr_int[6]_i_1_n_0 ;
  wire \addr_int[7]_i_1_n_0 ;
  wire \addr_int[8]_i_1_n_0 ;
  wire \addr_int[8]_i_3_n_0 ;
  wire \addr_int[8]_i_4_n_0 ;
  wire \addr_int[8]_i_5_n_0 ;
  wire \addr_int[8]_i_6_n_0 ;
  wire \addr_int[9]_i_1_n_0 ;
  wire \addr_int_reg[12]_i_2_n_0 ;
  wire \addr_int_reg[12]_i_2_n_1 ;
  wire \addr_int_reg[12]_i_2_n_2 ;
  wire \addr_int_reg[12]_i_2_n_3 ;
  wire [0:0]\addr_int_reg[15]_0 ;
  wire \addr_int_reg[15]_i_4_n_2 ;
  wire \addr_int_reg[15]_i_4_n_3 ;
  wire \addr_int_reg[4]_i_2_n_0 ;
  wire \addr_int_reg[4]_i_2_n_1 ;
  wire \addr_int_reg[4]_i_2_n_2 ;
  wire \addr_int_reg[4]_i_2_n_3 ;
  wire \addr_int_reg[8]_i_2_n_0 ;
  wire \addr_int_reg[8]_i_2_n_1 ;
  wire \addr_int_reg[8]_i_2_n_2 ;
  wire \addr_int_reg[8]_i_2_n_3 ;
  wire addr_pma_int;
  wire \addr_pma_int[0]_i_1_n_0 ;
  wire \addr_pma_int[10]_i_1_n_0 ;
  wire \addr_pma_int[11]_i_1_n_0 ;
  wire \addr_pma_int[12]_i_1_n_0 ;
  wire \addr_pma_int[12]_i_3_n_0 ;
  wire \addr_pma_int[12]_i_4_n_0 ;
  wire \addr_pma_int[12]_i_5_n_0 ;
  wire \addr_pma_int[12]_i_6_n_0 ;
  wire \addr_pma_int[13]_i_1_n_0 ;
  wire \addr_pma_int[14]_i_1_n_0 ;
  wire \addr_pma_int[15]_i_10_n_0 ;
  wire \addr_pma_int[15]_i_11_n_0 ;
  wire \addr_pma_int[15]_i_2_n_0 ;
  wire \addr_pma_int[15]_i_3_n_0 ;
  wire \addr_pma_int[15]_i_4_n_0 ;
  wire \addr_pma_int[15]_i_6_n_0 ;
  wire \addr_pma_int[15]_i_7_n_0 ;
  wire \addr_pma_int[15]_i_8_n_0 ;
  wire \addr_pma_int[15]_i_9_n_0 ;
  wire \addr_pma_int[1]_i_1_n_0 ;
  wire \addr_pma_int[2]_i_1_n_0 ;
  wire \addr_pma_int[3]_i_1_n_0 ;
  wire \addr_pma_int[4]_i_1_n_0 ;
  wire \addr_pma_int[4]_i_3_n_0 ;
  wire \addr_pma_int[4]_i_4_n_0 ;
  wire \addr_pma_int[4]_i_5_n_0 ;
  wire \addr_pma_int[4]_i_6_n_0 ;
  wire \addr_pma_int[5]_i_1_n_0 ;
  wire \addr_pma_int[6]_i_1_n_0 ;
  wire \addr_pma_int[7]_i_1_n_0 ;
  wire \addr_pma_int[8]_i_1_n_0 ;
  wire \addr_pma_int[8]_i_3_n_0 ;
  wire \addr_pma_int[8]_i_4_n_0 ;
  wire \addr_pma_int[8]_i_5_n_0 ;
  wire \addr_pma_int[8]_i_6_n_0 ;
  wire \addr_pma_int[9]_i_1_n_0 ;
  wire \addr_pma_int_reg[12]_i_2_n_0 ;
  wire \addr_pma_int_reg[12]_i_2_n_1 ;
  wire \addr_pma_int_reg[12]_i_2_n_2 ;
  wire \addr_pma_int_reg[12]_i_2_n_3 ;
  wire \addr_pma_int_reg[12]_i_2_n_4 ;
  wire \addr_pma_int_reg[12]_i_2_n_5 ;
  wire \addr_pma_int_reg[12]_i_2_n_6 ;
  wire \addr_pma_int_reg[12]_i_2_n_7 ;
  wire [0:0]\addr_pma_int_reg[15]_0 ;
  wire \addr_pma_int_reg[15]_i_5_n_2 ;
  wire \addr_pma_int_reg[15]_i_5_n_3 ;
  wire \addr_pma_int_reg[15]_i_5_n_5 ;
  wire \addr_pma_int_reg[15]_i_5_n_6 ;
  wire \addr_pma_int_reg[15]_i_5_n_7 ;
  wire \addr_pma_int_reg[4]_i_2_n_0 ;
  wire \addr_pma_int_reg[4]_i_2_n_1 ;
  wire \addr_pma_int_reg[4]_i_2_n_2 ;
  wire \addr_pma_int_reg[4]_i_2_n_3 ;
  wire \addr_pma_int_reg[4]_i_2_n_4 ;
  wire \addr_pma_int_reg[4]_i_2_n_5 ;
  wire \addr_pma_int_reg[4]_i_2_n_6 ;
  wire \addr_pma_int_reg[4]_i_2_n_7 ;
  wire \addr_pma_int_reg[8]_i_2_n_0 ;
  wire \addr_pma_int_reg[8]_i_2_n_1 ;
  wire \addr_pma_int_reg[8]_i_2_n_2 ;
  wire \addr_pma_int_reg[8]_i_2_n_3 ;
  wire \addr_pma_int_reg[8]_i_2_n_4 ;
  wire \addr_pma_int_reg[8]_i_2_n_5 ;
  wire \addr_pma_int_reg[8]_i_2_n_6 ;
  wire \addr_pma_int_reg[8]_i_2_n_7 ;
  wire \addr_pma_int_reg_n_0_[0] ;
  wire \addr_pma_int_reg_n_0_[10] ;
  wire \addr_pma_int_reg_n_0_[11] ;
  wire \addr_pma_int_reg_n_0_[12] ;
  wire \addr_pma_int_reg_n_0_[13] ;
  wire \addr_pma_int_reg_n_0_[14] ;
  wire \addr_pma_int_reg_n_0_[15] ;
  wire \addr_pma_int_reg_n_0_[1] ;
  wire \addr_pma_int_reg_n_0_[2] ;
  wire \addr_pma_int_reg_n_0_[3] ;
  wire \addr_pma_int_reg_n_0_[4] ;
  wire \addr_pma_int_reg_n_0_[5] ;
  wire \addr_pma_int_reg_n_0_[6] ;
  wire \addr_pma_int_reg_n_0_[7] ;
  wire \addr_pma_int_reg_n_0_[8] ;
  wire \addr_pma_int_reg_n_0_[9] ;
  wire address_match;
  wire aligned_reg;
  wire aligned_sticky_reg;
  wire \bit_count[2]_i_1_n_0 ;
  wire \bit_count[4]_i_1_n_0 ;
  wire \bit_count[4]_i_5_n_0 ;
  wire \bit_count[4]_i_6_n_0 ;
  wire \bit_count[4]_i_7_n_0 ;
  wire \bit_count[4]_i_8_n_0 ;
  wire bit_count_load_en;
  wire [4:0]bit_count_load_value;
  wire [4:0]bit_count_reg__0;
  wire clear_aligned0;
  wire clear_aligned2;
  wire clear_aligned_i_4_n_0;
  wire clear_aligned_i_5_n_0;
  wire clear_aligned_i_6_n_0;
  wire clear_aligned_i_7_n_0;
  wire clear_aligned_i_8_n_0;
  wire clear_aligned_i_9_n_0;
  wire clear_aligned_reg_i_2_n_3;
  wire clear_aligned_reg_i_3_n_0;
  wire clear_aligned_reg_i_3_n_1;
  wire clear_aligned_reg_i_3_n_2;
  wire clear_aligned_reg_i_3_n_3;
  wire clear_local_fault0;
  wire [3:0]clear_local_fault_reg;
  wire [1:0]clear_local_fault_reg_0;
  wire [15:0]data_wr;
  wire [4:0]devad_reg;
  wire devad_reg0;
  wire \devad_reg[4]_i_2_n_0 ;
  wire loopback_reg_i_10_n_0;
  wire loopback_reg_i_11_n_0;
  wire loopback_reg_i_12_n_0;
  wire loopback_reg_i_13_n_0;
  wire loopback_reg_i_14_n_0;
  wire loopback_reg_i_15_n_0;
  wire loopback_reg_i_16_n_0;
  wire loopback_reg_i_2_n_0;
  wire loopback_reg_i_3_n_0;
  wire loopback_reg_i_4_n_0;
  wire loopback_reg_i_5_n_0;
  wire loopback_reg_i_7_n_0;
  wire loopback_reg_i_8_n_0;
  wire loopback_reg_reg;
  wire loopback_reg_reg_0;
  wire loopback_reg_reg_i_6_n_3;
  wire loopback_reg_reg_i_9_n_0;
  wire loopback_reg_reg_i_9_n_1;
  wire loopback_reg_reg_i_9_n_2;
  wire loopback_reg_reg_i_9_n_3;
  wire mdc_rising;
  wire mdio_in_reg;
  wire mdio_in_reg3_reg;
  wire mdio_out;
  wire mdio_out_i_2_n_0;
  wire mdio_out_i_3_n_0;
  wire mdio_out_int;
  wire mdio_tri;
  wire mdio_tri_int;
  wire mdio_we;
  wire [1:0]opcode;
  wire opcode0;
  wire \opcode[0]_i_1_n_0 ;
  wire \opcode[1]_i_1_n_0 ;
  wire \opcode[1]_i_3_n_0 ;
  wire p_0_in;
  wire [4:0]p_0_in__0;
  wire [15:1]plusOp;
  wire powerdown_reg_i_2_n_0;
  wire [1:0]powerdown_reg_reg;
  wire powerdown_reg_reg_0;
  wire powerdown_reg_reg_1;
  wire [4:0]prtad;
  wire rd;
  wire rd_i_1_n_0;
  wire reset_reg4;
  wire reset_reg_reg;
  wire reset_reg_reg_0;
  wire rx_local_fault_reg;
  wire \shift_reg[0]_i_1_n_0 ;
  wire \shift_reg[0]_i_2_n_0 ;
  wire \shift_reg[0]_i_3_n_0 ;
  wire \shift_reg[0]_i_4_n_0 ;
  wire \shift_reg[0]_i_5_n_0 ;
  wire \shift_reg[10]_i_1_n_0 ;
  wire \shift_reg[10]_i_2_n_0 ;
  wire \shift_reg[11]_i_1_n_0 ;
  wire \shift_reg[11]_i_2_n_0 ;
  wire \shift_reg[11]_i_3_n_0 ;
  wire \shift_reg[11]_i_4_n_0 ;
  wire \shift_reg[12]_i_1_n_0 ;
  wire \shift_reg[12]_i_2_n_0 ;
  wire \shift_reg[13]_i_1_n_0 ;
  wire \shift_reg[13]_i_2_n_0 ;
  wire \shift_reg[13]_i_3_n_0 ;
  wire \shift_reg[14]_i_1_n_0 ;
  wire \shift_reg[14]_i_2_n_0 ;
  wire \shift_reg[14]_i_3_n_0 ;
  wire \shift_reg[15]_i_1_n_0 ;
  wire \shift_reg[15]_i_2_n_0 ;
  wire \shift_reg[15]_i_3_n_0 ;
  wire \shift_reg[15]_i_4_n_0 ;
  wire \shift_reg[15]_i_5_n_0 ;
  wire \shift_reg[15]_i_6_n_0 ;
  wire \shift_reg[15]_i_7_n_0 ;
  wire \shift_reg[15]_i_8_n_0 ;
  wire \shift_reg[15]_i_9_n_0 ;
  wire \shift_reg[1]_i_1_n_0 ;
  wire \shift_reg[1]_i_2_n_0 ;
  wire \shift_reg[1]_i_3_n_0 ;
  wire \shift_reg[1]_i_4_n_0 ;
  wire \shift_reg[1]_i_5_n_0 ;
  wire \shift_reg[2]_i_1_n_0 ;
  wire \shift_reg[2]_i_2_n_0 ;
  wire \shift_reg[2]_i_3_n_0 ;
  wire \shift_reg[2]_i_4_n_0 ;
  wire \shift_reg[2]_i_5_n_0 ;
  wire \shift_reg[3]_i_1_n_0 ;
  wire \shift_reg[3]_i_2_n_0 ;
  wire \shift_reg[3]_i_3_n_0 ;
  wire \shift_reg[3]_i_4_n_0 ;
  wire \shift_reg[4]_i_1_n_0 ;
  wire \shift_reg[4]_i_2_n_0 ;
  wire \shift_reg[5]_i_1_n_0 ;
  wire \shift_reg[5]_i_2_n_0 ;
  wire \shift_reg[5]_i_3_n_0 ;
  wire \shift_reg[6]_i_1_n_0 ;
  wire \shift_reg[7]_i_1_n_0 ;
  wire \shift_reg[7]_i_2_n_0 ;
  wire \shift_reg[8]_i_1_n_0 ;
  wire \shift_reg[9]_i_1_n_0 ;
  (* RTL_KEEP = "yes" *) wire [3:0]state;
  wire [3:0]\sync_reg_reg[3] ;
  wire [3:0]test_en_reg_reg;
  wire [1:0]test_en_reg_reg_0;
  wire test_en_reg_reg_1;
  wire test_en_reg_reg_2;
  wire \test_sel_reg_reg[0] ;
  wire \test_sel_reg_reg[0]_0 ;
  wire \test_sel_reg_reg[1] ;
  wire \test_sel_reg_reg[1]_0 ;
  wire tx_local_fault_reg;
  wire \type_sel_reg_reg[0] ;
  wire usrclk;
  wire usrclk_reset;
  wire we_i_1_n_0;
  wire [3:2]\NLW_addr_int_reg[15]_i_4_CO_UNCONNECTED ;
  wire [3:3]\NLW_addr_int_reg[15]_i_4_O_UNCONNECTED ;
  wire [3:2]\NLW_addr_pma_int_reg[15]_i_5_CO_UNCONNECTED ;
  wire [3:3]\NLW_addr_pma_int_reg[15]_i_5_O_UNCONNECTED ;
  wire [3:2]NLW_clear_aligned_reg_i_2_CO_UNCONNECTED;
  wire [3:0]NLW_clear_aligned_reg_i_2_O_UNCONNECTED;
  wire [3:0]NLW_clear_aligned_reg_i_3_O_UNCONNECTED;
  wire [3:2]NLW_loopback_reg_reg_i_6_CO_UNCONNECTED;
  wire [3:0]NLW_loopback_reg_reg_i_6_O_UNCONNECTED;
  wire [3:0]NLW_loopback_reg_reg_i_9_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hFFFFFFFF80B090B0)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(\FSM_sequential_state[3]_i_3_n_0 ),
        .I1(state[0]),
        .I2(\FSM_sequential_state[0]_i_2_n_0 ),
        .I3(state[1]),
        .I4(\FSM_sequential_state[2]_i_2_n_0 ),
        .I5(\FSM_sequential_state[0]_i_3_n_0 ),
        .O(\FSM_sequential_state[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_state[0]_i_2 
       (.I0(state[2]),
        .I1(state[3]),
        .O(\FSM_sequential_state[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00320132)) 
    \FSM_sequential_state[0]_i_3 
       (.I0(state[3]),
        .I1(state[2]),
        .I2(mdio_in_reg),
        .I3(state[1]),
        .I4(state[0]),
        .O(\FSM_sequential_state[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF44444555)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(\FSM_sequential_state[1]_i_2_n_0 ),
        .I1(\FSM_sequential_state[3]_i_3_n_0 ),
        .I2(\FSM_sequential_state[2]_i_2_n_0 ),
        .I3(state[2]),
        .I4(state[0]),
        .I5(\FSM_sequential_state[1]_i_3_n_0 ),
        .O(\FSM_sequential_state[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hDDFFDDFD)) 
    \FSM_sequential_state[1]_i_2 
       (.I0(state[1]),
        .I1(state[3]),
        .I2(mdio_in_reg),
        .I3(state[2]),
        .I4(state[0]),
        .O(\FSM_sequential_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0202000012130000)) 
    \FSM_sequential_state[1]_i_3 
       (.I0(state[3]),
        .I1(state[1]),
        .I2(state[2]),
        .I3(mdio_in_reg),
        .I4(state[0]),
        .I5(\FSM_sequential_state[3]_i_3_n_0 ),
        .O(\FSM_sequential_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00AF00BF0FF00000)) 
    \FSM_sequential_state[2]_i_1 
       (.I0(\FSM_sequential_state[3]_i_3_n_0 ),
        .I1(\FSM_sequential_state[2]_i_2_n_0 ),
        .I2(state[1]),
        .I3(state[3]),
        .I4(state[0]),
        .I5(state[2]),
        .O(\FSM_sequential_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \FSM_sequential_state[2]_i_2 
       (.I0(opcode[1]),
        .I1(address_match),
        .O(\FSM_sequential_state[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4444444444454444)) 
    \FSM_sequential_state[3]_i_1 
       (.I0(state[0]),
        .I1(\FSM_sequential_state[3]_i_2_n_0 ),
        .I2(\FSM_sequential_state[3]_i_3_n_0 ),
        .I3(opcode[1]),
        .I4(address_match),
        .I5(mdio_out_i_3_n_0),
        .O(\FSM_sequential_state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h6FF6)) 
    \FSM_sequential_state[3]_i_10 
       (.I0(data_wr[7]),
        .I1(prtad[3]),
        .I2(data_wr[5]),
        .I3(prtad[1]),
        .O(\FSM_sequential_state[3]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_state[3]_i_11 
       (.I0(data_wr[2]),
        .I1(data_wr[3]),
        .O(\FSM_sequential_state[3]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_state[3]_i_12 
       (.I0(devad_reg[4]),
        .I1(devad_reg[3]),
        .O(\FSM_sequential_state[3]_i_12_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \FSM_sequential_state[3]_i_2 
       (.I0(state[2]),
        .I1(state[3]),
        .I2(state[1]),
        .O(\FSM_sequential_state[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_sequential_state[3]_i_3 
       (.I0(bit_count_reg__0[4]),
        .I1(bit_count_reg__0[3]),
        .I2(bit_count_reg__0[2]),
        .I3(bit_count_reg__0[1]),
        .I4(bit_count_reg__0[0]),
        .O(\FSM_sequential_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000820000220022)) 
    \FSM_sequential_state[3]_i_4 
       (.I0(\FSM_sequential_state[3]_i_5_n_0 ),
        .I1(\FSM_sequential_state[3]_i_6_n_0 ),
        .I2(\type_sel_reg_reg[0] ),
        .I3(p_0_in),
        .I4(\FSM_sequential_state[3]_i_7_n_0 ),
        .I5(\FSM_sequential_state[3]_i_8_n_0 ),
        .O(address_match));
  LUT6 #(
    .INIT(64'h0000010011110111)) 
    \FSM_sequential_state[3]_i_5 
       (.I0(\FSM_sequential_state[3]_i_9_n_0 ),
        .I1(\FSM_sequential_state[3]_i_10_n_0 ),
        .I2(\FSM_sequential_state[3]_i_11_n_0 ),
        .I3(loopback_reg_i_12_n_0),
        .I4(mdio_out_i_2_n_0),
        .I5(\FSM_sequential_state[3]_i_12_n_0 ),
        .O(\FSM_sequential_state[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h5455555557555555)) 
    \FSM_sequential_state[3]_i_6 
       (.I0(devad_reg[0]),
        .I1(mdio_out_i_2_n_0),
        .I2(state[0]),
        .I3(state[1]),
        .I4(\FSM_sequential_state[0]_i_2_n_0 ),
        .I5(mdio_in_reg),
        .O(\FSM_sequential_state[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hABAAAAAAA8AAAAAA)) 
    \FSM_sequential_state[3]_i_7 
       (.I0(devad_reg[1]),
        .I1(mdio_out_i_2_n_0),
        .I2(state[0]),
        .I3(state[1]),
        .I4(\FSM_sequential_state[0]_i_2_n_0 ),
        .I5(data_wr[0]),
        .O(\FSM_sequential_state[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hABAAAAAAA8AAAAAA)) 
    \FSM_sequential_state[3]_i_8 
       (.I0(devad_reg[2]),
        .I1(mdio_out_i_2_n_0),
        .I2(state[0]),
        .I3(state[1]),
        .I4(\FSM_sequential_state[0]_i_2_n_0 ),
        .I5(data_wr[1]),
        .O(\FSM_sequential_state[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    \FSM_sequential_state[3]_i_9 
       (.I0(data_wr[8]),
        .I1(prtad[4]),
        .I2(prtad[2]),
        .I3(data_wr[6]),
        .I4(prtad[0]),
        .I5(data_wr[4]),
        .O(\FSM_sequential_state[3]_i_9_n_0 ));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[0] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\FSM_sequential_state[0]_i_1_n_0 ),
        .Q(state[0]),
        .R(usrclk_reset));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\FSM_sequential_state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(usrclk_reset));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[2] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\FSM_sequential_state[2]_i_1_n_0 ),
        .Q(state[2]),
        .R(usrclk_reset));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[3] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\FSM_sequential_state[3]_i_1_n_0 ),
        .Q(state[3]),
        .R(usrclk_reset));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \addr_int[0]_i_1 
       (.I0(addr[0]),
        .I1(opcode[1]),
        .I2(mdio_in_reg),
        .O(\addr_int[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[10]_i_1 
       (.I0(plusOp[10]),
        .I1(opcode[1]),
        .I2(data_wr[9]),
        .O(\addr_int[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[11]_i_1 
       (.I0(plusOp[11]),
        .I1(opcode[1]),
        .I2(data_wr[10]),
        .O(\addr_int[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[12]_i_1 
       (.I0(plusOp[12]),
        .I1(opcode[1]),
        .I2(data_wr[11]),
        .O(\addr_int[12]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[12]_i_3 
       (.I0(addr[12]),
        .O(\addr_int[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[12]_i_4 
       (.I0(addr[11]),
        .O(\addr_int[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[12]_i_5 
       (.I0(addr[10]),
        .O(\addr_int[12]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[12]_i_6 
       (.I0(addr[9]),
        .O(\addr_int[12]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[13]_i_1 
       (.I0(plusOp[13]),
        .I1(opcode[1]),
        .I2(data_wr[12]),
        .O(\addr_int[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[14]_i_1 
       (.I0(plusOp[14]),
        .I1(opcode[1]),
        .I2(data_wr[13]),
        .O(\addr_int[14]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000010)) 
    \addr_int[15]_i_1 
       (.I0(loopback_reg_i_5_n_0),
        .I1(\addr_pma_int[15]_i_4_n_0 ),
        .I2(mdc_rising),
        .I3(opcode[0]),
        .I4(\addr_int[15]_i_3_n_0 ),
        .O(\addr_int[15]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[15]_i_10 
       (.I0(addr[13]),
        .O(\addr_int[15]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[15]_i_2 
       (.I0(plusOp[15]),
        .I1(opcode[1]),
        .I2(data_wr[14]),
        .O(\addr_int[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0200000000000000)) 
    \addr_int[15]_i_3 
       (.I0(\addr_int[15]_i_5_n_0 ),
        .I1(\addr_int[15]_i_6_n_0 ),
        .I2(\addr_int[15]_i_7_n_0 ),
        .I3(addr[2]),
        .I4(addr[6]),
        .I5(addr[9]),
        .O(\addr_int[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \addr_int[15]_i_5 
       (.I0(addr[3]),
        .I1(addr[4]),
        .I2(addr[15]),
        .I3(addr[13]),
        .I4(opcode[1]),
        .I5(addr[11]),
        .O(\addr_int[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \addr_int[15]_i_6 
       (.I0(addr[7]),
        .I1(addr[5]),
        .I2(addr[1]),
        .I3(addr[12]),
        .O(\addr_int[15]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \addr_int[15]_i_7 
       (.I0(addr[0]),
        .I1(addr[10]),
        .I2(addr[8]),
        .I3(addr[14]),
        .O(\addr_int[15]_i_7_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[15]_i_8 
       (.I0(addr[15]),
        .O(\addr_int[15]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[15]_i_9 
       (.I0(addr[14]),
        .O(\addr_int[15]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[1]_i_1 
       (.I0(plusOp[1]),
        .I1(opcode[1]),
        .I2(data_wr[0]),
        .O(\addr_int[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[2]_i_1 
       (.I0(plusOp[2]),
        .I1(opcode[1]),
        .I2(data_wr[1]),
        .O(\addr_int[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[3]_i_1 
       (.I0(plusOp[3]),
        .I1(opcode[1]),
        .I2(data_wr[2]),
        .O(\addr_int[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[4]_i_1 
       (.I0(plusOp[4]),
        .I1(opcode[1]),
        .I2(data_wr[3]),
        .O(\addr_int[4]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[4]_i_3 
       (.I0(addr[4]),
        .O(\addr_int[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[4]_i_4 
       (.I0(addr[3]),
        .O(\addr_int[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[4]_i_5 
       (.I0(addr[2]),
        .O(\addr_int[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[4]_i_6 
       (.I0(addr[1]),
        .O(\addr_int[4]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[5]_i_1 
       (.I0(plusOp[5]),
        .I1(opcode[1]),
        .I2(data_wr[4]),
        .O(\addr_int[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[6]_i_1 
       (.I0(plusOp[6]),
        .I1(opcode[1]),
        .I2(data_wr[5]),
        .O(\addr_int[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[7]_i_1 
       (.I0(plusOp[7]),
        .I1(opcode[1]),
        .I2(data_wr[6]),
        .O(\addr_int[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[8]_i_1 
       (.I0(plusOp[8]),
        .I1(opcode[1]),
        .I2(data_wr[7]),
        .O(\addr_int[8]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[8]_i_3 
       (.I0(addr[8]),
        .O(\addr_int[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[8]_i_4 
       (.I0(addr[7]),
        .O(\addr_int[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[8]_i_5 
       (.I0(addr[6]),
        .O(\addr_int[8]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_int[8]_i_6 
       (.I0(addr[5]),
        .O(\addr_int[8]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_int[9]_i_1 
       (.I0(plusOp[9]),
        .I1(opcode[1]),
        .I2(data_wr[8]),
        .O(\addr_int[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[0] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[0]_i_1_n_0 ),
        .Q(addr[0]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[10] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[10]_i_1_n_0 ),
        .Q(addr[10]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[11] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[11]_i_1_n_0 ),
        .Q(addr[11]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[12] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[12]_i_1_n_0 ),
        .Q(addr[12]),
        .R(usrclk_reset));
  CARRY4 \addr_int_reg[12]_i_2 
       (.CI(\addr_int_reg[8]_i_2_n_0 ),
        .CO({\addr_int_reg[12]_i_2_n_0 ,\addr_int_reg[12]_i_2_n_1 ,\addr_int_reg[12]_i_2_n_2 ,\addr_int_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[12:9]),
        .S({\addr_int[12]_i_3_n_0 ,\addr_int[12]_i_4_n_0 ,\addr_int[12]_i_5_n_0 ,\addr_int[12]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[13] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[13]_i_1_n_0 ),
        .Q(addr[13]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[14] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[14]_i_1_n_0 ),
        .Q(addr[14]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[15] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[15]_i_2_n_0 ),
        .Q(addr[15]),
        .R(usrclk_reset));
  CARRY4 \addr_int_reg[15]_i_4 
       (.CI(\addr_int_reg[12]_i_2_n_0 ),
        .CO({\NLW_addr_int_reg[15]_i_4_CO_UNCONNECTED [3:2],\addr_int_reg[15]_i_4_n_2 ,\addr_int_reg[15]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_addr_int_reg[15]_i_4_O_UNCONNECTED [3],plusOp[15:13]}),
        .S({1'b0,\addr_int[15]_i_8_n_0 ,\addr_int[15]_i_9_n_0 ,\addr_int[15]_i_10_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[1] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[1]_i_1_n_0 ),
        .Q(addr[1]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[2] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[2]_i_1_n_0 ),
        .Q(addr[2]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[3] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[3]_i_1_n_0 ),
        .Q(addr[3]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[4] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[4]_i_1_n_0 ),
        .Q(addr[4]),
        .R(usrclk_reset));
  CARRY4 \addr_int_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\addr_int_reg[4]_i_2_n_0 ,\addr_int_reg[4]_i_2_n_1 ,\addr_int_reg[4]_i_2_n_2 ,\addr_int_reg[4]_i_2_n_3 }),
        .CYINIT(addr[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[4:1]),
        .S({\addr_int[4]_i_3_n_0 ,\addr_int[4]_i_4_n_0 ,\addr_int[4]_i_5_n_0 ,\addr_int[4]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[5] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[5]_i_1_n_0 ),
        .Q(addr[5]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[6] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[6]_i_1_n_0 ),
        .Q(addr[6]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[7] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[7]_i_1_n_0 ),
        .Q(addr[7]),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[8] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[8]_i_1_n_0 ),
        .Q(addr[8]),
        .R(usrclk_reset));
  CARRY4 \addr_int_reg[8]_i_2 
       (.CI(\addr_int_reg[4]_i_2_n_0 ),
        .CO({\addr_int_reg[8]_i_2_n_0 ,\addr_int_reg[8]_i_2_n_1 ,\addr_int_reg[8]_i_2_n_2 ,\addr_int_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[8:5]),
        .S({\addr_int[8]_i_3_n_0 ,\addr_int[8]_i_4_n_0 ,\addr_int[8]_i_5_n_0 ,\addr_int[8]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \addr_int_reg[9] 
       (.C(usrclk),
        .CE(\addr_int[15]_i_1_n_0 ),
        .D(\addr_int[9]_i_1_n_0 ),
        .Q(addr[9]),
        .R(usrclk_reset));
  LUT3 #(
    .INIT(8'h74)) 
    \addr_pma_int[0]_i_1 
       (.I0(\addr_pma_int_reg_n_0_[0] ),
        .I1(opcode[1]),
        .I2(mdio_in_reg),
        .O(\addr_pma_int[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[10]_i_1 
       (.I0(\addr_pma_int_reg[12]_i_2_n_6 ),
        .I1(opcode[1]),
        .I2(data_wr[9]),
        .O(\addr_pma_int[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[11]_i_1 
       (.I0(\addr_pma_int_reg[12]_i_2_n_5 ),
        .I1(opcode[1]),
        .I2(data_wr[10]),
        .O(\addr_pma_int[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[12]_i_1 
       (.I0(\addr_pma_int_reg[12]_i_2_n_4 ),
        .I1(opcode[1]),
        .I2(data_wr[11]),
        .O(\addr_pma_int[12]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[12]_i_3 
       (.I0(\addr_pma_int_reg_n_0_[12] ),
        .O(\addr_pma_int[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[12]_i_4 
       (.I0(\addr_pma_int_reg_n_0_[11] ),
        .O(\addr_pma_int[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[12]_i_5 
       (.I0(\addr_pma_int_reg_n_0_[10] ),
        .O(\addr_pma_int[12]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[12]_i_6 
       (.I0(\addr_pma_int_reg_n_0_[9] ),
        .O(\addr_pma_int[12]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[13]_i_1 
       (.I0(\addr_pma_int_reg[15]_i_5_n_7 ),
        .I1(opcode[1]),
        .I2(data_wr[12]),
        .O(\addr_pma_int[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[14]_i_1 
       (.I0(\addr_pma_int_reg[15]_i_5_n_6 ),
        .I1(opcode[1]),
        .I2(data_wr[13]),
        .O(\addr_pma_int[14]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000200)) 
    \addr_pma_int[15]_i_1 
       (.I0(loopback_reg_i_5_n_0),
        .I1(\addr_pma_int[15]_i_3_n_0 ),
        .I2(\addr_pma_int[15]_i_4_n_0 ),
        .I3(mdc_rising),
        .I4(opcode[0]),
        .O(addr_pma_int));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[15]_i_10 
       (.I0(\addr_pma_int_reg_n_0_[14] ),
        .O(\addr_pma_int[15]_i_10_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[15]_i_11 
       (.I0(\addr_pma_int_reg_n_0_[13] ),
        .O(\addr_pma_int[15]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[15]_i_2 
       (.I0(\addr_pma_int_reg[15]_i_5_n_5 ),
        .I1(opcode[1]),
        .I2(data_wr[14]),
        .O(\addr_pma_int[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0200000000000000)) 
    \addr_pma_int[15]_i_3 
       (.I0(\addr_pma_int[15]_i_6_n_0 ),
        .I1(\addr_pma_int[15]_i_7_n_0 ),
        .I2(\addr_pma_int[15]_i_8_n_0 ),
        .I3(\addr_pma_int_reg_n_0_[8] ),
        .I4(\addr_pma_int_reg_n_0_[7] ),
        .I5(\addr_pma_int_reg_n_0_[4] ),
        .O(\addr_pma_int[15]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    \addr_pma_int[15]_i_4 
       (.I0(mdio_out_i_2_n_0),
        .I1(state[1]),
        .I2(state[3]),
        .I3(state[2]),
        .I4(state[0]),
        .O(\addr_pma_int[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \addr_pma_int[15]_i_6 
       (.I0(\addr_pma_int_reg_n_0_[0] ),
        .I1(\addr_pma_int_reg_n_0_[2] ),
        .I2(\addr_pma_int_reg_n_0_[10] ),
        .I3(\addr_pma_int_reg_n_0_[15] ),
        .I4(\addr_pma_int_reg_n_0_[13] ),
        .I5(\addr_pma_int_reg_n_0_[12] ),
        .O(\addr_pma_int[15]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \addr_pma_int[15]_i_7 
       (.I0(\addr_pma_int_reg_n_0_[11] ),
        .I1(\addr_pma_int_reg_n_0_[6] ),
        .I2(\addr_pma_int_reg_n_0_[3] ),
        .I3(\addr_pma_int_reg_n_0_[1] ),
        .O(\addr_pma_int[15]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \addr_pma_int[15]_i_8 
       (.I0(\addr_pma_int_reg_n_0_[9] ),
        .I1(opcode[1]),
        .I2(\addr_pma_int_reg_n_0_[14] ),
        .I3(\addr_pma_int_reg_n_0_[5] ),
        .O(\addr_pma_int[15]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[15]_i_9 
       (.I0(\addr_pma_int_reg_n_0_[15] ),
        .O(\addr_pma_int[15]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[1]_i_1 
       (.I0(\addr_pma_int_reg[4]_i_2_n_7 ),
        .I1(opcode[1]),
        .I2(data_wr[0]),
        .O(\addr_pma_int[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[2]_i_1 
       (.I0(\addr_pma_int_reg[4]_i_2_n_6 ),
        .I1(opcode[1]),
        .I2(data_wr[1]),
        .O(\addr_pma_int[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[3]_i_1 
       (.I0(\addr_pma_int_reg[4]_i_2_n_5 ),
        .I1(opcode[1]),
        .I2(data_wr[2]),
        .O(\addr_pma_int[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[4]_i_1 
       (.I0(\addr_pma_int_reg[4]_i_2_n_4 ),
        .I1(opcode[1]),
        .I2(data_wr[3]),
        .O(\addr_pma_int[4]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[4]_i_3 
       (.I0(\addr_pma_int_reg_n_0_[4] ),
        .O(\addr_pma_int[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[4]_i_4 
       (.I0(\addr_pma_int_reg_n_0_[3] ),
        .O(\addr_pma_int[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[4]_i_5 
       (.I0(\addr_pma_int_reg_n_0_[2] ),
        .O(\addr_pma_int[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[4]_i_6 
       (.I0(\addr_pma_int_reg_n_0_[1] ),
        .O(\addr_pma_int[4]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[5]_i_1 
       (.I0(\addr_pma_int_reg[8]_i_2_n_7 ),
        .I1(opcode[1]),
        .I2(data_wr[4]),
        .O(\addr_pma_int[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[6]_i_1 
       (.I0(\addr_pma_int_reg[8]_i_2_n_6 ),
        .I1(opcode[1]),
        .I2(data_wr[5]),
        .O(\addr_pma_int[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[7]_i_1 
       (.I0(\addr_pma_int_reg[8]_i_2_n_5 ),
        .I1(opcode[1]),
        .I2(data_wr[6]),
        .O(\addr_pma_int[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[8]_i_1 
       (.I0(\addr_pma_int_reg[8]_i_2_n_4 ),
        .I1(opcode[1]),
        .I2(data_wr[7]),
        .O(\addr_pma_int[8]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[8]_i_3 
       (.I0(\addr_pma_int_reg_n_0_[8] ),
        .O(\addr_pma_int[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[8]_i_4 
       (.I0(\addr_pma_int_reg_n_0_[7] ),
        .O(\addr_pma_int[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[8]_i_5 
       (.I0(\addr_pma_int_reg_n_0_[6] ),
        .O(\addr_pma_int[8]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \addr_pma_int[8]_i_6 
       (.I0(\addr_pma_int_reg_n_0_[5] ),
        .O(\addr_pma_int[8]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \addr_pma_int[9]_i_1 
       (.I0(\addr_pma_int_reg[12]_i_2_n_7 ),
        .I1(opcode[1]),
        .I2(data_wr[8]),
        .O(\addr_pma_int[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[0] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[0]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[0] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[10] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[10]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[10] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[11] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[11]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[11] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[12] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[12]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[12] ),
        .R(usrclk_reset));
  CARRY4 \addr_pma_int_reg[12]_i_2 
       (.CI(\addr_pma_int_reg[8]_i_2_n_0 ),
        .CO({\addr_pma_int_reg[12]_i_2_n_0 ,\addr_pma_int_reg[12]_i_2_n_1 ,\addr_pma_int_reg[12]_i_2_n_2 ,\addr_pma_int_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\addr_pma_int_reg[12]_i_2_n_4 ,\addr_pma_int_reg[12]_i_2_n_5 ,\addr_pma_int_reg[12]_i_2_n_6 ,\addr_pma_int_reg[12]_i_2_n_7 }),
        .S({\addr_pma_int[12]_i_3_n_0 ,\addr_pma_int[12]_i_4_n_0 ,\addr_pma_int[12]_i_5_n_0 ,\addr_pma_int[12]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[13] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[13]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[13] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[14] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[14]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[14] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[15] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[15]_i_2_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[15] ),
        .R(usrclk_reset));
  CARRY4 \addr_pma_int_reg[15]_i_5 
       (.CI(\addr_pma_int_reg[12]_i_2_n_0 ),
        .CO({\NLW_addr_pma_int_reg[15]_i_5_CO_UNCONNECTED [3:2],\addr_pma_int_reg[15]_i_5_n_2 ,\addr_pma_int_reg[15]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_addr_pma_int_reg[15]_i_5_O_UNCONNECTED [3],\addr_pma_int_reg[15]_i_5_n_5 ,\addr_pma_int_reg[15]_i_5_n_6 ,\addr_pma_int_reg[15]_i_5_n_7 }),
        .S({1'b0,\addr_pma_int[15]_i_9_n_0 ,\addr_pma_int[15]_i_10_n_0 ,\addr_pma_int[15]_i_11_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[1] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[1]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[1] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[2] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[2]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[2] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[3] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[3]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[3] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[4] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[4]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[4] ),
        .R(usrclk_reset));
  CARRY4 \addr_pma_int_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\addr_pma_int_reg[4]_i_2_n_0 ,\addr_pma_int_reg[4]_i_2_n_1 ,\addr_pma_int_reg[4]_i_2_n_2 ,\addr_pma_int_reg[4]_i_2_n_3 }),
        .CYINIT(\addr_pma_int_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\addr_pma_int_reg[4]_i_2_n_4 ,\addr_pma_int_reg[4]_i_2_n_5 ,\addr_pma_int_reg[4]_i_2_n_6 ,\addr_pma_int_reg[4]_i_2_n_7 }),
        .S({\addr_pma_int[4]_i_3_n_0 ,\addr_pma_int[4]_i_4_n_0 ,\addr_pma_int[4]_i_5_n_0 ,\addr_pma_int[4]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[5] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[5]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[5] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[6] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[6]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[6] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[7] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[7]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[7] ),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[8] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[8]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[8] ),
        .R(usrclk_reset));
  CARRY4 \addr_pma_int_reg[8]_i_2 
       (.CI(\addr_pma_int_reg[4]_i_2_n_0 ),
        .CO({\addr_pma_int_reg[8]_i_2_n_0 ,\addr_pma_int_reg[8]_i_2_n_1 ,\addr_pma_int_reg[8]_i_2_n_2 ,\addr_pma_int_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\addr_pma_int_reg[8]_i_2_n_4 ,\addr_pma_int_reg[8]_i_2_n_5 ,\addr_pma_int_reg[8]_i_2_n_6 ,\addr_pma_int_reg[8]_i_2_n_7 }),
        .S({\addr_pma_int[8]_i_3_n_0 ,\addr_pma_int[8]_i_4_n_0 ,\addr_pma_int[8]_i_5_n_0 ,\addr_pma_int[8]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \addr_pma_int_reg[9] 
       (.C(usrclk),
        .CE(addr_pma_int),
        .D(\addr_pma_int[9]_i_1_n_0 ),
        .Q(\addr_pma_int_reg_n_0_[9] ),
        .R(usrclk_reset));
  LUT3 #(
    .INIT(8'h8B)) 
    \bit_count[0]_i_1 
       (.I0(bit_count_load_value[0]),
        .I1(bit_count_load_en),
        .I2(bit_count_reg__0[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'hB88B)) 
    \bit_count[1]_i_1 
       (.I0(bit_count_load_value[0]),
        .I1(bit_count_load_en),
        .I2(bit_count_reg__0[1]),
        .I3(bit_count_reg__0[0]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'hFEAB)) 
    \bit_count[2]_i_1 
       (.I0(bit_count_load_en),
        .I1(bit_count_reg__0[1]),
        .I2(bit_count_reg__0[0]),
        .I3(bit_count_reg__0[2]),
        .O(\bit_count[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB88888888B)) 
    \bit_count[3]_i_1 
       (.I0(bit_count_load_value[0]),
        .I1(bit_count_load_en),
        .I2(bit_count_reg__0[2]),
        .I3(bit_count_reg__0[0]),
        .I4(bit_count_reg__0[1]),
        .I5(bit_count_reg__0[3]),
        .O(p_0_in__0[3]));
  LUT6 #(
    .INIT(64'h0000040000AA0055)) 
    \bit_count[3]_i_2 
       (.I0(state[0]),
        .I1(address_match),
        .I2(\bit_count[4]_i_7_n_0 ),
        .I3(state[2]),
        .I4(state[3]),
        .I5(state[1]),
        .O(bit_count_load_value[0]));
  LUT3 #(
    .INIT(8'hA8)) 
    \bit_count[4]_i_1 
       (.I0(mdc_rising),
        .I1(mdio_out_i_2_n_0),
        .I2(bit_count_load_en),
        .O(\bit_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8B8B8B8B88B)) 
    \bit_count[4]_i_2 
       (.I0(bit_count_load_value[4]),
        .I1(bit_count_load_en),
        .I2(bit_count_reg__0[4]),
        .I3(bit_count_reg__0[2]),
        .I4(\bit_count[4]_i_5_n_0 ),
        .I5(bit_count_reg__0[3]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFF88888A88)) 
    \bit_count[4]_i_3 
       (.I0(\FSM_sequential_state[0]_i_2_n_0 ),
        .I1(\bit_count[4]_i_6_n_0 ),
        .I2(state[0]),
        .I3(address_match),
        .I4(\bit_count[4]_i_7_n_0 ),
        .I5(\bit_count[4]_i_8_n_0 ),
        .O(bit_count_load_en));
  LUT4 #(
    .INIT(16'h0001)) 
    \bit_count[4]_i_4 
       (.I0(state[1]),
        .I1(state[3]),
        .I2(state[0]),
        .I3(state[2]),
        .O(bit_count_load_value[4]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \bit_count[4]_i_5 
       (.I0(bit_count_reg__0[1]),
        .I1(bit_count_reg__0[0]),
        .O(\bit_count[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \bit_count[4]_i_6 
       (.I0(bit_count_reg__0[2]),
        .I1(bit_count_reg__0[0]),
        .I2(bit_count_reg__0[1]),
        .I3(bit_count_reg__0[4]),
        .I4(bit_count_reg__0[3]),
        .I5(state[1]),
        .O(\bit_count[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \bit_count[4]_i_7 
       (.I0(bit_count_reg__0[2]),
        .I1(bit_count_reg__0[0]),
        .I2(bit_count_reg__0[1]),
        .I3(bit_count_reg__0[4]),
        .I4(bit_count_reg__0[3]),
        .I5(opcode[1]),
        .O(\bit_count[4]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h1003)) 
    \bit_count[4]_i_8 
       (.I0(state[2]),
        .I1(state[1]),
        .I2(state[3]),
        .I3(state[0]),
        .O(\bit_count[4]_i_8_n_0 ));
  FDRE \bit_count_reg[0] 
       (.C(usrclk),
        .CE(\bit_count[4]_i_1_n_0 ),
        .D(p_0_in__0[0]),
        .Q(bit_count_reg__0[0]),
        .R(1'b0));
  FDRE \bit_count_reg[1] 
       (.C(usrclk),
        .CE(\bit_count[4]_i_1_n_0 ),
        .D(p_0_in__0[1]),
        .Q(bit_count_reg__0[1]),
        .R(1'b0));
  FDRE \bit_count_reg[2] 
       (.C(usrclk),
        .CE(\bit_count[4]_i_1_n_0 ),
        .D(\bit_count[2]_i_1_n_0 ),
        .Q(bit_count_reg__0[2]),
        .R(1'b0));
  FDRE \bit_count_reg[3] 
       (.C(usrclk),
        .CE(\bit_count[4]_i_1_n_0 ),
        .D(p_0_in__0[3]),
        .Q(bit_count_reg__0[3]),
        .R(1'b0));
  FDRE \bit_count_reg[4] 
       (.C(usrclk),
        .CE(\bit_count[4]_i_1_n_0 ),
        .D(p_0_in__0[4]),
        .Q(bit_count_reg__0[4]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h08)) 
    clear_aligned_i_1
       (.I0(clear_aligned2),
        .I1(rd),
        .I2(loopback_reg_i_5_n_0),
        .O(clear_aligned0));
  LUT1 #(
    .INIT(2'h1)) 
    clear_aligned_i_4
       (.I0(addr[15]),
        .O(clear_aligned_i_4_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    clear_aligned_i_5
       (.I0(addr[14]),
        .I1(addr[13]),
        .I2(addr[12]),
        .O(clear_aligned_i_5_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    clear_aligned_i_6
       (.I0(addr[9]),
        .I1(addr[11]),
        .I2(addr[10]),
        .O(clear_aligned_i_6_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    clear_aligned_i_7
       (.I0(addr[7]),
        .I1(addr[8]),
        .I2(addr[6]),
        .O(clear_aligned_i_7_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    clear_aligned_i_8
       (.I0(addr[5]),
        .I1(addr[3]),
        .I2(addr[4]),
        .O(clear_aligned_i_8_n_0));
  LUT3 #(
    .INIT(8'h02)) 
    clear_aligned_i_9
       (.I0(addr[0]),
        .I1(addr[2]),
        .I2(addr[1]),
        .O(clear_aligned_i_9_n_0));
  CARRY4 clear_aligned_reg_i_2
       (.CI(clear_aligned_reg_i_3_n_0),
        .CO({NLW_clear_aligned_reg_i_2_CO_UNCONNECTED[3:2],clear_aligned2,clear_aligned_reg_i_2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_clear_aligned_reg_i_2_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,clear_aligned_i_4_n_0,clear_aligned_i_5_n_0}));
  CARRY4 clear_aligned_reg_i_3
       (.CI(1'b0),
        .CO({clear_aligned_reg_i_3_n_0,clear_aligned_reg_i_3_n_1,clear_aligned_reg_i_3_n_2,clear_aligned_reg_i_3_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_clear_aligned_reg_i_3_O_UNCONNECTED[3:0]),
        .S({clear_aligned_i_6_n_0,clear_aligned_i_7_n_0,clear_aligned_i_8_n_0,clear_aligned_i_9_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    clear_local_fault2_carry__0_i_1
       (.I0(addr[15]),
        .O(clear_local_fault_reg_0[1]));
  LUT3 #(
    .INIT(8'h01)) 
    clear_local_fault2_carry__0_i_2
       (.I0(addr[14]),
        .I1(addr[13]),
        .I2(addr[12]),
        .O(clear_local_fault_reg_0[0]));
  LUT3 #(
    .INIT(8'h01)) 
    clear_local_fault2_carry_i_1
       (.I0(addr[9]),
        .I1(addr[11]),
        .I2(addr[10]),
        .O(clear_local_fault_reg[3]));
  LUT3 #(
    .INIT(8'h01)) 
    clear_local_fault2_carry_i_2
       (.I0(addr[7]),
        .I1(addr[8]),
        .I2(addr[6]),
        .O(clear_local_fault_reg[2]));
  LUT3 #(
    .INIT(8'h04)) 
    clear_local_fault2_carry_i_3
       (.I0(addr[4]),
        .I1(addr[3]),
        .I2(addr[5]),
        .O(clear_local_fault_reg[1]));
  LUT3 #(
    .INIT(8'h01)) 
    clear_local_fault2_carry_i_4
       (.I0(addr[0]),
        .I1(addr[2]),
        .I2(addr[1]),
        .O(clear_local_fault_reg[0]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h08)) 
    clear_local_fault_i_1
       (.I0(CO),
        .I1(rd),
        .I2(loopback_reg_i_5_n_0),
        .O(clear_local_fault0));
  LUT2 #(
    .INIT(4'h2)) 
    \devad_reg[4]_i_1 
       (.I0(mdc_rising),
        .I1(\devad_reg[4]_i_2_n_0 ),
        .O(devad_reg0));
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    \devad_reg[4]_i_2 
       (.I0(mdio_out_i_2_n_0),
        .I1(state[0]),
        .I2(state[1]),
        .I3(state[2]),
        .I4(state[3]),
        .O(\devad_reg[4]_i_2_n_0 ));
  FDRE \devad_reg_reg[0] 
       (.C(usrclk),
        .CE(devad_reg0),
        .D(mdio_in_reg),
        .Q(devad_reg[0]),
        .R(usrclk_reset));
  FDRE \devad_reg_reg[1] 
       (.C(usrclk),
        .CE(devad_reg0),
        .D(data_wr[0]),
        .Q(devad_reg[1]),
        .R(usrclk_reset));
  FDRE \devad_reg_reg[2] 
       (.C(usrclk),
        .CE(devad_reg0),
        .D(data_wr[1]),
        .Q(devad_reg[2]),
        .R(usrclk_reset));
  FDRE \devad_reg_reg[3] 
       (.C(usrclk),
        .CE(devad_reg0),
        .D(data_wr[2]),
        .Q(devad_reg[3]),
        .R(usrclk_reset));
  FDRE \devad_reg_reg[4] 
       (.C(usrclk),
        .CE(devad_reg0),
        .D(data_wr[3]),
        .Q(devad_reg[4]),
        .R(usrclk_reset));
  LUT6 #(
    .INIT(64'hBFBBBFBF80888080)) 
    loopback_reg_i_1
       (.I0(loopback_reg_i_2_n_0),
        .I1(mdc_rising),
        .I2(loopback_reg_i_3_n_0),
        .I3(loopback_reg_i_4_n_0),
        .I4(loopback_reg_i_5_n_0),
        .I5(loopback_reg_reg_0),
        .O(loopback_reg_reg));
  LUT1 #(
    .INIT(2'h1)) 
    loopback_reg_i_10
       (.I0(addr[15]),
        .O(loopback_reg_i_10_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    loopback_reg_i_11
       (.I0(addr[14]),
        .I1(addr[13]),
        .I2(addr[12]),
        .O(loopback_reg_i_11_n_0));
  LUT4 #(
    .INIT(16'h0040)) 
    loopback_reg_i_12
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[1]),
        .I3(state[0]),
        .O(loopback_reg_i_12_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    loopback_reg_i_13
       (.I0(addr[9]),
        .I1(addr[11]),
        .I2(addr[10]),
        .O(loopback_reg_i_13_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    loopback_reg_i_14
       (.I0(addr[7]),
        .I1(addr[8]),
        .I2(addr[6]),
        .O(loopback_reg_i_14_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    loopback_reg_i_15
       (.I0(addr[5]),
        .I1(addr[3]),
        .I2(addr[4]),
        .O(loopback_reg_i_15_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    loopback_reg_i_16
       (.I0(addr[0]),
        .I1(addr[2]),
        .I2(addr[1]),
        .O(loopback_reg_i_16_n_0));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    loopback_reg_i_2
       (.I0(data_wr[14]),
        .I1(p_0_in),
        .I2(data_wr[0]),
        .O(loopback_reg_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h80)) 
    loopback_reg_i_3
       (.I0(p_0_in),
        .I1(reset_reg4),
        .I2(mdio_we),
        .O(loopback_reg_i_3_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    loopback_reg_i_4
       (.I0(\addr_pma_int_reg[15]_0 ),
        .I1(mdio_we),
        .O(loopback_reg_i_4_n_0));
  LUT5 #(
    .INIT(32'h00001015)) 
    loopback_reg_i_5
       (.I0(loopback_reg_i_7_n_0),
        .I1(devad_reg[1]),
        .I2(\devad_reg[4]_i_2_n_0 ),
        .I3(data_wr[0]),
        .I4(loopback_reg_i_8_n_0),
        .O(loopback_reg_i_5_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFF3FFF355)) 
    loopback_reg_i_7
       (.I0(mdio_in_reg),
        .I1(devad_reg[0]),
        .I2(devad_reg[2]),
        .I3(\devad_reg[4]_i_2_n_0 ),
        .I4(data_wr[1]),
        .I5(p_0_in),
        .O(loopback_reg_i_7_n_0));
  LUT6 #(
    .INIT(64'hEFEEEFEEEFEEE0EE)) 
    loopback_reg_i_8
       (.I0(devad_reg[4]),
        .I1(devad_reg[3]),
        .I2(mdio_out_i_2_n_0),
        .I3(loopback_reg_i_12_n_0),
        .I4(data_wr[2]),
        .I5(data_wr[3]),
        .O(loopback_reg_i_8_n_0));
  CARRY4 loopback_reg_reg_i_6
       (.CI(loopback_reg_reg_i_9_n_0),
        .CO({NLW_loopback_reg_reg_i_6_CO_UNCONNECTED[3:2],reset_reg4,loopback_reg_reg_i_6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_loopback_reg_reg_i_6_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,loopback_reg_i_10_n_0,loopback_reg_i_11_n_0}));
  CARRY4 loopback_reg_reg_i_9
       (.CI(1'b0),
        .CO({loopback_reg_reg_i_9_n_0,loopback_reg_reg_i_9_n_1,loopback_reg_reg_i_9_n_2,loopback_reg_reg_i_9_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_loopback_reg_reg_i_9_O_UNCONNECTED[3:0]),
        .S({loopback_reg_i_13_n_0,loopback_reg_i_14_n_0,loopback_reg_i_15_n_0,loopback_reg_i_16_n_0}));
  FDRE mdio_in_reg_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(mdio_in_reg3_reg),
        .Q(mdio_in_reg),
        .R(usrclk_reset));
  LUT5 #(
    .INIT(32'hFCFFEEFF)) 
    mdio_out_i_1
       (.I0(mdio_out_i_2_n_0),
        .I1(mdio_out_i_3_n_0),
        .I2(data_wr[15]),
        .I3(opcode[1]),
        .I4(state[0]),
        .O(mdio_out_int));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    mdio_out_i_2
       (.I0(bit_count_reg__0[3]),
        .I1(bit_count_reg__0[4]),
        .I2(bit_count_reg__0[1]),
        .I3(bit_count_reg__0[0]),
        .I4(bit_count_reg__0[2]),
        .O(mdio_out_i_2_n_0));
  LUT3 #(
    .INIT(8'hDF)) 
    mdio_out_i_3
       (.I0(state[1]),
        .I1(state[3]),
        .I2(state[2]),
        .O(mdio_out_i_3_n_0));
  FDRE mdio_out_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(mdio_out_int),
        .Q(mdio_out),
        .R(usrclk_reset));
  LUT6 #(
    .INIT(64'hFFFF5DFFFFFFFFFF)) 
    mdio_tri_i_1
       (.I0(opcode[1]),
        .I1(mdio_out_i_2_n_0),
        .I2(state[0]),
        .I3(state[1]),
        .I4(state[3]),
        .I5(state[2]),
        .O(mdio_tri_int));
  FDSE mdio_tri_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(mdio_tri_int),
        .Q(mdio_tri),
        .S(usrclk_reset));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \opcode[0]_i_1 
       (.I0(data_wr[0]),
        .I1(opcode0),
        .I2(opcode[0]),
        .O(\opcode[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \opcode[1]_i_1 
       (.I0(data_wr[1]),
        .I1(opcode0),
        .I2(opcode[1]),
        .O(\opcode[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \opcode[1]_i_2 
       (.I0(bit_count_reg__0[1]),
        .I1(bit_count_reg__0[0]),
        .I2(bit_count_reg__0[4]),
        .I3(bit_count_reg__0[3]),
        .I4(\opcode[1]_i_3_n_0 ),
        .O(opcode0));
  LUT6 #(
    .INIT(64'hFFBFFFFFFFFFFFFF)) 
    \opcode[1]_i_3 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(state[2]),
        .I3(state[3]),
        .I4(mdc_rising),
        .I5(bit_count_reg__0[2]),
        .O(\opcode[1]_i_3_n_0 ));
  FDRE \opcode_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\opcode[0]_i_1_n_0 ),
        .Q(opcode[0]),
        .R(usrclk_reset));
  FDRE \opcode_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\opcode[1]_i_1_n_0 ),
        .Q(opcode[1]),
        .R(usrclk_reset));
  LUT6 #(
    .INIT(64'hFFFBBBFB00088808)) 
    powerdown_reg_i_1
       (.I0(data_wr[11]),
        .I1(mdc_rising),
        .I2(powerdown_reg_i_2_n_0),
        .I3(loopback_reg_i_5_n_0),
        .I4(loopback_reg_i_4_n_0),
        .I5(powerdown_reg_reg_1),
        .O(powerdown_reg_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT2 #(
    .INIT(4'h7)) 
    powerdown_reg_i_2
       (.I0(mdio_we),
        .I1(reset_reg4),
        .O(powerdown_reg_i_2_n_0));
  LUT5 #(
    .INIT(32'h00002E22)) 
    rd_i_1
       (.I0(rd),
        .I1(mdc_rising),
        .I2(\addr_pma_int[15]_i_4_n_0 ),
        .I3(opcode[1]),
        .I4(usrclk_reset),
        .O(rd_i_1_n_0));
  FDRE rd_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(rd_i_1_n_0),
        .Q(rd),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    reset_reg3_carry__0_i_1
       (.I0(\addr_pma_int_reg_n_0_[15] ),
        .O(powerdown_reg_reg[1]));
  LUT3 #(
    .INIT(8'h01)) 
    reset_reg3_carry__0_i_2
       (.I0(\addr_pma_int_reg_n_0_[13] ),
        .I1(\addr_pma_int_reg_n_0_[14] ),
        .I2(\addr_pma_int_reg_n_0_[12] ),
        .O(powerdown_reg_reg[0]));
  LUT3 #(
    .INIT(8'h01)) 
    reset_reg3_carry_i_1
       (.I0(\addr_pma_int_reg_n_0_[9] ),
        .I1(\addr_pma_int_reg_n_0_[11] ),
        .I2(\addr_pma_int_reg_n_0_[10] ),
        .O(S[3]));
  LUT3 #(
    .INIT(8'h01)) 
    reset_reg3_carry_i_2
       (.I0(\addr_pma_int_reg_n_0_[7] ),
        .I1(\addr_pma_int_reg_n_0_[8] ),
        .I2(\addr_pma_int_reg_n_0_[6] ),
        .O(S[2]));
  LUT3 #(
    .INIT(8'h01)) 
    reset_reg3_carry_i_3
       (.I0(\addr_pma_int_reg_n_0_[3] ),
        .I1(\addr_pma_int_reg_n_0_[5] ),
        .I2(\addr_pma_int_reg_n_0_[4] ),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h01)) 
    reset_reg3_carry_i_4
       (.I0(\addr_pma_int_reg_n_0_[0] ),
        .I1(\addr_pma_int_reg_n_0_[2] ),
        .I2(\addr_pma_int_reg_n_0_[1] ),
        .O(S[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFF02A20000)) 
    reset_reg_i_1
       (.I0(mdc_rising),
        .I1(powerdown_reg_i_2_n_0),
        .I2(loopback_reg_i_5_n_0),
        .I3(loopback_reg_i_4_n_0),
        .I4(data_wr[15]),
        .I5(reset_reg_reg_0),
        .O(reset_reg_reg));
  LUT6 #(
    .INIT(64'hF0F0F1F100FF1111)) 
    \shift_reg[0]_i_1 
       (.I0(\shift_reg[0]_i_2_n_0 ),
        .I1(\shift_reg[2]_i_3_n_0 ),
        .I2(mdio_in_reg),
        .I3(\shift_reg[0]_i_3_n_0 ),
        .I4(loopback_reg_i_5_n_0),
        .I5(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBB8B8B8BBBBBB)) 
    \shift_reg[0]_i_2 
       (.I0(\shift_reg[0]_i_4_n_0 ),
        .I1(addr[0]),
        .I2(addr[1]),
        .I3(\sync_reg_reg[3] [0]),
        .I4(addr[3]),
        .I5(addr[2]),
        .O(\shift_reg[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \shift_reg[0]_i_3 
       (.I0(\shift_reg[15]_i_7_n_0 ),
        .I1(\addr_pma_int_reg_n_0_[0] ),
        .I2(\shift_reg[0]_i_5_n_0 ),
        .O(\shift_reg[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hFBCFFBFF)) 
    \shift_reg[0]_i_4 
       (.I0(p_0_in),
        .I1(addr[1]),
        .I2(addr[3]),
        .I3(addr[2]),
        .I4(\test_sel_reg_reg[0]_0 ),
        .O(\shift_reg[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFFFFFFFAAAA1111)) 
    \shift_reg[0]_i_5 
       (.I0(\addr_pma_int_reg_n_0_[2] ),
        .I1(loopback_reg_reg_0),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\addr_pma_int_reg_n_0_[3] ),
        .I5(\addr_pma_int_reg_n_0_[1] ),
        .O(\shift_reg[0]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA0003)) 
    \shift_reg[10]_i_1 
       (.I0(data_wr[9]),
        .I1(\shift_reg[10]_i_2_n_0 ),
        .I2(loopback_reg_i_5_n_0),
        .I3(\shift_reg[15]_i_3_n_0 ),
        .I4(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFFFFFFFFFF)) 
    \shift_reg[10]_i_2 
       (.I0(addr[0]),
        .I1(addr[2]),
        .I2(addr[1]),
        .I3(addr[3]),
        .I4(addr[4]),
        .I5(rx_local_fault_reg),
        .O(\shift_reg[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hCCCC0F55)) 
    \shift_reg[11]_i_1 
       (.I0(\shift_reg[11]_i_2_n_0 ),
        .I1(data_wr[10]),
        .I2(\shift_reg[11]_i_3_n_0 ),
        .I3(loopback_reg_i_5_n_0),
        .I4(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAFBAAFBF)) 
    \shift_reg[11]_i_2 
       (.I0(\shift_reg[15]_i_3_n_0 ),
        .I1(tx_local_fault_reg),
        .I2(addr[3]),
        .I3(addr[4]),
        .I4(powerdown_reg_reg_1),
        .I5(\shift_reg[11]_i_4_n_0 ),
        .O(\shift_reg[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \shift_reg[11]_i_3 
       (.I0(\addr_pma_int_reg_n_0_[1] ),
        .I1(\addr_pma_int_reg_n_0_[2] ),
        .I2(\addr_pma_int_reg_n_0_[3] ),
        .I3(\shift_reg[15]_i_7_n_0 ),
        .I4(powerdown_reg_reg_1),
        .I5(\addr_pma_int_reg_n_0_[0] ),
        .O(\shift_reg[11]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \shift_reg[11]_i_4 
       (.I0(addr[1]),
        .I1(addr[2]),
        .I2(addr[0]),
        .O(\shift_reg[11]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA0003)) 
    \shift_reg[12]_i_1 
       (.I0(data_wr[11]),
        .I1(\shift_reg[12]_i_2_n_0 ),
        .I2(loopback_reg_i_5_n_0),
        .I3(\shift_reg[15]_i_3_n_0 ),
        .I4(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFEFFFFFFFFFFFFFF)) 
    \shift_reg[12]_i_2 
       (.I0(addr[0]),
        .I1(addr[2]),
        .I2(addr[1]),
        .I3(addr[4]),
        .I4(addr[3]),
        .I5(aligned_reg),
        .O(\shift_reg[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF101F0000101F)) 
    \shift_reg[13]_i_1 
       (.I0(\addr_pma_int_reg_n_0_[3] ),
        .I1(\shift_reg[13]_i_2_n_0 ),
        .I2(loopback_reg_i_5_n_0),
        .I3(\shift_reg[13]_i_3_n_0 ),
        .I4(\shift_reg[15]_i_5_n_0 ),
        .I5(data_wr[12]),
        .O(\shift_reg[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \shift_reg[13]_i_2 
       (.I0(\shift_reg[15]_i_7_n_0 ),
        .I1(\addr_pma_int_reg_n_0_[1] ),
        .I2(\addr_pma_int_reg_n_0_[2] ),
        .I3(\addr_pma_int_reg_n_0_[0] ),
        .O(\shift_reg[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \shift_reg[13]_i_3 
       (.I0(\shift_reg[15]_i_3_n_0 ),
        .I1(addr[4]),
        .I2(addr[3]),
        .I3(addr[1]),
        .I4(addr[2]),
        .I5(addr[0]),
        .O(\shift_reg[13]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \shift_reg[14]_i_1 
       (.I0(data_wr[13]),
        .I1(\shift_reg[15]_i_5_n_0 ),
        .I2(\shift_reg[14]_i_2_n_0 ),
        .I3(\shift_reg[15]_i_3_n_0 ),
        .O(\shift_reg[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \shift_reg[14]_i_2 
       (.I0(loopback_reg_reg_0),
        .I1(addr[1]),
        .I2(p_0_in),
        .I3(\shift_reg[14]_i_3_n_0 ),
        .I4(addr[0]),
        .I5(addr[2]),
        .O(\shift_reg[14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \shift_reg[14]_i_3 
       (.I0(addr[4]),
        .I1(addr[3]),
        .O(\shift_reg[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF0F0F0F000FF1111)) 
    \shift_reg[15]_i_1 
       (.I0(\shift_reg[15]_i_2_n_0 ),
        .I1(\shift_reg[15]_i_3_n_0 ),
        .I2(data_wr[14]),
        .I3(\shift_reg[15]_i_4_n_0 ),
        .I4(loopback_reg_i_5_n_0),
        .I5(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFEFEFF)) 
    \shift_reg[15]_i_2 
       (.I0(addr[0]),
        .I1(addr[2]),
        .I2(addr[1]),
        .I3(addr[3]),
        .I4(reset_reg_reg_0),
        .I5(addr[4]),
        .O(\shift_reg[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \shift_reg[15]_i_3 
       (.I0(addr[14]),
        .I1(addr[13]),
        .I2(addr[12]),
        .I3(addr[5]),
        .I4(addr[15]),
        .I5(\shift_reg[15]_i_6_n_0 ),
        .O(\shift_reg[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF1)) 
    \shift_reg[15]_i_4 
       (.I0(reset_reg_reg_0),
        .I1(\addr_pma_int_reg_n_0_[3] ),
        .I2(\addr_pma_int_reg_n_0_[0] ),
        .I3(\addr_pma_int_reg_n_0_[2] ),
        .I4(\addr_pma_int_reg_n_0_[1] ),
        .I5(\shift_reg[15]_i_7_n_0 ),
        .O(\shift_reg[15]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \shift_reg[15]_i_5 
       (.I0(\devad_reg[4]_i_2_n_0 ),
        .I1(opcode[1]),
        .O(\shift_reg[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \shift_reg[15]_i_6 
       (.I0(addr[9]),
        .I1(addr[11]),
        .I2(addr[10]),
        .I3(addr[7]),
        .I4(addr[8]),
        .I5(addr[6]),
        .O(\shift_reg[15]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \shift_reg[15]_i_7 
       (.I0(\addr_pma_int_reg_n_0_[15] ),
        .I1(\addr_pma_int_reg_n_0_[5] ),
        .I2(\addr_pma_int_reg_n_0_[4] ),
        .I3(\shift_reg[15]_i_8_n_0 ),
        .I4(\shift_reg[15]_i_9_n_0 ),
        .O(\shift_reg[15]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \shift_reg[15]_i_8 
       (.I0(\addr_pma_int_reg_n_0_[6] ),
        .I1(\addr_pma_int_reg_n_0_[8] ),
        .I2(\addr_pma_int_reg_n_0_[7] ),
        .O(\shift_reg[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \shift_reg[15]_i_9 
       (.I0(\addr_pma_int_reg_n_0_[9] ),
        .I1(\addr_pma_int_reg_n_0_[11] ),
        .I2(\addr_pma_int_reg_n_0_[10] ),
        .I3(\addr_pma_int_reg_n_0_[13] ),
        .I4(\addr_pma_int_reg_n_0_[14] ),
        .I5(\addr_pma_int_reg_n_0_[12] ),
        .O(\shift_reg[15]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hF0F0F0F000FF1111)) 
    \shift_reg[1]_i_1 
       (.I0(\shift_reg[15]_i_3_n_0 ),
        .I1(\shift_reg[1]_i_2_n_0 ),
        .I2(data_wr[0]),
        .I3(\shift_reg[1]_i_3_n_0 ),
        .I4(loopback_reg_i_5_n_0),
        .I5(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBABABBBABBBBBBBA)) 
    \shift_reg[1]_i_2 
       (.I0(addr[1]),
        .I1(\shift_reg[1]_i_4_n_0 ),
        .I2(\shift_reg[1]_i_5_n_0 ),
        .I3(p_0_in),
        .I4(addr[4]),
        .I5(\sync_reg_reg[3] [1]),
        .O(\shift_reg[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEEEEFFBFFFFF)) 
    \shift_reg[1]_i_3 
       (.I0(\shift_reg[15]_i_7_n_0 ),
        .I1(\addr_pma_int_reg_n_0_[3] ),
        .I2(Q[0]),
        .I3(\addr_pma_int_reg_n_0_[2] ),
        .I4(\addr_pma_int_reg_n_0_[1] ),
        .I5(\addr_pma_int_reg_n_0_[0] ),
        .O(\shift_reg[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0088000000000CCC)) 
    \shift_reg[1]_i_4 
       (.I0(\test_sel_reg_reg[1]_0 ),
        .I1(addr[0]),
        .I2(p_0_in),
        .I3(addr[2]),
        .I4(addr[3]),
        .I5(addr[4]),
        .O(\shift_reg[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \shift_reg[1]_i_5 
       (.I0(addr[2]),
        .I1(addr[0]),
        .I2(addr[3]),
        .O(\shift_reg[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hF0F0F1F100FF1111)) 
    \shift_reg[2]_i_1 
       (.I0(\shift_reg[2]_i_2_n_0 ),
        .I1(\shift_reg[2]_i_3_n_0 ),
        .I2(data_wr[1]),
        .I3(\shift_reg[2]_i_4_n_0 ),
        .I4(loopback_reg_i_5_n_0),
        .I5(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCCCFDDFFFFCFDDFF)) 
    \shift_reg[2]_i_2 
       (.I0(\sync_reg_reg[3] [2]),
        .I1(\shift_reg[2]_i_5_n_0 ),
        .I2(aligned_sticky_reg),
        .I3(addr[3]),
        .I4(addr[0]),
        .I5(test_en_reg_reg_2),
        .O(\shift_reg[2]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hEFFE)) 
    \shift_reg[2]_i_3 
       (.I0(\shift_reg[15]_i_3_n_0 ),
        .I1(\shift_reg[15]_i_5_n_0 ),
        .I2(addr[3]),
        .I3(addr[4]),
        .O(\shift_reg[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hEEFFFFEEFFBFFFFF)) 
    \shift_reg[2]_i_4 
       (.I0(\shift_reg[15]_i_7_n_0 ),
        .I1(\addr_pma_int_reg_n_0_[3] ),
        .I2(Q[0]),
        .I3(\addr_pma_int_reg_n_0_[2] ),
        .I4(\addr_pma_int_reg_n_0_[1] ),
        .I5(\addr_pma_int_reg_n_0_[0] ),
        .O(\shift_reg[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \shift_reg[2]_i_5 
       (.I0(addr[1]),
        .I1(addr[2]),
        .O(\shift_reg[2]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hCCCC0F55)) 
    \shift_reg[3]_i_1 
       (.I0(\shift_reg[3]_i_2_n_0 ),
        .I1(data_wr[2]),
        .I2(\shift_reg[3]_i_3_n_0 ),
        .I3(loopback_reg_i_5_n_0),
        .I4(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hFEFFFFEF)) 
    \shift_reg[3]_i_2 
       (.I0(\shift_reg[15]_i_3_n_0 ),
        .I1(\shift_reg[3]_i_4_n_0 ),
        .I2(addr[3]),
        .I3(addr[0]),
        .I4(addr[2]),
        .O(\shift_reg[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFDBFFFBF)) 
    \shift_reg[3]_i_3 
       (.I0(\addr_pma_int_reg_n_0_[1] ),
        .I1(\addr_pma_int_reg_n_0_[0] ),
        .I2(\addr_pma_int_reg_n_0_[2] ),
        .I3(\addr_pma_int_reg_n_0_[3] ),
        .I4(Q[1]),
        .I5(\shift_reg[15]_i_7_n_0 ),
        .O(\shift_reg[3]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hAFFEFFFE)) 
    \shift_reg[3]_i_4 
       (.I0(addr[1]),
        .I1(p_0_in),
        .I2(addr[4]),
        .I3(addr[3]),
        .I4(\sync_reg_reg[3] [3]),
        .O(\shift_reg[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hF2F2F2F222FF2222)) 
    \shift_reg[4]_i_1 
       (.I0(\type_sel_reg_reg[0] ),
        .I1(\shift_reg[5]_i_2_n_0 ),
        .I2(data_wr[3]),
        .I3(\shift_reg[4]_i_2_n_0 ),
        .I4(loopback_reg_i_5_n_0),
        .I5(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFF2FFFF)) 
    \shift_reg[4]_i_2 
       (.I0(\addr_pma_int_reg_n_0_[1] ),
        .I1(Q[1]),
        .I2(\addr_pma_int_reg_n_0_[2] ),
        .I3(\shift_reg[15]_i_7_n_0 ),
        .I4(\addr_pma_int_reg_n_0_[3] ),
        .I5(\addr_pma_int_reg_n_0_[0] ),
        .O(\shift_reg[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h888F)) 
    \shift_reg[5]_i_1 
       (.I0(\shift_reg[15]_i_5_n_0 ),
        .I1(data_wr[4]),
        .I2(\shift_reg[5]_i_2_n_0 ),
        .I3(\type_sel_reg_reg[0] ),
        .O(\shift_reg[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEFF)) 
    \shift_reg[5]_i_2 
       (.I0(\shift_reg[15]_i_3_n_0 ),
        .I1(\shift_reg[15]_i_5_n_0 ),
        .I2(addr[4]),
        .I3(p_0_in),
        .I4(addr[1]),
        .I5(\shift_reg[5]_i_3_n_0 ),
        .O(\shift_reg[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \shift_reg[5]_i_3 
       (.I0(addr[2]),
        .I1(addr[3]),
        .I2(addr[0]),
        .O(\shift_reg[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF011F000F011F0FF)) 
    \shift_reg[6]_i_1 
       (.I0(\addr_pma_int_reg_n_0_[3] ),
        .I1(\shift_reg[13]_i_2_n_0 ),
        .I2(data_wr[5]),
        .I3(\shift_reg[15]_i_5_n_0 ),
        .I4(loopback_reg_i_5_n_0),
        .I5(\shift_reg[13]_i_3_n_0 ),
        .O(\shift_reg[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA0003)) 
    \shift_reg[7]_i_1 
       (.I0(data_wr[6]),
        .I1(\shift_reg[7]_i_2_n_0 ),
        .I2(loopback_reg_i_5_n_0),
        .I3(\shift_reg[15]_i_3_n_0 ),
        .I4(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFEFEFEFFF)) 
    \shift_reg[7]_i_2 
       (.I0(addr[1]),
        .I1(addr[2]),
        .I2(addr[0]),
        .I3(tx_local_fault_reg),
        .I4(rx_local_fault_reg),
        .I5(\shift_reg[14]_i_3_n_0 ),
        .O(\shift_reg[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_reg[8]_i_1 
       (.I0(data_wr[7]),
        .I1(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_reg[9]_i_1 
       (.I0(data_wr[8]),
        .I1(\shift_reg[15]_i_5_n_0 ),
        .O(\shift_reg[9]_i_1_n_0 ));
  FDRE \shift_reg_reg[0] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[0]_i_1_n_0 ),
        .Q(data_wr[0]),
        .R(1'b0));
  FDRE \shift_reg_reg[10] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[10]_i_1_n_0 ),
        .Q(data_wr[10]),
        .R(1'b0));
  FDRE \shift_reg_reg[11] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[11]_i_1_n_0 ),
        .Q(data_wr[11]),
        .R(1'b0));
  FDRE \shift_reg_reg[12] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[12]_i_1_n_0 ),
        .Q(data_wr[12]),
        .R(1'b0));
  FDRE \shift_reg_reg[13] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[13]_i_1_n_0 ),
        .Q(data_wr[13]),
        .R(1'b0));
  FDRE \shift_reg_reg[14] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[14]_i_1_n_0 ),
        .Q(data_wr[14]),
        .R(1'b0));
  FDRE \shift_reg_reg[15] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[15]_i_1_n_0 ),
        .Q(data_wr[15]),
        .R(1'b0));
  FDRE \shift_reg_reg[1] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[1]_i_1_n_0 ),
        .Q(data_wr[1]),
        .R(1'b0));
  FDRE \shift_reg_reg[2] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[2]_i_1_n_0 ),
        .Q(data_wr[2]),
        .R(1'b0));
  FDRE \shift_reg_reg[3] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[3]_i_1_n_0 ),
        .Q(data_wr[3]),
        .R(1'b0));
  FDRE \shift_reg_reg[4] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[4]_i_1_n_0 ),
        .Q(data_wr[4]),
        .R(1'b0));
  FDRE \shift_reg_reg[5] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[5]_i_1_n_0 ),
        .Q(data_wr[5]),
        .R(1'b0));
  FDRE \shift_reg_reg[6] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[6]_i_1_n_0 ),
        .Q(data_wr[6]),
        .R(1'b0));
  FDRE \shift_reg_reg[7] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[7]_i_1_n_0 ),
        .Q(data_wr[7]),
        .R(1'b0));
  FDRE \shift_reg_reg[8] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[8]_i_1_n_0 ),
        .Q(data_wr[8]),
        .R(1'b0));
  FDRE \shift_reg_reg[9] 
       (.C(usrclk),
        .CE(mdc_rising),
        .D(\shift_reg[9]_i_1_n_0 ),
        .Q(data_wr[9]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    test_en_reg3_carry__0_i_1
       (.I0(addr[15]),
        .O(test_en_reg_reg_0[1]));
  LUT3 #(
    .INIT(8'h01)) 
    test_en_reg3_carry__0_i_2
       (.I0(addr[14]),
        .I1(addr[13]),
        .I2(addr[12]),
        .O(test_en_reg_reg_0[0]));
  LUT3 #(
    .INIT(8'h01)) 
    test_en_reg3_carry_i_1
       (.I0(addr[9]),
        .I1(addr[11]),
        .I2(addr[10]),
        .O(test_en_reg_reg[3]));
  LUT3 #(
    .INIT(8'h01)) 
    test_en_reg3_carry_i_2
       (.I0(addr[7]),
        .I1(addr[8]),
        .I2(addr[6]),
        .O(test_en_reg_reg[2]));
  LUT3 #(
    .INIT(8'h08)) 
    test_en_reg3_carry_i_3
       (.I0(addr[3]),
        .I1(addr[4]),
        .I2(addr[5]),
        .O(test_en_reg_reg[1]));
  LUT3 #(
    .INIT(8'h02)) 
    test_en_reg3_carry_i_4
       (.I0(addr[0]),
        .I1(addr[2]),
        .I2(addr[1]),
        .O(test_en_reg_reg[0]));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    test_en_reg_i_1
       (.I0(data_wr[2]),
        .I1(loopback_reg_i_5_n_0),
        .I2(mdc_rising),
        .I3(mdio_we),
        .I4(\addr_int_reg[15]_0 ),
        .I5(test_en_reg_reg_2),
        .O(test_en_reg_reg_1));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    \test_sel_reg[0]_i_1 
       (.I0(data_wr[0]),
        .I1(loopback_reg_i_5_n_0),
        .I2(mdc_rising),
        .I3(mdio_we),
        .I4(\addr_int_reg[15]_0 ),
        .I5(\test_sel_reg_reg[0]_0 ),
        .O(\test_sel_reg_reg[0] ));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    \test_sel_reg[1]_i_1 
       (.I0(data_wr[1]),
        .I1(loopback_reg_i_5_n_0),
        .I2(mdc_rising),
        .I3(mdio_we),
        .I4(\addr_int_reg[15]_0 ),
        .I5(\test_sel_reg_reg[1]_0 ),
        .O(\test_sel_reg_reg[1] ));
  LUT3 #(
    .INIT(8'h04)) 
    we_i_1
       (.I0(opcode[1]),
        .I1(opcode[0]),
        .I2(\addr_pma_int[15]_i_4_n_0 ),
        .O(we_i_1_n_0));
  FDRE we_reg
       (.C(usrclk),
        .CE(mdc_rising),
        .D(we_i_1_n_0),
        .Q(mdio_we),
        .R(usrclk_reset));
endmodule

(* ORIG_REF_NAME = "rx" *) 
module rxaui_0_rx
   (mgt_enchansync,
    align_status_int,
    mgt_enable_align_i,
    xgmii_rxd,
    xgmii_rxc,
    in0,
    usrclk,
    Q,
    usrclk_reset,
    \core_mgt_rx_reset_reg[0] ,
    mgt_rxlock,
    \core_mgt_rx_reset_reg[1] ,
    D,
    \mgt_rxdata_reg_reg[24] ,
    \mgt_rxdata_reg_reg[31] ,
    \mgt_rxcharisk_reg_reg[3] ,
    \mgt_rxdata_reg_reg[24]_0 ,
    \mgt_rxdata_reg_reg[30] ,
    \mgt_rxdata_reg_reg[14] ,
    \mgt_codecomma_reg_reg[3] ,
    \mgt_codevalid_reg_reg[3] ,
    \mgt_codecomma_reg_reg[3]_0 ,
    \mgt_codevalid_reg_reg[3]_0 );
  output mgt_enchansync;
  output align_status_int;
  output [3:0]mgt_enable_align_i;
  output [63:0]xgmii_rxd;
  output [7:0]xgmii_rxc;
  output [3:0]in0;
  input usrclk;
  input [1:0]Q;
  input usrclk_reset;
  input \core_mgt_rx_reset_reg[0] ;
  input [0:0]mgt_rxlock;
  input \core_mgt_rx_reset_reg[1] ;
  input [1:0]D;
  input [1:0]\mgt_rxdata_reg_reg[24] ;
  input [63:0]\mgt_rxdata_reg_reg[31] ;
  input [7:0]\mgt_rxcharisk_reg_reg[3] ;
  input [7:0]\mgt_rxdata_reg_reg[24]_0 ;
  input [7:0]\mgt_rxdata_reg_reg[30] ;
  input [3:0]\mgt_rxdata_reg_reg[14] ;
  input [3:0]\mgt_codecomma_reg_reg[3] ;
  input [3:0]\mgt_codevalid_reg_reg[3] ;
  input [3:0]\mgt_codecomma_reg_reg[3]_0 ;
  input [3:0]\mgt_codevalid_reg_reg[3]_0 ;

  wire [1:0]D;
  wire \G_PCS_SYNC_STATE[0].pcs_sync_state_n_1 ;
  wire \G_PCS_SYNC_STATE[0].pcs_sync_state_n_2 ;
  wire \G_PCS_SYNC_STATE[0].pcs_sync_state_n_3 ;
  wire \G_PCS_SYNC_STATE[0].pcs_sync_state_n_4 ;
  wire \G_PCS_SYNC_STATE[0].pcs_sync_state_n_5 ;
  wire \G_PCS_SYNC_STATE[1].pcs_sync_state_n_1 ;
  wire \G_PCS_SYNC_STATE[1].pcs_sync_state_n_2 ;
  wire \G_PCS_SYNC_STATE[1].pcs_sync_state_n_3 ;
  wire \G_PCS_SYNC_STATE[1].pcs_sync_state_n_4 ;
  wire \G_PCS_SYNC_STATE[2].pcs_sync_state_n_1 ;
  wire \G_PCS_SYNC_STATE[2].pcs_sync_state_n_2 ;
  wire \G_PCS_SYNC_STATE[2].pcs_sync_state_n_3 ;
  wire \G_PCS_SYNC_STATE[2].pcs_sync_state_n_4 ;
  wire \G_PCS_SYNC_STATE[3].pcs_sync_state_n_1 ;
  wire \G_PCS_SYNC_STATE[3].pcs_sync_state_n_2 ;
  wire \G_PCS_SYNC_STATE[3].pcs_sync_state_n_3 ;
  wire [1:0]Q;
  wire align_status_int;
  wire \core_mgt_rx_reset_reg[0] ;
  wire \core_mgt_rx_reset_reg[1] ;
  wire [3:0]in0;
  wire local_fault;
  wire [3:0]\mgt_codecomma_reg_reg[3] ;
  wire [3:0]\mgt_codecomma_reg_reg[3]_0 ;
  wire [3:0]\mgt_codevalid_reg_reg[3] ;
  wire [3:0]\mgt_codevalid_reg_reg[3]_0 ;
  wire [3:0]mgt_enable_align_i;
  wire mgt_enchansync;
  wire [7:0]\mgt_rxcharisk_reg_reg[3] ;
  wire [3:0]\mgt_rxdata_reg_reg[14] ;
  wire [1:0]\mgt_rxdata_reg_reg[24] ;
  wire [7:0]\mgt_rxdata_reg_reg[24]_0 ;
  wire [7:0]\mgt_rxdata_reg_reg[30] ;
  wire [63:0]\mgt_rxdata_reg_reg[31] ;
  wire [0:0]mgt_rxlock;
  wire sync_status;
  wire sync_status_i_2_n_0;
  wire sync_status_int;
  wire usrclk;
  wire usrclk_reset;
  wire [7:0]xgmii_rxc;
  wire [63:0]xgmii_rxd;

  rxaui_0_sync_state_machine \G_PCS_SYNC_STATE[0].pcs_sync_state 
       (.Q(Q[0]),
        .SR(\G_PCS_SYNC_STATE[0].pcs_sync_state_n_1 ),
        .\core_mgt_rx_reset_reg[0] (\core_mgt_rx_reset_reg[0] ),
        .\mgt_codecomma_reg_reg[2] ({\mgt_codecomma_reg_reg[3] [2],\mgt_codecomma_reg_reg[3] [0]}),
        .\mgt_codevalid_reg_reg[2] ({\mgt_codevalid_reg_reg[3] [2],\mgt_codevalid_reg_reg[3] [0]}),
        .mgt_enable_align_i(mgt_enable_align_i[0]),
        .mgt_rxlock(mgt_rxlock),
        .out({\G_PCS_SYNC_STATE[0].pcs_sync_state_n_2 ,\G_PCS_SYNC_STATE[0].pcs_sync_state_n_3 ,\G_PCS_SYNC_STATE[0].pcs_sync_state_n_4 }),
        .\sync_ok_reg[0] (\G_PCS_SYNC_STATE[0].pcs_sync_state_n_5 ),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset));
  rxaui_0_sync_state_machine_19 \G_PCS_SYNC_STATE[1].pcs_sync_state 
       (.Q(Q[0]),
        .SR(\G_PCS_SYNC_STATE[0].pcs_sync_state_n_1 ),
        .\mgt_codecomma_reg_reg[3] ({\mgt_codecomma_reg_reg[3] [3],\mgt_codecomma_reg_reg[3] [1]}),
        .\mgt_codevalid_reg_reg[3] ({\mgt_codevalid_reg_reg[3] [3],\mgt_codevalid_reg_reg[3] [1]}),
        .mgt_enable_align_i(mgt_enable_align_i[1]),
        .out({\G_PCS_SYNC_STATE[1].pcs_sync_state_n_1 ,\G_PCS_SYNC_STATE[1].pcs_sync_state_n_2 ,\G_PCS_SYNC_STATE[1].pcs_sync_state_n_3 }),
        .\sync_ok_reg[1] (\G_PCS_SYNC_STATE[1].pcs_sync_state_n_4 ),
        .usrclk(usrclk));
  rxaui_0_sync_state_machine_20 \G_PCS_SYNC_STATE[2].pcs_sync_state 
       (.Q(Q[1]),
        .SR(\G_PCS_SYNC_STATE[3].pcs_sync_state_n_1 ),
        .\mgt_codecomma_reg_reg[2] ({\mgt_codecomma_reg_reg[3]_0 [2],\mgt_codecomma_reg_reg[3]_0 [0]}),
        .\mgt_codevalid_reg_reg[2] ({\mgt_codevalid_reg_reg[3]_0 [2],\mgt_codevalid_reg_reg[3]_0 [0]}),
        .mgt_enable_align_i(mgt_enable_align_i[2]),
        .out({\G_PCS_SYNC_STATE[2].pcs_sync_state_n_1 ,\G_PCS_SYNC_STATE[2].pcs_sync_state_n_2 ,\G_PCS_SYNC_STATE[2].pcs_sync_state_n_3 }),
        .\sync_ok_reg[2] (\G_PCS_SYNC_STATE[2].pcs_sync_state_n_4 ),
        .usrclk(usrclk));
  rxaui_0_sync_state_machine_21 \G_PCS_SYNC_STATE[3].pcs_sync_state 
       (.Q(Q[1]),
        .SR(\G_PCS_SYNC_STATE[3].pcs_sync_state_n_1 ),
        .\core_mgt_rx_reset_reg[1] (\core_mgt_rx_reset_reg[1] ),
        .\mgt_codecomma_reg_reg[3] ({\mgt_codecomma_reg_reg[3]_0 [3],\mgt_codecomma_reg_reg[3]_0 [1]}),
        .\mgt_codevalid_reg_reg[3] ({\mgt_codevalid_reg_reg[3]_0 [3],\mgt_codevalid_reg_reg[3]_0 [1]}),
        .mgt_enable_align_i(mgt_enable_align_i[3]),
        .mgt_rxlock(mgt_rxlock),
        .out(\G_PCS_SYNC_STATE[3].pcs_sync_state_n_2 ),
        .\sync_ok_reg[3] (\G_PCS_SYNC_STATE[3].pcs_sync_state_n_3 ),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset));
  rxaui_0_deskew_state_machine deskew_state
       (.D(D),
        .\debug[5] (align_status_int),
        .local_fault(local_fault),
        .mgt_enchansync(mgt_enchansync),
        .\mgt_rxdata_reg_reg[24] (\mgt_rxdata_reg_reg[24] ),
        .sync_status(sync_status),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset));
  rxaui_0_rx_recoder recoder
       (.align_status_reg(align_status_int),
        .local_fault(local_fault),
        .\mgt_rxcharisk_reg_reg[3] (\mgt_rxcharisk_reg_reg[3] ),
        .\mgt_rxdata_reg_reg[14] (\mgt_rxdata_reg_reg[14] ),
        .\mgt_rxdata_reg_reg[24] (\mgt_rxdata_reg_reg[24]_0 ),
        .\mgt_rxdata_reg_reg[30] (\mgt_rxdata_reg_reg[30] ),
        .\mgt_rxdata_reg_reg[31] (\mgt_rxdata_reg_reg[31] ),
        .usrclk(usrclk),
        .xgmii_rxc(xgmii_rxc),
        .xgmii_rxd(xgmii_rxd));
  FDSE \sync_ok_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\G_PCS_SYNC_STATE[0].pcs_sync_state_n_5 ),
        .Q(in0[0]),
        .S(\G_PCS_SYNC_STATE[0].pcs_sync_state_n_2 ));
  FDSE \sync_ok_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\G_PCS_SYNC_STATE[1].pcs_sync_state_n_4 ),
        .Q(in0[1]),
        .S(\G_PCS_SYNC_STATE[1].pcs_sync_state_n_1 ));
  FDSE \sync_ok_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\G_PCS_SYNC_STATE[2].pcs_sync_state_n_4 ),
        .Q(in0[2]),
        .S(\G_PCS_SYNC_STATE[2].pcs_sync_state_n_1 ));
  FDSE \sync_ok_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\G_PCS_SYNC_STATE[3].pcs_sync_state_n_3 ),
        .Q(in0[3]),
        .S(\G_PCS_SYNC_STATE[3].pcs_sync_state_n_2 ));
  LUT6 #(
    .INIT(64'h00000000EEEEEEE0)) 
    sync_status_i_1
       (.I0(\G_PCS_SYNC_STATE[3].pcs_sync_state_n_3 ),
        .I1(\G_PCS_SYNC_STATE[3].pcs_sync_state_n_2 ),
        .I2(\G_PCS_SYNC_STATE[2].pcs_sync_state_n_3 ),
        .I3(\G_PCS_SYNC_STATE[2].pcs_sync_state_n_2 ),
        .I4(\G_PCS_SYNC_STATE[2].pcs_sync_state_n_1 ),
        .I5(sync_status_i_2_n_0),
        .O(sync_status_int));
  LUT6 #(
    .INIT(64'h01010101010101FF)) 
    sync_status_i_2
       (.I0(\G_PCS_SYNC_STATE[1].pcs_sync_state_n_1 ),
        .I1(\G_PCS_SYNC_STATE[1].pcs_sync_state_n_2 ),
        .I2(\G_PCS_SYNC_STATE[1].pcs_sync_state_n_3 ),
        .I3(\G_PCS_SYNC_STATE[0].pcs_sync_state_n_2 ),
        .I4(\G_PCS_SYNC_STATE[0].pcs_sync_state_n_3 ),
        .I5(\G_PCS_SYNC_STATE[0].pcs_sync_state_n_4 ),
        .O(sync_status_i_2_n_0));
  FDRE sync_status_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(sync_status_int),
        .Q(sync_status),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rx_recoder" *) 
module rxaui_0_rx_recoder
   (xgmii_rxd,
    xgmii_rxc,
    align_status_reg,
    usrclk,
    \mgt_rxdata_reg_reg[31] ,
    \mgt_rxcharisk_reg_reg[3] ,
    \mgt_rxdata_reg_reg[24] ,
    \mgt_rxdata_reg_reg[30] ,
    \mgt_rxdata_reg_reg[14] ,
    local_fault);
  output [63:0]xgmii_rxd;
  output [7:0]xgmii_rxc;
  input align_status_reg;
  input usrclk;
  input [63:0]\mgt_rxdata_reg_reg[31] ;
  input [7:0]\mgt_rxcharisk_reg_reg[3] ;
  input [7:0]\mgt_rxdata_reg_reg[24] ;
  input [7:0]\mgt_rxdata_reg_reg[30] ;
  input [3:0]\mgt_rxdata_reg_reg[14] ;
  input local_fault;

  wire align_status_reg;
  wire c11_in;
  wire c150_in;
  wire c3_in;
  wire c69_in;
  wire c72_in;
  wire c75_in;
  wire c7_in;
  wire [7:0]code_error_delay;
  wire \code_error_pipe_reg_n_0_[4] ;
  wire \code_error_pipe_reg_n_0_[5] ;
  wire \code_error_pipe_reg_n_0_[6] ;
  wire \code_error_pipe_reg_n_0_[7] ;
  wire [7:0]d;
  wire [3:0]lane_term_pipe;
  wire \lane_terminate_temp_reg_n_0_[0] ;
  wire \lane_terminate_temp_reg_n_0_[3] ;
  wire \lane_terminate_temp_reg_n_0_[4] ;
  wire \lane_terminate_temp_reg_n_0_[5] ;
  wire \lane_terminate_temp_reg_n_0_[6] ;
  wire \lane_terminate_temp_reg_n_0_[7] ;
  wire local_fault;
  wire [7:0]\mgt_rxcharisk_reg_reg[3] ;
  wire [3:0]\mgt_rxdata_reg_reg[14] ;
  wire [7:0]\mgt_rxdata_reg_reg[24] ;
  wire [7:0]\mgt_rxdata_reg_reg[30] ;
  wire [63:0]\mgt_rxdata_reg_reg[31] ;
  wire p_0_in;
  wire p_0_in0_in;
  wire p_0_in_0;
  wire [3:0]p_15_out;
  wire p_2_in;
  wire p_2_in_1;
  wire [7:0]p_3_in;
  wire p_4_in;
  wire [7:0]p_5_in;
  wire p_6_in;
  wire [3:0]rxc_half_pipe;
  wire \rxc_out[0]_i_1_n_0 ;
  wire \rxc_out[1]_i_1_n_0 ;
  wire \rxc_out[2]_i_1_n_0 ;
  wire \rxc_out[3]_i_1_n_0 ;
  wire \rxc_out[4]_i_1_n_0 ;
  wire \rxc_out[5]_i_1_n_0 ;
  wire \rxc_out[6]_i_1_n_0 ;
  wire \rxc_out[7]_i_1_n_0 ;
  wire \rxc_pipe_reg_n_0_[0] ;
  wire [31:0]rxd_half_pipe;
  wire \rxd_out[0]_i_1_n_0 ;
  wire \rxd_out[10]_i_1_n_0 ;
  wire \rxd_out[11]_i_1_n_0 ;
  wire \rxd_out[12]_i_1_n_0 ;
  wire \rxd_out[13]_i_1_n_0 ;
  wire \rxd_out[14]_i_1_n_0 ;
  wire \rxd_out[15]_i_1_n_0 ;
  wire \rxd_out[15]_i_2_n_0 ;
  wire \rxd_out[15]_i_3_n_0 ;
  wire \rxd_out[15]_i_4_n_0 ;
  wire \rxd_out[15]_i_5_n_0 ;
  wire \rxd_out[15]_i_6_n_0 ;
  wire \rxd_out[16]_i_1_n_0 ;
  wire \rxd_out[17]_i_1_n_0 ;
  wire \rxd_out[18]_i_1_n_0 ;
  wire \rxd_out[19]_i_1_n_0 ;
  wire \rxd_out[1]_i_1_n_0 ;
  wire \rxd_out[20]_i_1_n_0 ;
  wire \rxd_out[21]_i_1_n_0 ;
  wire \rxd_out[22]_i_1_n_0 ;
  wire \rxd_out[23]_i_1_n_0 ;
  wire \rxd_out[23]_i_2_n_0 ;
  wire \rxd_out[23]_i_3_n_0 ;
  wire \rxd_out[23]_i_4_n_0 ;
  wire \rxd_out[23]_i_5_n_0 ;
  wire \rxd_out[23]_i_6_n_0 ;
  wire \rxd_out[24]_i_1_n_0 ;
  wire \rxd_out[25]_i_1_n_0 ;
  wire \rxd_out[26]_i_1_n_0 ;
  wire \rxd_out[27]_i_1_n_0 ;
  wire \rxd_out[28]_i_1_n_0 ;
  wire \rxd_out[29]_i_1_n_0 ;
  wire \rxd_out[2]_i_1_n_0 ;
  wire \rxd_out[30]_i_1_n_0 ;
  wire \rxd_out[31]_i_1_n_0 ;
  wire \rxd_out[31]_i_2_n_0 ;
  wire \rxd_out[31]_i_3_n_0 ;
  wire \rxd_out[31]_i_4_n_0 ;
  wire \rxd_out[32]_i_1_n_0 ;
  wire \rxd_out[33]_i_1_n_0 ;
  wire \rxd_out[34]_i_1_n_0 ;
  wire \rxd_out[35]_i_1_n_0 ;
  wire \rxd_out[36]_i_1_n_0 ;
  wire \rxd_out[37]_i_1_n_0 ;
  wire \rxd_out[38]_i_1_n_0 ;
  wire \rxd_out[39]_i_1_n_0 ;
  wire \rxd_out[39]_i_2_n_0 ;
  wire \rxd_out[39]_i_3_n_0 ;
  wire \rxd_out[3]_i_1_n_0 ;
  wire \rxd_out[40]_i_1_n_0 ;
  wire \rxd_out[41]_i_1_n_0 ;
  wire \rxd_out[42]_i_1_n_0 ;
  wire \rxd_out[43]_i_1_n_0 ;
  wire \rxd_out[44]_i_1_n_0 ;
  wire \rxd_out[45]_i_1_n_0 ;
  wire \rxd_out[46]_i_1_n_0 ;
  wire \rxd_out[47]_i_1_n_0 ;
  wire \rxd_out[47]_i_2_n_0 ;
  wire \rxd_out[47]_i_3_n_0 ;
  wire \rxd_out[47]_i_4_n_0 ;
  wire \rxd_out[47]_i_5_n_0 ;
  wire \rxd_out[48]_i_1_n_0 ;
  wire \rxd_out[49]_i_1_n_0 ;
  wire \rxd_out[4]_i_1_n_0 ;
  wire \rxd_out[50]_i_1_n_0 ;
  wire \rxd_out[51]_i_1_n_0 ;
  wire \rxd_out[52]_i_1_n_0 ;
  wire \rxd_out[53]_i_1_n_0 ;
  wire \rxd_out[54]_i_1_n_0 ;
  wire \rxd_out[55]_i_1_n_0 ;
  wire \rxd_out[55]_i_2_n_0 ;
  wire \rxd_out[55]_i_3_n_0 ;
  wire \rxd_out[55]_i_4_n_0 ;
  wire \rxd_out[55]_i_5_n_0 ;
  wire \rxd_out[56]_i_1_n_0 ;
  wire \rxd_out[57]_i_1_n_0 ;
  wire \rxd_out[58]_i_1_n_0 ;
  wire \rxd_out[59]_i_1_n_0 ;
  wire \rxd_out[5]_i_1_n_0 ;
  wire \rxd_out[60]_i_1_n_0 ;
  wire \rxd_out[61]_i_1_n_0 ;
  wire \rxd_out[62]_i_1_n_0 ;
  wire \rxd_out[63]_i_2_n_0 ;
  wire \rxd_out[63]_i_3_n_0 ;
  wire \rxd_out[63]_i_4_n_0 ;
  wire \rxd_out[6]_i_1_n_0 ;
  wire \rxd_out[7]_i_1_n_0 ;
  wire \rxd_out[7]_i_2_n_0 ;
  wire \rxd_out[7]_i_3_n_0 ;
  wire \rxd_out[7]_i_4_n_0 ;
  wire \rxd_out[8]_i_1_n_0 ;
  wire \rxd_out[9]_i_1_n_0 ;
  wire \rxd_pipe_reg_n_0_[0] ;
  wire \rxd_pipe_reg_n_0_[1] ;
  wire \rxd_pipe_reg_n_0_[24] ;
  wire \rxd_pipe_reg_n_0_[25] ;
  wire \rxd_pipe_reg_n_0_[26] ;
  wire \rxd_pipe_reg_n_0_[27] ;
  wire \rxd_pipe_reg_n_0_[28] ;
  wire \rxd_pipe_reg_n_0_[29] ;
  wire \rxd_pipe_reg_n_0_[2] ;
  wire \rxd_pipe_reg_n_0_[30] ;
  wire \rxd_pipe_reg_n_0_[31] ;
  wire \rxd_pipe_reg_n_0_[3] ;
  wire \rxd_pipe_reg_n_0_[40] ;
  wire \rxd_pipe_reg_n_0_[41] ;
  wire \rxd_pipe_reg_n_0_[42] ;
  wire \rxd_pipe_reg_n_0_[43] ;
  wire \rxd_pipe_reg_n_0_[44] ;
  wire \rxd_pipe_reg_n_0_[45] ;
  wire \rxd_pipe_reg_n_0_[46] ;
  wire \rxd_pipe_reg_n_0_[47] ;
  wire \rxd_pipe_reg_n_0_[48] ;
  wire \rxd_pipe_reg_n_0_[49] ;
  wire \rxd_pipe_reg_n_0_[4] ;
  wire \rxd_pipe_reg_n_0_[50] ;
  wire \rxd_pipe_reg_n_0_[51] ;
  wire \rxd_pipe_reg_n_0_[52] ;
  wire \rxd_pipe_reg_n_0_[53] ;
  wire \rxd_pipe_reg_n_0_[54] ;
  wire \rxd_pipe_reg_n_0_[55] ;
  wire \rxd_pipe_reg_n_0_[56] ;
  wire \rxd_pipe_reg_n_0_[57] ;
  wire \rxd_pipe_reg_n_0_[58] ;
  wire \rxd_pipe_reg_n_0_[59] ;
  wire \rxd_pipe_reg_n_0_[5] ;
  wire \rxd_pipe_reg_n_0_[60] ;
  wire \rxd_pipe_reg_n_0_[61] ;
  wire \rxd_pipe_reg_n_0_[62] ;
  wire \rxd_pipe_reg_n_0_[63] ;
  wire \rxd_pipe_reg_n_0_[6] ;
  wire \rxd_pipe_reg_n_0_[7] ;
  wire \tx_is_idle_half_pipe[0]_i_2_n_0 ;
  wire \tx_is_idle_half_pipe[1]_i_2_n_0 ;
  wire \tx_is_idle_half_pipe[2]_i_2_n_0 ;
  wire \tx_is_idle_half_pipe[3]_i_2_n_0 ;
  wire \tx_is_idle_half_pipe_reg_n_0_[0] ;
  wire \tx_is_idle_half_pipe_reg_n_0_[3] ;
  wire \tx_is_idle_pipe_reg_n_0_[0] ;
  wire usrclk;
  wire [7:0]xgmii_rxc;
  wire [63:0]xgmii_rxd;

  FDRE #(
    .INIT(1'b0)) 
    \code_error_delay_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\code_error_pipe_reg_n_0_[4] ),
        .Q(code_error_delay[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_delay_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\code_error_pipe_reg_n_0_[5] ),
        .Q(code_error_delay[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_delay_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\code_error_pipe_reg_n_0_[6] ),
        .Q(code_error_delay[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_delay_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\code_error_pipe_reg_n_0_[7] ),
        .Q(code_error_delay[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [0]),
        .Q(code_error_delay[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [1]),
        .Q(code_error_delay[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [2]),
        .Q(code_error_delay[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [3]),
        .Q(code_error_delay[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_pipe_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [4]),
        .Q(\code_error_pipe_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_pipe_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [5]),
        .Q(\code_error_pipe_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_pipe_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [6]),
        .Q(\code_error_pipe_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_error_pipe_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[24] [7]),
        .Q(\code_error_pipe_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_term_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\lane_terminate_temp_reg_n_0_[4] ),
        .Q(lane_term_pipe[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_term_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\lane_terminate_temp_reg_n_0_[5] ),
        .Q(lane_term_pipe[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_term_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\lane_terminate_temp_reg_n_0_[6] ),
        .Q(lane_term_pipe[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_term_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\lane_terminate_temp_reg_n_0_[7] ),
        .Q(lane_term_pipe[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_terminate_temp_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[30] [0]),
        .Q(\lane_terminate_temp_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_terminate_temp_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[30] [1]),
        .Q(p_2_in_1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_terminate_temp_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[30] [2]),
        .Q(p_0_in_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_terminate_temp_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[30] [3]),
        .Q(\lane_terminate_temp_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_terminate_temp_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[30] [4]),
        .Q(\lane_terminate_temp_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_terminate_temp_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[30] [5]),
        .Q(\lane_terminate_temp_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_terminate_temp_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[30] [6]),
        .Q(\lane_terminate_temp_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \lane_terminate_temp_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[30] [7]),
        .Q(\lane_terminate_temp_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxc_half_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c150_in),
        .Q(rxc_half_pipe[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_half_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c3_in),
        .Q(rxc_half_pipe[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_half_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c7_in),
        .Q(rxc_half_pipe[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_half_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c11_in),
        .Q(rxc_half_pipe[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT4 #(
    .INIT(16'hFFFB)) 
    \rxc_out[0]_i_1 
       (.I0(\rxd_out[7]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(rxc_half_pipe[0]),
        .I3(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .O(\rxc_out[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxc_out[1]_i_1 
       (.I0(p_0_in),
        .I1(\rxd_out[15]_i_2_n_0 ),
        .I2(rxc_half_pipe[1]),
        .O(\rxc_out[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxc_out[2]_i_1 
       (.I0(p_0_in0_in),
        .I1(\rxd_out[23]_i_2_n_0 ),
        .I2(rxc_half_pipe[2]),
        .O(\rxc_out[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxc_out[3]_i_1 
       (.I0(rxc_half_pipe[3]),
        .I1(\rxd_out[31]_i_2_n_0 ),
        .I2(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .O(\rxc_out[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT4 #(
    .INIT(16'hFFFB)) 
    \rxc_out[4]_i_1 
       (.I0(\rxd_out[39]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(\rxc_pipe_reg_n_0_[0] ),
        .I3(\tx_is_idle_pipe_reg_n_0_[0] ),
        .O(\rxc_out[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxc_out[5]_i_1 
       (.I0(p_2_in),
        .I1(\rxd_out[47]_i_2_n_0 ),
        .I2(c75_in),
        .O(\rxc_out[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxc_out[6]_i_1 
       (.I0(c72_in),
        .I1(\rxd_out[55]_i_2_n_0 ),
        .I2(p_4_in),
        .O(\rxc_out[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxc_out[7]_i_1 
       (.I0(c69_in),
        .I1(\rxd_out[63]_i_3_n_0 ),
        .I2(p_6_in),
        .O(\rxc_out[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \rxc_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxc_out[0]_i_1_n_0 ),
        .Q(xgmii_rxc[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxc_out[1]_i_1_n_0 ),
        .Q(xgmii_rxc[1]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxc_out[2]_i_1_n_0 ),
        .Q(xgmii_rxc[2]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxc_out[3]_i_1_n_0 ),
        .Q(xgmii_rxc[3]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b1)) 
    \rxc_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxc_out[4]_i_1_n_0 ),
        .Q(xgmii_rxc[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxc_out[5]_i_1_n_0 ),
        .Q(xgmii_rxc[5]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxc_out[6]_i_1_n_0 ),
        .Q(xgmii_rxc[6]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxc_out[7]_i_1_n_0 ),
        .Q(xgmii_rxc[7]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b1)) 
    \rxc_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg_reg[3] [0]),
        .Q(\rxc_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg_reg[3] [1]),
        .Q(c75_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg_reg[3] [2]),
        .Q(c72_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg_reg[3] [3]),
        .Q(c69_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxc_pipe_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg_reg[3] [4]),
        .Q(c150_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_pipe_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg_reg[3] [5]),
        .Q(c3_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_pipe_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg_reg[3] [6]),
        .Q(c7_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxc_pipe_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg_reg[3] [7]),
        .Q(c11_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[0]),
        .Q(rxd_half_pipe[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[10] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[42] ),
        .Q(rxd_half_pipe[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[11] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[43] ),
        .Q(rxd_half_pipe[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[12] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[44] ),
        .Q(rxd_half_pipe[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[13] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[45] ),
        .Q(rxd_half_pipe[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[14] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[46] ),
        .Q(rxd_half_pipe[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[15] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[47] ),
        .Q(rxd_half_pipe[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[16] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[48] ),
        .Q(rxd_half_pipe[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[17] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[49] ),
        .Q(rxd_half_pipe[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[18] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[50] ),
        .Q(rxd_half_pipe[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[19] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[51] ),
        .Q(rxd_half_pipe[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[1]),
        .Q(rxd_half_pipe[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[20] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[52] ),
        .Q(rxd_half_pipe[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[21] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[53] ),
        .Q(rxd_half_pipe[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[22] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[54] ),
        .Q(rxd_half_pipe[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[23] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[55] ),
        .Q(rxd_half_pipe[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_half_pipe_reg[24] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[56] ),
        .Q(rxd_half_pipe[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[25] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[57] ),
        .Q(rxd_half_pipe[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[26] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[58] ),
        .Q(rxd_half_pipe[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[27] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[59] ),
        .Q(rxd_half_pipe[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[28] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[60] ),
        .Q(rxd_half_pipe[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[29] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[61] ),
        .Q(rxd_half_pipe[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_half_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[2]),
        .Q(rxd_half_pipe[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[30] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[62] ),
        .Q(rxd_half_pipe[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[31] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[63] ),
        .Q(rxd_half_pipe[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_half_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[3]),
        .Q(rxd_half_pipe[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_half_pipe_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[4]),
        .Q(rxd_half_pipe[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[5]),
        .Q(rxd_half_pipe[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[6]),
        .Q(rxd_half_pipe[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_half_pipe_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[7]),
        .Q(rxd_half_pipe[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[8] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[40] ),
        .Q(rxd_half_pipe[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_half_pipe_reg[9] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_pipe_reg_n_0_[41] ),
        .Q(rxd_half_pipe[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT4 #(
    .INIT(16'h4440)) 
    \rxd_out[0]_i_1 
       (.I0(\rxd_out[7]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(rxd_half_pipe[0]),
        .I3(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .O(\rxd_out[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[10]_i_1 
       (.I0(p_0_in),
        .I1(\rxd_out[15]_i_2_n_0 ),
        .I2(rxd_half_pipe[10]),
        .O(\rxd_out[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[11]_i_1 
       (.I0(p_0_in),
        .I1(rxd_half_pipe[11]),
        .I2(\rxd_out[15]_i_2_n_0 ),
        .O(\rxd_out[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[12]_i_1 
       (.I0(p_0_in),
        .I1(rxd_half_pipe[12]),
        .I2(\rxd_out[15]_i_2_n_0 ),
        .O(\rxd_out[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[13]_i_1 
       (.I0(p_0_in),
        .I1(rxd_half_pipe[13]),
        .I2(\rxd_out[15]_i_2_n_0 ),
        .O(\rxd_out[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[14]_i_1 
       (.I0(p_0_in),
        .I1(rxd_half_pipe[14]),
        .I2(\rxd_out[15]_i_2_n_0 ),
        .O(\rxd_out[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[15]_i_1 
       (.I0(p_0_in),
        .I1(rxd_half_pipe[15]),
        .I2(\rxd_out[15]_i_2_n_0 ),
        .O(\rxd_out[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFFF5551)) 
    \rxd_out[15]_i_2 
       (.I0(\lane_terminate_temp_reg_n_0_[0] ),
        .I1(\rxd_out[15]_i_3_n_0 ),
        .I2(lane_term_pipe[1]),
        .I3(lane_term_pipe[0]),
        .I4(\rxd_out[15]_i_4_n_0 ),
        .I5(code_error_delay[1]),
        .O(\rxd_out[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \rxd_out[15]_i_3 
       (.I0(lane_term_pipe[3]),
        .I1(lane_term_pipe[2]),
        .O(\rxd_out[15]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h22220020)) 
    \rxd_out[15]_i_4 
       (.I0(\rxd_out[15]_i_5_n_0 ),
        .I1(\rxd_out[15]_i_6_n_0 ),
        .I2(p_3_in[6]),
        .I3(\lane_terminate_temp_reg_n_0_[0] ),
        .I4(p_3_in[7]),
        .O(\rxd_out[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0700000000000000)) 
    \rxd_out[15]_i_5 
       (.I0(p_3_in[6]),
        .I1(p_3_in[7]),
        .I2(p_3_in[1]),
        .I3(p_3_in[2]),
        .I4(p_3_in[3]),
        .I5(c75_in),
        .O(\rxd_out[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFF7)) 
    \rxd_out[15]_i_6 
       (.I0(p_3_in[4]),
        .I1(p_3_in[5]),
        .I2(p_3_in[0]),
        .I3(code_error_delay[5]),
        .O(\rxd_out[15]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h8880)) 
    \rxd_out[16]_i_1 
       (.I0(\rxd_out[23]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(p_0_in0_in),
        .I3(rxd_half_pipe[16]),
        .O(\rxd_out[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[17]_i_1 
       (.I0(p_0_in0_in),
        .I1(\rxd_out[23]_i_2_n_0 ),
        .I2(rxd_half_pipe[17]),
        .O(\rxd_out[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[18]_i_1 
       (.I0(p_0_in0_in),
        .I1(\rxd_out[23]_i_2_n_0 ),
        .I2(rxd_half_pipe[18]),
        .O(\rxd_out[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[19]_i_1 
       (.I0(p_0_in0_in),
        .I1(rxd_half_pipe[19]),
        .I2(\rxd_out[23]_i_2_n_0 ),
        .O(\rxd_out[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \rxd_out[1]_i_1 
       (.I0(\rxd_out[7]_i_2_n_0 ),
        .I1(rxd_half_pipe[1]),
        .I2(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .O(\rxd_out[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[20]_i_1 
       (.I0(p_0_in0_in),
        .I1(rxd_half_pipe[20]),
        .I2(\rxd_out[23]_i_2_n_0 ),
        .O(\rxd_out[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[21]_i_1 
       (.I0(p_0_in0_in),
        .I1(rxd_half_pipe[21]),
        .I2(\rxd_out[23]_i_2_n_0 ),
        .O(\rxd_out[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[22]_i_1 
       (.I0(p_0_in0_in),
        .I1(rxd_half_pipe[22]),
        .I2(\rxd_out[23]_i_2_n_0 ),
        .O(\rxd_out[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[23]_i_1 
       (.I0(p_0_in0_in),
        .I1(rxd_half_pipe[23]),
        .I2(\rxd_out[23]_i_2_n_0 ),
        .O(\rxd_out[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000555557FF)) 
    \rxd_out[23]_i_2 
       (.I0(\rxd_out[23]_i_3_n_0 ),
        .I1(p_5_in[7]),
        .I2(\rxd_out[23]_i_4_n_0 ),
        .I3(p_5_in[6]),
        .I4(\rxd_out[23]_i_5_n_0 ),
        .I5(code_error_delay[2]),
        .O(\rxd_out[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEEEEEEEFEE)) 
    \rxd_out[23]_i_3 
       (.I0(p_2_in_1),
        .I1(\lane_terminate_temp_reg_n_0_[0] ),
        .I2(lane_term_pipe[2]),
        .I3(lane_term_pipe[3]),
        .I4(lane_term_pipe[1]),
        .I5(lane_term_pipe[0]),
        .O(\rxd_out[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \rxd_out[23]_i_4 
       (.I0(\lane_terminate_temp_reg_n_0_[0] ),
        .I1(p_2_in_1),
        .O(\rxd_out[23]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFBFF)) 
    \rxd_out[23]_i_5 
       (.I0(code_error_delay[6]),
        .I1(p_5_in[5]),
        .I2(p_5_in[0]),
        .I3(p_5_in[4]),
        .I4(\rxd_out[23]_i_6_n_0 ),
        .O(\rxd_out[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hF1FFFFFFFFFFFFFF)) 
    \rxd_out[23]_i_6 
       (.I0(p_5_in[6]),
        .I1(p_5_in[7]),
        .I2(p_5_in[1]),
        .I3(p_5_in[3]),
        .I4(p_5_in[2]),
        .I5(c72_in),
        .O(\rxd_out[23]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hE0FF)) 
    \rxd_out[24]_i_1 
       (.I0(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .I1(rxd_half_pipe[24]),
        .I2(\rxd_out[31]_i_2_n_0 ),
        .I3(align_status_reg),
        .O(\rxd_out[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[25]_i_1 
       (.I0(rxd_half_pipe[25]),
        .I1(\rxd_out[31]_i_2_n_0 ),
        .I2(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .O(\rxd_out[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[26]_i_1 
       (.I0(rxd_half_pipe[26]),
        .I1(\rxd_out[31]_i_2_n_0 ),
        .I2(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .O(\rxd_out[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[27]_i_1 
       (.I0(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .I1(rxd_half_pipe[27]),
        .I2(\rxd_out[31]_i_2_n_0 ),
        .O(\rxd_out[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[28]_i_1 
       (.I0(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .I1(rxd_half_pipe[28]),
        .I2(\rxd_out[31]_i_2_n_0 ),
        .O(\rxd_out[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[29]_i_1 
       (.I0(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .I1(rxd_half_pipe[29]),
        .I2(\rxd_out[31]_i_2_n_0 ),
        .O(\rxd_out[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT4 #(
    .INIT(16'hFFFB)) 
    \rxd_out[2]_i_1 
       (.I0(\rxd_out[7]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(rxd_half_pipe[2]),
        .I3(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .O(\rxd_out[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[30]_i_1 
       (.I0(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .I1(rxd_half_pipe[30]),
        .I2(\rxd_out[31]_i_2_n_0 ),
        .O(\rxd_out[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[31]_i_1 
       (.I0(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .I1(rxd_half_pipe[31]),
        .I2(\rxd_out[31]_i_2_n_0 ),
        .O(\rxd_out[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000000101FF01)) 
    \rxd_out[31]_i_2 
       (.I0(p_0_in_0),
        .I1(p_2_in_1),
        .I2(\lane_terminate_temp_reg_n_0_[0] ),
        .I3(\rxd_out[31]_i_3_n_0 ),
        .I4(\rxd_out[31]_i_4_n_0 ),
        .I5(code_error_delay[3]),
        .O(\rxd_out[31]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \rxd_out[31]_i_3 
       (.I0(\rxd_pipe_reg_n_0_[30] ),
        .I1(code_error_delay[7]),
        .I2(\rxd_pipe_reg_n_0_[31] ),
        .I3(\rxd_pipe_reg_n_0_[24] ),
        .O(\rxd_out[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hDFFFFFFFFFFFFFFF)) 
    \rxd_out[31]_i_4 
       (.I0(\rxd_pipe_reg_n_0_[26] ),
        .I1(\rxd_pipe_reg_n_0_[25] ),
        .I2(c69_in),
        .I3(\rxd_pipe_reg_n_0_[29] ),
        .I4(\rxd_pipe_reg_n_0_[27] ),
        .I5(\rxd_pipe_reg_n_0_[28] ),
        .O(\rxd_out[31]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT4 #(
    .INIT(16'h4440)) 
    \rxd_out[32]_i_1 
       (.I0(\rxd_out[39]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(\tx_is_idle_pipe_reg_n_0_[0] ),
        .I3(\rxd_pipe_reg_n_0_[0] ),
        .O(\rxd_out[32]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \rxd_out[33]_i_1 
       (.I0(\rxd_out[39]_i_2_n_0 ),
        .I1(\tx_is_idle_pipe_reg_n_0_[0] ),
        .I2(\rxd_pipe_reg_n_0_[1] ),
        .O(\rxd_out[33]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT4 #(
    .INIT(16'hFFFB)) 
    \rxd_out[34]_i_1 
       (.I0(\rxd_out[39]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(\rxd_pipe_reg_n_0_[2] ),
        .I3(\tx_is_idle_pipe_reg_n_0_[0] ),
        .O(\rxd_out[34]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT4 #(
    .INIT(16'hBFBB)) 
    \rxd_out[35]_i_1 
       (.I0(\rxd_out[39]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(\tx_is_idle_pipe_reg_n_0_[0] ),
        .I3(\rxd_pipe_reg_n_0_[3] ),
        .O(\rxd_out[35]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT4 #(
    .INIT(16'hBFBB)) 
    \rxd_out[36]_i_1 
       (.I0(\rxd_out[39]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(\tx_is_idle_pipe_reg_n_0_[0] ),
        .I3(\rxd_pipe_reg_n_0_[4] ),
        .O(\rxd_out[36]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBA)) 
    \rxd_out[37]_i_1 
       (.I0(\rxd_out[39]_i_2_n_0 ),
        .I1(\tx_is_idle_pipe_reg_n_0_[0] ),
        .I2(\rxd_pipe_reg_n_0_[5] ),
        .O(\rxd_out[37]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \rxd_out[38]_i_1 
       (.I0(\rxd_out[39]_i_2_n_0 ),
        .I1(\tx_is_idle_pipe_reg_n_0_[0] ),
        .I2(\rxd_pipe_reg_n_0_[6] ),
        .O(\rxd_out[38]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT4 #(
    .INIT(16'hBFBB)) 
    \rxd_out[39]_i_1 
       (.I0(\rxd_out[39]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(\tx_is_idle_pipe_reg_n_0_[0] ),
        .I3(\rxd_pipe_reg_n_0_[7] ),
        .O(\rxd_out[39]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAABBBBBBBA)) 
    \rxd_out[39]_i_2 
       (.I0(code_error_delay[4]),
        .I1(\rxd_out[39]_i_3_n_0 ),
        .I2(p_0_in_0),
        .I3(\lane_terminate_temp_reg_n_0_[3] ),
        .I4(p_2_in_1),
        .I5(\lane_terminate_temp_reg_n_0_[0] ),
        .O(\rxd_out[39]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000002800)) 
    \rxd_out[39]_i_3 
       (.I0(c150_in),
        .I1(d[7]),
        .I2(d[6]),
        .I3(d[5]),
        .I4(\tx_is_idle_half_pipe[0]_i_2_n_0 ),
        .I5(\code_error_pipe_reg_n_0_[4] ),
        .O(\rxd_out[39]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT4 #(
    .INIT(16'hBFBB)) 
    \rxd_out[3]_i_1 
       (.I0(\rxd_out[7]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .I3(rxd_half_pipe[3]),
        .O(\rxd_out[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8880)) 
    \rxd_out[40]_i_1 
       (.I0(\rxd_out[47]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(p_2_in),
        .I3(p_3_in[0]),
        .O(\rxd_out[40]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[41]_i_1 
       (.I0(p_2_in),
        .I1(\rxd_out[47]_i_2_n_0 ),
        .I2(p_3_in[1]),
        .O(\rxd_out[41]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[42]_i_1 
       (.I0(p_2_in),
        .I1(\rxd_out[47]_i_2_n_0 ),
        .I2(p_3_in[2]),
        .O(\rxd_out[42]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[43]_i_1 
       (.I0(p_2_in),
        .I1(p_3_in[3]),
        .I2(\rxd_out[47]_i_2_n_0 ),
        .O(\rxd_out[43]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[44]_i_1 
       (.I0(p_2_in),
        .I1(p_3_in[4]),
        .I2(\rxd_out[47]_i_2_n_0 ),
        .O(\rxd_out[44]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[45]_i_1 
       (.I0(p_2_in),
        .I1(p_3_in[5]),
        .I2(\rxd_out[47]_i_2_n_0 ),
        .O(\rxd_out[45]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[46]_i_1 
       (.I0(p_2_in),
        .I1(p_3_in[6]),
        .I2(\rxd_out[47]_i_2_n_0 ),
        .O(\rxd_out[46]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[47]_i_1 
       (.I0(p_2_in),
        .I1(p_3_in[7]),
        .I2(\rxd_out[47]_i_2_n_0 ),
        .O(\rxd_out[47]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00A2)) 
    \rxd_out[47]_i_2 
       (.I0(\rxd_out[47]_i_3_n_0 ),
        .I1(\lane_terminate_temp_reg_n_0_[4] ),
        .I2(\rxd_out[47]_i_4_n_0 ),
        .I3(code_error_delay[5]),
        .O(\rxd_out[47]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF1F1F1F1F1FFF1F1)) 
    \rxd_out[47]_i_3 
       (.I0(p_0_in_0),
        .I1(\lane_terminate_temp_reg_n_0_[3] ),
        .I2(\rxd_out[23]_i_4_n_0 ),
        .I3(\rxd_out[47]_i_5_n_0 ),
        .I4(\tx_is_idle_half_pipe[1]_i_2_n_0 ),
        .I5(\code_error_pipe_reg_n_0_[5] ),
        .O(\rxd_out[47]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \rxd_out[47]_i_4 
       (.I0(\tx_is_idle_half_pipe[1]_i_2_n_0 ),
        .I1(\code_error_pipe_reg_n_0_[5] ),
        .I2(\rxd_pipe_reg_n_0_[47] ),
        .I3(\rxd_pipe_reg_n_0_[46] ),
        .I4(c3_in),
        .I5(\rxd_pipe_reg_n_0_[45] ),
        .O(\rxd_out[47]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT4 #(
    .INIT(16'hF77F)) 
    \rxd_out[47]_i_5 
       (.I0(\rxd_pipe_reg_n_0_[45] ),
        .I1(c3_in),
        .I2(\rxd_pipe_reg_n_0_[47] ),
        .I3(\rxd_pipe_reg_n_0_[46] ),
        .O(\rxd_out[47]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h8880)) 
    \rxd_out[48]_i_1 
       (.I0(\rxd_out[55]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(p_4_in),
        .I3(p_5_in[0]),
        .O(\rxd_out[48]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[49]_i_1 
       (.I0(p_5_in[1]),
        .I1(\rxd_out[55]_i_2_n_0 ),
        .I2(p_4_in),
        .O(\rxd_out[49]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT4 #(
    .INIT(16'hBFBB)) 
    \rxd_out[4]_i_1 
       (.I0(\rxd_out[7]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .I3(rxd_half_pipe[4]),
        .O(\rxd_out[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[50]_i_1 
       (.I0(p_5_in[2]),
        .I1(\rxd_out[55]_i_2_n_0 ),
        .I2(p_4_in),
        .O(\rxd_out[50]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[51]_i_1 
       (.I0(p_4_in),
        .I1(p_5_in[3]),
        .I2(\rxd_out[55]_i_2_n_0 ),
        .O(\rxd_out[51]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[52]_i_1 
       (.I0(p_4_in),
        .I1(p_5_in[4]),
        .I2(\rxd_out[55]_i_2_n_0 ),
        .O(\rxd_out[52]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[53]_i_1 
       (.I0(p_4_in),
        .I1(p_5_in[5]),
        .I2(\rxd_out[55]_i_2_n_0 ),
        .O(\rxd_out[53]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[54]_i_1 
       (.I0(p_4_in),
        .I1(p_5_in[6]),
        .I2(\rxd_out[55]_i_2_n_0 ),
        .O(\rxd_out[54]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[55]_i_1 
       (.I0(p_4_in),
        .I1(p_5_in[7]),
        .I2(\rxd_out[55]_i_2_n_0 ),
        .O(\rxd_out[55]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h0000AA02)) 
    \rxd_out[55]_i_2 
       (.I0(\rxd_out[55]_i_3_n_0 ),
        .I1(\lane_terminate_temp_reg_n_0_[5] ),
        .I2(\lane_terminate_temp_reg_n_0_[4] ),
        .I3(\rxd_out[55]_i_4_n_0 ),
        .I4(code_error_delay[6]),
        .O(\rxd_out[55]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFFFDFFFDFFFF)) 
    \rxd_out[55]_i_3 
       (.I0(\lane_terminate_temp_reg_n_0_[3] ),
        .I1(p_0_in_0),
        .I2(p_2_in_1),
        .I3(\lane_terminate_temp_reg_n_0_[0] ),
        .I4(\rxd_out[55]_i_5_n_0 ),
        .I5(\code_error_pipe_reg_n_0_[6] ),
        .O(\rxd_out[55]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \rxd_out[55]_i_4 
       (.I0(\tx_is_idle_half_pipe[2]_i_2_n_0 ),
        .I1(\code_error_pipe_reg_n_0_[6] ),
        .I2(\rxd_pipe_reg_n_0_[55] ),
        .I3(\rxd_pipe_reg_n_0_[54] ),
        .I4(c7_in),
        .I5(\rxd_pipe_reg_n_0_[53] ),
        .O(\rxd_out[55]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT5 #(
    .INIT(32'h9FFFFFFF)) 
    \rxd_out[55]_i_5 
       (.I0(\rxd_pipe_reg_n_0_[54] ),
        .I1(\rxd_pipe_reg_n_0_[55] ),
        .I2(c7_in),
        .I3(\rxd_pipe_reg_n_0_[53] ),
        .I4(\tx_is_idle_half_pipe[2]_i_2_n_0 ),
        .O(\rxd_out[55]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hE0FF)) 
    \rxd_out[56]_i_1 
       (.I0(p_6_in),
        .I1(\rxd_pipe_reg_n_0_[24] ),
        .I2(\rxd_out[63]_i_3_n_0 ),
        .I3(align_status_reg),
        .O(\rxd_out[56]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[57]_i_1 
       (.I0(\rxd_pipe_reg_n_0_[25] ),
        .I1(\rxd_out[63]_i_3_n_0 ),
        .I2(p_6_in),
        .O(\rxd_out[57]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[58]_i_1 
       (.I0(\rxd_pipe_reg_n_0_[26] ),
        .I1(\rxd_out[63]_i_3_n_0 ),
        .I2(p_6_in),
        .O(\rxd_out[58]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[59]_i_1 
       (.I0(p_6_in),
        .I1(\rxd_pipe_reg_n_0_[27] ),
        .I2(\rxd_out[63]_i_3_n_0 ),
        .O(\rxd_out[59]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \rxd_out[5]_i_1 
       (.I0(\rxd_out[7]_i_2_n_0 ),
        .I1(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .I2(rxd_half_pipe[5]),
        .O(\rxd_out[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[60]_i_1 
       (.I0(p_6_in),
        .I1(\rxd_pipe_reg_n_0_[28] ),
        .I2(\rxd_out[63]_i_3_n_0 ),
        .O(\rxd_out[60]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[61]_i_1 
       (.I0(p_6_in),
        .I1(\rxd_pipe_reg_n_0_[29] ),
        .I2(\rxd_out[63]_i_3_n_0 ),
        .O(\rxd_out[61]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[62]_i_1 
       (.I0(p_6_in),
        .I1(\rxd_pipe_reg_n_0_[30] ),
        .I2(\rxd_out[63]_i_3_n_0 ),
        .O(\rxd_out[62]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \rxd_out[63]_i_2 
       (.I0(p_6_in),
        .I1(\rxd_pipe_reg_n_0_[31] ),
        .I2(\rxd_out[63]_i_3_n_0 ),
        .O(\rxd_out[63]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000000000101FF01)) 
    \rxd_out[63]_i_3 
       (.I0(\lane_terminate_temp_reg_n_0_[6] ),
        .I1(\lane_terminate_temp_reg_n_0_[5] ),
        .I2(\lane_terminate_temp_reg_n_0_[4] ),
        .I3(\tx_is_idle_half_pipe[3]_i_2_n_0 ),
        .I4(\rxd_out[63]_i_4_n_0 ),
        .I5(code_error_delay[7]),
        .O(\rxd_out[63]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFBFFFFF)) 
    \rxd_out[63]_i_4 
       (.I0(\code_error_pipe_reg_n_0_[7] ),
        .I1(\rxd_pipe_reg_n_0_[63] ),
        .I2(c11_in),
        .I3(\rxd_pipe_reg_n_0_[62] ),
        .I4(\rxd_pipe_reg_n_0_[61] ),
        .O(\rxd_out[63]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hBA)) 
    \rxd_out[6]_i_1 
       (.I0(\rxd_out[7]_i_2_n_0 ),
        .I1(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .I2(rxd_half_pipe[6]),
        .O(\rxd_out[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT4 #(
    .INIT(16'hBFBB)) 
    \rxd_out[7]_i_1 
       (.I0(\rxd_out[7]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .I3(rxd_half_pipe[7]),
        .O(\rxd_out[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBBAAAAAAAAA)) 
    \rxd_out[7]_i_2 
       (.I0(code_error_delay[0]),
        .I1(lane_term_pipe[0]),
        .I2(lane_term_pipe[3]),
        .I3(lane_term_pipe[2]),
        .I4(lane_term_pipe[1]),
        .I5(\rxd_out[7]_i_3_n_0 ),
        .O(\rxd_out[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFBFFF)) 
    \rxd_out[7]_i_3 
       (.I0(code_error_delay[4]),
        .I1(\rxc_pipe_reg_n_0_[0] ),
        .I2(\rxd_pipe_reg_n_0_[5] ),
        .I3(\rxd_pipe_reg_n_0_[4] ),
        .I4(\rxd_out[7]_i_4_n_0 ),
        .O(\rxd_out[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF9FFFFFFFFFF)) 
    \rxd_out[7]_i_4 
       (.I0(\rxd_pipe_reg_n_0_[6] ),
        .I1(\rxd_pipe_reg_n_0_[7] ),
        .I2(\rxd_pipe_reg_n_0_[1] ),
        .I3(\rxd_pipe_reg_n_0_[2] ),
        .I4(\rxd_pipe_reg_n_0_[0] ),
        .I5(\rxd_pipe_reg_n_0_[3] ),
        .O(\rxd_out[7]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h8880)) 
    \rxd_out[8]_i_1 
       (.I0(\rxd_out[15]_i_2_n_0 ),
        .I1(align_status_reg),
        .I2(p_0_in),
        .I3(rxd_half_pipe[8]),
        .O(\rxd_out[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rxd_out[9]_i_1 
       (.I0(p_0_in),
        .I1(\rxd_out[15]_i_2_n_0 ),
        .I2(rxd_half_pipe[9]),
        .O(\rxd_out[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[0]_i_1_n_0 ),
        .Q(xgmii_rxd[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[10] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[10]_i_1_n_0 ),
        .Q(xgmii_rxd[10]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[11] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[11]_i_1_n_0 ),
        .Q(xgmii_rxd[11]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[12] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[12]_i_1_n_0 ),
        .Q(xgmii_rxd[12]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[13] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[13]_i_1_n_0 ),
        .Q(xgmii_rxd[13]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[14] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[14]_i_1_n_0 ),
        .Q(xgmii_rxd[14]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[15] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[15]_i_1_n_0 ),
        .Q(xgmii_rxd[15]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[16] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[16]_i_1_n_0 ),
        .Q(xgmii_rxd[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[17] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[17]_i_1_n_0 ),
        .Q(xgmii_rxd[17]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[18] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[18]_i_1_n_0 ),
        .Q(xgmii_rxd[18]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[19] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[19]_i_1_n_0 ),
        .Q(xgmii_rxd[19]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[1]_i_1_n_0 ),
        .Q(xgmii_rxd[1]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[20] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[20]_i_1_n_0 ),
        .Q(xgmii_rxd[20]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[21] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[21]_i_1_n_0 ),
        .Q(xgmii_rxd[21]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[22] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[22]_i_1_n_0 ),
        .Q(xgmii_rxd[22]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[23] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[23]_i_1_n_0 ),
        .Q(xgmii_rxd[23]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[24] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[24]_i_1_n_0 ),
        .Q(xgmii_rxd[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[25] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[25]_i_1_n_0 ),
        .Q(xgmii_rxd[25]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[26] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[26]_i_1_n_0 ),
        .Q(xgmii_rxd[26]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[27] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[27]_i_1_n_0 ),
        .Q(xgmii_rxd[27]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[28] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[28]_i_1_n_0 ),
        .Q(xgmii_rxd[28]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[29] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[29]_i_1_n_0 ),
        .Q(xgmii_rxd[29]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[2]_i_1_n_0 ),
        .Q(xgmii_rxd[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[30] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[30]_i_1_n_0 ),
        .Q(xgmii_rxd[30]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[31] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[31]_i_1_n_0 ),
        .Q(xgmii_rxd[31]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[32] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[32]_i_1_n_0 ),
        .Q(xgmii_rxd[32]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[33] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[33]_i_1_n_0 ),
        .Q(xgmii_rxd[33]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[34] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[34]_i_1_n_0 ),
        .Q(xgmii_rxd[34]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[35] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[35]_i_1_n_0 ),
        .Q(xgmii_rxd[35]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[36] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[36]_i_1_n_0 ),
        .Q(xgmii_rxd[36]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[37] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[37]_i_1_n_0 ),
        .Q(xgmii_rxd[37]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[38] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[38]_i_1_n_0 ),
        .Q(xgmii_rxd[38]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[39] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[39]_i_1_n_0 ),
        .Q(xgmii_rxd[39]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[3]_i_1_n_0 ),
        .Q(xgmii_rxd[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[40] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[40]_i_1_n_0 ),
        .Q(xgmii_rxd[40]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[41] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[41]_i_1_n_0 ),
        .Q(xgmii_rxd[41]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[42] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[42]_i_1_n_0 ),
        .Q(xgmii_rxd[42]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[43] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[43]_i_1_n_0 ),
        .Q(xgmii_rxd[43]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[44] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[44]_i_1_n_0 ),
        .Q(xgmii_rxd[44]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[45] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[45]_i_1_n_0 ),
        .Q(xgmii_rxd[45]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[46] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[46]_i_1_n_0 ),
        .Q(xgmii_rxd[46]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[47] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[47]_i_1_n_0 ),
        .Q(xgmii_rxd[47]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[48] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[48]_i_1_n_0 ),
        .Q(xgmii_rxd[48]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[49] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[49]_i_1_n_0 ),
        .Q(xgmii_rxd[49]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[4]_i_1_n_0 ),
        .Q(xgmii_rxd[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[50] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[50]_i_1_n_0 ),
        .Q(xgmii_rxd[50]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[51] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[51]_i_1_n_0 ),
        .Q(xgmii_rxd[51]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[52] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[52]_i_1_n_0 ),
        .Q(xgmii_rxd[52]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[53] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[53]_i_1_n_0 ),
        .Q(xgmii_rxd[53]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[54] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[54]_i_1_n_0 ),
        .Q(xgmii_rxd[54]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[55] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[55]_i_1_n_0 ),
        .Q(xgmii_rxd[55]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[56] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[56]_i_1_n_0 ),
        .Q(xgmii_rxd[56]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[57] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[57]_i_1_n_0 ),
        .Q(xgmii_rxd[57]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[58] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[58]_i_1_n_0 ),
        .Q(xgmii_rxd[58]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[59] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[59]_i_1_n_0 ),
        .Q(xgmii_rxd[59]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[5]_i_1_n_0 ),
        .Q(xgmii_rxd[5]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[60] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[60]_i_1_n_0 ),
        .Q(xgmii_rxd[60]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[61] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[61]_i_1_n_0 ),
        .Q(xgmii_rxd[61]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[62] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[62]_i_1_n_0 ),
        .Q(xgmii_rxd[62]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[63] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[63]_i_2_n_0 ),
        .Q(xgmii_rxd[63]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[6]_i_1_n_0 ),
        .Q(xgmii_rxd[6]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[7]_i_1_n_0 ),
        .Q(xgmii_rxd[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[8] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[8]_i_1_n_0 ),
        .Q(xgmii_rxd[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_out_reg[9] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\rxd_out[9]_i_1_n_0 ),
        .Q(xgmii_rxd[9]),
        .R(local_fault));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [0]),
        .Q(\rxd_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[10] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [10]),
        .Q(p_3_in[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[11] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [11]),
        .Q(p_3_in[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[12] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [12]),
        .Q(p_3_in[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[13] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [13]),
        .Q(p_3_in[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[14] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [14]),
        .Q(p_3_in[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[15] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [15]),
        .Q(p_3_in[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[16] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [16]),
        .Q(p_5_in[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[17] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [17]),
        .Q(p_5_in[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[18] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [18]),
        .Q(p_5_in[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[19] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [19]),
        .Q(p_5_in[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [1]),
        .Q(\rxd_pipe_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[20] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [20]),
        .Q(p_5_in[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[21] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [21]),
        .Q(p_5_in[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[22] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [22]),
        .Q(p_5_in[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[23] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [23]),
        .Q(p_5_in[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[24] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [24]),
        .Q(\rxd_pipe_reg_n_0_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[25] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [25]),
        .Q(\rxd_pipe_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[26] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [26]),
        .Q(\rxd_pipe_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[27] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [27]),
        .Q(\rxd_pipe_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[28] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [28]),
        .Q(\rxd_pipe_reg_n_0_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[29] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [29]),
        .Q(\rxd_pipe_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [2]),
        .Q(\rxd_pipe_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[30] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [30]),
        .Q(\rxd_pipe_reg_n_0_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[31] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [31]),
        .Q(\rxd_pipe_reg_n_0_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[32] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [32]),
        .Q(d[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[33] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [33]),
        .Q(d[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[34] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [34]),
        .Q(d[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[35] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [35]),
        .Q(d[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[36] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [36]),
        .Q(d[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[37] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [37]),
        .Q(d[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[38] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [38]),
        .Q(d[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[39] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [39]),
        .Q(d[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [3]),
        .Q(\rxd_pipe_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[40] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [40]),
        .Q(\rxd_pipe_reg_n_0_[40] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[41] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [41]),
        .Q(\rxd_pipe_reg_n_0_[41] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[42] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [42]),
        .Q(\rxd_pipe_reg_n_0_[42] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[43] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [43]),
        .Q(\rxd_pipe_reg_n_0_[43] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[44] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [44]),
        .Q(\rxd_pipe_reg_n_0_[44] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[45] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [45]),
        .Q(\rxd_pipe_reg_n_0_[45] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[46] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [46]),
        .Q(\rxd_pipe_reg_n_0_[46] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[47] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [47]),
        .Q(\rxd_pipe_reg_n_0_[47] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[48] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [48]),
        .Q(\rxd_pipe_reg_n_0_[48] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[49] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [49]),
        .Q(\rxd_pipe_reg_n_0_[49] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [4]),
        .Q(\rxd_pipe_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[50] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [50]),
        .Q(\rxd_pipe_reg_n_0_[50] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[51] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [51]),
        .Q(\rxd_pipe_reg_n_0_[51] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[52] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [52]),
        .Q(\rxd_pipe_reg_n_0_[52] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[53] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [53]),
        .Q(\rxd_pipe_reg_n_0_[53] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[54] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [54]),
        .Q(\rxd_pipe_reg_n_0_[54] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[55] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [55]),
        .Q(\rxd_pipe_reg_n_0_[55] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[56] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [56]),
        .Q(\rxd_pipe_reg_n_0_[56] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[57] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [57]),
        .Q(\rxd_pipe_reg_n_0_[57] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[58] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [58]),
        .Q(\rxd_pipe_reg_n_0_[58] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[59] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [59]),
        .Q(\rxd_pipe_reg_n_0_[59] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [5]),
        .Q(\rxd_pipe_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[60] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [60]),
        .Q(\rxd_pipe_reg_n_0_[60] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[61] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [61]),
        .Q(\rxd_pipe_reg_n_0_[61] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[62] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [62]),
        .Q(\rxd_pipe_reg_n_0_[62] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[63] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [63]),
        .Q(\rxd_pipe_reg_n_0_[63] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [6]),
        .Q(\rxd_pipe_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \rxd_pipe_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [7]),
        .Q(\rxd_pipe_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[8] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [8]),
        .Q(p_3_in[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxd_pipe_reg[9] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[31] [9]),
        .Q(p_3_in[1]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h14000100)) 
    \tx_is_idle_half_pipe[0]_i_1 
       (.I0(\tx_is_idle_half_pipe[0]_i_2_n_0 ),
        .I1(d[6]),
        .I2(d[7]),
        .I3(c150_in),
        .I4(d[5]),
        .O(p_15_out[0]));
  LUT5 #(
    .INIT(32'hFFBFFFFF)) 
    \tx_is_idle_half_pipe[0]_i_2 
       (.I0(d[1]),
        .I1(d[2]),
        .I2(d[3]),
        .I3(d[0]),
        .I4(d[4]),
        .O(\tx_is_idle_half_pipe[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT5 #(
    .INIT(32'h28000200)) 
    \tx_is_idle_half_pipe[1]_i_1 
       (.I0(\tx_is_idle_half_pipe[1]_i_2_n_0 ),
        .I1(\rxd_pipe_reg_n_0_[47] ),
        .I2(\rxd_pipe_reg_n_0_[46] ),
        .I3(c3_in),
        .I4(\rxd_pipe_reg_n_0_[45] ),
        .O(p_15_out[1]));
  LUT5 #(
    .INIT(32'h00400000)) 
    \tx_is_idle_half_pipe[1]_i_2 
       (.I0(\rxd_pipe_reg_n_0_[40] ),
        .I1(\rxd_pipe_reg_n_0_[42] ),
        .I2(\rxd_pipe_reg_n_0_[44] ),
        .I3(\rxd_pipe_reg_n_0_[41] ),
        .I4(\rxd_pipe_reg_n_0_[43] ),
        .O(\tx_is_idle_half_pipe[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT5 #(
    .INIT(32'h28000200)) 
    \tx_is_idle_half_pipe[2]_i_1 
       (.I0(\tx_is_idle_half_pipe[2]_i_2_n_0 ),
        .I1(\rxd_pipe_reg_n_0_[55] ),
        .I2(\rxd_pipe_reg_n_0_[54] ),
        .I3(c7_in),
        .I4(\rxd_pipe_reg_n_0_[53] ),
        .O(p_15_out[2]));
  LUT5 #(
    .INIT(32'h00400000)) 
    \tx_is_idle_half_pipe[2]_i_2 
       (.I0(\rxd_pipe_reg_n_0_[48] ),
        .I1(\rxd_pipe_reg_n_0_[50] ),
        .I2(\rxd_pipe_reg_n_0_[52] ),
        .I3(\rxd_pipe_reg_n_0_[49] ),
        .I4(\rxd_pipe_reg_n_0_[51] ),
        .O(\tx_is_idle_half_pipe[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h08008008)) 
    \tx_is_idle_half_pipe[3]_i_1 
       (.I0(\tx_is_idle_half_pipe[3]_i_2_n_0 ),
        .I1(c11_in),
        .I2(\rxd_pipe_reg_n_0_[63] ),
        .I3(\rxd_pipe_reg_n_0_[61] ),
        .I4(\rxd_pipe_reg_n_0_[62] ),
        .O(p_15_out[3]));
  LUT5 #(
    .INIT(32'h00080000)) 
    \tx_is_idle_half_pipe[3]_i_2 
       (.I0(\rxd_pipe_reg_n_0_[58] ),
        .I1(\rxd_pipe_reg_n_0_[59] ),
        .I2(\rxd_pipe_reg_n_0_[56] ),
        .I3(\rxd_pipe_reg_n_0_[57] ),
        .I4(\rxd_pipe_reg_n_0_[60] ),
        .O(\tx_is_idle_half_pipe[3]_i_2_n_0 ));
  FDRE \tx_is_idle_half_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_15_out[0]),
        .Q(\tx_is_idle_half_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \tx_is_idle_half_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_15_out[1]),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \tx_is_idle_half_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_15_out[2]),
        .Q(p_0_in0_in),
        .R(1'b0));
  FDRE \tx_is_idle_half_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_15_out[3]),
        .Q(\tx_is_idle_half_pipe_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \tx_is_idle_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[14] [0]),
        .Q(\tx_is_idle_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \tx_is_idle_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[14] [1]),
        .Q(p_2_in),
        .R(1'b0));
  FDRE \tx_is_idle_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[14] [2]),
        .Q(p_4_in),
        .R(1'b0));
  FDRE \tx_is_idle_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg_reg[14] [3]),
        .Q(p_6_in),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "rxaui_gen" *) 
module rxaui_0_rxaui_gen
   (mgt_txdata,
    mgt_txcharisk,
    mgt_enchansync,
    xgmii_rxd,
    xgmii_rxc,
    reset_reg_reg,
    sync_status,
    align_status,
    mdio_out,
    mdio_tri,
    mgt_enable_align,
    mgt_powerdown_r_reg,
    mgt_loopback_r_reg,
    mgt_rx_reset,
    xgmii_txd,
    usrclk,
    xgmii_txc,
    mdc,
    mdio_in,
    type_sel,
    mgt_rxcharisk,
    mgt_codevalid,
    mgt_rxdata,
    mgt_codecomma,
    prtad,
    reset,
    mgt_rxlock,
    signal_detect,
    mgt_tx_reset);
  output [63:0]mgt_txdata;
  output [7:0]mgt_txcharisk;
  output mgt_enchansync;
  output [63:0]xgmii_rxd;
  output [7:0]xgmii_rxc;
  output reset_reg_reg;
  output [3:0]sync_status;
  output align_status;
  output mdio_out;
  output mdio_tri;
  output [1:0]mgt_enable_align;
  output mgt_powerdown_r_reg;
  output mgt_loopback_r_reg;
  input [1:0]mgt_rx_reset;
  input [63:0]xgmii_txd;
  input usrclk;
  input [7:0]xgmii_txc;
  input mdc;
  input mdio_in;
  input [1:0]type_sel;
  input [7:0]mgt_rxcharisk;
  input [7:0]mgt_codevalid;
  input [63:0]mgt_rxdata;
  input [7:0]mgt_codecomma;
  input [4:0]prtad;
  input reset;
  input [0:0]mgt_rxlock;
  input [1:0]signal_detect;
  input [0:0]mgt_tx_reset;

  wire \G_DUNE.G_ALIGN_TOGGLE_DETECT[0].i_toggle_detect_n_0 ;
  wire \G_DUNE.G_ALIGN_TOGGLE_DETECT[1].i_toggle_detect_n_1 ;
  wire \G_DUNE.dune_align0_i_n_1 ;
  wire \G_DUNE.dune_align0_i_n_2 ;
  wire \G_DUNE.dune_align0_i_n_53 ;
  wire \G_DUNE.dune_align0_i_n_54 ;
  wire \G_DUNE.dune_align1_i_n_10 ;
  wire \G_DUNE.dune_align1_i_n_11 ;
  wire \G_DUNE.dune_align1_i_n_12 ;
  wire \G_DUNE.dune_align1_i_n_13 ;
  wire \G_DUNE.dune_align1_i_n_14 ;
  wire \G_DUNE.dune_align1_i_n_15 ;
  wire \G_DUNE.dune_align1_i_n_16 ;
  wire \G_DUNE.dune_align1_i_n_17 ;
  wire \G_DUNE.dune_align1_i_n_18 ;
  wire \G_DUNE.dune_align1_i_n_19 ;
  wire \G_DUNE.dune_align1_i_n_20 ;
  wire \G_DUNE.dune_align1_i_n_21 ;
  wire \G_DUNE.dune_align1_i_n_22 ;
  wire \G_DUNE.dune_align1_i_n_23 ;
  wire \G_DUNE.dune_align1_i_n_24 ;
  wire \G_DUNE.dune_align1_i_n_25 ;
  wire \G_DUNE.dune_align1_i_n_26 ;
  wire \G_DUNE.dune_align1_i_n_27 ;
  wire \G_DUNE.dune_align1_i_n_28 ;
  wire \G_DUNE.dune_align1_i_n_29 ;
  wire \G_DUNE.dune_align1_i_n_30 ;
  wire \G_DUNE.dune_align1_i_n_31 ;
  wire \G_DUNE.dune_align1_i_n_32 ;
  wire \G_DUNE.dune_align1_i_n_33 ;
  wire \G_DUNE.dune_align1_i_n_34 ;
  wire \G_DUNE.dune_align1_i_n_35 ;
  wire \G_DUNE.dune_align1_i_n_36 ;
  wire \G_DUNE.dune_align1_i_n_37 ;
  wire \G_DUNE.dune_align1_i_n_38 ;
  wire \G_DUNE.dune_align1_i_n_39 ;
  wire \G_DUNE.dune_align1_i_n_40 ;
  wire \G_DUNE.dune_align1_i_n_41 ;
  wire \G_DUNE.dune_align1_i_n_42 ;
  wire \G_DUNE.dune_align1_i_n_43 ;
  wire \G_DUNE.dune_align1_i_n_44 ;
  wire \G_DUNE.dune_align1_i_n_45 ;
  wire \G_DUNE.dune_align1_i_n_46 ;
  wire \G_DUNE.dune_align1_i_n_55 ;
  wire \G_DUNE.dune_align1_i_n_56 ;
  wire \G_DUNE.dune_align1_i_n_57 ;
  wire \G_DUNE.dune_align1_i_n_58 ;
  wire \G_DUNE.dune_align1_i_n_59 ;
  wire \G_DUNE.dune_align1_i_n_60 ;
  wire \G_DUNE.dune_align1_i_n_7 ;
  wire \G_DUNE.dune_align1_i_n_8 ;
  wire \G_DUNE.dune_align1_i_n_9 ;
  wire align_status;
  wire align_toggle_0;
  wire align_toggle_1;
  wire [3:0]codecomma3_out;
  wire [3:0]codevalid4_out;
  wire mdc;
  wire mdio_in;
  wire mdio_out;
  wire mdio_tri;
  wire [7:0]mgt_codecomma;
  wire [7:0]mgt_codevalid;
  wire [1:0]mgt_enable_align;
  wire [3:0]mgt_enable_align_i;
  wire mgt_enchansync;
  wire mgt_loopback_r_reg;
  wire mgt_powerdown_r_reg;
  wire [1:0]mgt_rx_reset;
  wire [7:0]mgt_rxcharisk;
  wire [63:0]mgt_rxdata;
  wire [0:0]mgt_rxlock;
  wire [0:0]mgt_tx_reset;
  wire [7:0]mgt_txcharisk;
  wire [63:0]mgt_txdata;
  wire [1:0]p_3_out;
  wire [4:0]prtad;
  wire [7:0]\receiver/code_error ;
  wire \receiver/deskew_state/G_GOT_A[2].got_a_reg ;
  wire \receiver/deskew_state/G_GOT_A[3].got_a_reg ;
  wire \receiver/deskew_state/G_GOT_A[6].got_a_reg ;
  wire \receiver/deskew_state/G_GOT_A[7].got_a_reg ;
  wire [1:0]\receiver/deskew_state/p_1_out ;
  wire [1:0]\receiver/deskew_state/p_7_out ;
  wire [7:0]\receiver/recoder/p_21_out ;
  wire reset;
  wire reset_reg_reg;
  wire rx_local_fault0;
  wire [3:0]rxcharisk5_out;
  wire [31:0]rxdata6_out;
  wire [1:0]signal_detect;
  wire [3:0]sync_status;
  wire [1:0]type_sel;
  wire usrclk;
  wire [7:0]xgmii_rxc;
  wire [63:0]xgmii_rxd;
  wire [7:0]xgmii_txc;
  wire [63:0]xgmii_txd;

  rxaui_0_toggle_detect \G_DUNE.G_ALIGN_TOGGLE_DETECT[0].i_toggle_detect 
       (.\FSM_sequential_state_reg[1][0] (\G_DUNE.G_ALIGN_TOGGLE_DETECT[0].i_toggle_detect_n_0 ),
        .align_toggle_0(align_toggle_0),
        .mgt_rx_reset(mgt_rx_reset[0]),
        .usrclk(usrclk));
  rxaui_0_toggle_detect_10 \G_DUNE.G_ALIGN_TOGGLE_DETECT[1].i_toggle_detect 
       (.\FSM_sequential_state_reg[1][4] (\G_DUNE.G_ALIGN_TOGGLE_DETECT[1].i_toggle_detect_n_1 ),
        .align_status(align_status),
        .align_toggle_1(align_toggle_1),
        .\core_mgt_rx_reset_reg[0] (\G_DUNE.G_ALIGN_TOGGLE_DETECT[0].i_toggle_detect_n_0 ),
        .mgt_rx_reset(mgt_rx_reset[1]),
        .rx_local_fault0(rx_local_fault0),
        .usrclk(usrclk));
  rxaui_0_rxaui_v4_3_7_dune_align \G_DUNE.dune_align0_i 
       (.D(\receiver/deskew_state/p_1_out ),
        .\G_GOT_A[2].got_a_reg (\receiver/deskew_state/G_GOT_A[2].got_a_reg ),
        .\G_GOT_A[3].got_a_reg (\receiver/deskew_state/G_GOT_A[3].got_a_reg ),
        .\G_GOT_A[6].got_a_reg (\receiver/deskew_state/G_GOT_A[6].got_a_reg ),
        .\G_GOT_A[7].got_a_reg (\receiver/deskew_state/G_GOT_A[7].got_a_reg ),
        .Q(rxdata6_out),
        .align_toggle_0(align_toggle_0),
        .\code_comma_pipe_reg[1] (codecomma3_out),
        .\code_error_pipe_reg[5] ({\receiver/code_error [5:4],\receiver/code_error [1:0]}),
        .\code_valid_pipe_reg[1] (codevalid4_out),
        .\err_cnt_reg[0]_0 (\G_DUNE.dune_align0_i_n_1 ),
        .\err_cnt_reg[0]_1 (\G_DUNE.dune_align0_i_n_2 ),
        .\lane_terminate_temp_reg[5] ({\receiver/recoder/p_21_out [5:4],\receiver/recoder/p_21_out [1:0]}),
        .mgt_codecomma(mgt_codecomma[3:0]),
        .mgt_codevalid(mgt_codevalid[3:0]),
        .mgt_rx_reset(mgt_rx_reset[0]),
        .mgt_rxcharisk(mgt_rxcharisk[3:0]),
        .mgt_rxdata(mgt_rxdata[31:0]),
        .\rxc_pipe_reg[5] (rxcharisk5_out),
        .\tx_is_idle_pipe_reg[1] ({\G_DUNE.dune_align0_i_n_53 ,\G_DUNE.dune_align0_i_n_54 }),
        .usrclk(usrclk));
  rxaui_0_rxaui_v4_3_7_dune_align_11 \G_DUNE.dune_align1_i 
       (.D(\receiver/deskew_state/p_7_out ),
        .\G_GOT_A[2].got_a_reg (\receiver/deskew_state/G_GOT_A[2].got_a_reg ),
        .\G_GOT_A[3].got_a_reg (\receiver/deskew_state/G_GOT_A[3].got_a_reg ),
        .\G_GOT_A[6].got_a_reg (\receiver/deskew_state/G_GOT_A[6].got_a_reg ),
        .\G_GOT_A[7].got_a_reg (\receiver/deskew_state/G_GOT_A[7].got_a_reg ),
        .Q({\G_DUNE.dune_align1_i_n_7 ,\G_DUNE.dune_align1_i_n_8 ,\G_DUNE.dune_align1_i_n_9 ,\G_DUNE.dune_align1_i_n_10 ,\G_DUNE.dune_align1_i_n_11 ,\G_DUNE.dune_align1_i_n_12 ,\G_DUNE.dune_align1_i_n_13 ,\G_DUNE.dune_align1_i_n_14 ,\G_DUNE.dune_align1_i_n_15 ,\G_DUNE.dune_align1_i_n_16 ,\G_DUNE.dune_align1_i_n_17 ,\G_DUNE.dune_align1_i_n_18 ,\G_DUNE.dune_align1_i_n_19 ,\G_DUNE.dune_align1_i_n_20 ,\G_DUNE.dune_align1_i_n_21 ,\G_DUNE.dune_align1_i_n_22 ,\G_DUNE.dune_align1_i_n_23 ,\G_DUNE.dune_align1_i_n_24 ,\G_DUNE.dune_align1_i_n_25 ,\G_DUNE.dune_align1_i_n_26 ,\G_DUNE.dune_align1_i_n_27 ,\G_DUNE.dune_align1_i_n_28 ,\G_DUNE.dune_align1_i_n_29 ,\G_DUNE.dune_align1_i_n_30 ,\G_DUNE.dune_align1_i_n_31 ,\G_DUNE.dune_align1_i_n_32 ,\G_DUNE.dune_align1_i_n_33 ,\G_DUNE.dune_align1_i_n_34 ,\G_DUNE.dune_align1_i_n_35 ,\G_DUNE.dune_align1_i_n_36 ,\G_DUNE.dune_align1_i_n_37 ,\G_DUNE.dune_align1_i_n_38 }),
        .align_toggle_1(align_toggle_1),
        .\code_comma_pipe_reg[1] ({\G_DUNE.dune_align1_i_n_57 ,\G_DUNE.dune_align1_i_n_58 ,\G_DUNE.dune_align1_i_n_59 ,\G_DUNE.dune_align1_i_n_60 }),
        .\code_error_pipe_reg[7] ({\receiver/code_error [7:6],\receiver/code_error [3:2]}),
        .\code_valid_pipe_reg[1] ({\G_DUNE.dune_align1_i_n_39 ,\G_DUNE.dune_align1_i_n_40 ,\G_DUNE.dune_align1_i_n_41 ,\G_DUNE.dune_align1_i_n_42 }),
        .\lane_terminate_temp_reg[7] ({\receiver/recoder/p_21_out [7:6],\receiver/recoder/p_21_out [3:2]}),
        .mgt_codecomma(mgt_codecomma[7:4]),
        .mgt_codevalid(mgt_codevalid[7:4]),
        .mgt_rx_reset(mgt_rx_reset[1]),
        .mgt_rxcharisk(mgt_rxcharisk[7:4]),
        .mgt_rxdata(mgt_rxdata[63:32]),
        .\mgt_rxdata_reg_reg[0]_0 (\G_DUNE.dune_align0_i_n_1 ),
        .\mgt_rxdata_reg_reg[16]_0 (\G_DUNE.dune_align0_i_n_2 ),
        .\rxc_pipe_reg[7] ({\G_DUNE.dune_align1_i_n_43 ,\G_DUNE.dune_align1_i_n_44 ,\G_DUNE.dune_align1_i_n_45 ,\G_DUNE.dune_align1_i_n_46 }),
        .\tx_is_idle_pipe_reg[3] ({\G_DUNE.dune_align1_i_n_55 ,\G_DUNE.dune_align1_i_n_56 }),
        .usrclk(usrclk));
  LUT2 #(
    .INIT(4'hE)) 
    \mgt_enable_align_d[0]_i_1 
       (.I0(mgt_enable_align_i[1]),
        .I1(mgt_enable_align_i[0]),
        .O(p_3_out[0]));
  LUT2 #(
    .INIT(4'hE)) 
    \mgt_enable_align_d[1]_i_1 
       (.I0(mgt_enable_align_i[3]),
        .I1(mgt_enable_align_i[2]),
        .O(p_3_out[1]));
  FDRE \mgt_enable_align_d_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_3_out[0]),
        .Q(mgt_enable_align[0]),
        .R(1'b0));
  FDRE \mgt_enable_align_d_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_3_out[1]),
        .Q(mgt_enable_align[1]),
        .R(1'b0));
  rxaui_0_xaui_gen xaui_i
       (.D(\receiver/deskew_state/p_7_out ),
        .\core_mgt_rx_reset_reg[0] (\G_DUNE.G_ALIGN_TOGGLE_DETECT[0].i_toggle_detect_n_0 ),
        .\core_mgt_rx_reset_reg[1] (\G_DUNE.G_ALIGN_TOGGLE_DETECT[1].i_toggle_detect_n_1 ),
        .mdc(mdc),
        .mdio_in(mdio_in),
        .mdio_out(mdio_out),
        .mdio_tri(mdio_tri),
        .\mgt_codecomma_reg_reg[3] (codecomma3_out),
        .\mgt_codecomma_reg_reg[3]_0 ({\G_DUNE.dune_align1_i_n_57 ,\G_DUNE.dune_align1_i_n_58 ,\G_DUNE.dune_align1_i_n_59 ,\G_DUNE.dune_align1_i_n_60 }),
        .\mgt_codevalid_reg_reg[3] (codevalid4_out),
        .\mgt_codevalid_reg_reg[3]_0 ({\G_DUNE.dune_align1_i_n_39 ,\G_DUNE.dune_align1_i_n_40 ,\G_DUNE.dune_align1_i_n_41 ,\G_DUNE.dune_align1_i_n_42 }),
        .mgt_enable_align_i(mgt_enable_align_i),
        .mgt_enchansync(mgt_enchansync),
        .mgt_loopback_r_reg(mgt_loopback_r_reg),
        .mgt_powerdown_r_reg(mgt_powerdown_r_reg),
        .\mgt_rxcharisk_reg_reg[3] ({\G_DUNE.dune_align1_i_n_43 ,\G_DUNE.dune_align1_i_n_44 ,rxcharisk5_out[3:2],\G_DUNE.dune_align1_i_n_45 ,\G_DUNE.dune_align1_i_n_46 ,rxcharisk5_out[1:0]}),
        .\mgt_rxdata_reg_reg[14] ({\G_DUNE.dune_align1_i_n_55 ,\G_DUNE.dune_align1_i_n_56 ,\G_DUNE.dune_align0_i_n_53 ,\G_DUNE.dune_align0_i_n_54 }),
        .\mgt_rxdata_reg_reg[24] (\receiver/deskew_state/p_1_out ),
        .\mgt_rxdata_reg_reg[24]_0 (\receiver/code_error ),
        .\mgt_rxdata_reg_reg[30] (\receiver/recoder/p_21_out ),
        .\mgt_rxdata_reg_reg[31] ({\G_DUNE.dune_align1_i_n_7 ,\G_DUNE.dune_align1_i_n_8 ,\G_DUNE.dune_align1_i_n_9 ,\G_DUNE.dune_align1_i_n_10 ,\G_DUNE.dune_align1_i_n_11 ,\G_DUNE.dune_align1_i_n_12 ,\G_DUNE.dune_align1_i_n_13 ,\G_DUNE.dune_align1_i_n_14 ,\G_DUNE.dune_align1_i_n_15 ,\G_DUNE.dune_align1_i_n_16 ,\G_DUNE.dune_align1_i_n_17 ,\G_DUNE.dune_align1_i_n_18 ,\G_DUNE.dune_align1_i_n_19 ,\G_DUNE.dune_align1_i_n_20 ,\G_DUNE.dune_align1_i_n_21 ,\G_DUNE.dune_align1_i_n_22 ,rxdata6_out[31:16],\G_DUNE.dune_align1_i_n_23 ,\G_DUNE.dune_align1_i_n_24 ,\G_DUNE.dune_align1_i_n_25 ,\G_DUNE.dune_align1_i_n_26 ,\G_DUNE.dune_align1_i_n_27 ,\G_DUNE.dune_align1_i_n_28 ,\G_DUNE.dune_align1_i_n_29 ,\G_DUNE.dune_align1_i_n_30 ,\G_DUNE.dune_align1_i_n_31 ,\G_DUNE.dune_align1_i_n_32 ,\G_DUNE.dune_align1_i_n_33 ,\G_DUNE.dune_align1_i_n_34 ,\G_DUNE.dune_align1_i_n_35 ,\G_DUNE.dune_align1_i_n_36 ,\G_DUNE.dune_align1_i_n_37 ,\G_DUNE.dune_align1_i_n_38 ,rxdata6_out[15:0]}),
        .mgt_rxlock(mgt_rxlock),
        .mgt_tx_reset(mgt_tx_reset),
        .mgt_txcharisk(mgt_txcharisk),
        .mgt_txdata(mgt_txdata),
        .out(align_status),
        .prtad(prtad),
        .reset(reset),
        .reset_reg_reg(reset_reg_reg),
        .rx_local_fault0(rx_local_fault0),
        .signal_detect(signal_detect),
        .sync_status(sync_status),
        .type_sel(type_sel),
        .usrclk(usrclk),
        .xgmii_rxc(xgmii_rxc),
        .xgmii_rxd(xgmii_rxd),
        .xgmii_txc(xgmii_txc),
        .xgmii_txd(xgmii_txd));
endmodule

(* ORIG_REF_NAME = "rxaui_v4_3_7_dune_align" *) 
module rxaui_0_rxaui_v4_3_7_dune_align
   (align_toggle_0,
    \err_cnt_reg[0]_0 ,
    \err_cnt_reg[0]_1 ,
    D,
    Q,
    \rxc_pipe_reg[5] ,
    \code_valid_pipe_reg[1] ,
    \lane_terminate_temp_reg[5] ,
    \code_error_pipe_reg[5] ,
    \tx_is_idle_pipe_reg[1] ,
    \code_comma_pipe_reg[1] ,
    mgt_rxcharisk,
    usrclk,
    mgt_codevalid,
    mgt_codecomma,
    mgt_rx_reset,
    \G_GOT_A[7].got_a_reg ,
    \G_GOT_A[6].got_a_reg ,
    \G_GOT_A[3].got_a_reg ,
    \G_GOT_A[2].got_a_reg ,
    mgt_rxdata);
  output align_toggle_0;
  output \err_cnt_reg[0]_0 ;
  output \err_cnt_reg[0]_1 ;
  output [1:0]D;
  output [31:0]Q;
  output [3:0]\rxc_pipe_reg[5] ;
  output [3:0]\code_valid_pipe_reg[1] ;
  output [3:0]\lane_terminate_temp_reg[5] ;
  output [3:0]\code_error_pipe_reg[5] ;
  output [1:0]\tx_is_idle_pipe_reg[1] ;
  output [3:0]\code_comma_pipe_reg[1] ;
  input [3:0]mgt_rxcharisk;
  input usrclk;
  input [3:0]mgt_codevalid;
  input [3:0]mgt_codecomma;
  input [0:0]mgt_rx_reset;
  input \G_GOT_A[7].got_a_reg ;
  input \G_GOT_A[6].got_a_reg ;
  input \G_GOT_A[3].got_a_reg ;
  input \G_GOT_A[2].got_a_reg ;
  input [31:0]mgt_rxdata;

  wire [1:0]D;
  wire \G_GOT_A[2].got_a_reg ;
  wire \G_GOT_A[3].got_a_reg ;
  wire \G_GOT_A[6].got_a_reg ;
  wire \G_GOT_A[7].got_a_reg ;
  wire [31:0]Q;
  wire align_toggle_0;
  wire [3:0]\code_comma_pipe_reg[1] ;
  wire \code_error_pipe[0]_i_2_n_0 ;
  wire \code_error_pipe[0]_i_3_n_0 ;
  wire \code_error_pipe[1]_i_2_n_0 ;
  wire \code_error_pipe[1]_i_3_n_0 ;
  wire \code_error_pipe[4]_i_2_n_0 ;
  wire \code_error_pipe[4]_i_3_n_0 ;
  wire \code_error_pipe[5]_i_2_n_0 ;
  wire \code_error_pipe[5]_i_3_n_0 ;
  wire [3:0]\code_error_pipe_reg[5] ;
  wire [3:0]\code_valid_pipe_reg[1] ;
  wire codecomma_prev;
  wire codevalid_prev;
  wire \deskew_error[0]_i_4_n_0 ;
  wire \deskew_error[0]_i_5_n_0 ;
  wire \deskew_error[1]_i_4_n_0 ;
  wire \deskew_error[1]_i_5_n_0 ;
  wire [1:0]err_cnt;
  wire \err_cnt[0]_i_1_n_0 ;
  wire \err_cnt[1]_i_1_n_0 ;
  wire \err_cnt[1]_i_2_n_0 ;
  wire \err_cnt_reg[0]_0 ;
  wire \err_cnt_reg[0]_1 ;
  wire \lane_terminate_temp[0]_i_2_n_0 ;
  wire \lane_terminate_temp[1]_i_2_n_0 ;
  wire \lane_terminate_temp[4]_i_2_n_0 ;
  wire \lane_terminate_temp[5]_i_2_n_0 ;
  wire [3:0]\lane_terminate_temp_reg[5] ;
  wire [3:0]mgt_codecomma;
  wire \mgt_codecomma_reg[0]_i_1_n_0 ;
  wire \mgt_codecomma_reg[1]_i_1_n_0 ;
  wire \mgt_codecomma_reg[2]_i_1_n_0 ;
  wire \mgt_codecomma_reg[3]_i_1_n_0 ;
  wire [3:0]mgt_codevalid;
  wire \mgt_codevalid_reg[0]_i_1_n_0 ;
  wire \mgt_codevalid_reg[1]_i_1_n_0 ;
  wire \mgt_codevalid_reg[2]_i_1_n_0 ;
  wire \mgt_codevalid_reg[3]_i_1_n_0 ;
  wire [0:0]mgt_rx_reset;
  wire [3:0]mgt_rxcharisk;
  wire \mgt_rxcharisk_reg[0]_i_1_n_0 ;
  wire \mgt_rxcharisk_reg[1]_i_1_n_0 ;
  wire \mgt_rxcharisk_reg[2]_i_1_n_0 ;
  wire \mgt_rxcharisk_reg[3]_i_1_n_0 ;
  wire [31:0]mgt_rxdata;
  wire [31:0]p_0_in;
  wire [3:0]\rxc_pipe_reg[5] ;
  wire rxcharisk_prev;
  wire [7:0]rxdata_prev;
  wire shift_en_i_1_n_0;
  wire \tx_is_idle_pipe[0]_i_2_n_0 ;
  wire \tx_is_idle_pipe[1]_i_2_n_0 ;
  wire [1:0]\tx_is_idle_pipe_reg[1] ;
  wire usrclk;
  wire \xaui_i/receiver/deskew_state/G_GOT_A[0].got_a_reg ;
  wire \xaui_i/receiver/deskew_state/G_GOT_A[1].got_a_reg ;
  wire \xaui_i/receiver/deskew_state/G_GOT_A[4].got_a_reg ;
  wire \xaui_i/receiver/deskew_state/G_GOT_A[5].got_a_reg ;

  LUT6 #(
    .INIT(64'hFEAA0000FFFFFFFF)) 
    \code_error_pipe[0]_i_1 
       (.I0(\code_error_pipe[0]_i_2_n_0 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\code_error_pipe[0]_i_3_n_0 ),
        .I4(\rxc_pipe_reg[5] [0]),
        .I5(\code_valid_pipe_reg[1] [0]),
        .O(\code_error_pipe_reg[5] [0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \code_error_pipe[0]_i_2 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(\code_error_pipe[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \code_error_pipe[0]_i_3 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[7]),
        .O(\code_error_pipe[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFEAA0000FFFFFFFF)) 
    \code_error_pipe[1]_i_1 
       (.I0(\code_error_pipe[1]_i_2_n_0 ),
        .I1(Q[8]),
        .I2(Q[9]),
        .I3(\code_error_pipe[1]_i_3_n_0 ),
        .I4(\rxc_pipe_reg[5] [1]),
        .I5(\code_valid_pipe_reg[1] [1]),
        .O(\code_error_pipe_reg[5] [1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \code_error_pipe[1]_i_2 
       (.I0(Q[12]),
        .I1(Q[11]),
        .I2(Q[10]),
        .I3(Q[8]),
        .I4(Q[9]),
        .O(\code_error_pipe[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \code_error_pipe[1]_i_3 
       (.I0(Q[14]),
        .I1(Q[13]),
        .I2(Q[15]),
        .O(\code_error_pipe[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFEAA0000FFFFFFFF)) 
    \code_error_pipe[4]_i_1 
       (.I0(\code_error_pipe[4]_i_2_n_0 ),
        .I1(Q[16]),
        .I2(Q[17]),
        .I3(\code_error_pipe[4]_i_3_n_0 ),
        .I4(\rxc_pipe_reg[5] [2]),
        .I5(\code_valid_pipe_reg[1] [2]),
        .O(\code_error_pipe_reg[5] [2]));
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \code_error_pipe[4]_i_2 
       (.I0(Q[20]),
        .I1(Q[19]),
        .I2(Q[18]),
        .I3(Q[16]),
        .I4(Q[17]),
        .O(\code_error_pipe[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \code_error_pipe[4]_i_3 
       (.I0(Q[22]),
        .I1(Q[21]),
        .I2(Q[23]),
        .O(\code_error_pipe[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFEAA0000FFFFFFFF)) 
    \code_error_pipe[5]_i_1 
       (.I0(\code_error_pipe[5]_i_2_n_0 ),
        .I1(Q[24]),
        .I2(Q[25]),
        .I3(\code_error_pipe[5]_i_3_n_0 ),
        .I4(\rxc_pipe_reg[5] [3]),
        .I5(\code_valid_pipe_reg[1] [3]),
        .O(\code_error_pipe_reg[5] [3]));
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \code_error_pipe[5]_i_2 
       (.I0(Q[28]),
        .I1(Q[27]),
        .I2(Q[26]),
        .I3(Q[24]),
        .I4(Q[25]),
        .O(\code_error_pipe[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \code_error_pipe[5]_i_3 
       (.I0(Q[30]),
        .I1(Q[29]),
        .I2(Q[31]),
        .O(\code_error_pipe[5]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    codecomma_prev_reg
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_codecomma[3]),
        .Q(codecomma_prev),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    codevalid_prev_reg
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_codevalid[3]),
        .Q(codevalid_prev),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h7FFE)) 
    \deskew_error[0]_i_1 
       (.I0(\xaui_i/receiver/deskew_state/G_GOT_A[1].got_a_reg ),
        .I1(\G_GOT_A[3].got_a_reg ),
        .I2(\G_GOT_A[2].got_a_reg ),
        .I3(\xaui_i/receiver/deskew_state/G_GOT_A[0].got_a_reg ),
        .O(D[0]));
  LUT5 #(
    .INIT(32'h02000000)) 
    \deskew_error[0]_i_2 
       (.I0(\deskew_error[0]_i_4_n_0 ),
        .I1(Q[8]),
        .I2(Q[9]),
        .I3(\rxc_pipe_reg[5] [1]),
        .I4(\code_valid_pipe_reg[1] [1]),
        .O(\xaui_i/receiver/deskew_state/G_GOT_A[1].got_a_reg ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \deskew_error[0]_i_3 
       (.I0(\deskew_error[0]_i_5_n_0 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\rxc_pipe_reg[5] [0]),
        .I4(\code_valid_pipe_reg[1] [0]),
        .O(\xaui_i/receiver/deskew_state/G_GOT_A[0].got_a_reg ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \deskew_error[0]_i_4 
       (.I0(Q[11]),
        .I1(Q[10]),
        .I2(Q[12]),
        .I3(Q[15]),
        .I4(Q[14]),
        .I5(Q[13]),
        .O(\deskew_error[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \deskew_error[0]_i_5 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[4]),
        .I3(Q[7]),
        .I4(Q[6]),
        .I5(Q[5]),
        .O(\deskew_error[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h7FFE)) 
    \deskew_error[1]_i_1 
       (.I0(\xaui_i/receiver/deskew_state/G_GOT_A[5].got_a_reg ),
        .I1(\G_GOT_A[7].got_a_reg ),
        .I2(\G_GOT_A[6].got_a_reg ),
        .I3(\xaui_i/receiver/deskew_state/G_GOT_A[4].got_a_reg ),
        .O(D[1]));
  LUT5 #(
    .INIT(32'h02000000)) 
    \deskew_error[1]_i_2 
       (.I0(\deskew_error[1]_i_4_n_0 ),
        .I1(Q[24]),
        .I2(Q[25]),
        .I3(\rxc_pipe_reg[5] [3]),
        .I4(\code_valid_pipe_reg[1] [3]),
        .O(\xaui_i/receiver/deskew_state/G_GOT_A[5].got_a_reg ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \deskew_error[1]_i_3 
       (.I0(\deskew_error[1]_i_5_n_0 ),
        .I1(Q[16]),
        .I2(Q[17]),
        .I3(\rxc_pipe_reg[5] [2]),
        .I4(\code_valid_pipe_reg[1] [2]),
        .O(\xaui_i/receiver/deskew_state/G_GOT_A[4].got_a_reg ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \deskew_error[1]_i_4 
       (.I0(Q[27]),
        .I1(Q[26]),
        .I2(Q[28]),
        .I3(Q[31]),
        .I4(Q[30]),
        .I5(Q[29]),
        .O(\deskew_error[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \deskew_error[1]_i_5 
       (.I0(Q[19]),
        .I1(Q[18]),
        .I2(Q[20]),
        .I3(Q[23]),
        .I4(Q[22]),
        .I5(Q[21]),
        .O(\deskew_error[1]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h000008F0)) 
    \err_cnt[0]_i_1 
       (.I0(\err_cnt_reg[0]_0 ),
        .I1(\err_cnt_reg[0]_1 ),
        .I2(err_cnt[0]),
        .I3(\err_cnt[1]_i_2_n_0 ),
        .I4(mgt_rx_reset),
        .O(\err_cnt[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000008F080F0)) 
    \err_cnt[1]_i_1 
       (.I0(\err_cnt_reg[0]_0 ),
        .I1(\err_cnt_reg[0]_1 ),
        .I2(err_cnt[1]),
        .I3(\err_cnt[1]_i_2_n_0 ),
        .I4(err_cnt[0]),
        .I5(mgt_rx_reset),
        .O(\err_cnt[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF8)) 
    \err_cnt[1]_i_2 
       (.I0(err_cnt[0]),
        .I1(err_cnt[1]),
        .I2(\xaui_i/receiver/deskew_state/G_GOT_A[1].got_a_reg ),
        .I3(\xaui_i/receiver/deskew_state/G_GOT_A[0].got_a_reg ),
        .I4(\xaui_i/receiver/deskew_state/G_GOT_A[5].got_a_reg ),
        .I5(\xaui_i/receiver/deskew_state/G_GOT_A[4].got_a_reg ),
        .O(\err_cnt[1]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \err_cnt_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\err_cnt[0]_i_1_n_0 ),
        .Q(err_cnt[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \err_cnt_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\err_cnt[1]_i_1_n_0 ),
        .Q(err_cnt[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \got_align[0]_i_2 
       (.I0(\xaui_i/receiver/deskew_state/G_GOT_A[0].got_a_reg ),
        .I1(\xaui_i/receiver/deskew_state/G_GOT_A[1].got_a_reg ),
        .O(\err_cnt_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \got_align[1]_i_2 
       (.I0(\xaui_i/receiver/deskew_state/G_GOT_A[4].got_a_reg ),
        .I1(\xaui_i/receiver/deskew_state/G_GOT_A[5].got_a_reg ),
        .O(\err_cnt_reg[0]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \lane_terminate_temp[0]_i_1 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[7]),
        .I3(\lane_terminate_temp[0]_i_2_n_0 ),
        .I4(\code_error_pipe_reg[5] [0]),
        .O(\lane_terminate_temp_reg[5] [0]));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \lane_terminate_temp[0]_i_2 
       (.I0(Q[4]),
        .I1(Q[1]),
        .I2(\rxc_pipe_reg[5] [0]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\lane_terminate_temp[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \lane_terminate_temp[1]_i_1 
       (.I0(Q[14]),
        .I1(Q[13]),
        .I2(Q[15]),
        .I3(\lane_terminate_temp[1]_i_2_n_0 ),
        .I4(\code_error_pipe_reg[5] [1]),
        .O(\lane_terminate_temp_reg[5] [1]));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \lane_terminate_temp[1]_i_2 
       (.I0(Q[12]),
        .I1(Q[9]),
        .I2(\rxc_pipe_reg[5] [1]),
        .I3(Q[8]),
        .I4(Q[10]),
        .I5(Q[11]),
        .O(\lane_terminate_temp[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \lane_terminate_temp[4]_i_1 
       (.I0(Q[22]),
        .I1(Q[21]),
        .I2(Q[23]),
        .I3(\lane_terminate_temp[4]_i_2_n_0 ),
        .I4(\code_error_pipe_reg[5] [2]),
        .O(\lane_terminate_temp_reg[5] [2]));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \lane_terminate_temp[4]_i_2 
       (.I0(Q[20]),
        .I1(Q[17]),
        .I2(\rxc_pipe_reg[5] [2]),
        .I3(Q[16]),
        .I4(Q[18]),
        .I5(Q[19]),
        .O(\lane_terminate_temp[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \lane_terminate_temp[5]_i_1 
       (.I0(Q[30]),
        .I1(Q[29]),
        .I2(Q[31]),
        .I3(\lane_terminate_temp[5]_i_2_n_0 ),
        .I4(\code_error_pipe_reg[5] [3]),
        .O(\lane_terminate_temp_reg[5] [3]));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \lane_terminate_temp[5]_i_2 
       (.I0(Q[28]),
        .I1(Q[25]),
        .I2(\rxc_pipe_reg[5] [3]),
        .I3(Q[24]),
        .I4(Q[26]),
        .I5(Q[27]),
        .O(\lane_terminate_temp[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codecomma_reg[0]_i_1 
       (.I0(codecomma_prev),
        .I1(align_toggle_0),
        .I2(mgt_codecomma[0]),
        .O(\mgt_codecomma_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codecomma_reg[1]_i_1 
       (.I0(mgt_codecomma[0]),
        .I1(align_toggle_0),
        .I2(mgt_codecomma[1]),
        .O(\mgt_codecomma_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codecomma_reg[2]_i_1 
       (.I0(mgt_codecomma[1]),
        .I1(align_toggle_0),
        .I2(mgt_codecomma[2]),
        .O(\mgt_codecomma_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codecomma_reg[3]_i_1 
       (.I0(mgt_codecomma[2]),
        .I1(align_toggle_0),
        .I2(mgt_codecomma[3]),
        .O(\mgt_codecomma_reg[3]_i_1_n_0 ));
  FDRE \mgt_codecomma_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg[0]_i_1_n_0 ),
        .Q(\code_comma_pipe_reg[1] [0]),
        .R(1'b0));
  FDRE \mgt_codecomma_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg[1]_i_1_n_0 ),
        .Q(\code_comma_pipe_reg[1] [1]),
        .R(1'b0));
  FDRE \mgt_codecomma_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg[2]_i_1_n_0 ),
        .Q(\code_comma_pipe_reg[1] [2]),
        .R(1'b0));
  FDRE \mgt_codecomma_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg[3]_i_1_n_0 ),
        .Q(\code_comma_pipe_reg[1] [3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codevalid_reg[0]_i_1 
       (.I0(codevalid_prev),
        .I1(align_toggle_0),
        .I2(mgt_codevalid[0]),
        .O(\mgt_codevalid_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codevalid_reg[1]_i_1 
       (.I0(mgt_codevalid[0]),
        .I1(align_toggle_0),
        .I2(mgt_codevalid[1]),
        .O(\mgt_codevalid_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codevalid_reg[2]_i_1 
       (.I0(mgt_codevalid[1]),
        .I1(align_toggle_0),
        .I2(mgt_codevalid[2]),
        .O(\mgt_codevalid_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codevalid_reg[3]_i_1 
       (.I0(mgt_codevalid[2]),
        .I1(align_toggle_0),
        .I2(mgt_codevalid[3]),
        .O(\mgt_codevalid_reg[3]_i_1_n_0 ));
  FDRE \mgt_codevalid_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg[0]_i_1_n_0 ),
        .Q(\code_valid_pipe_reg[1] [0]),
        .R(1'b0));
  FDRE \mgt_codevalid_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg[1]_i_1_n_0 ),
        .Q(\code_valid_pipe_reg[1] [1]),
        .R(1'b0));
  FDRE \mgt_codevalid_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg[2]_i_1_n_0 ),
        .Q(\code_valid_pipe_reg[1] [2]),
        .R(1'b0));
  FDRE \mgt_codevalid_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg[3]_i_1_n_0 ),
        .Q(\code_valid_pipe_reg[1] [3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_rxcharisk_reg[0]_i_1 
       (.I0(rxcharisk_prev),
        .I1(align_toggle_0),
        .I2(mgt_rxcharisk[0]),
        .O(\mgt_rxcharisk_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_rxcharisk_reg[1]_i_1 
       (.I0(mgt_rxcharisk[0]),
        .I1(align_toggle_0),
        .I2(mgt_rxcharisk[1]),
        .O(\mgt_rxcharisk_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_rxcharisk_reg[2]_i_1 
       (.I0(mgt_rxcharisk[1]),
        .I1(align_toggle_0),
        .I2(mgt_rxcharisk[2]),
        .O(\mgt_rxcharisk_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_rxcharisk_reg[3]_i_1 
       (.I0(mgt_rxcharisk[2]),
        .I1(align_toggle_0),
        .I2(mgt_rxcharisk[3]),
        .O(\mgt_rxcharisk_reg[3]_i_1_n_0 ));
  FDRE \mgt_rxcharisk_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg[0]_i_1_n_0 ),
        .Q(\rxc_pipe_reg[5] [0]),
        .R(1'b0));
  FDRE \mgt_rxcharisk_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg[1]_i_1_n_0 ),
        .Q(\rxc_pipe_reg[5] [1]),
        .R(1'b0));
  FDRE \mgt_rxcharisk_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg[2]_i_1_n_0 ),
        .Q(\rxc_pipe_reg[5] [2]),
        .R(1'b0));
  FDRE \mgt_rxcharisk_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg[3]_i_1_n_0 ),
        .Q(\rxc_pipe_reg[5] [3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[0]_i_1 
       (.I0(rxdata_prev[0]),
        .I1(mgt_rxdata[0]),
        .I2(align_toggle_0),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[10]_i_1 
       (.I0(mgt_rxdata[2]),
        .I1(mgt_rxdata[10]),
        .I2(align_toggle_0),
        .O(p_0_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[11]_i_1 
       (.I0(mgt_rxdata[3]),
        .I1(mgt_rxdata[11]),
        .I2(align_toggle_0),
        .O(p_0_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[12]_i_1 
       (.I0(mgt_rxdata[4]),
        .I1(mgt_rxdata[12]),
        .I2(align_toggle_0),
        .O(p_0_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[13]_i_1 
       (.I0(mgt_rxdata[5]),
        .I1(mgt_rxdata[13]),
        .I2(align_toggle_0),
        .O(p_0_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[14]_i_1 
       (.I0(mgt_rxdata[6]),
        .I1(mgt_rxdata[14]),
        .I2(align_toggle_0),
        .O(p_0_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[15]_i_1 
       (.I0(mgt_rxdata[7]),
        .I1(mgt_rxdata[15]),
        .I2(align_toggle_0),
        .O(p_0_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[16]_i_1 
       (.I0(mgt_rxdata[8]),
        .I1(mgt_rxdata[16]),
        .I2(align_toggle_0),
        .O(p_0_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[17]_i_1 
       (.I0(mgt_rxdata[9]),
        .I1(mgt_rxdata[17]),
        .I2(align_toggle_0),
        .O(p_0_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[18]_i_1 
       (.I0(mgt_rxdata[10]),
        .I1(mgt_rxdata[18]),
        .I2(align_toggle_0),
        .O(p_0_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[19]_i_1 
       (.I0(mgt_rxdata[11]),
        .I1(mgt_rxdata[19]),
        .I2(align_toggle_0),
        .O(p_0_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[1]_i_1 
       (.I0(rxdata_prev[1]),
        .I1(mgt_rxdata[1]),
        .I2(align_toggle_0),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[20]_i_1 
       (.I0(mgt_rxdata[12]),
        .I1(mgt_rxdata[20]),
        .I2(align_toggle_0),
        .O(p_0_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[21]_i_1 
       (.I0(mgt_rxdata[13]),
        .I1(mgt_rxdata[21]),
        .I2(align_toggle_0),
        .O(p_0_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[22]_i_1 
       (.I0(mgt_rxdata[14]),
        .I1(mgt_rxdata[22]),
        .I2(align_toggle_0),
        .O(p_0_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[23]_i_1 
       (.I0(mgt_rxdata[15]),
        .I1(mgt_rxdata[23]),
        .I2(align_toggle_0),
        .O(p_0_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[24]_i_1 
       (.I0(mgt_rxdata[16]),
        .I1(mgt_rxdata[24]),
        .I2(align_toggle_0),
        .O(p_0_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[25]_i_1 
       (.I0(mgt_rxdata[17]),
        .I1(mgt_rxdata[25]),
        .I2(align_toggle_0),
        .O(p_0_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[26]_i_1 
       (.I0(mgt_rxdata[18]),
        .I1(mgt_rxdata[26]),
        .I2(align_toggle_0),
        .O(p_0_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[27]_i_1 
       (.I0(mgt_rxdata[19]),
        .I1(mgt_rxdata[27]),
        .I2(align_toggle_0),
        .O(p_0_in[27]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[28]_i_1 
       (.I0(mgt_rxdata[20]),
        .I1(mgt_rxdata[28]),
        .I2(align_toggle_0),
        .O(p_0_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[29]_i_1 
       (.I0(mgt_rxdata[21]),
        .I1(mgt_rxdata[29]),
        .I2(align_toggle_0),
        .O(p_0_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[2]_i_1 
       (.I0(rxdata_prev[2]),
        .I1(mgt_rxdata[2]),
        .I2(align_toggle_0),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[30]_i_1 
       (.I0(mgt_rxdata[22]),
        .I1(mgt_rxdata[30]),
        .I2(align_toggle_0),
        .O(p_0_in[30]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[31]_i_1 
       (.I0(mgt_rxdata[23]),
        .I1(mgt_rxdata[31]),
        .I2(align_toggle_0),
        .O(p_0_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[3]_i_1 
       (.I0(rxdata_prev[3]),
        .I1(mgt_rxdata[3]),
        .I2(align_toggle_0),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[4]_i_1 
       (.I0(rxdata_prev[4]),
        .I1(mgt_rxdata[4]),
        .I2(align_toggle_0),
        .O(p_0_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[5]_i_1 
       (.I0(rxdata_prev[5]),
        .I1(mgt_rxdata[5]),
        .I2(align_toggle_0),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[6]_i_1 
       (.I0(rxdata_prev[6]),
        .I1(mgt_rxdata[6]),
        .I2(align_toggle_0),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[7]_i_1 
       (.I0(rxdata_prev[7]),
        .I1(mgt_rxdata[7]),
        .I2(align_toggle_0),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[8]_i_1 
       (.I0(mgt_rxdata[0]),
        .I1(mgt_rxdata[8]),
        .I2(align_toggle_0),
        .O(p_0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[9]_i_1 
       (.I0(mgt_rxdata[1]),
        .I1(mgt_rxdata[9]),
        .I2(align_toggle_0),
        .O(p_0_in[9]));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[10] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(Q[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[11] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(Q[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[12] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(Q[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[13] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(Q[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[14] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(Q[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[15] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[15]),
        .Q(Q[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[16] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[16]),
        .Q(Q[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[17] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[17]),
        .Q(Q[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[18] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[18]),
        .Q(Q[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[19] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[19]),
        .Q(Q[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(Q[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[20] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[20]),
        .Q(Q[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[21] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[21]),
        .Q(Q[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[22] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[22]),
        .Q(Q[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[23] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[23]),
        .Q(Q[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[24] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[24]),
        .Q(Q[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[25] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[25]),
        .Q(Q[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[26] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[26]),
        .Q(Q[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[27] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[27]),
        .Q(Q[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[28] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[28]),
        .Q(Q[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[29] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[29]),
        .Q(Q[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(Q[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[30] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[30]),
        .Q(Q[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[31] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[31]),
        .Q(Q[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(Q[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(Q[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(Q[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(Q[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(Q[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[8] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(Q[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[9] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(Q[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxcharisk_prev_reg
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_rxcharisk[3]),
        .Q(rxcharisk_prev),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[0] 
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_rxdata[24]),
        .Q(rxdata_prev[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[1] 
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_rxdata[25]),
        .Q(rxdata_prev[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[2] 
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_rxdata[26]),
        .Q(rxdata_prev[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[3] 
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_rxdata[27]),
        .Q(rxdata_prev[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[4] 
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_rxdata[28]),
        .Q(rxdata_prev[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[5] 
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_rxdata[29]),
        .Q(rxdata_prev[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[6] 
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_rxdata[30]),
        .Q(rxdata_prev[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[7] 
       (.C(usrclk),
        .CE(align_toggle_0),
        .D(mgt_rxdata[31]),
        .Q(rxdata_prev[7]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h006A)) 
    shift_en_i_1
       (.I0(align_toggle_0),
        .I1(err_cnt[0]),
        .I2(err_cnt[1]),
        .I3(mgt_rx_reset),
        .O(shift_en_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    shift_en_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(shift_en_i_1_n_0),
        .Q(align_toggle_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h20800020)) 
    \tx_is_idle_pipe[0]_i_1 
       (.I0(\tx_is_idle_pipe[0]_i_2_n_0 ),
        .I1(Q[6]),
        .I2(\rxc_pipe_reg[5] [0]),
        .I3(Q[7]),
        .I4(Q[5]),
        .O(\tx_is_idle_pipe_reg[1] [0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h04000000)) 
    \tx_is_idle_pipe[0]_i_2 
       (.I0(Q[0]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[4]),
        .I4(Q[3]),
        .O(\tx_is_idle_pipe[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h20800020)) 
    \tx_is_idle_pipe[1]_i_1 
       (.I0(\tx_is_idle_pipe[1]_i_2_n_0 ),
        .I1(Q[14]),
        .I2(\rxc_pipe_reg[5] [1]),
        .I3(Q[15]),
        .I4(Q[13]),
        .O(\tx_is_idle_pipe_reg[1] [1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h04000000)) 
    \tx_is_idle_pipe[1]_i_2 
       (.I0(Q[8]),
        .I1(Q[10]),
        .I2(Q[9]),
        .I3(Q[12]),
        .I4(Q[11]),
        .O(\tx_is_idle_pipe[1]_i_2_n_0 ));
endmodule

(* ORIG_REF_NAME = "rxaui_v4_3_7_dune_align" *) 
module rxaui_0_rxaui_v4_3_7_dune_align_11
   (align_toggle_1,
    \G_GOT_A[7].got_a_reg ,
    \G_GOT_A[6].got_a_reg ,
    \G_GOT_A[3].got_a_reg ,
    \G_GOT_A[2].got_a_reg ,
    D,
    Q,
    \code_valid_pipe_reg[1] ,
    \rxc_pipe_reg[7] ,
    \lane_terminate_temp_reg[7] ,
    \code_error_pipe_reg[7] ,
    \tx_is_idle_pipe_reg[3] ,
    \code_comma_pipe_reg[1] ,
    mgt_rxcharisk,
    usrclk,
    mgt_codevalid,
    mgt_codecomma,
    \mgt_rxdata_reg_reg[16]_0 ,
    \mgt_rxdata_reg_reg[0]_0 ,
    mgt_rx_reset,
    mgt_rxdata);
  output align_toggle_1;
  output \G_GOT_A[7].got_a_reg ;
  output \G_GOT_A[6].got_a_reg ;
  output \G_GOT_A[3].got_a_reg ;
  output \G_GOT_A[2].got_a_reg ;
  output [1:0]D;
  output [31:0]Q;
  output [3:0]\code_valid_pipe_reg[1] ;
  output [3:0]\rxc_pipe_reg[7] ;
  output [3:0]\lane_terminate_temp_reg[7] ;
  output [3:0]\code_error_pipe_reg[7] ;
  output [1:0]\tx_is_idle_pipe_reg[3] ;
  output [3:0]\code_comma_pipe_reg[1] ;
  input [3:0]mgt_rxcharisk;
  input usrclk;
  input [3:0]mgt_codevalid;
  input [3:0]mgt_codecomma;
  input \mgt_rxdata_reg_reg[16]_0 ;
  input \mgt_rxdata_reg_reg[0]_0 ;
  input [0:0]mgt_rx_reset;
  input [31:0]mgt_rxdata;

  wire [1:0]D;
  wire \G_GOT_A[2].got_a_reg ;
  wire \G_GOT_A[3].got_a_reg ;
  wire \G_GOT_A[6].got_a_reg ;
  wire \G_GOT_A[7].got_a_reg ;
  wire [31:0]Q;
  wire align_toggle_1;
  wire [3:0]\code_comma_pipe_reg[1] ;
  wire \code_error_pipe[2]_i_2_n_0 ;
  wire \code_error_pipe[2]_i_3_n_0 ;
  wire \code_error_pipe[3]_i_2_n_0 ;
  wire \code_error_pipe[3]_i_3_n_0 ;
  wire \code_error_pipe[6]_i_2_n_0 ;
  wire \code_error_pipe[6]_i_3_n_0 ;
  wire \code_error_pipe[7]_i_2_n_0 ;
  wire \code_error_pipe[7]_i_3_n_0 ;
  wire [3:0]\code_error_pipe_reg[7] ;
  wire [3:0]\code_valid_pipe_reg[1] ;
  wire codecomma_prev;
  wire codevalid_prev;
  wire [1:0]err_cnt;
  wire err_cnt12_out;
  wire \err_cnt[0]_i_1_n_0 ;
  wire \err_cnt[1]_i_1_n_0 ;
  wire \err_cnt[1]_i_2__0_n_0 ;
  wire \got_align[0]_i_5_n_0 ;
  wire \got_align[0]_i_6_n_0 ;
  wire \got_align[1]_i_5_n_0 ;
  wire \got_align[1]_i_6_n_0 ;
  wire \lane_terminate_temp[2]_i_2_n_0 ;
  wire \lane_terminate_temp[3]_i_2_n_0 ;
  wire \lane_terminate_temp[6]_i_2_n_0 ;
  wire \lane_terminate_temp[7]_i_2_n_0 ;
  wire [3:0]\lane_terminate_temp_reg[7] ;
  wire [3:0]mgt_codecomma;
  wire \mgt_codecomma_reg[0]_i_1_n_0 ;
  wire \mgt_codecomma_reg[1]_i_1_n_0 ;
  wire \mgt_codecomma_reg[2]_i_1_n_0 ;
  wire \mgt_codecomma_reg[3]_i_1_n_0 ;
  wire [3:0]mgt_codevalid;
  wire \mgt_codevalid_reg[0]_i_1_n_0 ;
  wire \mgt_codevalid_reg[1]_i_1_n_0 ;
  wire \mgt_codevalid_reg[2]_i_1_n_0 ;
  wire \mgt_codevalid_reg[3]_i_1_n_0 ;
  wire [0:0]mgt_rx_reset;
  wire [3:0]mgt_rxcharisk;
  wire \mgt_rxcharisk_reg[0]_i_1_n_0 ;
  wire \mgt_rxcharisk_reg[1]_i_1_n_0 ;
  wire \mgt_rxcharisk_reg[2]_i_1_n_0 ;
  wire \mgt_rxcharisk_reg[3]_i_1_n_0 ;
  wire [31:0]mgt_rxdata;
  wire \mgt_rxdata_reg[0]_i_1_n_0 ;
  wire \mgt_rxdata_reg[10]_i_1_n_0 ;
  wire \mgt_rxdata_reg[11]_i_1_n_0 ;
  wire \mgt_rxdata_reg[12]_i_1_n_0 ;
  wire \mgt_rxdata_reg[13]_i_1_n_0 ;
  wire \mgt_rxdata_reg[14]_i_1_n_0 ;
  wire \mgt_rxdata_reg[15]_i_1_n_0 ;
  wire \mgt_rxdata_reg[16]_i_1_n_0 ;
  wire \mgt_rxdata_reg[17]_i_1_n_0 ;
  wire \mgt_rxdata_reg[18]_i_1_n_0 ;
  wire \mgt_rxdata_reg[19]_i_1_n_0 ;
  wire \mgt_rxdata_reg[1]_i_1_n_0 ;
  wire \mgt_rxdata_reg[20]_i_1_n_0 ;
  wire \mgt_rxdata_reg[21]_i_1_n_0 ;
  wire \mgt_rxdata_reg[22]_i_1_n_0 ;
  wire \mgt_rxdata_reg[23]_i_1_n_0 ;
  wire \mgt_rxdata_reg[24]_i_1_n_0 ;
  wire \mgt_rxdata_reg[25]_i_1_n_0 ;
  wire \mgt_rxdata_reg[26]_i_1_n_0 ;
  wire \mgt_rxdata_reg[27]_i_1_n_0 ;
  wire \mgt_rxdata_reg[28]_i_1_n_0 ;
  wire \mgt_rxdata_reg[29]_i_1_n_0 ;
  wire \mgt_rxdata_reg[2]_i_1_n_0 ;
  wire \mgt_rxdata_reg[30]_i_1_n_0 ;
  wire \mgt_rxdata_reg[31]_i_1_n_0 ;
  wire \mgt_rxdata_reg[3]_i_1_n_0 ;
  wire \mgt_rxdata_reg[4]_i_1_n_0 ;
  wire \mgt_rxdata_reg[5]_i_1_n_0 ;
  wire \mgt_rxdata_reg[6]_i_1_n_0 ;
  wire \mgt_rxdata_reg[7]_i_1_n_0 ;
  wire \mgt_rxdata_reg[8]_i_1_n_0 ;
  wire \mgt_rxdata_reg[9]_i_1_n_0 ;
  wire \mgt_rxdata_reg_reg[0]_0 ;
  wire \mgt_rxdata_reg_reg[16]_0 ;
  wire [3:0]\rxc_pipe_reg[7] ;
  wire rxcharisk_prev;
  wire \rxdata_prev_reg_n_0_[0] ;
  wire \rxdata_prev_reg_n_0_[1] ;
  wire \rxdata_prev_reg_n_0_[2] ;
  wire \rxdata_prev_reg_n_0_[3] ;
  wire \rxdata_prev_reg_n_0_[4] ;
  wire \rxdata_prev_reg_n_0_[5] ;
  wire \rxdata_prev_reg_n_0_[6] ;
  wire \rxdata_prev_reg_n_0_[7] ;
  wire shift_en_i_1__0_n_0;
  wire \tx_is_idle_pipe[2]_i_2_n_0 ;
  wire \tx_is_idle_pipe[3]_i_2_n_0 ;
  wire [1:0]\tx_is_idle_pipe_reg[3] ;
  wire usrclk;

  LUT6 #(
    .INIT(64'hBBBF0000FFFFFFFF)) 
    \code_error_pipe[2]_i_1 
       (.I0(\code_error_pipe[2]_i_2_n_0 ),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\rxc_pipe_reg[7] [0]),
        .I5(\code_valid_pipe_reg[1] [0]),
        .O(\code_error_pipe_reg[7] [0]));
  LUT6 #(
    .INIT(64'hFF7F7FFF7FFF00FF)) 
    \code_error_pipe[2]_i_2 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[7]),
        .I3(\code_error_pipe[2]_i_3_n_0 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\code_error_pipe[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \code_error_pipe[2]_i_3 
       (.I0(Q[3]),
        .I1(Q[2]),
        .O(\code_error_pipe[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hBBBF0000FFFFFFFF)) 
    \code_error_pipe[3]_i_1 
       (.I0(\code_error_pipe[3]_i_2_n_0 ),
        .I1(Q[12]),
        .I2(Q[11]),
        .I3(Q[10]),
        .I4(\rxc_pipe_reg[7] [1]),
        .I5(\code_valid_pipe_reg[1] [1]),
        .O(\code_error_pipe_reg[7] [1]));
  LUT6 #(
    .INIT(64'hFF7F7FFF7FFF00FF)) 
    \code_error_pipe[3]_i_2 
       (.I0(Q[14]),
        .I1(Q[13]),
        .I2(Q[15]),
        .I3(\code_error_pipe[3]_i_3_n_0 ),
        .I4(Q[8]),
        .I5(Q[9]),
        .O(\code_error_pipe[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \code_error_pipe[3]_i_3 
       (.I0(Q[11]),
        .I1(Q[10]),
        .O(\code_error_pipe[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFEAA0000FFFFFFFF)) 
    \code_error_pipe[6]_i_1 
       (.I0(\code_error_pipe[6]_i_2_n_0 ),
        .I1(Q[16]),
        .I2(Q[17]),
        .I3(\code_error_pipe[6]_i_3_n_0 ),
        .I4(\rxc_pipe_reg[7] [2]),
        .I5(\code_valid_pipe_reg[1] [2]),
        .O(\code_error_pipe_reg[7] [2]));
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \code_error_pipe[6]_i_2 
       (.I0(Q[20]),
        .I1(Q[19]),
        .I2(Q[18]),
        .I3(Q[16]),
        .I4(Q[17]),
        .O(\code_error_pipe[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \code_error_pipe[6]_i_3 
       (.I0(Q[22]),
        .I1(Q[21]),
        .I2(Q[23]),
        .O(\code_error_pipe[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFEAA0000FFFFFFFF)) 
    \code_error_pipe[7]_i_1 
       (.I0(\code_error_pipe[7]_i_2_n_0 ),
        .I1(Q[24]),
        .I2(Q[25]),
        .I3(\code_error_pipe[7]_i_3_n_0 ),
        .I4(\rxc_pipe_reg[7] [3]),
        .I5(\code_valid_pipe_reg[1] [3]),
        .O(\code_error_pipe_reg[7] [3]));
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \code_error_pipe[7]_i_2 
       (.I0(Q[28]),
        .I1(Q[26]),
        .I2(Q[27]),
        .I3(Q[24]),
        .I4(Q[25]),
        .O(\code_error_pipe[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \code_error_pipe[7]_i_3 
       (.I0(Q[30]),
        .I1(Q[29]),
        .I2(Q[31]),
        .O(\code_error_pipe[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    codecomma_prev_reg
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_codecomma[3]),
        .Q(codecomma_prev),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    codevalid_prev_reg
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_codevalid[3]),
        .Q(codevalid_prev),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h0026)) 
    \err_cnt[0]_i_1 
       (.I0(err_cnt[0]),
        .I1(\err_cnt[1]_i_2__0_n_0 ),
        .I2(err_cnt12_out),
        .I3(mgt_rx_reset),
        .O(\err_cnt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'h0000226A)) 
    \err_cnt[1]_i_1 
       (.I0(err_cnt[1]),
        .I1(\err_cnt[1]_i_2__0_n_0 ),
        .I2(err_cnt[0]),
        .I3(err_cnt12_out),
        .I4(mgt_rx_reset),
        .O(\err_cnt[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF8)) 
    \err_cnt[1]_i_2__0 
       (.I0(err_cnt[0]),
        .I1(err_cnt[1]),
        .I2(\G_GOT_A[7].got_a_reg ),
        .I3(\G_GOT_A[6].got_a_reg ),
        .I4(\G_GOT_A[3].got_a_reg ),
        .I5(\G_GOT_A[2].got_a_reg ),
        .O(\err_cnt[1]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \err_cnt[1]_i_3 
       (.I0(\G_GOT_A[2].got_a_reg ),
        .I1(\G_GOT_A[3].got_a_reg ),
        .I2(\G_GOT_A[6].got_a_reg ),
        .I3(\G_GOT_A[7].got_a_reg ),
        .O(err_cnt12_out));
  FDRE #(
    .INIT(1'b0)) 
    \err_cnt_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\err_cnt[0]_i_1_n_0 ),
        .Q(err_cnt[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \err_cnt_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\err_cnt[1]_i_1_n_0 ),
        .Q(err_cnt[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \got_align[0]_i_1 
       (.I0(\mgt_rxdata_reg_reg[0]_0 ),
        .I1(\G_GOT_A[2].got_a_reg ),
        .I2(\G_GOT_A[3].got_a_reg ),
        .O(D[0]));
  LUT5 #(
    .INIT(32'h02000000)) 
    \got_align[0]_i_3 
       (.I0(\got_align[0]_i_5_n_0 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\code_valid_pipe_reg[1] [0]),
        .I4(\rxc_pipe_reg[7] [0]),
        .O(\G_GOT_A[2].got_a_reg ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \got_align[0]_i_4 
       (.I0(\got_align[0]_i_6_n_0 ),
        .I1(Q[8]),
        .I2(Q[9]),
        .I3(\code_valid_pipe_reg[1] [1]),
        .I4(\rxc_pipe_reg[7] [1]),
        .O(\G_GOT_A[3].got_a_reg ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \got_align[0]_i_5 
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(Q[4]),
        .I3(Q[7]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\got_align[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \got_align[0]_i_6 
       (.I0(Q[13]),
        .I1(Q[14]),
        .I2(Q[12]),
        .I3(Q[15]),
        .I4(Q[10]),
        .I5(Q[11]),
        .O(\got_align[0]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \got_align[1]_i_1 
       (.I0(\mgt_rxdata_reg_reg[16]_0 ),
        .I1(\G_GOT_A[6].got_a_reg ),
        .I2(\G_GOT_A[7].got_a_reg ),
        .O(D[1]));
  LUT5 #(
    .INIT(32'h02000000)) 
    \got_align[1]_i_3 
       (.I0(\got_align[1]_i_5_n_0 ),
        .I1(Q[16]),
        .I2(Q[17]),
        .I3(\code_valid_pipe_reg[1] [2]),
        .I4(\rxc_pipe_reg[7] [2]),
        .O(\G_GOT_A[6].got_a_reg ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \got_align[1]_i_4 
       (.I0(\got_align[1]_i_6_n_0 ),
        .I1(Q[24]),
        .I2(Q[25]),
        .I3(\code_valid_pipe_reg[1] [3]),
        .I4(\rxc_pipe_reg[7] [3]),
        .O(\G_GOT_A[7].got_a_reg ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \got_align[1]_i_5 
       (.I0(Q[19]),
        .I1(Q[18]),
        .I2(Q[20]),
        .I3(Q[23]),
        .I4(Q[22]),
        .I5(Q[21]),
        .O(\got_align[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \got_align[1]_i_6 
       (.I0(Q[28]),
        .I1(Q[31]),
        .I2(Q[26]),
        .I3(Q[27]),
        .I4(Q[30]),
        .I5(Q[29]),
        .O(\got_align[1]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \lane_terminate_temp[2]_i_1 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[7]),
        .I3(\lane_terminate_temp[2]_i_2_n_0 ),
        .I4(\code_error_pipe_reg[7] [0]),
        .O(\lane_terminate_temp_reg[7] [0]));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \lane_terminate_temp[2]_i_2 
       (.I0(Q[4]),
        .I1(Q[1]),
        .I2(\rxc_pipe_reg[7] [0]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\lane_terminate_temp[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \lane_terminate_temp[3]_i_1 
       (.I0(Q[14]),
        .I1(Q[13]),
        .I2(Q[15]),
        .I3(\lane_terminate_temp[3]_i_2_n_0 ),
        .I4(\code_error_pipe_reg[7] [1]),
        .O(\lane_terminate_temp_reg[7] [1]));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \lane_terminate_temp[3]_i_2 
       (.I0(Q[12]),
        .I1(Q[9]),
        .I2(\rxc_pipe_reg[7] [1]),
        .I3(Q[8]),
        .I4(Q[10]),
        .I5(Q[11]),
        .O(\lane_terminate_temp[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \lane_terminate_temp[6]_i_1 
       (.I0(Q[22]),
        .I1(Q[21]),
        .I2(Q[23]),
        .I3(\lane_terminate_temp[6]_i_2_n_0 ),
        .I4(\code_error_pipe_reg[7] [2]),
        .O(\lane_terminate_temp_reg[7] [2]));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \lane_terminate_temp[6]_i_2 
       (.I0(Q[20]),
        .I1(Q[17]),
        .I2(\rxc_pipe_reg[7] [2]),
        .I3(Q[16]),
        .I4(Q[18]),
        .I5(Q[19]),
        .O(\lane_terminate_temp[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \lane_terminate_temp[7]_i_1 
       (.I0(Q[30]),
        .I1(Q[29]),
        .I2(Q[31]),
        .I3(\lane_terminate_temp[7]_i_2_n_0 ),
        .I4(\code_error_pipe_reg[7] [3]),
        .O(\lane_terminate_temp_reg[7] [3]));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \lane_terminate_temp[7]_i_2 
       (.I0(Q[28]),
        .I1(Q[25]),
        .I2(\rxc_pipe_reg[7] [3]),
        .I3(Q[24]),
        .I4(Q[27]),
        .I5(Q[26]),
        .O(\lane_terminate_temp[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codecomma_reg[0]_i_1 
       (.I0(codecomma_prev),
        .I1(align_toggle_1),
        .I2(mgt_codecomma[0]),
        .O(\mgt_codecomma_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codecomma_reg[1]_i_1 
       (.I0(mgt_codecomma[0]),
        .I1(align_toggle_1),
        .I2(mgt_codecomma[1]),
        .O(\mgt_codecomma_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codecomma_reg[2]_i_1 
       (.I0(mgt_codecomma[1]),
        .I1(align_toggle_1),
        .I2(mgt_codecomma[2]),
        .O(\mgt_codecomma_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codecomma_reg[3]_i_1 
       (.I0(mgt_codecomma[2]),
        .I1(align_toggle_1),
        .I2(mgt_codecomma[3]),
        .O(\mgt_codecomma_reg[3]_i_1_n_0 ));
  FDRE \mgt_codecomma_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg[0]_i_1_n_0 ),
        .Q(\code_comma_pipe_reg[1] [0]),
        .R(1'b0));
  FDRE \mgt_codecomma_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg[1]_i_1_n_0 ),
        .Q(\code_comma_pipe_reg[1] [1]),
        .R(1'b0));
  FDRE \mgt_codecomma_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg[2]_i_1_n_0 ),
        .Q(\code_comma_pipe_reg[1] [2]),
        .R(1'b0));
  FDRE \mgt_codecomma_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg[3]_i_1_n_0 ),
        .Q(\code_comma_pipe_reg[1] [3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codevalid_reg[0]_i_1 
       (.I0(codevalid_prev),
        .I1(align_toggle_1),
        .I2(mgt_codevalid[0]),
        .O(\mgt_codevalid_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codevalid_reg[1]_i_1 
       (.I0(mgt_codevalid[0]),
        .I1(align_toggle_1),
        .I2(mgt_codevalid[1]),
        .O(\mgt_codevalid_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codevalid_reg[2]_i_1 
       (.I0(mgt_codevalid[1]),
        .I1(align_toggle_1),
        .I2(mgt_codevalid[2]),
        .O(\mgt_codevalid_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_codevalid_reg[3]_i_1 
       (.I0(mgt_codevalid[2]),
        .I1(align_toggle_1),
        .I2(mgt_codevalid[3]),
        .O(\mgt_codevalid_reg[3]_i_1_n_0 ));
  FDRE \mgt_codevalid_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg[0]_i_1_n_0 ),
        .Q(\code_valid_pipe_reg[1] [0]),
        .R(1'b0));
  FDRE \mgt_codevalid_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg[1]_i_1_n_0 ),
        .Q(\code_valid_pipe_reg[1] [1]),
        .R(1'b0));
  FDRE \mgt_codevalid_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg[2]_i_1_n_0 ),
        .Q(\code_valid_pipe_reg[1] [2]),
        .R(1'b0));
  FDRE \mgt_codevalid_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg[3]_i_1_n_0 ),
        .Q(\code_valid_pipe_reg[1] [3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_rxcharisk_reg[0]_i_1 
       (.I0(rxcharisk_prev),
        .I1(align_toggle_1),
        .I2(mgt_rxcharisk[0]),
        .O(\mgt_rxcharisk_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_rxcharisk_reg[1]_i_1 
       (.I0(mgt_rxcharisk[0]),
        .I1(align_toggle_1),
        .I2(mgt_rxcharisk[1]),
        .O(\mgt_rxcharisk_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_rxcharisk_reg[2]_i_1 
       (.I0(mgt_rxcharisk[1]),
        .I1(align_toggle_1),
        .I2(mgt_rxcharisk[2]),
        .O(\mgt_rxcharisk_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mgt_rxcharisk_reg[3]_i_1 
       (.I0(mgt_rxcharisk[2]),
        .I1(align_toggle_1),
        .I2(mgt_rxcharisk[3]),
        .O(\mgt_rxcharisk_reg[3]_i_1_n_0 ));
  FDRE \mgt_rxcharisk_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg[0]_i_1_n_0 ),
        .Q(\rxc_pipe_reg[7] [0]),
        .R(1'b0));
  FDRE \mgt_rxcharisk_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg[1]_i_1_n_0 ),
        .Q(\rxc_pipe_reg[7] [1]),
        .R(1'b0));
  FDRE \mgt_rxcharisk_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg[2]_i_1_n_0 ),
        .Q(\rxc_pipe_reg[7] [2]),
        .R(1'b0));
  FDRE \mgt_rxcharisk_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxcharisk_reg[3]_i_1_n_0 ),
        .Q(\rxc_pipe_reg[7] [3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[0]_i_1 
       (.I0(\rxdata_prev_reg_n_0_[0] ),
        .I1(mgt_rxdata[0]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[10]_i_1 
       (.I0(mgt_rxdata[2]),
        .I1(mgt_rxdata[10]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[11]_i_1 
       (.I0(mgt_rxdata[3]),
        .I1(mgt_rxdata[11]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[12]_i_1 
       (.I0(mgt_rxdata[4]),
        .I1(mgt_rxdata[12]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[13]_i_1 
       (.I0(mgt_rxdata[5]),
        .I1(mgt_rxdata[13]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[14]_i_1 
       (.I0(mgt_rxdata[6]),
        .I1(mgt_rxdata[14]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[15]_i_1 
       (.I0(mgt_rxdata[7]),
        .I1(mgt_rxdata[15]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[16]_i_1 
       (.I0(mgt_rxdata[8]),
        .I1(mgt_rxdata[16]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[17]_i_1 
       (.I0(mgt_rxdata[9]),
        .I1(mgt_rxdata[17]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[18]_i_1 
       (.I0(mgt_rxdata[10]),
        .I1(mgt_rxdata[18]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[19]_i_1 
       (.I0(mgt_rxdata[11]),
        .I1(mgt_rxdata[19]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[1]_i_1 
       (.I0(\rxdata_prev_reg_n_0_[1] ),
        .I1(mgt_rxdata[1]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[20]_i_1 
       (.I0(mgt_rxdata[12]),
        .I1(mgt_rxdata[20]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[21]_i_1 
       (.I0(mgt_rxdata[13]),
        .I1(mgt_rxdata[21]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[22]_i_1 
       (.I0(mgt_rxdata[14]),
        .I1(mgt_rxdata[22]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[23]_i_1 
       (.I0(mgt_rxdata[15]),
        .I1(mgt_rxdata[23]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[24]_i_1 
       (.I0(mgt_rxdata[16]),
        .I1(mgt_rxdata[24]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[25]_i_1 
       (.I0(mgt_rxdata[17]),
        .I1(mgt_rxdata[25]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[26]_i_1 
       (.I0(mgt_rxdata[18]),
        .I1(mgt_rxdata[26]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[27]_i_1 
       (.I0(mgt_rxdata[19]),
        .I1(mgt_rxdata[27]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[28]_i_1 
       (.I0(mgt_rxdata[20]),
        .I1(mgt_rxdata[28]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[29]_i_1 
       (.I0(mgt_rxdata[21]),
        .I1(mgt_rxdata[29]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[2]_i_1 
       (.I0(\rxdata_prev_reg_n_0_[2] ),
        .I1(mgt_rxdata[2]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[30]_i_1 
       (.I0(mgt_rxdata[22]),
        .I1(mgt_rxdata[30]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[31]_i_1 
       (.I0(mgt_rxdata[23]),
        .I1(mgt_rxdata[31]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[3]_i_1 
       (.I0(\rxdata_prev_reg_n_0_[3] ),
        .I1(mgt_rxdata[3]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[4]_i_1 
       (.I0(\rxdata_prev_reg_n_0_[4] ),
        .I1(mgt_rxdata[4]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[5]_i_1 
       (.I0(\rxdata_prev_reg_n_0_[5] ),
        .I1(mgt_rxdata[5]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[6]_i_1 
       (.I0(\rxdata_prev_reg_n_0_[6] ),
        .I1(mgt_rxdata[6]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[7]_i_1 
       (.I0(\rxdata_prev_reg_n_0_[7] ),
        .I1(mgt_rxdata[7]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[8]_i_1 
       (.I0(mgt_rxdata[0]),
        .I1(mgt_rxdata[8]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \mgt_rxdata_reg[9]_i_1 
       (.I0(mgt_rxdata[1]),
        .I1(mgt_rxdata[9]),
        .I2(align_toggle_1),
        .O(\mgt_rxdata_reg[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[0]_i_1_n_0 ),
        .Q(Q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[10] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[10]_i_1_n_0 ),
        .Q(Q[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[11] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[11]_i_1_n_0 ),
        .Q(Q[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[12] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[12]_i_1_n_0 ),
        .Q(Q[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[13] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[13]_i_1_n_0 ),
        .Q(Q[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[14] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[14]_i_1_n_0 ),
        .Q(Q[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[15] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[15]_i_1_n_0 ),
        .Q(Q[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[16] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[16]_i_1_n_0 ),
        .Q(Q[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[17] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[17]_i_1_n_0 ),
        .Q(Q[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[18] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[18]_i_1_n_0 ),
        .Q(Q[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[19] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[19]_i_1_n_0 ),
        .Q(Q[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[1]_i_1_n_0 ),
        .Q(Q[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[20] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[20]_i_1_n_0 ),
        .Q(Q[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[21] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[21]_i_1_n_0 ),
        .Q(Q[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[22] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[22]_i_1_n_0 ),
        .Q(Q[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[23] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[23]_i_1_n_0 ),
        .Q(Q[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[24] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[24]_i_1_n_0 ),
        .Q(Q[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[25] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[25]_i_1_n_0 ),
        .Q(Q[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[26] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[26]_i_1_n_0 ),
        .Q(Q[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[27] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[27]_i_1_n_0 ),
        .Q(Q[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[28] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[28]_i_1_n_0 ),
        .Q(Q[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[29] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[29]_i_1_n_0 ),
        .Q(Q[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[2]_i_1_n_0 ),
        .Q(Q[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[30] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[30]_i_1_n_0 ),
        .Q(Q[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[31] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[31]_i_1_n_0 ),
        .Q(Q[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[3]_i_1_n_0 ),
        .Q(Q[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[4]_i_1_n_0 ),
        .Q(Q[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[5]_i_1_n_0 ),
        .Q(Q[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[6]_i_1_n_0 ),
        .Q(Q[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[7]_i_1_n_0 ),
        .Q(Q[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[8] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[8]_i_1_n_0 ),
        .Q(Q[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mgt_rxdata_reg_reg[9] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_rxdata_reg[9]_i_1_n_0 ),
        .Q(Q[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxcharisk_prev_reg
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_rxcharisk[3]),
        .Q(rxcharisk_prev),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[0] 
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_rxdata[24]),
        .Q(\rxdata_prev_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[1] 
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_rxdata[25]),
        .Q(\rxdata_prev_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[2] 
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_rxdata[26]),
        .Q(\rxdata_prev_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[3] 
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_rxdata[27]),
        .Q(\rxdata_prev_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[4] 
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_rxdata[28]),
        .Q(\rxdata_prev_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[5] 
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_rxdata[29]),
        .Q(\rxdata_prev_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[6] 
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_rxdata[30]),
        .Q(\rxdata_prev_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rxdata_prev_reg[7] 
       (.C(usrclk),
        .CE(align_toggle_1),
        .D(mgt_rxdata[31]),
        .Q(\rxdata_prev_reg_n_0_[7] ),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h006A)) 
    shift_en_i_1__0
       (.I0(align_toggle_1),
        .I1(err_cnt[0]),
        .I2(err_cnt[1]),
        .I3(mgt_rx_reset),
        .O(shift_en_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    shift_en_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(shift_en_i_1__0_n_0),
        .Q(align_toggle_1),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h20800020)) 
    \tx_is_idle_pipe[2]_i_1 
       (.I0(\tx_is_idle_pipe[2]_i_2_n_0 ),
        .I1(Q[6]),
        .I2(\rxc_pipe_reg[7] [0]),
        .I3(Q[7]),
        .I4(Q[5]),
        .O(\tx_is_idle_pipe_reg[3] [0]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'h04000000)) 
    \tx_is_idle_pipe[2]_i_2 
       (.I0(Q[0]),
        .I1(Q[4]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[3]),
        .O(\tx_is_idle_pipe[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h20800020)) 
    \tx_is_idle_pipe[3]_i_1 
       (.I0(\tx_is_idle_pipe[3]_i_2_n_0 ),
        .I1(Q[14]),
        .I2(\rxc_pipe_reg[7] [1]),
        .I3(Q[15]),
        .I4(Q[13]),
        .O(\tx_is_idle_pipe_reg[3] [1]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'h04000000)) 
    \tx_is_idle_pipe[3]_i_2 
       (.I0(Q[8]),
        .I1(Q[12]),
        .I2(Q[9]),
        .I3(Q[10]),
        .I4(Q[11]),
        .O(\tx_is_idle_pipe[3]_i_2_n_0 ));
endmodule

(* ORIG_REF_NAME = "rxaui_v4_3_7_top" *) (* c_family = "kintex7" *) (* c_has_mdio = "TRUE" *) 
(* c_rxaui_mode = "0" *) (* c_rxdata_width = "64" *) (* c_txdata_width = "64" *) 
module rxaui_0_rxaui_v4_3_7_top
   (reset,
    usrclk,
    rxclk,
    xgmii_txd,
    xgmii_txc,
    xgmii_rxd,
    xgmii_rxc,
    mgt_txdata,
    mgt_txcharisk,
    mgt_rxdata,
    mgt_rxcharisk,
    mgt_codevalid,
    mgt_codecomma,
    mgt_enable_align,
    mgt_enchansync,
    mgt_rxlock,
    mgt_loopback,
    mgt_powerdown,
    mgt_tx_reset,
    mgt_rx_reset,
    soft_reset,
    signal_detect,
    align_status,
    sync_status,
    mdc,
    mdio_in,
    mdio_out,
    mdio_tri,
    type_sel,
    prtad,
    configuration_vector,
    status_vector);
  input reset;
  input usrclk;
  input rxclk;
  input [63:0]xgmii_txd;
  input [7:0]xgmii_txc;
  output [63:0]xgmii_rxd;
  output [7:0]xgmii_rxc;
  output [63:0]mgt_txdata;
  output [7:0]mgt_txcharisk;
  input [63:0]mgt_rxdata;
  input [7:0]mgt_rxcharisk;
  input [7:0]mgt_codevalid;
  input [7:0]mgt_codecomma;
  output [1:0]mgt_enable_align;
  output mgt_enchansync;
  input [1:0]mgt_rxlock;
  output mgt_loopback;
  output mgt_powerdown;
  input [1:0]mgt_tx_reset;
  input [1:0]mgt_rx_reset;
  output soft_reset;
  input [1:0]signal_detect;
  output align_status;
  output [3:0]sync_status;
  input mdc;
  input mdio_in;
  output mdio_out;
  output mdio_tri;
  input [1:0]type_sel;
  input [4:0]prtad;
  input [6:0]configuration_vector;
  output [7:0]status_vector;

  wire \<const0> ;
  wire align_status;
  wire mdc;
  wire mdio_in;
  wire mdio_out;
  wire mdio_tri;
  wire [7:0]mgt_codecomma;
  wire [7:0]mgt_codevalid;
  wire [1:0]mgt_enable_align;
  wire mgt_enchansync;
  wire mgt_loopback;
  wire mgt_powerdown;
  wire [1:0]mgt_rx_reset;
  wire [7:0]mgt_rxcharisk;
  wire [63:0]mgt_rxdata;
  wire [1:0]mgt_rxlock;
  wire [1:0]mgt_tx_reset;
  wire [7:0]mgt_txcharisk;
  wire [63:0]mgt_txdata;
  wire [4:0]prtad;
  wire reset;
  wire [1:0]signal_detect;
  wire soft_reset;
  wire [3:0]sync_status;
  wire [1:0]type_sel;
  wire usrclk;
  wire [7:0]xgmii_rxc;
  wire [63:0]xgmii_rxd;
  wire [7:0]xgmii_txc;
  wire [63:0]xgmii_txd;

  assign status_vector[7] = \<const0> ;
  assign status_vector[6] = \<const0> ;
  assign status_vector[5] = \<const0> ;
  assign status_vector[4] = \<const0> ;
  assign status_vector[3] = \<const0> ;
  assign status_vector[2] = \<const0> ;
  assign status_vector[1] = \<const0> ;
  assign status_vector[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  rxaui_0_rxaui_gen rxaui_inst
       (.align_status(align_status),
        .mdc(mdc),
        .mdio_in(mdio_in),
        .mdio_out(mdio_out),
        .mdio_tri(mdio_tri),
        .mgt_codecomma(mgt_codecomma),
        .mgt_codevalid(mgt_codevalid),
        .mgt_enable_align(mgt_enable_align),
        .mgt_enchansync(mgt_enchansync),
        .mgt_loopback_r_reg(mgt_loopback),
        .mgt_powerdown_r_reg(mgt_powerdown),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_rxcharisk(mgt_rxcharisk),
        .mgt_rxdata(mgt_rxdata),
        .mgt_rxlock(mgt_rxlock[0]),
        .mgt_tx_reset(mgt_tx_reset[1]),
        .mgt_txcharisk(mgt_txcharisk),
        .mgt_txdata(mgt_txdata),
        .prtad(prtad),
        .reset(reset),
        .reset_reg_reg(soft_reset),
        .signal_detect(signal_detect),
        .sync_status(sync_status),
        .type_sel(type_sel),
        .usrclk(usrclk),
        .xgmii_rxc(xgmii_rxc),
        .xgmii_rxd(xgmii_rxd),
        .xgmii_txc(xgmii_txc),
        .xgmii_txd(xgmii_txd));
endmodule

(* ORIG_REF_NAME = "sync_state_machine" *) 
module rxaui_0_sync_state_machine
   (mgt_enable_align_i,
    SR,
    out,
    \sync_ok_reg[0] ,
    Q,
    usrclk,
    \core_mgt_rx_reset_reg[0] ,
    usrclk_reset,
    mgt_rxlock,
    \mgt_codecomma_reg_reg[2] ,
    \mgt_codevalid_reg_reg[2] );
  output [0:0]mgt_enable_align_i;
  output [0:0]SR;
  output [2:0]out;
  output \sync_ok_reg[0] ;
  input [0:0]Q;
  input usrclk;
  input \core_mgt_rx_reset_reg[0] ;
  input usrclk_reset;
  input [0:0]mgt_rxlock;
  input [1:0]\mgt_codecomma_reg_reg[2] ;
  input [1:0]\mgt_codevalid_reg_reg[2] ;

  wire \FSM_sequential_state[1][0]_i_10_n_0 ;
  wire \FSM_sequential_state[1][0]_i_11_n_0 ;
  wire \FSM_sequential_state[1][0]_i_12_n_0 ;
  wire \FSM_sequential_state[1][0]_i_1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_3_n_0 ;
  wire \FSM_sequential_state[1][0]_i_4_n_0 ;
  wire \FSM_sequential_state[1][0]_i_5_n_0 ;
  wire \FSM_sequential_state[1][0]_i_6_n_0 ;
  wire \FSM_sequential_state[1][0]_i_7_n_0 ;
  wire \FSM_sequential_state[1][0]_i_8_n_0 ;
  wire \FSM_sequential_state[1][1]_i_1_n_0 ;
  wire \FSM_sequential_state[1][1]_i_2_n_0 ;
  wire \FSM_sequential_state[1][1]_i_3_n_0 ;
  wire \FSM_sequential_state[1][1]_i_4_n_0 ;
  wire \FSM_sequential_state[1][1]_i_5_n_0 ;
  wire \FSM_sequential_state[1][2]_i_1_n_0 ;
  wire \FSM_sequential_state[1][2]_i_2_n_0 ;
  wire \FSM_sequential_state[1][2]_i_3_n_0 ;
  wire \FSM_sequential_state[1][2]_i_4_n_0 ;
  wire \FSM_sequential_state[1][2]_i_5_n_0 ;
  wire \FSM_sequential_state[1][3]_i_1_n_0 ;
  wire \FSM_sequential_state[1][3]_i_2_n_0 ;
  wire \FSM_sequential_state[1][4]_i_2_n_0 ;
  wire \FSM_sequential_state[1][4]_i_4_n_0 ;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \code_comma_pipe_reg_n_0_[0] ;
  wire \code_comma_pipe_reg_n_0_[1] ;
  wire \code_valid_pipe_reg_n_0_[0] ;
  wire \code_valid_pipe_reg_n_0_[1] ;
  wire \core_mgt_rx_reset_reg[0] ;
  wire enable_align_i;
  wire get_next_state;
  wire [1:0]\mgt_codecomma_reg_reg[2] ;
  wire [1:0]\mgt_codevalid_reg_reg[2] ;
  wire [0:0]mgt_enable_align_i;
  wire [0:0]mgt_rxlock;
  wire \next_state[1]1 ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire signal_detect_last;
  (* RTL_KEEP = "yes" *) wire [1:0]state;
  wire \sync_ok_reg[0] ;
  wire usrclk;
  wire usrclk_reset;

  LUT6 #(
    .INIT(64'h88F0FFFF88F00000)) 
    \FSM_sequential_state[1][0]_i_1 
       (.I0(\FSM_sequential_state[1][2]_i_2_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[1] ),
        .I2(\FSM_sequential_state[1][0]_i_2_n_0 ),
        .I3(\code_valid_pipe_reg_n_0_[0] ),
        .I4(out[2]),
        .I5(\FSM_sequential_state[1][0]_i_3_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF28)) 
    \FSM_sequential_state[1][0]_i_10 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(out[1]),
        .O(\FSM_sequential_state[1][0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA8888EEEEFEEE)) 
    \FSM_sequential_state[1][0]_i_11 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(\code_comma_pipe_reg_n_0_[1] ),
        .I3(Q),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h00000000CCCCDCCC)) 
    \FSM_sequential_state[1][0]_i_12 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(\code_comma_pipe_reg_n_0_[1] ),
        .I3(Q),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][0]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][0]_i_13 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[0] ),
        .O(\next_state[1]1 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \FSM_sequential_state[1][0]_i_2 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(Q),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(state[0]),
        .I5(out[0]),
        .O(\FSM_sequential_state[1][0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \FSM_sequential_state[1][0]_i_3 
       (.I0(\FSM_sequential_state[1][0]_i_4_n_0 ),
        .I1(\FSM_sequential_state[1][0]_i_5_n_0 ),
        .I2(\code_valid_pipe_reg_n_0_[1] ),
        .I3(\FSM_sequential_state[1][0]_i_6_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[0] ),
        .I5(\FSM_sequential_state[1][0]_i_7_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \FSM_sequential_state[1][0]_i_4 
       (.I0(state[0]),
        .I1(out[1]),
        .I2(state[1]),
        .I3(out[0]),
        .I4(\FSM_sequential_state[1][0]_i_8_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFF6AFFAFFF6AFAAA)) 
    \FSM_sequential_state[1][0]_i_5 
       (.I0(out[0]),
        .I1(out[1]),
        .I2(state[1]),
        .I3(get_next_state),
        .I4(state[0]),
        .I5(\FSM_sequential_state[1][0]_i_10_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h3BFBFBFB38F8F8F8)) 
    \FSM_sequential_state[1][0]_i_6 
       (.I0(\FSM_sequential_state[1][0]_i_11_n_0 ),
        .I1(state[0]),
        .I2(out[0]),
        .I3(out[1]),
        .I4(state[1]),
        .I5(\FSM_sequential_state[1][0]_i_12_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hBCBC7E7F28282A2A)) 
    \FSM_sequential_state[1][0]_i_7 
       (.I0(out[0]),
        .I1(state[1]),
        .I2(state[0]),
        .I3(\next_state[1]1 ),
        .I4(out[1]),
        .I5(get_next_state),
        .O(\FSM_sequential_state[1][0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h33333333443232CC)) 
    \FSM_sequential_state[1][0]_i_8 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(Q),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(out[1]),
        .O(\FSM_sequential_state[1][0]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][0]_i_9 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .O(get_next_state));
  LUT6 #(
    .INIT(64'h8F800F0F8F800000)) 
    \FSM_sequential_state[1][1]_i_1 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(\FSM_sequential_state[1][2]_i_2_n_0 ),
        .I2(out[2]),
        .I3(\FSM_sequential_state[1][1]_i_2_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[1] ),
        .I5(\FSM_sequential_state[1][1]_i_3_n_0 ),
        .O(\FSM_sequential_state[1][1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h08380838F80BF808)) 
    \FSM_sequential_state[1][1]_i_2 
       (.I0(\FSM_sequential_state[1][1]_i_4_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(out[0]),
        .I3(state[0]),
        .I4(\FSM_sequential_state[1][1]_i_5_n_0 ),
        .I5(state[1]),
        .O(\FSM_sequential_state[1][1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h27F98808)) 
    \FSM_sequential_state[1][1]_i_3 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(out[1]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(out[0]),
        .O(\FSM_sequential_state[1][1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h6566666A6466666A)) 
    \FSM_sequential_state[1][1]_i_4 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(out[1]),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(Q),
        .O(\FSM_sequential_state[1][1]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_state[1][1]_i_5 
       (.I0(out[1]),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(Q),
        .O(\FSM_sequential_state[1][1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8F800F0F8F800000)) 
    \FSM_sequential_state[1][2]_i_1 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(\FSM_sequential_state[1][2]_i_2_n_0 ),
        .I2(out[2]),
        .I3(\FSM_sequential_state[1][2]_i_3_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[1] ),
        .I5(\FSM_sequential_state[1][2]_i_4_n_0 ),
        .O(\FSM_sequential_state[1][2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_state[1][2]_i_2 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(state[0]),
        .I3(out[0]),
        .O(\FSM_sequential_state[1][2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8888888B88888888)) 
    \FSM_sequential_state[1][2]_i_3 
       (.I0(\FSM_sequential_state[1][2]_i_5_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(state[1]),
        .I3(out[1]),
        .I4(state[0]),
        .I5(out[0]),
        .O(\FSM_sequential_state[1][2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0CFFFFCFC8C000C0)) 
    \FSM_sequential_state[1][2]_i_4 
       (.I0(\code_comma_pipe_reg_n_0_[0] ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(out[1]),
        .I3(state[0]),
        .I4(state[1]),
        .I5(out[0]),
        .O(\FSM_sequential_state[1][2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h55AA54AAAAFFEAAA)) 
    \FSM_sequential_state[1][2]_i_5 
       (.I0(out[0]),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(state[1]),
        .I4(out[1]),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][2]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_state[1][3]_i_1 
       (.I0(\FSM_sequential_state[1][3]_i_2_n_0 ),
        .I1(out[2]),
        .O(\FSM_sequential_state[1][3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h32C88F800FC08A48)) 
    \FSM_sequential_state[1][3]_i_2 
       (.I0(\code_valid_pipe_reg_n_0_[1] ),
        .I1(out[1]),
        .I2(\code_valid_pipe_reg_n_0_[0] ),
        .I3(out[0]),
        .I4(state[1]),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFBEFFFF)) 
    \FSM_sequential_state[1][4]_i_1__1 
       (.I0(\core_mgt_rx_reset_reg[0] ),
        .I1(Q),
        .I2(signal_detect_last),
        .I3(usrclk_reset),
        .I4(mgt_rxlock),
        .O(SR));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \FSM_sequential_state[1][4]_i_2 
       (.I0(\code_valid_pipe_reg_n_0_[1] ),
        .I1(out[0]),
        .I2(\FSM_sequential_state[1][4]_i_4_n_0 ),
        .I3(state[1]),
        .I4(\code_valid_pipe_reg_n_0_[0] ),
        .I5(out[2]),
        .O(\FSM_sequential_state[1][4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][4]_i_4 
       (.I0(out[1]),
        .I1(state[0]),
        .O(\FSM_sequential_state[1][4]_i_4_n_0 ));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][0]_i_1_n_0 ),
        .Q(state[0]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][1]_i_1_n_0 ),
        .Q(state[1]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][2]_i_1_n_0 ),
        .Q(out[0]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][3]_i_1_n_0 ),
        .Q(out[1]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][4]_i_2_n_0 ),
        .Q(out[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \code_comma_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg_reg[2] [0]),
        .Q(\code_comma_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_comma_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg_reg[2] [1]),
        .Q(\code_comma_pipe_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_valid_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg_reg[2] [0]),
        .Q(\code_valid_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_valid_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg_reg[2] [1]),
        .Q(\code_valid_pipe_reg_n_0_[1] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00000001)) 
    enable_align_i_1
       (.I0(out[0]),
        .I1(state[0]),
        .I2(out[1]),
        .I3(state[1]),
        .I4(out[2]),
        .O(enable_align_i));
  FDRE enable_align_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(enable_align_i),
        .Q(mgt_enable_align_i),
        .R(1'b0));
  FDRE signal_detect_last_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(signal_detect_last),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    \sync_ok[0]_i_1 
       (.I0(out[1]),
        .I1(out[0]),
        .O(\sync_ok_reg[0] ));
endmodule

(* ORIG_REF_NAME = "sync_state_machine" *) 
module rxaui_0_sync_state_machine_19
   (mgt_enable_align_i,
    out,
    \sync_ok_reg[1] ,
    usrclk,
    Q,
    \mgt_codecomma_reg_reg[3] ,
    \mgt_codevalid_reg_reg[3] ,
    SR);
  output [0:0]mgt_enable_align_i;
  output [2:0]out;
  output \sync_ok_reg[1] ;
  input usrclk;
  input [0:0]Q;
  input [1:0]\mgt_codecomma_reg_reg[3] ;
  input [1:0]\mgt_codevalid_reg_reg[3] ;
  input [0:0]SR;

  wire \FSM_sequential_state[1][0]_i_10__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_11__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_12__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_1__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_2__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_3__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_4__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_5__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_6__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_7__0_n_0 ;
  wire \FSM_sequential_state[1][0]_i_8__0_n_0 ;
  wire \FSM_sequential_state[1][1]_i_1__0_n_0 ;
  wire \FSM_sequential_state[1][1]_i_2__0_n_0 ;
  wire \FSM_sequential_state[1][1]_i_3__0_n_0 ;
  wire \FSM_sequential_state[1][1]_i_4__0_n_0 ;
  wire \FSM_sequential_state[1][1]_i_5__0_n_0 ;
  wire \FSM_sequential_state[1][2]_i_1__0_n_0 ;
  wire \FSM_sequential_state[1][2]_i_2__0_n_0 ;
  wire \FSM_sequential_state[1][2]_i_3__0_n_0 ;
  wire \FSM_sequential_state[1][2]_i_4__0_n_0 ;
  wire \FSM_sequential_state[1][2]_i_5__0_n_0 ;
  wire \FSM_sequential_state[1][3]_i_1__0_n_0 ;
  wire \FSM_sequential_state[1][3]_i_2__0_n_0 ;
  wire \FSM_sequential_state[1][4]_i_1_n_0 ;
  wire \FSM_sequential_state[1][4]_i_2__0_n_0 ;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \code_comma_pipe_reg_n_0_[0] ;
  wire \code_comma_pipe_reg_n_0_[1] ;
  wire \code_valid_pipe_reg_n_0_[0] ;
  wire \code_valid_pipe_reg_n_0_[1] ;
  wire enable_align_i;
  wire get_next_state;
  wire [1:0]\mgt_codecomma_reg_reg[3] ;
  wire [1:0]\mgt_codevalid_reg_reg[3] ;
  wire [0:0]mgt_enable_align_i;
  wire \next_state[1]1 ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  (* RTL_KEEP = "yes" *) wire [1:0]state;
  wire \sync_ok_reg[1] ;
  wire usrclk;

  LUT4 #(
    .INIT(16'hFF28)) 
    \FSM_sequential_state[1][0]_i_10__0 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(out[1]),
        .O(\FSM_sequential_state[1][0]_i_10__0_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA8888EEEEFEEE)) 
    \FSM_sequential_state[1][0]_i_11__0 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(\code_comma_pipe_reg_n_0_[1] ),
        .I3(Q),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][0]_i_11__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000CCCCDCCC)) 
    \FSM_sequential_state[1][0]_i_12__0 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(\code_comma_pipe_reg_n_0_[1] ),
        .I3(Q),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][0]_i_12__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][0]_i_13__0 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[0] ),
        .O(\next_state[1]1 ));
  LUT6 #(
    .INIT(64'h88F0FFFF88F00000)) 
    \FSM_sequential_state[1][0]_i_1__0 
       (.I0(\FSM_sequential_state[1][2]_i_2__0_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[1] ),
        .I2(\FSM_sequential_state[1][0]_i_2__0_n_0 ),
        .I3(\code_valid_pipe_reg_n_0_[0] ),
        .I4(out[2]),
        .I5(\FSM_sequential_state[1][0]_i_3__0_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \FSM_sequential_state[1][0]_i_2__0 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(Q),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(state[0]),
        .I5(out[0]),
        .O(\FSM_sequential_state[1][0]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \FSM_sequential_state[1][0]_i_3__0 
       (.I0(\FSM_sequential_state[1][0]_i_4__0_n_0 ),
        .I1(\FSM_sequential_state[1][0]_i_5__0_n_0 ),
        .I2(\code_valid_pipe_reg_n_0_[1] ),
        .I3(\FSM_sequential_state[1][0]_i_6__0_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[0] ),
        .I5(\FSM_sequential_state[1][0]_i_7__0_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_3__0_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \FSM_sequential_state[1][0]_i_4__0 
       (.I0(state[0]),
        .I1(out[1]),
        .I2(state[1]),
        .I3(out[0]),
        .I4(\FSM_sequential_state[1][0]_i_8__0_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'hFF6AFFAFFF6AFAAA)) 
    \FSM_sequential_state[1][0]_i_5__0 
       (.I0(out[0]),
        .I1(out[1]),
        .I2(state[1]),
        .I3(get_next_state),
        .I4(state[0]),
        .I5(\FSM_sequential_state[1][0]_i_10__0_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h3BFBFBFB38F8F8F8)) 
    \FSM_sequential_state[1][0]_i_6__0 
       (.I0(\FSM_sequential_state[1][0]_i_11__0_n_0 ),
        .I1(state[0]),
        .I2(out[0]),
        .I3(out[1]),
        .I4(state[1]),
        .I5(\FSM_sequential_state[1][0]_i_12__0_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_6__0_n_0 ));
  LUT6 #(
    .INIT(64'hBCBC7E7F28282A2A)) 
    \FSM_sequential_state[1][0]_i_7__0 
       (.I0(out[0]),
        .I1(state[1]),
        .I2(state[0]),
        .I3(\next_state[1]1 ),
        .I4(out[1]),
        .I5(get_next_state),
        .O(\FSM_sequential_state[1][0]_i_7__0_n_0 ));
  LUT6 #(
    .INIT(64'h33333333443232CC)) 
    \FSM_sequential_state[1][0]_i_8__0 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(Q),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(out[1]),
        .O(\FSM_sequential_state[1][0]_i_8__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][0]_i_9__0 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .O(get_next_state));
  LUT6 #(
    .INIT(64'h8F800F0F8F800000)) 
    \FSM_sequential_state[1][1]_i_1__0 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(\FSM_sequential_state[1][2]_i_2__0_n_0 ),
        .I2(out[2]),
        .I3(\FSM_sequential_state[1][1]_i_2__0_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[1] ),
        .I5(\FSM_sequential_state[1][1]_i_3__0_n_0 ),
        .O(\FSM_sequential_state[1][1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h08380838F80BF808)) 
    \FSM_sequential_state[1][1]_i_2__0 
       (.I0(\FSM_sequential_state[1][1]_i_4__0_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(out[0]),
        .I3(state[0]),
        .I4(\FSM_sequential_state[1][1]_i_5__0_n_0 ),
        .I5(state[1]),
        .O(\FSM_sequential_state[1][1]_i_2__0_n_0 ));
  LUT5 #(
    .INIT(32'h27F98808)) 
    \FSM_sequential_state[1][1]_i_3__0 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(out[1]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(out[0]),
        .O(\FSM_sequential_state[1][1]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h6566666A6466666A)) 
    \FSM_sequential_state[1][1]_i_4__0 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(out[1]),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(Q),
        .O(\FSM_sequential_state[1][1]_i_4__0_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_state[1][1]_i_5__0 
       (.I0(out[1]),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(Q),
        .O(\FSM_sequential_state[1][1]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h8F800F0F8F800000)) 
    \FSM_sequential_state[1][2]_i_1__0 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(\FSM_sequential_state[1][2]_i_2__0_n_0 ),
        .I2(out[2]),
        .I3(\FSM_sequential_state[1][2]_i_3__0_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[1] ),
        .I5(\FSM_sequential_state[1][2]_i_4__0_n_0 ),
        .O(\FSM_sequential_state[1][2]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_state[1][2]_i_2__0 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(state[0]),
        .I3(out[0]),
        .O(\FSM_sequential_state[1][2]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h8888888B88888888)) 
    \FSM_sequential_state[1][2]_i_3__0 
       (.I0(\FSM_sequential_state[1][2]_i_5__0_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(state[1]),
        .I3(out[1]),
        .I4(state[0]),
        .I5(out[0]),
        .O(\FSM_sequential_state[1][2]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h0CFFFFCFC8C000C0)) 
    \FSM_sequential_state[1][2]_i_4__0 
       (.I0(\code_comma_pipe_reg_n_0_[0] ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(out[1]),
        .I3(state[0]),
        .I4(state[1]),
        .I5(out[0]),
        .O(\FSM_sequential_state[1][2]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'h55AA54AAAAFFEAAA)) 
    \FSM_sequential_state[1][2]_i_5__0 
       (.I0(out[0]),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(state[1]),
        .I4(out[1]),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][2]_i_5__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_state[1][3]_i_1__0 
       (.I0(\FSM_sequential_state[1][3]_i_2__0_n_0 ),
        .I1(out[2]),
        .O(\FSM_sequential_state[1][3]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h32C88F800FC08A48)) 
    \FSM_sequential_state[1][3]_i_2__0 
       (.I0(\code_valid_pipe_reg_n_0_[1] ),
        .I1(out[1]),
        .I2(\code_valid_pipe_reg_n_0_[0] ),
        .I3(out[0]),
        .I4(state[1]),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][3]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \FSM_sequential_state[1][4]_i_1 
       (.I0(\code_valid_pipe_reg_n_0_[1] ),
        .I1(out[0]),
        .I2(\FSM_sequential_state[1][4]_i_2__0_n_0 ),
        .I3(state[1]),
        .I4(\code_valid_pipe_reg_n_0_[0] ),
        .I5(out[2]),
        .O(\FSM_sequential_state[1][4]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][4]_i_2__0 
       (.I0(out[1]),
        .I1(state[0]),
        .O(\FSM_sequential_state[1][4]_i_2__0_n_0 ));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][0]_i_1__0_n_0 ),
        .Q(state[0]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][1]_i_1__0_n_0 ),
        .Q(state[1]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][2]_i_1__0_n_0 ),
        .Q(out[0]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][3]_i_1__0_n_0 ),
        .Q(out[1]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][4]_i_1_n_0 ),
        .Q(out[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \code_comma_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg_reg[3] [0]),
        .Q(\code_comma_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_comma_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg_reg[3] [1]),
        .Q(\code_comma_pipe_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_valid_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg_reg[3] [0]),
        .Q(\code_valid_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_valid_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg_reg[3] [1]),
        .Q(\code_valid_pipe_reg_n_0_[1] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00000001)) 
    enable_align_i_1__0
       (.I0(out[0]),
        .I1(state[0]),
        .I2(out[1]),
        .I3(state[1]),
        .I4(out[2]),
        .O(enable_align_i));
  FDRE enable_align_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(enable_align_i),
        .Q(mgt_enable_align_i),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    \sync_ok[1]_i_1 
       (.I0(out[1]),
        .I1(out[0]),
        .O(\sync_ok_reg[1] ));
endmodule

(* ORIG_REF_NAME = "sync_state_machine" *) 
module rxaui_0_sync_state_machine_20
   (mgt_enable_align_i,
    out,
    \sync_ok_reg[2] ,
    usrclk,
    Q,
    \mgt_codecomma_reg_reg[2] ,
    \mgt_codevalid_reg_reg[2] ,
    SR);
  output [0:0]mgt_enable_align_i;
  output [2:0]out;
  output \sync_ok_reg[2] ;
  input usrclk;
  input [0:0]Q;
  input [1:0]\mgt_codecomma_reg_reg[2] ;
  input [1:0]\mgt_codevalid_reg_reg[2] ;
  input [0:0]SR;

  wire \FSM_sequential_state[1][0]_i_10__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_11__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_12__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_1__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_2__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_3__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_4__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_5__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_6__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_7__1_n_0 ;
  wire \FSM_sequential_state[1][0]_i_8__1_n_0 ;
  wire \FSM_sequential_state[1][1]_i_1__1_n_0 ;
  wire \FSM_sequential_state[1][1]_i_2__1_n_0 ;
  wire \FSM_sequential_state[1][1]_i_3__1_n_0 ;
  wire \FSM_sequential_state[1][1]_i_4__1_n_0 ;
  wire \FSM_sequential_state[1][1]_i_5__1_n_0 ;
  wire \FSM_sequential_state[1][2]_i_1__1_n_0 ;
  wire \FSM_sequential_state[1][2]_i_2__1_n_0 ;
  wire \FSM_sequential_state[1][2]_i_3__1_n_0 ;
  wire \FSM_sequential_state[1][2]_i_4__1_n_0 ;
  wire \FSM_sequential_state[1][2]_i_5__1_n_0 ;
  wire \FSM_sequential_state[1][3]_i_1__1_n_0 ;
  wire \FSM_sequential_state[1][3]_i_2__1_n_0 ;
  wire \FSM_sequential_state[1][4]_i_2__1_n_0 ;
  wire \FSM_sequential_state[1][4]_i_4__0_n_0 ;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \code_comma_pipe_reg_n_0_[0] ;
  wire \code_comma_pipe_reg_n_0_[1] ;
  wire \code_valid_pipe_reg_n_0_[0] ;
  wire \code_valid_pipe_reg_n_0_[1] ;
  wire enable_align_i;
  wire get_next_state;
  wire [1:0]\mgt_codecomma_reg_reg[2] ;
  wire [1:0]\mgt_codevalid_reg_reg[2] ;
  wire [0:0]mgt_enable_align_i;
  wire \next_state[1]1 ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  (* RTL_KEEP = "yes" *) wire [1:0]state;
  wire \sync_ok_reg[2] ;
  wire usrclk;

  LUT4 #(
    .INIT(16'hFF28)) 
    \FSM_sequential_state[1][0]_i_10__1 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(out[1]),
        .O(\FSM_sequential_state[1][0]_i_10__1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA8888EEEEFEEE)) 
    \FSM_sequential_state[1][0]_i_11__1 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(\code_comma_pipe_reg_n_0_[1] ),
        .I3(Q),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][0]_i_11__1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000CCCCDCCC)) 
    \FSM_sequential_state[1][0]_i_12__1 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(\code_comma_pipe_reg_n_0_[1] ),
        .I3(Q),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][0]_i_12__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][0]_i_13__1 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[0] ),
        .O(\next_state[1]1 ));
  LUT6 #(
    .INIT(64'h88F0FFFF88F00000)) 
    \FSM_sequential_state[1][0]_i_1__1 
       (.I0(\FSM_sequential_state[1][2]_i_2__1_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[1] ),
        .I2(\FSM_sequential_state[1][0]_i_2__1_n_0 ),
        .I3(\code_valid_pipe_reg_n_0_[0] ),
        .I4(out[2]),
        .I5(\FSM_sequential_state[1][0]_i_3__1_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \FSM_sequential_state[1][0]_i_2__1 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(Q),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(state[0]),
        .I5(out[0]),
        .O(\FSM_sequential_state[1][0]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \FSM_sequential_state[1][0]_i_3__1 
       (.I0(\FSM_sequential_state[1][0]_i_4__1_n_0 ),
        .I1(\FSM_sequential_state[1][0]_i_5__1_n_0 ),
        .I2(\code_valid_pipe_reg_n_0_[1] ),
        .I3(\FSM_sequential_state[1][0]_i_6__1_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[0] ),
        .I5(\FSM_sequential_state[1][0]_i_7__1_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_3__1_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \FSM_sequential_state[1][0]_i_4__1 
       (.I0(state[0]),
        .I1(out[1]),
        .I2(state[1]),
        .I3(out[0]),
        .I4(\FSM_sequential_state[1][0]_i_8__1_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_4__1_n_0 ));
  LUT6 #(
    .INIT(64'hFF6AFFAFFF6AFAAA)) 
    \FSM_sequential_state[1][0]_i_5__1 
       (.I0(out[0]),
        .I1(out[1]),
        .I2(state[1]),
        .I3(get_next_state),
        .I4(state[0]),
        .I5(\FSM_sequential_state[1][0]_i_10__1_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_5__1_n_0 ));
  LUT6 #(
    .INIT(64'h3BFBFBFB38F8F8F8)) 
    \FSM_sequential_state[1][0]_i_6__1 
       (.I0(\FSM_sequential_state[1][0]_i_11__1_n_0 ),
        .I1(state[0]),
        .I2(out[0]),
        .I3(out[1]),
        .I4(state[1]),
        .I5(\FSM_sequential_state[1][0]_i_12__1_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_6__1_n_0 ));
  LUT6 #(
    .INIT(64'hBCBC7E7F28282A2A)) 
    \FSM_sequential_state[1][0]_i_7__1 
       (.I0(out[0]),
        .I1(state[1]),
        .I2(state[0]),
        .I3(\next_state[1]1 ),
        .I4(out[1]),
        .I5(get_next_state),
        .O(\FSM_sequential_state[1][0]_i_7__1_n_0 ));
  LUT6 #(
    .INIT(64'h33333333443232CC)) 
    \FSM_sequential_state[1][0]_i_8__1 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(Q),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(out[1]),
        .O(\FSM_sequential_state[1][0]_i_8__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][0]_i_9__1 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .O(get_next_state));
  LUT6 #(
    .INIT(64'h8F800F0F8F800000)) 
    \FSM_sequential_state[1][1]_i_1__1 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(\FSM_sequential_state[1][2]_i_2__1_n_0 ),
        .I2(out[2]),
        .I3(\FSM_sequential_state[1][1]_i_2__1_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[1] ),
        .I5(\FSM_sequential_state[1][1]_i_3__1_n_0 ),
        .O(\FSM_sequential_state[1][1]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h08380838F80BF808)) 
    \FSM_sequential_state[1][1]_i_2__1 
       (.I0(\FSM_sequential_state[1][1]_i_4__1_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(out[0]),
        .I3(state[0]),
        .I4(\FSM_sequential_state[1][1]_i_5__1_n_0 ),
        .I5(state[1]),
        .O(\FSM_sequential_state[1][1]_i_2__1_n_0 ));
  LUT5 #(
    .INIT(32'h27F98808)) 
    \FSM_sequential_state[1][1]_i_3__1 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(out[1]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(out[0]),
        .O(\FSM_sequential_state[1][1]_i_3__1_n_0 ));
  LUT6 #(
    .INIT(64'h6566666A6466666A)) 
    \FSM_sequential_state[1][1]_i_4__1 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(out[1]),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(Q),
        .O(\FSM_sequential_state[1][1]_i_4__1_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_state[1][1]_i_5__1 
       (.I0(out[1]),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(Q),
        .O(\FSM_sequential_state[1][1]_i_5__1_n_0 ));
  LUT6 #(
    .INIT(64'h8F800F0F8F800000)) 
    \FSM_sequential_state[1][2]_i_1__1 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(\FSM_sequential_state[1][2]_i_2__1_n_0 ),
        .I2(out[2]),
        .I3(\FSM_sequential_state[1][2]_i_3__1_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[1] ),
        .I5(\FSM_sequential_state[1][2]_i_4__1_n_0 ),
        .O(\FSM_sequential_state[1][2]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_state[1][2]_i_2__1 
       (.I0(state[1]),
        .I1(out[1]),
        .I2(state[0]),
        .I3(out[0]),
        .O(\FSM_sequential_state[1][2]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'h8888888B88888888)) 
    \FSM_sequential_state[1][2]_i_3__1 
       (.I0(\FSM_sequential_state[1][2]_i_5__1_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(state[1]),
        .I3(out[1]),
        .I4(state[0]),
        .I5(out[0]),
        .O(\FSM_sequential_state[1][2]_i_3__1_n_0 ));
  LUT6 #(
    .INIT(64'h0CFFFFCFC8C000C0)) 
    \FSM_sequential_state[1][2]_i_4__1 
       (.I0(\code_comma_pipe_reg_n_0_[0] ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(out[1]),
        .I3(state[0]),
        .I4(state[1]),
        .I5(out[0]),
        .O(\FSM_sequential_state[1][2]_i_4__1_n_0 ));
  LUT6 #(
    .INIT(64'h55AA54AAAAFFEAAA)) 
    \FSM_sequential_state[1][2]_i_5__1 
       (.I0(out[0]),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(state[1]),
        .I4(out[1]),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][2]_i_5__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_state[1][3]_i_1__1 
       (.I0(\FSM_sequential_state[1][3]_i_2__1_n_0 ),
        .I1(out[2]),
        .O(\FSM_sequential_state[1][3]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h32C88F800FC08A48)) 
    \FSM_sequential_state[1][3]_i_2__1 
       (.I0(\code_valid_pipe_reg_n_0_[1] ),
        .I1(out[1]),
        .I2(\code_valid_pipe_reg_n_0_[0] ),
        .I3(out[0]),
        .I4(state[1]),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][3]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \FSM_sequential_state[1][4]_i_2__1 
       (.I0(\code_valid_pipe_reg_n_0_[1] ),
        .I1(out[0]),
        .I2(\FSM_sequential_state[1][4]_i_4__0_n_0 ),
        .I3(state[1]),
        .I4(\code_valid_pipe_reg_n_0_[0] ),
        .I5(out[2]),
        .O(\FSM_sequential_state[1][4]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][4]_i_4__0 
       (.I0(out[1]),
        .I1(state[0]),
        .O(\FSM_sequential_state[1][4]_i_4__0_n_0 ));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][0]_i_1__1_n_0 ),
        .Q(state[0]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][1]_i_1__1_n_0 ),
        .Q(state[1]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][2]_i_1__1_n_0 ),
        .Q(out[0]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][3]_i_1__1_n_0 ),
        .Q(out[1]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][4]_i_2__1_n_0 ),
        .Q(out[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \code_comma_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg_reg[2] [0]),
        .Q(\code_comma_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_comma_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg_reg[2] [1]),
        .Q(\code_comma_pipe_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_valid_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg_reg[2] [0]),
        .Q(\code_valid_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_valid_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg_reg[2] [1]),
        .Q(\code_valid_pipe_reg_n_0_[1] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00000001)) 
    enable_align_i_1__1
       (.I0(out[0]),
        .I1(state[0]),
        .I2(out[1]),
        .I3(state[1]),
        .I4(out[2]),
        .O(enable_align_i));
  FDRE enable_align_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(enable_align_i),
        .Q(mgt_enable_align_i),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    \sync_ok[2]_i_1 
       (.I0(out[1]),
        .I1(out[0]),
        .O(\sync_ok_reg[2] ));
endmodule

(* ORIG_REF_NAME = "sync_state_machine" *) 
module rxaui_0_sync_state_machine_21
   (mgt_enable_align_i,
    SR,
    out,
    \sync_ok_reg[3] ,
    Q,
    usrclk,
    \core_mgt_rx_reset_reg[1] ,
    usrclk_reset,
    mgt_rxlock,
    \mgt_codecomma_reg_reg[3] ,
    \mgt_codevalid_reg_reg[3] );
  output [0:0]mgt_enable_align_i;
  output [0:0]SR;
  output [0:0]out;
  output \sync_ok_reg[3] ;
  input [0:0]Q;
  input usrclk;
  input \core_mgt_rx_reset_reg[1] ;
  input usrclk_reset;
  input [0:0]mgt_rxlock;
  input [1:0]\mgt_codecomma_reg_reg[3] ;
  input [1:0]\mgt_codevalid_reg_reg[3] ;

  wire \FSM_sequential_state[1][0]_i_10__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_11__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_12__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_1__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_2__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_3__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_4__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_5__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_6__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_7__2_n_0 ;
  wire \FSM_sequential_state[1][0]_i_8__2_n_0 ;
  wire \FSM_sequential_state[1][1]_i_1__2_n_0 ;
  wire \FSM_sequential_state[1][1]_i_2__2_n_0 ;
  wire \FSM_sequential_state[1][1]_i_3__2_n_0 ;
  wire \FSM_sequential_state[1][1]_i_4__2_n_0 ;
  wire \FSM_sequential_state[1][1]_i_5__2_n_0 ;
  wire \FSM_sequential_state[1][2]_i_1__2_n_0 ;
  wire \FSM_sequential_state[1][2]_i_2__2_n_0 ;
  wire \FSM_sequential_state[1][2]_i_3__2_n_0 ;
  wire \FSM_sequential_state[1][2]_i_4__2_n_0 ;
  wire \FSM_sequential_state[1][2]_i_5__2_n_0 ;
  wire \FSM_sequential_state[1][3]_i_1__2_n_0 ;
  wire \FSM_sequential_state[1][3]_i_2__2_n_0 ;
  wire \FSM_sequential_state[1][4]_i_1__0_n_0 ;
  wire \FSM_sequential_state[1][4]_i_2__2_n_0 ;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \code_comma_pipe_reg_n_0_[0] ;
  wire \code_comma_pipe_reg_n_0_[1] ;
  wire \code_valid_pipe_reg_n_0_[0] ;
  wire \code_valid_pipe_reg_n_0_[1] ;
  wire \core_mgt_rx_reset_reg[1] ;
  wire enable_align_i;
  wire get_next_state;
  wire [1:0]\mgt_codecomma_reg_reg[3] ;
  wire [1:0]\mgt_codevalid_reg_reg[3] ;
  wire [0:0]mgt_enable_align_i;
  wire [0:0]mgt_rxlock;
  wire \next_state[1]1 ;
  (* RTL_KEEP = "yes" *) wire [0:0]out;
  wire signal_detect_last;
  (* RTL_KEEP = "yes" *) wire [3:0]state;
  wire \sync_ok_reg[3] ;
  wire usrclk;
  wire usrclk_reset;

  LUT4 #(
    .INIT(16'hFF28)) 
    \FSM_sequential_state[1][0]_i_10__2 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(state[3]),
        .O(\FSM_sequential_state[1][0]_i_10__2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA8888EEEEFEEE)) 
    \FSM_sequential_state[1][0]_i_11__2 
       (.I0(state[1]),
        .I1(state[3]),
        .I2(\code_comma_pipe_reg_n_0_[1] ),
        .I3(Q),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][0]_i_11__2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000CCCCDCCC)) 
    \FSM_sequential_state[1][0]_i_12__2 
       (.I0(state[1]),
        .I1(state[3]),
        .I2(\code_comma_pipe_reg_n_0_[1] ),
        .I3(Q),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][0]_i_12__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][0]_i_13__2 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[0] ),
        .O(\next_state[1]1 ));
  LUT6 #(
    .INIT(64'h88F0FFFF88F00000)) 
    \FSM_sequential_state[1][0]_i_1__2 
       (.I0(\FSM_sequential_state[1][2]_i_2__2_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[1] ),
        .I2(\FSM_sequential_state[1][0]_i_2__2_n_0 ),
        .I3(\code_valid_pipe_reg_n_0_[0] ),
        .I4(out),
        .I5(\FSM_sequential_state[1][0]_i_3__2_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \FSM_sequential_state[1][0]_i_2__2 
       (.I0(state[1]),
        .I1(state[3]),
        .I2(Q),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(state[0]),
        .I5(state[2]),
        .O(\FSM_sequential_state[1][0]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \FSM_sequential_state[1][0]_i_3__2 
       (.I0(\FSM_sequential_state[1][0]_i_4__2_n_0 ),
        .I1(\FSM_sequential_state[1][0]_i_5__2_n_0 ),
        .I2(\code_valid_pipe_reg_n_0_[1] ),
        .I3(\FSM_sequential_state[1][0]_i_6__2_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[0] ),
        .I5(\FSM_sequential_state[1][0]_i_7__2_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_3__2_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \FSM_sequential_state[1][0]_i_4__2 
       (.I0(state[0]),
        .I1(state[3]),
        .I2(state[1]),
        .I3(state[2]),
        .I4(\FSM_sequential_state[1][0]_i_8__2_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_4__2_n_0 ));
  LUT6 #(
    .INIT(64'hFF6AFFAFFF6AFAAA)) 
    \FSM_sequential_state[1][0]_i_5__2 
       (.I0(state[2]),
        .I1(state[3]),
        .I2(state[1]),
        .I3(get_next_state),
        .I4(state[0]),
        .I5(\FSM_sequential_state[1][0]_i_10__2_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_5__2_n_0 ));
  LUT6 #(
    .INIT(64'h3BFBFBFB38F8F8F8)) 
    \FSM_sequential_state[1][0]_i_6__2 
       (.I0(\FSM_sequential_state[1][0]_i_11__2_n_0 ),
        .I1(state[0]),
        .I2(state[2]),
        .I3(state[3]),
        .I4(state[1]),
        .I5(\FSM_sequential_state[1][0]_i_12__2_n_0 ),
        .O(\FSM_sequential_state[1][0]_i_6__2_n_0 ));
  LUT6 #(
    .INIT(64'hBCBC7E7F28282A2A)) 
    \FSM_sequential_state[1][0]_i_7__2 
       (.I0(state[2]),
        .I1(state[1]),
        .I2(state[0]),
        .I3(\next_state[1]1 ),
        .I4(state[3]),
        .I5(get_next_state),
        .O(\FSM_sequential_state[1][0]_i_7__2_n_0 ));
  LUT6 #(
    .INIT(64'h33333333443232CC)) 
    \FSM_sequential_state[1][0]_i_8__2 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(Q),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(state[3]),
        .O(\FSM_sequential_state[1][0]_i_8__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][0]_i_9__2 
       (.I0(Q),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .O(get_next_state));
  LUT6 #(
    .INIT(64'h8F800F0F8F800000)) 
    \FSM_sequential_state[1][1]_i_1__2 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(\FSM_sequential_state[1][2]_i_2__2_n_0 ),
        .I2(out),
        .I3(\FSM_sequential_state[1][1]_i_2__2_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[1] ),
        .I5(\FSM_sequential_state[1][1]_i_3__2_n_0 ),
        .O(\FSM_sequential_state[1][1]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h08380838F80BF808)) 
    \FSM_sequential_state[1][1]_i_2__2 
       (.I0(\FSM_sequential_state[1][1]_i_4__2_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(state[2]),
        .I3(state[0]),
        .I4(\FSM_sequential_state[1][1]_i_5__2_n_0 ),
        .I5(state[1]),
        .O(\FSM_sequential_state[1][1]_i_2__2_n_0 ));
  LUT5 #(
    .INIT(32'h27F98808)) 
    \FSM_sequential_state[1][1]_i_3__2 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(state[3]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(state[2]),
        .O(\FSM_sequential_state[1][1]_i_3__2_n_0 ));
  LUT6 #(
    .INIT(64'h6566666A6466666A)) 
    \FSM_sequential_state[1][1]_i_4__2 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(state[3]),
        .I3(\code_comma_pipe_reg_n_0_[1] ),
        .I4(\code_comma_pipe_reg_n_0_[0] ),
        .I5(Q),
        .O(\FSM_sequential_state[1][1]_i_4__2_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_state[1][1]_i_5__2 
       (.I0(state[3]),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(Q),
        .O(\FSM_sequential_state[1][1]_i_5__2_n_0 ));
  LUT6 #(
    .INIT(64'h8F800F0F8F800000)) 
    \FSM_sequential_state[1][2]_i_1__2 
       (.I0(\code_valid_pipe_reg_n_0_[0] ),
        .I1(\FSM_sequential_state[1][2]_i_2__2_n_0 ),
        .I2(out),
        .I3(\FSM_sequential_state[1][2]_i_3__2_n_0 ),
        .I4(\code_valid_pipe_reg_n_0_[1] ),
        .I5(\FSM_sequential_state[1][2]_i_4__2_n_0 ),
        .O(\FSM_sequential_state[1][2]_i_1__2_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_state[1][2]_i_2__2 
       (.I0(state[1]),
        .I1(state[3]),
        .I2(state[0]),
        .I3(state[2]),
        .O(\FSM_sequential_state[1][2]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'h8888888B88888888)) 
    \FSM_sequential_state[1][2]_i_3__2 
       (.I0(\FSM_sequential_state[1][2]_i_5__2_n_0 ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(state[1]),
        .I3(state[3]),
        .I4(state[0]),
        .I5(state[2]),
        .O(\FSM_sequential_state[1][2]_i_3__2_n_0 ));
  LUT6 #(
    .INIT(64'h0CFFFFCFC8C000C0)) 
    \FSM_sequential_state[1][2]_i_4__2 
       (.I0(\code_comma_pipe_reg_n_0_[0] ),
        .I1(\code_valid_pipe_reg_n_0_[0] ),
        .I2(state[3]),
        .I3(state[0]),
        .I4(state[1]),
        .I5(state[2]),
        .O(\FSM_sequential_state[1][2]_i_4__2_n_0 ));
  LUT6 #(
    .INIT(64'h55AA54AAAAFFEAAA)) 
    \FSM_sequential_state[1][2]_i_5__2 
       (.I0(state[2]),
        .I1(\code_comma_pipe_reg_n_0_[1] ),
        .I2(\code_comma_pipe_reg_n_0_[0] ),
        .I3(state[1]),
        .I4(state[3]),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][2]_i_5__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_state[1][3]_i_1__2 
       (.I0(\FSM_sequential_state[1][3]_i_2__2_n_0 ),
        .I1(out),
        .O(\FSM_sequential_state[1][3]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h32C88F800FC08A48)) 
    \FSM_sequential_state[1][3]_i_2__2 
       (.I0(\code_valid_pipe_reg_n_0_[1] ),
        .I1(state[3]),
        .I2(\code_valid_pipe_reg_n_0_[0] ),
        .I3(state[2]),
        .I4(state[1]),
        .I5(state[0]),
        .O(\FSM_sequential_state[1][3]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \FSM_sequential_state[1][4]_i_1__0 
       (.I0(\code_valid_pipe_reg_n_0_[1] ),
        .I1(state[2]),
        .I2(\FSM_sequential_state[1][4]_i_2__2_n_0 ),
        .I3(state[1]),
        .I4(\code_valid_pipe_reg_n_0_[0] ),
        .I5(out),
        .O(\FSM_sequential_state[1][4]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'hFFBEFFFF)) 
    \FSM_sequential_state[1][4]_i_1__2 
       (.I0(\core_mgt_rx_reset_reg[1] ),
        .I1(Q),
        .I2(signal_detect_last),
        .I3(usrclk_reset),
        .I4(mgt_rxlock),
        .O(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_state[1][4]_i_2__2 
       (.I0(state[3]),
        .I1(state[0]),
        .O(\FSM_sequential_state[1][4]_i_2__2_n_0 ));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][0]_i_1__2_n_0 ),
        .Q(state[0]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][1]_i_1__2_n_0 ),
        .Q(state[1]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][2]_i_1__2_n_0 ),
        .Q(state[2]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][3]_i_1__2_n_0 ),
        .Q(state[3]),
        .R(SR));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1][4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1][4]_i_1__0_n_0 ),
        .Q(out),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \code_comma_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg_reg[3] [0]),
        .Q(\code_comma_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_comma_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codecomma_reg_reg[3] [1]),
        .Q(\code_comma_pipe_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_valid_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg_reg[3] [0]),
        .Q(\code_valid_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \code_valid_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\mgt_codevalid_reg_reg[3] [1]),
        .Q(\code_valid_pipe_reg_n_0_[1] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00000001)) 
    enable_align_i_1__2
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[3]),
        .I3(state[1]),
        .I4(out),
        .O(enable_align_i));
  FDRE enable_align_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(enable_align_i),
        .Q(mgt_enable_align_i),
        .R(1'b0));
  FDRE signal_detect_last_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(signal_detect_last),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    \sync_ok[3]_i_1 
       (.I0(state[3]),
        .I1(state[2]),
        .O(\sync_ok_reg[3] ));
endmodule

(* ORIG_REF_NAME = "toggle_detect" *) 
module rxaui_0_toggle_detect
   (\FSM_sequential_state_reg[1][0] ,
    align_toggle_0,
    usrclk,
    mgt_rx_reset);
  output \FSM_sequential_state_reg[1][0] ;
  input align_toggle_0;
  input usrclk;
  input [0:0]mgt_rx_reset;

  wire \FSM_sequential_state_reg[1][0] ;
  wire align_toggle_0;
  (* async_reg = "true" *) wire d1;
  wire d2;
  wire d3;
  wire [0:0]mgt_rx_reset;
  wire usrclk;

  LUT3 #(
    .INIT(8'hBE)) 
    \FSM_sequential_state[1][4]_i_3 
       (.I0(mgt_rx_reset),
        .I1(d3),
        .I2(d2),
        .O(\FSM_sequential_state_reg[1][0] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    d1_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(align_toggle_0),
        .Q(d1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    d2_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(d1),
        .Q(d2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    d3_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(d2),
        .Q(d3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "toggle_detect" *) 
module rxaui_0_toggle_detect_10
   (rx_local_fault0,
    \FSM_sequential_state_reg[1][4] ,
    align_toggle_1,
    usrclk,
    mgt_rx_reset,
    align_status,
    \core_mgt_rx_reset_reg[0] );
  output rx_local_fault0;
  output \FSM_sequential_state_reg[1][4] ;
  input align_toggle_1;
  input usrclk;
  input [0:0]mgt_rx_reset;
  input align_status;
  input \core_mgt_rx_reset_reg[0] ;

  wire \FSM_sequential_state_reg[1][4] ;
  wire align_status;
  wire align_toggle_1;
  wire \core_mgt_rx_reset_reg[0] ;
  (* async_reg = "true" *) wire d1;
  wire d2;
  wire d3;
  wire [0:0]mgt_rx_reset;
  wire rx_local_fault0;
  wire usrclk;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hBE)) 
    \FSM_sequential_state[1][4]_i_3__0 
       (.I0(mgt_rx_reset),
        .I1(d3),
        .I2(d2),
        .O(\FSM_sequential_state_reg[1][4] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    d1_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(align_toggle_1),
        .Q(d1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    d2_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(d1),
        .Q(d2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    d3_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(d2),
        .Q(d3),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFF6FF)) 
    rx_local_fault_i_2
       (.I0(d2),
        .I1(d3),
        .I2(mgt_rx_reset),
        .I3(align_status),
        .I4(\core_mgt_rx_reset_reg[0] ),
        .O(rx_local_fault0));
endmodule

(* ORIG_REF_NAME = "tqmsg_capture" *) 
module rxaui_0_tqmsg_capture
   (q_det,
    last_qmsg,
    \state_reg[1][1] ,
    \state_reg[1][0] ,
    \state_reg[0][1] ,
    Q,
    txc_out_reg,
    usrclk,
    D,
    txd_filtered,
    txc_out_reg_0);
  output q_det;
  output [31:0]last_qmsg;
  input \state_reg[1][1] ;
  input [0:0]\state_reg[1][0] ;
  input \state_reg[0][1] ;
  input [0:0]Q;
  input txc_out_reg;
  input usrclk;
  input [23:0]D;
  input [7:0]txd_filtered;
  input txc_out_reg_0;

  wire [23:0]D;
  wire [0:0]Q;
  wire [31:0]last_qmsg;
  wire \last_qmsg[6]_i_1_n_0 ;
  wire q_det;
  wire q_det_i_1_n_0;
  wire \state_reg[0][1] ;
  wire [0:0]\state_reg[1][0] ;
  wire \state_reg[1][1] ;
  wire txc_out_reg;
  wire txc_out_reg_0;
  wire [7:0]txd_filtered;
  wire usrclk;

  LUT2 #(
    .INIT(4'h8)) 
    \last_qmsg[6]_i_1 
       (.I0(txc_out_reg_0),
        .I1(txc_out_reg),
        .O(\last_qmsg[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[0] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(txd_filtered[0]),
        .Q(last_qmsg[0]),
        .R(\last_qmsg[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[10] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[2]),
        .Q(last_qmsg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[11] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[3]),
        .Q(last_qmsg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[12] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[4]),
        .Q(last_qmsg[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[13] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[5]),
        .Q(last_qmsg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[14] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[6]),
        .Q(last_qmsg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[15] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[7]),
        .Q(last_qmsg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[16] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[8]),
        .Q(last_qmsg[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[17] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[9]),
        .Q(last_qmsg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[18] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[10]),
        .Q(last_qmsg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[19] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[11]),
        .Q(last_qmsg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[1] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(txd_filtered[1]),
        .Q(last_qmsg[1]),
        .R(\last_qmsg[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[20] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[12]),
        .Q(last_qmsg[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[21] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[13]),
        .Q(last_qmsg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[22] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[14]),
        .Q(last_qmsg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[23] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[15]),
        .Q(last_qmsg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[24] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[16]),
        .Q(last_qmsg[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[25] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[17]),
        .Q(last_qmsg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[26] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[18]),
        .Q(last_qmsg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[27] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[19]),
        .Q(last_qmsg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[28] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[20]),
        .Q(last_qmsg[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[29] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[21]),
        .Q(last_qmsg[29]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[2] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(txd_filtered[2]),
        .Q(last_qmsg[2]),
        .S(\last_qmsg[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[30] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[22]),
        .Q(last_qmsg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[31] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[23]),
        .Q(last_qmsg[31]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[3] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(txd_filtered[3]),
        .Q(last_qmsg[3]),
        .S(\last_qmsg[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[4] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(txd_filtered[4]),
        .Q(last_qmsg[4]),
        .S(\last_qmsg[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[5] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(txd_filtered[5]),
        .Q(last_qmsg[5]),
        .R(\last_qmsg[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[6] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(txd_filtered[6]),
        .Q(last_qmsg[6]),
        .R(\last_qmsg[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[7] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(txd_filtered[7]),
        .Q(last_qmsg[7]),
        .S(\last_qmsg[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[8] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[0]),
        .Q(last_qmsg[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_qmsg_reg[9] 
       (.C(usrclk),
        .CE(txc_out_reg),
        .D(D[1]),
        .Q(last_qmsg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFF07770000)) 
    q_det_i_1
       (.I0(\state_reg[1][1] ),
        .I1(\state_reg[1][0] ),
        .I2(\state_reg[0][1] ),
        .I3(Q),
        .I4(q_det),
        .I5(txc_out_reg),
        .O(q_det_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    q_det_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(q_det_i_1_n_0),
        .Q(q_det),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "tx" *) 
module rxaui_0_tx
   (\txc_out_reg[4] ,
    Q,
    \txd_out_reg[31] ,
    \txd_out_reg[31]_0 ,
    \txd_out_reg[54] ,
    \txd_out_reg[54]_0 ,
    \txd_out_reg[54]_1 ,
    \txc_out_reg[7] ,
    \txd_out_reg[27] ,
    \txc_out_reg[1] ,
    mgt_txdata,
    mgt_txcharisk,
    usrclk,
    usrclk_reset,
    test_en_reg_reg,
    test_pattern_en,
    test_pattern_sel,
    test_en_reg_reg_0,
    \test_sel_reg_reg[1] ,
    \test_sel_reg_reg[0] ,
    test_en_reg_reg_1,
    \test_sel_reg_reg[1]_0 ,
    xgmii_txd,
    xgmii_txc,
    test_en_reg_reg_2,
    test_en_reg_reg_3,
    \test_sel_reg_reg[1]_1 ,
    \test_sel_reg_reg[1]_2 ,
    D);
  output [4:0]\txc_out_reg[4] ;
  output [0:0]Q;
  output \txd_out_reg[31] ;
  output \txd_out_reg[31]_0 ;
  output [0:0]\txd_out_reg[54] ;
  output \txd_out_reg[54]_0 ;
  output \txd_out_reg[54]_1 ;
  output \txc_out_reg[7] ;
  output \txd_out_reg[27] ;
  output \txc_out_reg[1] ;
  output [63:0]mgt_txdata;
  output [7:0]mgt_txcharisk;
  input usrclk;
  input usrclk_reset;
  input test_en_reg_reg;
  input test_pattern_en;
  input [1:0]test_pattern_sel;
  input test_en_reg_reg_0;
  input \test_sel_reg_reg[1] ;
  input \test_sel_reg_reg[0] ;
  input test_en_reg_reg_1;
  input \test_sel_reg_reg[1]_0 ;
  input [63:0]xgmii_txd;
  input [7:0]xgmii_txc;
  input test_en_reg_reg_2;
  input test_en_reg_reg_3;
  input \test_sel_reg_reg[1]_1 ;
  input \test_sel_reg_reg[1]_2 ;
  input [4:0]D;

  wire [4:0]D;
  wire \G_FILTER_HIGH[4].filter_n_1 ;
  wire \G_FILTER_HIGH[5].filter_n_10 ;
  wire \G_FILTER_HIGH[5].filter_n_11 ;
  wire \G_FILTER_HIGH[5].filter_n_12 ;
  wire \G_FILTER_HIGH[5].filter_n_13 ;
  wire \G_FILTER_HIGH[5].filter_n_14 ;
  wire \G_FILTER_HIGH[5].filter_n_15 ;
  wire \G_FILTER_HIGH[5].filter_n_16 ;
  wire \G_FILTER_HIGH[5].filter_n_9 ;
  wire \G_FILTER_HIGH[6].filter_n_10 ;
  wire \G_FILTER_HIGH[6].filter_n_11 ;
  wire \G_FILTER_HIGH[6].filter_n_12 ;
  wire \G_FILTER_HIGH[6].filter_n_13 ;
  wire \G_FILTER_HIGH[6].filter_n_14 ;
  wire \G_FILTER_HIGH[6].filter_n_15 ;
  wire \G_FILTER_HIGH[6].filter_n_16 ;
  wire \G_FILTER_HIGH[6].filter_n_9 ;
  wire \G_FILTER_HIGH[7].filter_n_10 ;
  wire \G_FILTER_HIGH[7].filter_n_11 ;
  wire \G_FILTER_HIGH[7].filter_n_12 ;
  wire \G_FILTER_HIGH[7].filter_n_13 ;
  wire \G_FILTER_HIGH[7].filter_n_14 ;
  wire \G_FILTER_HIGH[7].filter_n_15 ;
  wire \G_FILTER_HIGH[7].filter_n_16 ;
  wire \G_FILTER_HIGH[7].filter_n_9 ;
  wire \G_FILTER_LOW[0].filter_n_1 ;
  wire [0:0]Q;
  wire align_n_6;
  wire align_n_7;
  wire align_n_8;
  wire [3:0]c;
  wire c12_in;
  wire c15_in;
  wire c9_in;
  wire [1:0]code_sel;
  wire [4:0]count;
  wire [7:0]d;
  wire [31:8]data1;
  wire [1:0]is_idle;
  wire [7:0]is_invalid_k;
  wire [1:0]is_q;
  wire is_terminate;
  wire \is_terminate[0]_i_1_n_0 ;
  wire \is_terminate[0]_i_2_n_0 ;
  wire \is_terminate[0]_i_3_n_0 ;
  wire \is_terminate[0]_i_4_n_0 ;
  wire \is_terminate[0]_i_5_n_0 ;
  wire \is_terminate[0]_i_6_n_0 ;
  wire \is_terminate[1]_i_2_n_0 ;
  wire \is_terminate[1]_i_3_n_0 ;
  wire \is_terminate[1]_i_4_n_0 ;
  wire \is_terminate[1]_i_5_n_0 ;
  wire \is_terminate[1]_i_6_n_0 ;
  wire \is_terminate_reg_n_0_[0] ;
  wire is_txd_IDLE;
  wire [31:0]last_qmsg;
  wire [7:0]mgt_txcharisk;
  wire [63:0]mgt_txdata;
  wire [31:8]p_1_in;
  wire [1:0]p_1_in_0;
  wire [1:1]p_1_out;
  wire q_det;
  wire state_machine_n_0;
  wire state_machine_n_1;
  wire state_machine_n_10;
  wire state_machine_n_11;
  wire state_machine_n_12;
  wire state_machine_n_13;
  wire state_machine_n_14;
  wire state_machine_n_15;
  wire state_machine_n_16;
  wire state_machine_n_17;
  wire state_machine_n_18;
  wire state_machine_n_19;
  wire state_machine_n_20;
  wire state_machine_n_21;
  wire state_machine_n_22;
  wire state_machine_n_23;
  wire state_machine_n_24;
  wire state_machine_n_25;
  wire state_machine_n_26;
  wire state_machine_n_27;
  wire state_machine_n_28;
  wire state_machine_n_29;
  wire state_machine_n_30;
  wire state_machine_n_31;
  wire state_machine_n_32;
  wire state_machine_n_33;
  wire state_machine_n_34;
  wire state_machine_n_35;
  wire state_machine_n_36;
  wire state_machine_n_37;
  wire state_machine_n_38;
  wire state_machine_n_39;
  wire state_machine_n_40;
  wire state_machine_n_42;
  wire state_machine_n_43;
  wire state_machine_n_44;
  wire state_machine_n_45;
  wire state_machine_n_46;
  wire state_machine_n_47;
  wire state_machine_n_48;
  wire state_machine_n_49;
  wire state_machine_n_5;
  wire state_machine_n_50;
  wire state_machine_n_51;
  wire state_machine_n_52;
  wire state_machine_n_53;
  wire state_machine_n_54;
  wire state_machine_n_56;
  wire state_machine_n_57;
  wire state_machine_n_58;
  wire state_machine_n_59;
  wire state_machine_n_6;
  wire state_machine_n_60;
  wire state_machine_n_62;
  wire state_machine_n_63;
  wire state_machine_n_64;
  wire state_machine_n_65;
  wire state_machine_n_66;
  wire state_machine_n_67;
  wire state_machine_n_68;
  wire state_machine_n_69;
  wire state_machine_n_70;
  wire state_machine_n_71;
  wire state_machine_n_72;
  wire state_machine_n_73;
  wire state_machine_n_74;
  wire state_machine_n_75;
  wire state_machine_n_76;
  wire state_machine_n_77;
  wire state_machine_n_78;
  wire state_machine_n_79;
  wire state_machine_n_80;
  wire state_machine_n_81;
  wire state_machine_n_82;
  wire state_machine_n_83;
  wire test_en_reg_reg;
  wire test_en_reg_reg_0;
  wire test_en_reg_reg_1;
  wire test_en_reg_reg_2;
  wire test_en_reg_reg_3;
  wire test_pattern_en;
  wire [1:0]test_pattern_sel;
  wire \test_sel_reg_reg[0] ;
  wire \test_sel_reg_reg[1] ;
  wire \test_sel_reg_reg[1]_0 ;
  wire \test_sel_reg_reg[1]_1 ;
  wire \test_sel_reg_reg[1]_2 ;
  wire \tx_is_idle[0]_i_2_n_0 ;
  wire \tx_is_idle[1]_i_2_n_0 ;
  wire [1:0]tx_is_idle_comb;
  wire \tx_is_invalid_k_reg[0]_i_1_n_0 ;
  wire \tx_is_invalid_k_reg[0]_i_2_n_0 ;
  wire \tx_is_invalid_k_reg[0]_i_3_n_0 ;
  wire \tx_is_invalid_k_reg[1]_i_1_n_0 ;
  wire \tx_is_invalid_k_reg[1]_i_2_n_0 ;
  wire \tx_is_invalid_k_reg[1]_i_3_n_0 ;
  wire \tx_is_invalid_k_reg[2]_i_1_n_0 ;
  wire \tx_is_invalid_k_reg[2]_i_2_n_0 ;
  wire \tx_is_invalid_k_reg[2]_i_3_n_0 ;
  wire \tx_is_invalid_k_reg[3]_i_1_n_0 ;
  wire \tx_is_invalid_k_reg[3]_i_2_n_0 ;
  wire \tx_is_invalid_k_reg[3]_i_3_n_0 ;
  wire \tx_is_invalid_k_reg[4]_i_1_n_0 ;
  wire \tx_is_invalid_k_reg[4]_i_2_n_0 ;
  wire \tx_is_invalid_k_reg[4]_i_3_n_0 ;
  wire \tx_is_invalid_k_reg[5]_i_1_n_0 ;
  wire \tx_is_invalid_k_reg[5]_i_2_n_0 ;
  wire \tx_is_invalid_k_reg[5]_i_3_n_0 ;
  wire \tx_is_invalid_k_reg[6]_i_1_n_0 ;
  wire \tx_is_invalid_k_reg[6]_i_2_n_0 ;
  wire \tx_is_invalid_k_reg[6]_i_3_n_0 ;
  wire \tx_is_invalid_k_reg[7]_i_1_n_0 ;
  wire \tx_is_invalid_k_reg[7]_i_2_n_0 ;
  wire \tx_is_invalid_k_reg[7]_i_3_n_0 ;
  wire \tx_is_q[0]_i_2_n_0 ;
  wire \tx_is_q[0]_i_3_n_0 ;
  wire \tx_is_q[1]_i_2_n_0 ;
  wire \tx_is_q[1]_i_3_n_0 ;
  wire [1:0]tx_is_q_comb;
  wire [7:5]txc_filtered;
  wire \txc_out_reg[1] ;
  wire [4:0]\txc_out_reg[4] ;
  wire \txc_out_reg[7] ;
  wire [7:0]txc_pipe_2;
  wire \txc_pipe_reg_n_0_[0] ;
  wire [39:0]txd_filtered;
  wire txd_is_IDLE;
  wire \txd_is_IDLE_reg[0]_i_1_n_0 ;
  wire \txd_is_IDLE_reg[0]_i_2_n_0 ;
  wire \txd_is_IDLE_reg[1]_i_1_n_0 ;
  wire \txd_is_IDLE_reg[1]_i_2_n_0 ;
  wire \txd_is_IDLE_reg[2]_i_1_n_0 ;
  wire \txd_is_IDLE_reg[2]_i_2_n_0 ;
  wire \txd_is_IDLE_reg[3]_i_1_n_0 ;
  wire \txd_is_IDLE_reg[3]_i_2_n_0 ;
  wire \txd_is_IDLE_reg[4]_i_1_n_0 ;
  wire \txd_is_IDLE_reg[4]_i_2_n_0 ;
  wire \txd_is_IDLE_reg[5]_i_1_n_0 ;
  wire \txd_is_IDLE_reg[5]_i_2_n_0 ;
  wire \txd_is_IDLE_reg[6]_i_1_n_0 ;
  wire \txd_is_IDLE_reg[6]_i_2_n_0 ;
  wire \txd_is_IDLE_reg[7]_i_2_n_0 ;
  wire \txd_is_IDLE_reg_reg_n_0_[0] ;
  wire \txd_is_IDLE_reg_reg_n_0_[2] ;
  wire \txd_is_IDLE_reg_reg_n_0_[3] ;
  wire \txd_is_IDLE_reg_reg_n_0_[4] ;
  wire \txd_is_IDLE_reg_reg_n_0_[5] ;
  wire \txd_is_IDLE_reg_reg_n_0_[6] ;
  wire \txd_is_IDLE_reg_reg_n_0_[7] ;
  wire \txd_out_reg[27] ;
  wire \txd_out_reg[31] ;
  wire \txd_out_reg[31]_0 ;
  wire [0:0]\txd_out_reg[54] ;
  wire \txd_out_reg[54]_0 ;
  wire \txd_out_reg[54]_1 ;
  wire [63:0]txd_pipe_2;
  wire \txd_pipe_reg_n_0_[0] ;
  wire \txd_pipe_reg_n_0_[16] ;
  wire \txd_pipe_reg_n_0_[17] ;
  wire \txd_pipe_reg_n_0_[18] ;
  wire \txd_pipe_reg_n_0_[19] ;
  wire \txd_pipe_reg_n_0_[1] ;
  wire \txd_pipe_reg_n_0_[20] ;
  wire \txd_pipe_reg_n_0_[21] ;
  wire \txd_pipe_reg_n_0_[22] ;
  wire \txd_pipe_reg_n_0_[23] ;
  wire \txd_pipe_reg_n_0_[24] ;
  wire \txd_pipe_reg_n_0_[25] ;
  wire \txd_pipe_reg_n_0_[26] ;
  wire \txd_pipe_reg_n_0_[27] ;
  wire \txd_pipe_reg_n_0_[28] ;
  wire \txd_pipe_reg_n_0_[29] ;
  wire \txd_pipe_reg_n_0_[2] ;
  wire \txd_pipe_reg_n_0_[30] ;
  wire \txd_pipe_reg_n_0_[31] ;
  wire \txd_pipe_reg_n_0_[32] ;
  wire \txd_pipe_reg_n_0_[33] ;
  wire \txd_pipe_reg_n_0_[34] ;
  wire \txd_pipe_reg_n_0_[35] ;
  wire \txd_pipe_reg_n_0_[36] ;
  wire \txd_pipe_reg_n_0_[37] ;
  wire \txd_pipe_reg_n_0_[38] ;
  wire \txd_pipe_reg_n_0_[39] ;
  wire \txd_pipe_reg_n_0_[3] ;
  wire \txd_pipe_reg_n_0_[40] ;
  wire \txd_pipe_reg_n_0_[41] ;
  wire \txd_pipe_reg_n_0_[42] ;
  wire \txd_pipe_reg_n_0_[43] ;
  wire \txd_pipe_reg_n_0_[44] ;
  wire \txd_pipe_reg_n_0_[45] ;
  wire \txd_pipe_reg_n_0_[46] ;
  wire \txd_pipe_reg_n_0_[47] ;
  wire \txd_pipe_reg_n_0_[48] ;
  wire \txd_pipe_reg_n_0_[49] ;
  wire \txd_pipe_reg_n_0_[4] ;
  wire \txd_pipe_reg_n_0_[50] ;
  wire \txd_pipe_reg_n_0_[51] ;
  wire \txd_pipe_reg_n_0_[52] ;
  wire \txd_pipe_reg_n_0_[53] ;
  wire \txd_pipe_reg_n_0_[54] ;
  wire \txd_pipe_reg_n_0_[55] ;
  wire \txd_pipe_reg_n_0_[56] ;
  wire \txd_pipe_reg_n_0_[57] ;
  wire \txd_pipe_reg_n_0_[58] ;
  wire \txd_pipe_reg_n_0_[59] ;
  wire \txd_pipe_reg_n_0_[5] ;
  wire \txd_pipe_reg_n_0_[60] ;
  wire \txd_pipe_reg_n_0_[61] ;
  wire \txd_pipe_reg_n_0_[62] ;
  wire \txd_pipe_reg_n_0_[63] ;
  wire \txd_pipe_reg_n_0_[6] ;
  wire \txd_pipe_reg_n_0_[7] ;
  wire usrclk;
  wire usrclk_reset;
  wire [7:0]xgmii_txc;
  wire [63:0]xgmii_txd;

  rxaui_0_tx_filter \G_FILTER_HIGH[4].filter 
       (.Q(txc_pipe_2[4]),
        .is_invalid_k(is_invalid_k[4]),
        .is_terminate(is_terminate),
        .\last_qmsg_reg[6] (\G_FILTER_HIGH[4].filter_n_1 ),
        .txc_filtered(txc_filtered),
        .\txc_out_reg[4] (\txc_out_reg[4] [4]),
        .txd_filtered(txd_filtered[39:32]),
        .\txd_is_IDLE_reg_reg[4] (\txd_is_IDLE_reg_reg_n_0_[4] ),
        .\txd_pipe_2_reg[39] (txd_pipe_2[39:32]),
        .usrclk(usrclk));
  rxaui_0_tx_filter_12 \G_FILTER_HIGH[5].filter 
       (.D(p_1_in[15:8]),
        .Q(txc_pipe_2[5]),
        .data1(data1[15:8]),
        .is_invalid_k(is_invalid_k[5]),
        .is_terminate(is_terminate),
        .\last_qmsg_reg[10] (\G_FILTER_HIGH[5].filter_n_12 ),
        .\last_qmsg_reg[11] (\G_FILTER_HIGH[5].filter_n_13 ),
        .\last_qmsg_reg[12] (\G_FILTER_HIGH[5].filter_n_14 ),
        .\last_qmsg_reg[13] (\G_FILTER_HIGH[5].filter_n_15 ),
        .\last_qmsg_reg[14] ({\G_FILTER_HIGH[5].filter_n_9 ,\G_FILTER_HIGH[5].filter_n_10 ,\G_FILTER_HIGH[5].filter_n_11 }),
        .\last_qmsg_reg[15] (\G_FILTER_HIGH[5].filter_n_16 ),
        .txc_filtered(txc_filtered[5]),
        .txc_out_reg_0(\G_FILTER_HIGH[4].filter_n_1 ),
        .\txd_is_IDLE_reg_reg[5] (\txd_is_IDLE_reg_reg_n_0_[5] ),
        .\txd_pipe_2_reg[47] (txd_pipe_2[47:40]),
        .usrclk(usrclk));
  rxaui_0_tx_filter_13 \G_FILTER_HIGH[6].filter 
       (.D(p_1_in[23:16]),
        .Q(txc_pipe_2[6]),
        .data1(data1[23:16]),
        .is_invalid_k(is_invalid_k[6]),
        .is_terminate(is_terminate),
        .\last_qmsg_reg[18] (\G_FILTER_HIGH[6].filter_n_12 ),
        .\last_qmsg_reg[19] (\G_FILTER_HIGH[6].filter_n_13 ),
        .\last_qmsg_reg[20] (\G_FILTER_HIGH[6].filter_n_14 ),
        .\last_qmsg_reg[21] (\G_FILTER_HIGH[6].filter_n_15 ),
        .\last_qmsg_reg[22] ({\G_FILTER_HIGH[6].filter_n_9 ,\G_FILTER_HIGH[6].filter_n_10 ,\G_FILTER_HIGH[6].filter_n_11 }),
        .\last_qmsg_reg[23] (\G_FILTER_HIGH[6].filter_n_16 ),
        .txc_filtered(txc_filtered[6]),
        .txc_out_reg_0(\G_FILTER_HIGH[4].filter_n_1 ),
        .\txd_is_IDLE_reg_reg[6] (\txd_is_IDLE_reg_reg_n_0_[6] ),
        .\txd_pipe_2_reg[55] (txd_pipe_2[55:48]),
        .usrclk(usrclk));
  rxaui_0_tx_filter_14 \G_FILTER_HIGH[7].filter 
       (.D(p_1_in[31:24]),
        .Q(txc_pipe_2[7]),
        .data1(data1[31:24]),
        .is_invalid_k(is_invalid_k[7]),
        .is_terminate(is_terminate),
        .\last_qmsg_reg[26] (\G_FILTER_HIGH[7].filter_n_12 ),
        .\last_qmsg_reg[27] (\G_FILTER_HIGH[7].filter_n_13 ),
        .\last_qmsg_reg[28] (\G_FILTER_HIGH[7].filter_n_14 ),
        .\last_qmsg_reg[29] (\G_FILTER_HIGH[7].filter_n_15 ),
        .\last_qmsg_reg[30] ({\G_FILTER_HIGH[7].filter_n_9 ,\G_FILTER_HIGH[7].filter_n_10 ,\G_FILTER_HIGH[7].filter_n_11 }),
        .\last_qmsg_reg[31] (\G_FILTER_HIGH[7].filter_n_16 ),
        .txc_filtered(txc_filtered[7]),
        .txc_out_reg_0(\G_FILTER_HIGH[4].filter_n_1 ),
        .\txd_is_IDLE_reg_reg[7] (\txd_is_IDLE_reg_reg_n_0_[7] ),
        .\txd_pipe_2_reg[63] (txd_pipe_2[63:56]),
        .usrclk(usrclk));
  rxaui_0_tx_filter_15 \G_FILTER_LOW[0].filter 
       (.Q(txc_pipe_2[0]),
        .is_invalid_k(is_invalid_k[0]),
        .\is_terminate_reg[0] (\is_terminate_reg_n_0_[0] ),
        .\last_qmsg_reg[31] (\G_FILTER_LOW[0].filter_n_1 ),
        .\txc_out_reg[0] (\txc_out_reg[4] [0]),
        .txc_out_reg_0(\txc_out_reg[4] [3:1]),
        .txc_out_reg_1(\G_FILTER_HIGH[4].filter_n_1 ),
        .txd_filtered(txd_filtered[7:0]),
        .\txd_is_IDLE_reg_reg[0] (\txd_is_IDLE_reg_reg_n_0_[0] ),
        .\txd_pipe_2_reg[7] (txd_pipe_2[7:0]),
        .usrclk(usrclk));
  rxaui_0_tx_filter_16 \G_FILTER_LOW[1].filter 
       (.Q(txc_pipe_2[1]),
        .data1(data1[15:8]),
        .is_invalid_k(is_invalid_k[1]),
        .\is_terminate_reg[0] (\is_terminate_reg_n_0_[0] ),
        .\txc_out_reg[1] (\txc_out_reg[4] [1]),
        .\txd_is_IDLE_reg_reg[1] (is_txd_IDLE),
        .\txd_pipe_2_reg[15] (txd_pipe_2[15:8]),
        .usrclk(usrclk));
  rxaui_0_tx_filter_17 \G_FILTER_LOW[2].filter 
       (.Q(txc_pipe_2[2]),
        .data1(data1[23:16]),
        .is_invalid_k(is_invalid_k[2]),
        .\is_terminate_reg[0] (\is_terminate_reg_n_0_[0] ),
        .\txc_out_reg[2] (\txc_out_reg[4] [2]),
        .\txd_is_IDLE_reg_reg[2] (\txd_is_IDLE_reg_reg_n_0_[2] ),
        .\txd_pipe_2_reg[23] (txd_pipe_2[23:16]),
        .usrclk(usrclk));
  rxaui_0_tx_filter_18 \G_FILTER_LOW[3].filter 
       (.Q(txc_pipe_2[3]),
        .data1(data1[31:24]),
        .is_invalid_k(is_invalid_k[3]),
        .\is_terminate_reg[0] (\is_terminate_reg_n_0_[0] ),
        .\last_qmsg_reg[31] (\txc_out_reg[4] [3]),
        .\txd_is_IDLE_reg_reg[3] (\txd_is_IDLE_reg_reg_n_0_[3] ),
        .\txd_pipe_2_reg[31] (txd_pipe_2[31:24]),
        .usrclk(usrclk));
  rxaui_0_align_counter align
       (.D({state_machine_n_42,state_machine_n_43}),
        .E(state_machine_n_44),
        .Q(p_1_in_0),
        .\count_reg[4]_0 ({count[4:3],count[1:0]}),
        .\count_reg[4]_1 (state_machine_n_45),
        .next_ifg_is_a_reg(state_machine_n_0),
        .\state_reg[0][0] (align_n_8),
        .\state_reg[0][0]_0 (Q),
        .\state_reg[0][1] (\txd_out_reg[31]_0 ),
        .\state_reg[1][0] (state_machine_n_46),
        .\state_reg[1][0]_0 (\txd_out_reg[54] ),
        .\state_reg[1][1] (align_n_6),
        .\state_reg[1][1]_0 (align_n_7),
        .\state_reg[1][1]_1 (\txd_out_reg[54]_0 ),
        .\tx_is_idle_reg[0] (is_idle[0]),
        .\tx_is_q_reg[0] (is_q[0]),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset));
  LUT5 #(
    .INIT(32'hFFFF22F2)) 
    \is_terminate[0]_i_1 
       (.I0(\is_terminate[0]_i_2_n_0 ),
        .I1(\tx_is_invalid_k_reg[2]_i_2_n_0 ),
        .I2(\is_terminate[0]_i_3_n_0 ),
        .I3(\tx_is_invalid_k_reg[3]_i_2_n_0 ),
        .I4(\is_terminate[0]_i_4_n_0 ),
        .O(\is_terminate[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \is_terminate[0]_i_2 
       (.I0(\txd_pipe_reg_n_0_[18] ),
        .I1(\txd_pipe_reg_n_0_[17] ),
        .I2(c12_in),
        .I3(\txd_pipe_reg_n_0_[16] ),
        .I4(\txd_pipe_reg_n_0_[20] ),
        .I5(\txd_pipe_reg_n_0_[19] ),
        .O(\is_terminate[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \is_terminate[0]_i_3 
       (.I0(\txd_pipe_reg_n_0_[26] ),
        .I1(\txd_pipe_reg_n_0_[25] ),
        .I2(c9_in),
        .I3(\txd_pipe_reg_n_0_[24] ),
        .I4(\txd_pipe_reg_n_0_[28] ),
        .I5(\txd_pipe_reg_n_0_[27] ),
        .O(\is_terminate[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000FFFF80008000)) 
    \is_terminate[0]_i_4 
       (.I0(\txd_pipe_reg_n_0_[6] ),
        .I1(\txd_pipe_reg_n_0_[7] ),
        .I2(\txd_pipe_reg_n_0_[5] ),
        .I3(\is_terminate[0]_i_5_n_0 ),
        .I4(\tx_is_invalid_k_reg[1]_i_2_n_0 ),
        .I5(\is_terminate[0]_i_6_n_0 ),
        .O(\is_terminate[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \is_terminate[0]_i_5 
       (.I0(\txd_pipe_reg_n_0_[2] ),
        .I1(\txd_pipe_reg_n_0_[1] ),
        .I2(\txc_pipe_reg_n_0_[0] ),
        .I3(\txd_pipe_reg_n_0_[0] ),
        .I4(\txd_pipe_reg_n_0_[4] ),
        .I5(\txd_pipe_reg_n_0_[3] ),
        .O(\is_terminate[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \is_terminate[0]_i_6 
       (.I0(d[2]),
        .I1(d[1]),
        .I2(c15_in),
        .I3(d[0]),
        .I4(d[4]),
        .I5(d[3]),
        .O(\is_terminate[0]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAEFFAEAE)) 
    \is_terminate[1]_i_1 
       (.I0(\is_terminate[1]_i_2_n_0 ),
        .I1(\is_terminate[1]_i_3_n_0 ),
        .I2(\tx_is_invalid_k_reg[5]_i_2_n_0 ),
        .I3(\tx_is_invalid_k_reg[4]_i_2_n_0 ),
        .I4(\is_terminate[1]_i_4_n_0 ),
        .O(p_1_out));
  LUT6 #(
    .INIT(64'h8000FFFF80008000)) 
    \is_terminate[1]_i_2 
       (.I0(\txd_pipe_reg_n_0_[62] ),
        .I1(\txd_pipe_reg_n_0_[63] ),
        .I2(\txd_pipe_reg_n_0_[61] ),
        .I3(\is_terminate[1]_i_5_n_0 ),
        .I4(\tx_is_invalid_k_reg[6]_i_2_n_0 ),
        .I5(\is_terminate[1]_i_6_n_0 ),
        .O(\is_terminate[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \is_terminate[1]_i_3 
       (.I0(\txd_pipe_reg_n_0_[42] ),
        .I1(\txd_pipe_reg_n_0_[41] ),
        .I2(c[1]),
        .I3(\txd_pipe_reg_n_0_[40] ),
        .I4(\txd_pipe_reg_n_0_[44] ),
        .I5(\txd_pipe_reg_n_0_[43] ),
        .O(\is_terminate[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \is_terminate[1]_i_4 
       (.I0(\txd_pipe_reg_n_0_[34] ),
        .I1(\txd_pipe_reg_n_0_[33] ),
        .I2(c[0]),
        .I3(\txd_pipe_reg_n_0_[32] ),
        .I4(\txd_pipe_reg_n_0_[36] ),
        .I5(\txd_pipe_reg_n_0_[35] ),
        .O(\is_terminate[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \is_terminate[1]_i_5 
       (.I0(\txd_pipe_reg_n_0_[58] ),
        .I1(\txd_pipe_reg_n_0_[57] ),
        .I2(c[3]),
        .I3(\txd_pipe_reg_n_0_[56] ),
        .I4(\txd_pipe_reg_n_0_[60] ),
        .I5(\txd_pipe_reg_n_0_[59] ),
        .O(\is_terminate[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \is_terminate[1]_i_6 
       (.I0(\txd_pipe_reg_n_0_[50] ),
        .I1(\txd_pipe_reg_n_0_[49] ),
        .I2(c[2]),
        .I3(\txd_pipe_reg_n_0_[48] ),
        .I4(\txd_pipe_reg_n_0_[52] ),
        .I5(\txd_pipe_reg_n_0_[51] ),
        .O(\is_terminate[1]_i_6_n_0 ));
  FDRE \is_terminate_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\is_terminate[0]_i_1_n_0 ),
        .Q(\is_terminate_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \is_terminate_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(p_1_out),
        .Q(is_terminate),
        .R(1'b0));
  rxaui_0_k_r_prbs k_r_prbs_i
       (.Q({code_sel[0],code_sel[1]}),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset));
  rxaui_0_tx_recoder recoder
       (.D({state_machine_n_52,state_machine_n_53,state_machine_n_54}),
        .Q(Q),
        .\last_qmsg_reg[0] (state_machine_n_31),
        .\last_qmsg_reg[0]_0 (state_machine_n_13),
        .\last_qmsg_reg[13] (state_machine_n_56),
        .\last_qmsg_reg[14] (state_machine_n_10),
        .\last_qmsg_reg[14]_0 (state_machine_n_5),
        .\last_qmsg_reg[16] (state_machine_n_35),
        .\last_qmsg_reg[16]_0 (state_machine_n_21),
        .\last_qmsg_reg[21] (state_machine_n_58),
        .\last_qmsg_reg[22] (state_machine_n_40),
        .\last_qmsg_reg[22]_0 (state_machine_n_6),
        .\last_qmsg_reg[24] (state_machine_n_37),
        .\last_qmsg_reg[24]_0 (state_machine_n_25),
        .\last_qmsg_reg[30] (state_machine_n_11),
        .\last_qmsg_reg[30]_0 (state_machine_n_29),
        .\last_qmsg_reg[5] (state_machine_n_51),
        .\last_qmsg_reg[6] (state_machine_n_39),
        .\last_qmsg_reg[6]_0 (state_machine_n_1),
        .\last_qmsg_reg[8] (state_machine_n_33),
        .\last_qmsg_reg[8]_0 (state_machine_n_17),
        .mgt_txcharisk(mgt_txcharisk),
        .mgt_txdata(mgt_txdata),
        .\state_reg[0][0] (state_machine_n_60),
        .\state_reg[0][0]_0 (state_machine_n_49),
        .\state_reg[0][0]_1 (state_machine_n_48),
        .\state_reg[0][0]_10 (state_machine_n_15),
        .\state_reg[0][0]_2 (state_machine_n_47),
        .\state_reg[0][0]_3 (state_machine_n_28),
        .\state_reg[0][0]_4 (state_machine_n_27),
        .\state_reg[0][0]_5 (state_machine_n_24),
        .\state_reg[0][0]_6 (state_machine_n_23),
        .\state_reg[0][0]_7 (state_machine_n_20),
        .\state_reg[0][0]_8 (state_machine_n_19),
        .\state_reg[0][0]_9 (state_machine_n_16),
        .\state_reg[0][1] (\txd_out_reg[31]_0 ),
        .\state_reg[0][2] (state_machine_n_26),
        .\state_reg[0][2]_0 (state_machine_n_22),
        .\state_reg[0][2]_1 (state_machine_n_18),
        .\state_reg[0][2]_2 (state_machine_n_14),
        .\state_reg[0][2]_3 (state_machine_n_57),
        .\state_reg[0][2]_4 (state_machine_n_50),
        .\state_reg[0][2]_5 (state_machine_n_30),
        .\state_reg[0][2]_6 (state_machine_n_59),
        .\state_reg[0][2]_7 (\txd_out_reg[31] ),
        .\state_reg[1][2] (state_machine_n_38),
        .\state_reg[1][2]_0 (state_machine_n_36),
        .\state_reg[1][2]_1 (state_machine_n_34),
        .\state_reg[1][2]_10 (state_machine_n_70),
        .\state_reg[1][2]_11 (state_machine_n_65),
        .\state_reg[1][2]_12 (state_machine_n_81),
        .\state_reg[1][2]_13 (state_machine_n_77),
        .\state_reg[1][2]_14 (state_machine_n_74),
        .\state_reg[1][2]_15 (state_machine_n_72),
        .\state_reg[1][2]_16 (state_machine_n_69),
        .\state_reg[1][2]_17 (state_machine_n_67),
        .\state_reg[1][2]_18 (state_machine_n_64),
        .\state_reg[1][2]_19 (state_machine_n_62),
        .\state_reg[1][2]_2 (state_machine_n_32),
        .\state_reg[1][2]_20 (state_machine_n_73),
        .\state_reg[1][2]_21 (state_machine_n_68),
        .\state_reg[1][2]_22 (state_machine_n_63),
        .\state_reg[1][2]_3 (state_machine_n_12),
        .\state_reg[1][2]_4 (state_machine_n_83),
        .\state_reg[1][2]_5 (state_machine_n_76),
        .\state_reg[1][2]_6 (state_machine_n_71),
        .\state_reg[1][2]_7 (state_machine_n_66),
        .\state_reg[1][2]_8 (state_machine_n_82),
        .\state_reg[1][2]_9 (state_machine_n_75),
        .test_en_reg_reg(test_en_reg_reg_0),
        .test_en_reg_reg_0(test_en_reg_reg_2),
        .test_en_reg_reg_1(test_en_reg_reg_3),
        .test_pattern_en(test_pattern_en),
        .test_pattern_sel(test_pattern_sel),
        .\test_sel_reg_reg[1] (\test_sel_reg_reg[1]_1 ),
        .\test_sel_reg_reg[1]_0 (\test_sel_reg_reg[1]_2 ),
        .\test_sel_reg_reg[1]_1 (\test_sel_reg_reg[1] ),
        .txc_out_reg({state_machine_n_78,state_machine_n_79,state_machine_n_80,D}),
        .usrclk(usrclk));
  rxaui_0_tx_state_machine state_machine
       (.D({state_machine_n_42,state_machine_n_43}),
        .E(state_machine_n_44),
        .Q(Q),
        .\count_reg[1] (align_n_7),
        .\count_reg[4] (state_machine_n_46),
        .\count_reg[4]_0 ({count[4:3],count[1:0]}),
        .data1(data1),
        .extra_a_reg(align_n_8),
        .last_qmsg(last_qmsg),
        .next_ifg_is_a_reg_0(state_machine_n_0),
        .\prbs_reg[2] (p_1_in_0),
        .\prbs_reg[8] ({code_sel[0],code_sel[1]}),
        .q_det(q_det),
        .\state_reg[1][1]_0 (state_machine_n_45),
        .test_en_reg_reg(test_en_reg_reg),
        .test_en_reg_reg_0(test_en_reg_reg_0),
        .test_en_reg_reg_1(test_en_reg_reg_1),
        .test_pattern_en(test_pattern_en),
        .test_pattern_sel(test_pattern_sel),
        .\test_sel_reg_reg[0] (\test_sel_reg_reg[0] ),
        .\test_sel_reg_reg[1] (\test_sel_reg_reg[1] ),
        .\test_sel_reg_reg[1]_0 (\test_sel_reg_reg[1]_0 ),
        .\tx_is_idle_reg[0] (align_n_6),
        .\tx_is_idle_reg[1] (is_idle),
        .\tx_is_q_reg[1] (is_q),
        .txc_filtered(txc_filtered),
        .\txc_out_reg[1] (\txc_out_reg[1] ),
        .\txc_out_reg[7] (\txc_out_reg[7] ),
        .\txc_out_reg[7]_0 ({state_machine_n_78,state_machine_n_79,state_machine_n_80}),
        .txd_filtered({txd_filtered[39:32],txd_filtered[7:0]}),
        .\txd_out_reg[0] (state_machine_n_13),
        .\txd_out_reg[10] (state_machine_n_19),
        .\txd_out_reg[12] (state_machine_n_20),
        .\txd_out_reg[13] (state_machine_n_56),
        .\txd_out_reg[14] (state_machine_n_5),
        .\txd_out_reg[15] (state_machine_n_48),
        .\txd_out_reg[16] (state_machine_n_21),
        .\txd_out_reg[17] (state_machine_n_22),
        .\txd_out_reg[18] (state_machine_n_23),
        .\txd_out_reg[19] (state_machine_n_57),
        .\txd_out_reg[1] (state_machine_n_14),
        .\txd_out_reg[20] (state_machine_n_24),
        .\txd_out_reg[21] (state_machine_n_58),
        .\txd_out_reg[22] (state_machine_n_6),
        .\txd_out_reg[23] (state_machine_n_49),
        .\txd_out_reg[24] (state_machine_n_25),
        .\txd_out_reg[25] (state_machine_n_26),
        .\txd_out_reg[26] (state_machine_n_27),
        .\txd_out_reg[27] (\txd_out_reg[27] ),
        .\txd_out_reg[28] (state_machine_n_28),
        .\txd_out_reg[29] (state_machine_n_59),
        .\txd_out_reg[2] (state_machine_n_15),
        .\txd_out_reg[2]_0 (\G_FILTER_HIGH[5].filter_n_12 ),
        .\txd_out_reg[2]_1 (\G_FILTER_HIGH[6].filter_n_12 ),
        .\txd_out_reg[2]_2 (\G_FILTER_HIGH[7].filter_n_12 ),
        .\txd_out_reg[30] (state_machine_n_29),
        .\txd_out_reg[31] (\txd_out_reg[31] ),
        .\txd_out_reg[31]_0 (\txd_out_reg[31]_0 ),
        .\txd_out_reg[31]_1 (state_machine_n_60),
        .\txd_out_reg[32] (state_machine_n_31),
        .\txd_out_reg[33] (state_machine_n_32),
        .\txd_out_reg[34] (state_machine_n_62),
        .\txd_out_reg[35] (state_machine_n_63),
        .\txd_out_reg[36] (state_machine_n_64),
        .\txd_out_reg[37] (state_machine_n_65),
        .\txd_out_reg[38] (state_machine_n_39),
        .\txd_out_reg[39] (state_machine_n_12),
        .\txd_out_reg[39]_0 (state_machine_n_66),
        .\txd_out_reg[3] (state_machine_n_50),
        .\txd_out_reg[3]_0 (\G_FILTER_HIGH[5].filter_n_13 ),
        .\txd_out_reg[3]_1 (\G_FILTER_HIGH[6].filter_n_13 ),
        .\txd_out_reg[3]_2 (\G_FILTER_HIGH[7].filter_n_13 ),
        .\txd_out_reg[40] (state_machine_n_33),
        .\txd_out_reg[41] (state_machine_n_34),
        .\txd_out_reg[42] (state_machine_n_67),
        .\txd_out_reg[43] (state_machine_n_68),
        .\txd_out_reg[44] (state_machine_n_69),
        .\txd_out_reg[45] (state_machine_n_70),
        .\txd_out_reg[46] (state_machine_n_10),
        .\txd_out_reg[47] (state_machine_n_71),
        .\txd_out_reg[48] (state_machine_n_35),
        .\txd_out_reg[49] (state_machine_n_36),
        .\txd_out_reg[4] (state_machine_n_16),
        .\txd_out_reg[4]_0 (\G_FILTER_HIGH[5].filter_n_14 ),
        .\txd_out_reg[4]_1 (\G_FILTER_HIGH[6].filter_n_14 ),
        .\txd_out_reg[4]_2 (\G_FILTER_HIGH[7].filter_n_14 ),
        .\txd_out_reg[50] (state_machine_n_72),
        .\txd_out_reg[51] (state_machine_n_73),
        .\txd_out_reg[52] (state_machine_n_74),
        .\txd_out_reg[53] (state_machine_n_75),
        .\txd_out_reg[54] (\txd_out_reg[54] ),
        .\txd_out_reg[54]_0 (\txd_out_reg[54]_0 ),
        .\txd_out_reg[54]_1 (\txd_out_reg[54]_1 ),
        .\txd_out_reg[54]_2 (state_machine_n_40),
        .\txd_out_reg[55] (state_machine_n_76),
        .\txd_out_reg[56] (state_machine_n_37),
        .\txd_out_reg[57] (state_machine_n_38),
        .\txd_out_reg[58] (state_machine_n_77),
        .\txd_out_reg[59] ({state_machine_n_52,state_machine_n_53,state_machine_n_54}),
        .\txd_out_reg[5] (state_machine_n_51),
        .\txd_out_reg[5]_0 (\G_FILTER_HIGH[5].filter_n_15 ),
        .\txd_out_reg[5]_1 (\G_FILTER_HIGH[6].filter_n_15 ),
        .\txd_out_reg[5]_2 (\G_FILTER_HIGH[7].filter_n_15 ),
        .\txd_out_reg[60] (state_machine_n_81),
        .\txd_out_reg[61] (state_machine_n_82),
        .\txd_out_reg[62] (state_machine_n_11),
        .\txd_out_reg[63] (state_machine_n_83),
        .\txd_out_reg[6] (state_machine_n_1),
        .\txd_out_reg[6]_0 ({\G_FILTER_HIGH[5].filter_n_9 ,\G_FILTER_HIGH[5].filter_n_10 ,\G_FILTER_HIGH[5].filter_n_11 }),
        .\txd_out_reg[6]_1 ({\G_FILTER_HIGH[7].filter_n_9 ,\G_FILTER_HIGH[7].filter_n_10 ,\G_FILTER_HIGH[7].filter_n_11 }),
        .\txd_out_reg[6]_2 ({\G_FILTER_HIGH[6].filter_n_9 ,\G_FILTER_HIGH[6].filter_n_10 ,\G_FILTER_HIGH[6].filter_n_11 }),
        .\txd_out_reg[7] (state_machine_n_30),
        .\txd_out_reg[7]_0 (state_machine_n_47),
        .\txd_out_reg[7]_1 (\G_FILTER_HIGH[5].filter_n_16 ),
        .\txd_out_reg[7]_2 (\G_FILTER_HIGH[6].filter_n_16 ),
        .\txd_out_reg[7]_3 (\G_FILTER_HIGH[7].filter_n_16 ),
        .\txd_out_reg[8] (state_machine_n_17),
        .\txd_out_reg[9] (state_machine_n_18),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset));
  rxaui_0_tqmsg_capture tqmsg_capture_1
       (.D(p_1_in),
        .Q(Q),
        .last_qmsg(last_qmsg),
        .q_det(q_det),
        .\state_reg[0][1] (\txd_out_reg[31]_0 ),
        .\state_reg[1][0] (\txd_out_reg[54] ),
        .\state_reg[1][1] (\txd_out_reg[54]_0 ),
        .txc_out_reg(\G_FILTER_LOW[0].filter_n_1 ),
        .txc_out_reg_0(\G_FILTER_HIGH[4].filter_n_1 ),
        .txd_filtered(txd_filtered[7:0]),
        .usrclk(usrclk));
  LUT5 #(
    .INIT(32'h80000000)) 
    \tx_is_idle[0]_i_1 
       (.I0(\txd_is_IDLE_reg[0]_i_1_n_0 ),
        .I1(\txd_is_IDLE_reg[1]_i_1_n_0 ),
        .I2(\txd_is_IDLE_reg[2]_i_1_n_0 ),
        .I3(\txd_is_IDLE_reg[3]_i_1_n_0 ),
        .I4(\tx_is_idle[0]_i_2_n_0 ),
        .O(tx_is_idle_comb[0]));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \tx_is_idle[0]_i_2 
       (.I0(c15_in),
        .I1(\txc_pipe_reg_n_0_[0] ),
        .I2(c9_in),
        .I3(c12_in),
        .O(\tx_is_idle[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \tx_is_idle[1]_i_1 
       (.I0(\txd_is_IDLE_reg[4]_i_1_n_0 ),
        .I1(\txd_is_IDLE_reg[5]_i_1_n_0 ),
        .I2(\txd_is_IDLE_reg[6]_i_1_n_0 ),
        .I3(txd_is_IDLE),
        .I4(\tx_is_idle[1]_i_2_n_0 ),
        .O(tx_is_idle_comb[1]));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \tx_is_idle[1]_i_2 
       (.I0(c[1]),
        .I1(c[0]),
        .I2(c[3]),
        .I3(c[2]),
        .O(\tx_is_idle[1]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \tx_is_idle_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(tx_is_idle_comb[0]),
        .Q(is_idle[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \tx_is_idle_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(tx_is_idle_comb[1]),
        .Q(is_idle[1]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    \tx_is_invalid_k_reg[0]_i_1 
       (.I0(\txc_pipe_reg_n_0_[0] ),
        .I1(\tx_is_invalid_k_reg[0]_i_2_n_0 ),
        .I2(\txd_pipe_reg_n_0_[1] ),
        .I3(\txd_pipe_reg_n_0_[0] ),
        .I4(\tx_is_invalid_k_reg[0]_i_3_n_0 ),
        .O(\tx_is_invalid_k_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \tx_is_invalid_k_reg[0]_i_2 
       (.I0(\txd_pipe_reg_n_0_[6] ),
        .I1(\txd_pipe_reg_n_0_[7] ),
        .I2(\txd_pipe_reg_n_0_[5] ),
        .O(\tx_is_invalid_k_reg[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \tx_is_invalid_k_reg[0]_i_3 
       (.I0(\txd_pipe_reg_n_0_[4] ),
        .I1(\txd_pipe_reg_n_0_[3] ),
        .I2(\txd_pipe_reg_n_0_[2] ),
        .I3(\txd_pipe_reg_n_0_[0] ),
        .I4(\txd_pipe_reg_n_0_[1] ),
        .O(\tx_is_invalid_k_reg[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    \tx_is_invalid_k_reg[1]_i_1 
       (.I0(c15_in),
        .I1(\tx_is_invalid_k_reg[1]_i_2_n_0 ),
        .I2(d[1]),
        .I3(d[0]),
        .I4(\tx_is_invalid_k_reg[1]_i_3_n_0 ),
        .O(\tx_is_invalid_k_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \tx_is_invalid_k_reg[1]_i_2 
       (.I0(d[6]),
        .I1(d[7]),
        .I2(d[5]),
        .O(\tx_is_invalid_k_reg[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \tx_is_invalid_k_reg[1]_i_3 
       (.I0(d[4]),
        .I1(d[3]),
        .I2(d[2]),
        .I3(d[0]),
        .I4(d[1]),
        .O(\tx_is_invalid_k_reg[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    \tx_is_invalid_k_reg[2]_i_1 
       (.I0(c12_in),
        .I1(\tx_is_invalid_k_reg[2]_i_2_n_0 ),
        .I2(\txd_pipe_reg_n_0_[17] ),
        .I3(\txd_pipe_reg_n_0_[16] ),
        .I4(\tx_is_invalid_k_reg[2]_i_3_n_0 ),
        .O(\tx_is_invalid_k_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \tx_is_invalid_k_reg[2]_i_2 
       (.I0(\txd_pipe_reg_n_0_[22] ),
        .I1(\txd_pipe_reg_n_0_[23] ),
        .I2(\txd_pipe_reg_n_0_[21] ),
        .O(\tx_is_invalid_k_reg[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \tx_is_invalid_k_reg[2]_i_3 
       (.I0(\txd_pipe_reg_n_0_[20] ),
        .I1(\txd_pipe_reg_n_0_[19] ),
        .I2(\txd_pipe_reg_n_0_[18] ),
        .I3(\txd_pipe_reg_n_0_[16] ),
        .I4(\txd_pipe_reg_n_0_[17] ),
        .O(\tx_is_invalid_k_reg[2]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    \tx_is_invalid_k_reg[3]_i_1 
       (.I0(c9_in),
        .I1(\tx_is_invalid_k_reg[3]_i_2_n_0 ),
        .I2(\txd_pipe_reg_n_0_[25] ),
        .I3(\txd_pipe_reg_n_0_[24] ),
        .I4(\tx_is_invalid_k_reg[3]_i_3_n_0 ),
        .O(\tx_is_invalid_k_reg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \tx_is_invalid_k_reg[3]_i_2 
       (.I0(\txd_pipe_reg_n_0_[30] ),
        .I1(\txd_pipe_reg_n_0_[31] ),
        .I2(\txd_pipe_reg_n_0_[29] ),
        .O(\tx_is_invalid_k_reg[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \tx_is_invalid_k_reg[3]_i_3 
       (.I0(\txd_pipe_reg_n_0_[28] ),
        .I1(\txd_pipe_reg_n_0_[27] ),
        .I2(\txd_pipe_reg_n_0_[26] ),
        .I3(\txd_pipe_reg_n_0_[24] ),
        .I4(\txd_pipe_reg_n_0_[25] ),
        .O(\tx_is_invalid_k_reg[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    \tx_is_invalid_k_reg[4]_i_1 
       (.I0(c[0]),
        .I1(\tx_is_invalid_k_reg[4]_i_2_n_0 ),
        .I2(\txd_pipe_reg_n_0_[33] ),
        .I3(\txd_pipe_reg_n_0_[32] ),
        .I4(\tx_is_invalid_k_reg[4]_i_3_n_0 ),
        .O(\tx_is_invalid_k_reg[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \tx_is_invalid_k_reg[4]_i_2 
       (.I0(\txd_pipe_reg_n_0_[38] ),
        .I1(\txd_pipe_reg_n_0_[39] ),
        .I2(\txd_pipe_reg_n_0_[37] ),
        .O(\tx_is_invalid_k_reg[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \tx_is_invalid_k_reg[4]_i_3 
       (.I0(\txd_pipe_reg_n_0_[36] ),
        .I1(\txd_pipe_reg_n_0_[35] ),
        .I2(\txd_pipe_reg_n_0_[34] ),
        .I3(\txd_pipe_reg_n_0_[32] ),
        .I4(\txd_pipe_reg_n_0_[33] ),
        .O(\tx_is_invalid_k_reg[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    \tx_is_invalid_k_reg[5]_i_1 
       (.I0(c[1]),
        .I1(\tx_is_invalid_k_reg[5]_i_2_n_0 ),
        .I2(\txd_pipe_reg_n_0_[41] ),
        .I3(\txd_pipe_reg_n_0_[40] ),
        .I4(\tx_is_invalid_k_reg[5]_i_3_n_0 ),
        .O(\tx_is_invalid_k_reg[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \tx_is_invalid_k_reg[5]_i_2 
       (.I0(\txd_pipe_reg_n_0_[46] ),
        .I1(\txd_pipe_reg_n_0_[47] ),
        .I2(\txd_pipe_reg_n_0_[45] ),
        .O(\tx_is_invalid_k_reg[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \tx_is_invalid_k_reg[5]_i_3 
       (.I0(\txd_pipe_reg_n_0_[44] ),
        .I1(\txd_pipe_reg_n_0_[43] ),
        .I2(\txd_pipe_reg_n_0_[42] ),
        .I3(\txd_pipe_reg_n_0_[40] ),
        .I4(\txd_pipe_reg_n_0_[41] ),
        .O(\tx_is_invalid_k_reg[5]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    \tx_is_invalid_k_reg[6]_i_1 
       (.I0(c[2]),
        .I1(\tx_is_invalid_k_reg[6]_i_2_n_0 ),
        .I2(\txd_pipe_reg_n_0_[49] ),
        .I3(\txd_pipe_reg_n_0_[48] ),
        .I4(\tx_is_invalid_k_reg[6]_i_3_n_0 ),
        .O(\tx_is_invalid_k_reg[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \tx_is_invalid_k_reg[6]_i_2 
       (.I0(\txd_pipe_reg_n_0_[54] ),
        .I1(\txd_pipe_reg_n_0_[55] ),
        .I2(\txd_pipe_reg_n_0_[53] ),
        .O(\tx_is_invalid_k_reg[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \tx_is_invalid_k_reg[6]_i_3 
       (.I0(\txd_pipe_reg_n_0_[52] ),
        .I1(\txd_pipe_reg_n_0_[51] ),
        .I2(\txd_pipe_reg_n_0_[50] ),
        .I3(\txd_pipe_reg_n_0_[48] ),
        .I4(\txd_pipe_reg_n_0_[49] ),
        .O(\tx_is_invalid_k_reg[6]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    \tx_is_invalid_k_reg[7]_i_1 
       (.I0(c[3]),
        .I1(\tx_is_invalid_k_reg[7]_i_2_n_0 ),
        .I2(\txd_pipe_reg_n_0_[57] ),
        .I3(\txd_pipe_reg_n_0_[56] ),
        .I4(\tx_is_invalid_k_reg[7]_i_3_n_0 ),
        .O(\tx_is_invalid_k_reg[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \tx_is_invalid_k_reg[7]_i_2 
       (.I0(\txd_pipe_reg_n_0_[62] ),
        .I1(\txd_pipe_reg_n_0_[63] ),
        .I2(\txd_pipe_reg_n_0_[61] ),
        .O(\tx_is_invalid_k_reg[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT5 #(
    .INIT(32'hD77F7F7F)) 
    \tx_is_invalid_k_reg[7]_i_3 
       (.I0(\txd_pipe_reg_n_0_[60] ),
        .I1(\txd_pipe_reg_n_0_[59] ),
        .I2(\txd_pipe_reg_n_0_[58] ),
        .I3(\txd_pipe_reg_n_0_[56] ),
        .I4(\txd_pipe_reg_n_0_[57] ),
        .O(\tx_is_invalid_k_reg[7]_i_3_n_0 ));
  FDRE \tx_is_invalid_k_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\tx_is_invalid_k_reg[0]_i_1_n_0 ),
        .Q(is_invalid_k[0]),
        .R(1'b0));
  FDRE \tx_is_invalid_k_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\tx_is_invalid_k_reg[1]_i_1_n_0 ),
        .Q(is_invalid_k[1]),
        .R(1'b0));
  FDRE \tx_is_invalid_k_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\tx_is_invalid_k_reg[2]_i_1_n_0 ),
        .Q(is_invalid_k[2]),
        .R(1'b0));
  FDRE \tx_is_invalid_k_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\tx_is_invalid_k_reg[3]_i_1_n_0 ),
        .Q(is_invalid_k[3]),
        .R(1'b0));
  FDRE \tx_is_invalid_k_reg_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\tx_is_invalid_k_reg[4]_i_1_n_0 ),
        .Q(is_invalid_k[4]),
        .R(1'b0));
  FDRE \tx_is_invalid_k_reg_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\tx_is_invalid_k_reg[5]_i_1_n_0 ),
        .Q(is_invalid_k[5]),
        .R(1'b0));
  FDRE \tx_is_invalid_k_reg_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\tx_is_invalid_k_reg[6]_i_1_n_0 ),
        .Q(is_invalid_k[6]),
        .R(1'b0));
  FDRE \tx_is_invalid_k_reg_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\tx_is_invalid_k_reg[7]_i_1_n_0 ),
        .Q(is_invalid_k[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \tx_is_q[0]_i_1 
       (.I0(\tx_is_q[0]_i_2_n_0 ),
        .I1(\txd_pipe_reg_n_0_[6] ),
        .I2(\txd_pipe_reg_n_0_[7] ),
        .I3(\txd_pipe_reg_n_0_[5] ),
        .I4(\txd_pipe_reg_n_0_[4] ),
        .I5(\tx_is_q[0]_i_3_n_0 ),
        .O(tx_is_q_comb[0]));
  LUT4 #(
    .INIT(16'h0008)) 
    \tx_is_q[0]_i_2 
       (.I0(\txd_pipe_reg_n_0_[3] ),
        .I1(\txd_pipe_reg_n_0_[2] ),
        .I2(\txd_pipe_reg_n_0_[1] ),
        .I3(\txd_pipe_reg_n_0_[0] ),
        .O(\tx_is_q[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \tx_is_q[0]_i_3 
       (.I0(c15_in),
        .I1(\txc_pipe_reg_n_0_[0] ),
        .I2(c9_in),
        .I3(c12_in),
        .O(\tx_is_q[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \tx_is_q[1]_i_1 
       (.I0(\tx_is_q[1]_i_2_n_0 ),
        .I1(\txd_pipe_reg_n_0_[38] ),
        .I2(\txd_pipe_reg_n_0_[39] ),
        .I3(\txd_pipe_reg_n_0_[37] ),
        .I4(\txd_pipe_reg_n_0_[36] ),
        .I5(\tx_is_q[1]_i_3_n_0 ),
        .O(tx_is_q_comb[1]));
  LUT4 #(
    .INIT(16'h0008)) 
    \tx_is_q[1]_i_2 
       (.I0(\txd_pipe_reg_n_0_[35] ),
        .I1(\txd_pipe_reg_n_0_[34] ),
        .I2(\txd_pipe_reg_n_0_[33] ),
        .I3(\txd_pipe_reg_n_0_[32] ),
        .O(\tx_is_q[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \tx_is_q[1]_i_3 
       (.I0(c[1]),
        .I1(c[0]),
        .I2(c[3]),
        .I3(c[2]),
        .O(\tx_is_q[1]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tx_is_q_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(tx_is_q_comb[0]),
        .Q(is_q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tx_is_q_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(tx_is_q_comb[1]),
        .Q(is_q[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_2_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txc_pipe_reg_n_0_[0] ),
        .Q(txc_pipe_2[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_2_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c15_in),
        .Q(txc_pipe_2[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_2_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c12_in),
        .Q(txc_pipe_2[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_2_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c9_in),
        .Q(txc_pipe_2[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_2_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c[0]),
        .Q(txc_pipe_2[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_2_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c[1]),
        .Q(txc_pipe_2[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_2_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c[2]),
        .Q(txc_pipe_2[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_2_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(c[3]),
        .Q(txc_pipe_2[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txc[0]),
        .Q(\txc_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txc[1]),
        .Q(c15_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txc[2]),
        .Q(c12_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txc[3]),
        .Q(c9_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txc[4]),
        .Q(c[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txc[5]),
        .Q(c[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txc[6]),
        .Q(c[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txc_pipe_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txc[7]),
        .Q(c[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \txd_is_IDLE_reg[0]_i_1 
       (.I0(\txd_pipe_reg_n_0_[4] ),
        .I1(\txd_pipe_reg_n_0_[5] ),
        .I2(\txd_pipe_reg_n_0_[6] ),
        .I3(\txd_pipe_reg_n_0_[7] ),
        .I4(\txd_is_IDLE_reg[0]_i_2_n_0 ),
        .O(\txd_is_IDLE_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \txd_is_IDLE_reg[0]_i_2 
       (.I0(\txd_pipe_reg_n_0_[1] ),
        .I1(\txd_pipe_reg_n_0_[0] ),
        .I2(\txd_pipe_reg_n_0_[3] ),
        .I3(\txd_pipe_reg_n_0_[2] ),
        .O(\txd_is_IDLE_reg[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \txd_is_IDLE_reg[1]_i_1 
       (.I0(d[4]),
        .I1(d[5]),
        .I2(d[6]),
        .I3(d[7]),
        .I4(\txd_is_IDLE_reg[1]_i_2_n_0 ),
        .O(\txd_is_IDLE_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \txd_is_IDLE_reg[1]_i_2 
       (.I0(d[1]),
        .I1(d[0]),
        .I2(d[3]),
        .I3(d[2]),
        .O(\txd_is_IDLE_reg[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \txd_is_IDLE_reg[2]_i_1 
       (.I0(\txd_pipe_reg_n_0_[20] ),
        .I1(\txd_pipe_reg_n_0_[21] ),
        .I2(\txd_pipe_reg_n_0_[22] ),
        .I3(\txd_pipe_reg_n_0_[23] ),
        .I4(\txd_is_IDLE_reg[2]_i_2_n_0 ),
        .O(\txd_is_IDLE_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \txd_is_IDLE_reg[2]_i_2 
       (.I0(\txd_pipe_reg_n_0_[17] ),
        .I1(\txd_pipe_reg_n_0_[16] ),
        .I2(\txd_pipe_reg_n_0_[19] ),
        .I3(\txd_pipe_reg_n_0_[18] ),
        .O(\txd_is_IDLE_reg[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \txd_is_IDLE_reg[3]_i_1 
       (.I0(\txd_pipe_reg_n_0_[28] ),
        .I1(\txd_pipe_reg_n_0_[29] ),
        .I2(\txd_pipe_reg_n_0_[30] ),
        .I3(\txd_pipe_reg_n_0_[31] ),
        .I4(\txd_is_IDLE_reg[3]_i_2_n_0 ),
        .O(\txd_is_IDLE_reg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \txd_is_IDLE_reg[3]_i_2 
       (.I0(\txd_pipe_reg_n_0_[25] ),
        .I1(\txd_pipe_reg_n_0_[24] ),
        .I2(\txd_pipe_reg_n_0_[27] ),
        .I3(\txd_pipe_reg_n_0_[26] ),
        .O(\txd_is_IDLE_reg[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \txd_is_IDLE_reg[4]_i_1 
       (.I0(\txd_pipe_reg_n_0_[36] ),
        .I1(\txd_pipe_reg_n_0_[37] ),
        .I2(\txd_pipe_reg_n_0_[38] ),
        .I3(\txd_pipe_reg_n_0_[39] ),
        .I4(\txd_is_IDLE_reg[4]_i_2_n_0 ),
        .O(\txd_is_IDLE_reg[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \txd_is_IDLE_reg[4]_i_2 
       (.I0(\txd_pipe_reg_n_0_[33] ),
        .I1(\txd_pipe_reg_n_0_[32] ),
        .I2(\txd_pipe_reg_n_0_[35] ),
        .I3(\txd_pipe_reg_n_0_[34] ),
        .O(\txd_is_IDLE_reg[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \txd_is_IDLE_reg[5]_i_1 
       (.I0(\txd_pipe_reg_n_0_[44] ),
        .I1(\txd_pipe_reg_n_0_[45] ),
        .I2(\txd_pipe_reg_n_0_[46] ),
        .I3(\txd_pipe_reg_n_0_[47] ),
        .I4(\txd_is_IDLE_reg[5]_i_2_n_0 ),
        .O(\txd_is_IDLE_reg[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \txd_is_IDLE_reg[5]_i_2 
       (.I0(\txd_pipe_reg_n_0_[41] ),
        .I1(\txd_pipe_reg_n_0_[40] ),
        .I2(\txd_pipe_reg_n_0_[43] ),
        .I3(\txd_pipe_reg_n_0_[42] ),
        .O(\txd_is_IDLE_reg[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \txd_is_IDLE_reg[6]_i_1 
       (.I0(\txd_pipe_reg_n_0_[52] ),
        .I1(\txd_pipe_reg_n_0_[53] ),
        .I2(\txd_pipe_reg_n_0_[54] ),
        .I3(\txd_pipe_reg_n_0_[55] ),
        .I4(\txd_is_IDLE_reg[6]_i_2_n_0 ),
        .O(\txd_is_IDLE_reg[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \txd_is_IDLE_reg[6]_i_2 
       (.I0(\txd_pipe_reg_n_0_[49] ),
        .I1(\txd_pipe_reg_n_0_[48] ),
        .I2(\txd_pipe_reg_n_0_[51] ),
        .I3(\txd_pipe_reg_n_0_[50] ),
        .O(\txd_is_IDLE_reg[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \txd_is_IDLE_reg[7]_i_1 
       (.I0(\txd_pipe_reg_n_0_[60] ),
        .I1(\txd_pipe_reg_n_0_[61] ),
        .I2(\txd_pipe_reg_n_0_[62] ),
        .I3(\txd_pipe_reg_n_0_[63] ),
        .I4(\txd_is_IDLE_reg[7]_i_2_n_0 ),
        .O(txd_is_IDLE));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \txd_is_IDLE_reg[7]_i_2 
       (.I0(\txd_pipe_reg_n_0_[57] ),
        .I1(\txd_pipe_reg_n_0_[56] ),
        .I2(\txd_pipe_reg_n_0_[59] ),
        .I3(\txd_pipe_reg_n_0_[58] ),
        .O(\txd_is_IDLE_reg[7]_i_2_n_0 ));
  FDRE \txd_is_IDLE_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_is_IDLE_reg[0]_i_1_n_0 ),
        .Q(\txd_is_IDLE_reg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \txd_is_IDLE_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_is_IDLE_reg[1]_i_1_n_0 ),
        .Q(is_txd_IDLE),
        .R(1'b0));
  FDRE \txd_is_IDLE_reg_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_is_IDLE_reg[2]_i_1_n_0 ),
        .Q(\txd_is_IDLE_reg_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \txd_is_IDLE_reg_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_is_IDLE_reg[3]_i_1_n_0 ),
        .Q(\txd_is_IDLE_reg_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \txd_is_IDLE_reg_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_is_IDLE_reg[4]_i_1_n_0 ),
        .Q(\txd_is_IDLE_reg_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \txd_is_IDLE_reg_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_is_IDLE_reg[5]_i_1_n_0 ),
        .Q(\txd_is_IDLE_reg_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \txd_is_IDLE_reg_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_is_IDLE_reg[6]_i_1_n_0 ),
        .Q(\txd_is_IDLE_reg_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \txd_is_IDLE_reg_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(txd_is_IDLE),
        .Q(\txd_is_IDLE_reg_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[0] ),
        .Q(txd_pipe_2[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[10] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[2]),
        .Q(txd_pipe_2[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[11] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[3]),
        .Q(txd_pipe_2[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[12] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[4]),
        .Q(txd_pipe_2[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[13] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[5]),
        .Q(txd_pipe_2[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[14] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[6]),
        .Q(txd_pipe_2[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[15] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[7]),
        .Q(txd_pipe_2[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[16] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[16] ),
        .Q(txd_pipe_2[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[17] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[17] ),
        .Q(txd_pipe_2[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[18] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[18] ),
        .Q(txd_pipe_2[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[19] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[19] ),
        .Q(txd_pipe_2[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[1] ),
        .Q(txd_pipe_2[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[20] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[20] ),
        .Q(txd_pipe_2[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[21] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[21] ),
        .Q(txd_pipe_2[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[22] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[22] ),
        .Q(txd_pipe_2[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[23] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[23] ),
        .Q(txd_pipe_2[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[24] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[24] ),
        .Q(txd_pipe_2[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[25] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[25] ),
        .Q(txd_pipe_2[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[26] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[26] ),
        .Q(txd_pipe_2[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[27] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[27] ),
        .Q(txd_pipe_2[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[28] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[28] ),
        .Q(txd_pipe_2[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[29] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[29] ),
        .Q(txd_pipe_2[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[2] ),
        .Q(txd_pipe_2[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[30] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[30] ),
        .Q(txd_pipe_2[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[31] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[31] ),
        .Q(txd_pipe_2[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[32] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[32] ),
        .Q(txd_pipe_2[32]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[33] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[33] ),
        .Q(txd_pipe_2[33]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[34] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[34] ),
        .Q(txd_pipe_2[34]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[35] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[35] ),
        .Q(txd_pipe_2[35]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[36] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[36] ),
        .Q(txd_pipe_2[36]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[37] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[37] ),
        .Q(txd_pipe_2[37]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[38] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[38] ),
        .Q(txd_pipe_2[38]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[39] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[39] ),
        .Q(txd_pipe_2[39]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[3] ),
        .Q(txd_pipe_2[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[40] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[40] ),
        .Q(txd_pipe_2[40]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[41] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[41] ),
        .Q(txd_pipe_2[41]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[42] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[42] ),
        .Q(txd_pipe_2[42]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[43] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[43] ),
        .Q(txd_pipe_2[43]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[44] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[44] ),
        .Q(txd_pipe_2[44]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[45] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[45] ),
        .Q(txd_pipe_2[45]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[46] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[46] ),
        .Q(txd_pipe_2[46]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[47] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[47] ),
        .Q(txd_pipe_2[47]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[48] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[48] ),
        .Q(txd_pipe_2[48]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[49] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[49] ),
        .Q(txd_pipe_2[49]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[4] ),
        .Q(txd_pipe_2[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[50] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[50] ),
        .Q(txd_pipe_2[50]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[51] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[51] ),
        .Q(txd_pipe_2[51]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[52] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[52] ),
        .Q(txd_pipe_2[52]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[53] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[53] ),
        .Q(txd_pipe_2[53]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[54] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[54] ),
        .Q(txd_pipe_2[54]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[55] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[55] ),
        .Q(txd_pipe_2[55]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[56] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[56] ),
        .Q(txd_pipe_2[56]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[57] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[57] ),
        .Q(txd_pipe_2[57]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[58] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[58] ),
        .Q(txd_pipe_2[58]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[59] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[59] ),
        .Q(txd_pipe_2[59]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[5] ),
        .Q(txd_pipe_2[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[60] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[60] ),
        .Q(txd_pipe_2[60]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[61] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[61] ),
        .Q(txd_pipe_2[61]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[62] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[62] ),
        .Q(txd_pipe_2[62]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[63] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[63] ),
        .Q(txd_pipe_2[63]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[6] ),
        .Q(txd_pipe_2[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_2_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_reg_n_0_[7] ),
        .Q(txd_pipe_2[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[8] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[0]),
        .Q(txd_pipe_2[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_2_reg[9] 
       (.C(usrclk),
        .CE(1'b1),
        .D(d[1]),
        .Q(txd_pipe_2[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[0]),
        .Q(\txd_pipe_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[10] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[10]),
        .Q(d[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[11] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[11]),
        .Q(d[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[12] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[12]),
        .Q(d[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[13] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[13]),
        .Q(d[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[14] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[14]),
        .Q(d[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[15] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[15]),
        .Q(d[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[16] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[16]),
        .Q(\txd_pipe_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[17] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[17]),
        .Q(\txd_pipe_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[18] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[18]),
        .Q(\txd_pipe_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[19] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[19]),
        .Q(\txd_pipe_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[1]),
        .Q(\txd_pipe_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[20] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[20]),
        .Q(\txd_pipe_reg_n_0_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[21] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[21]),
        .Q(\txd_pipe_reg_n_0_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[22] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[22]),
        .Q(\txd_pipe_reg_n_0_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[23] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[23]),
        .Q(\txd_pipe_reg_n_0_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[24] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[24]),
        .Q(\txd_pipe_reg_n_0_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[25] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[25]),
        .Q(\txd_pipe_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[26] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[26]),
        .Q(\txd_pipe_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[27] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[27]),
        .Q(\txd_pipe_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[28] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[28]),
        .Q(\txd_pipe_reg_n_0_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[29] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[29]),
        .Q(\txd_pipe_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[2]),
        .Q(\txd_pipe_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[30] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[30]),
        .Q(\txd_pipe_reg_n_0_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[31] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[31]),
        .Q(\txd_pipe_reg_n_0_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[32] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[32]),
        .Q(\txd_pipe_reg_n_0_[32] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[33] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[33]),
        .Q(\txd_pipe_reg_n_0_[33] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[34] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[34]),
        .Q(\txd_pipe_reg_n_0_[34] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[35] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[35]),
        .Q(\txd_pipe_reg_n_0_[35] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[36] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[36]),
        .Q(\txd_pipe_reg_n_0_[36] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[37] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[37]),
        .Q(\txd_pipe_reg_n_0_[37] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[38] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[38]),
        .Q(\txd_pipe_reg_n_0_[38] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[39] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[39]),
        .Q(\txd_pipe_reg_n_0_[39] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[3]),
        .Q(\txd_pipe_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[40] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[40]),
        .Q(\txd_pipe_reg_n_0_[40] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[41] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[41]),
        .Q(\txd_pipe_reg_n_0_[41] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[42] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[42]),
        .Q(\txd_pipe_reg_n_0_[42] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[43] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[43]),
        .Q(\txd_pipe_reg_n_0_[43] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[44] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[44]),
        .Q(\txd_pipe_reg_n_0_[44] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[45] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[45]),
        .Q(\txd_pipe_reg_n_0_[45] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[46] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[46]),
        .Q(\txd_pipe_reg_n_0_[46] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[47] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[47]),
        .Q(\txd_pipe_reg_n_0_[47] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[48] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[48]),
        .Q(\txd_pipe_reg_n_0_[48] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[49] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[49]),
        .Q(\txd_pipe_reg_n_0_[49] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[4]),
        .Q(\txd_pipe_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[50] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[50]),
        .Q(\txd_pipe_reg_n_0_[50] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[51] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[51]),
        .Q(\txd_pipe_reg_n_0_[51] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[52] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[52]),
        .Q(\txd_pipe_reg_n_0_[52] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[53] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[53]),
        .Q(\txd_pipe_reg_n_0_[53] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[54] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[54]),
        .Q(\txd_pipe_reg_n_0_[54] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[55] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[55]),
        .Q(\txd_pipe_reg_n_0_[55] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[56] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[56]),
        .Q(\txd_pipe_reg_n_0_[56] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[57] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[57]),
        .Q(\txd_pipe_reg_n_0_[57] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[58] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[58]),
        .Q(\txd_pipe_reg_n_0_[58] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[59] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[59]),
        .Q(\txd_pipe_reg_n_0_[59] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[5]),
        .Q(\txd_pipe_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[60] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[60]),
        .Q(\txd_pipe_reg_n_0_[60] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[61] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[61]),
        .Q(\txd_pipe_reg_n_0_[61] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[62] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[62]),
        .Q(\txd_pipe_reg_n_0_[62] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[63] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[63]),
        .Q(\txd_pipe_reg_n_0_[63] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[6]),
        .Q(\txd_pipe_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_pipe_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[7]),
        .Q(\txd_pipe_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[8] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[8]),
        .Q(d[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \txd_pipe_reg[9] 
       (.C(usrclk),
        .CE(1'b1),
        .D(xgmii_txd[9]),
        .Q(d[1]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "tx_filter" *) 
module rxaui_0_tx_filter
   (\txc_out_reg[4] ,
    \last_qmsg_reg[6] ,
    txd_filtered,
    Q,
    usrclk,
    is_terminate,
    \txd_is_IDLE_reg_reg[4] ,
    is_invalid_k,
    txc_filtered,
    \txd_pipe_2_reg[39] );
  output [0:0]\txc_out_reg[4] ;
  output \last_qmsg_reg[6] ;
  output [7:0]txd_filtered;
  input [0:0]Q;
  input usrclk;
  input is_terminate;
  input [0:0]\txd_is_IDLE_reg_reg[4] ;
  input [0:0]is_invalid_k;
  input [2:0]txc_filtered;
  input [7:0]\txd_pipe_2_reg[39] ;

  wire [0:0]Q;
  wire [0:0]is_invalid_k;
  wire is_terminate;
  wire \last_qmsg[31]_i_6_n_0 ;
  wire \last_qmsg[31]_i_7_n_0 ;
  wire \last_qmsg_reg[6] ;
  wire [2:0]txc_filtered;
  wire [0:0]\txc_out_reg[4] ;
  wire [7:0]txd_filtered;
  wire [0:0]\txd_is_IDLE_reg_reg[4] ;
  wire \txd_out[0]_i_1_n_0 ;
  wire \txd_out[1]_i_1_n_0 ;
  wire \txd_out[6]_i_1_n_0 ;
  wire \txd_out[7]_i_1__4_n_0 ;
  wire [7:0]\txd_pipe_2_reg[39] ;
  wire usrclk;

  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \last_qmsg[31]_i_4 
       (.I0(txc_filtered[1]),
        .I1(txd_filtered[6]),
        .I2(txd_filtered[2]),
        .I3(txd_filtered[4]),
        .I4(\last_qmsg[31]_i_6_n_0 ),
        .I5(\last_qmsg[31]_i_7_n_0 ),
        .O(\last_qmsg_reg[6] ));
  LUT4 #(
    .INIT(16'hFFDF)) 
    \last_qmsg[31]_i_6 
       (.I0(txd_filtered[3]),
        .I1(txc_filtered[2]),
        .I2(txd_filtered[7]),
        .I3(txc_filtered[0]),
        .O(\last_qmsg[31]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \last_qmsg[31]_i_7 
       (.I0(\txc_out_reg[4] ),
        .I1(txd_filtered[5]),
        .I2(txd_filtered[0]),
        .I3(txd_filtered[1]),
        .O(\last_qmsg[31]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    txc_out_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(\txc_out_reg[4] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0222AAAA)) 
    \txd_out[0]_i_1 
       (.I0(\txd_pipe_2_reg[39] [0]),
        .I1(is_invalid_k),
        .I2(\txd_is_IDLE_reg_reg[4] ),
        .I3(is_terminate),
        .I4(Q),
        .O(\txd_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[1]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[39] [1]),
        .I3(is_terminate),
        .I4(\txd_is_IDLE_reg_reg[4] ),
        .O(\txd_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[6]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[39] [6]),
        .I3(is_terminate),
        .I4(\txd_is_IDLE_reg_reg[4] ),
        .O(\txd_out[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAA80)) 
    \txd_out[7]_i_1__4 
       (.I0(Q),
        .I1(is_terminate),
        .I2(\txd_is_IDLE_reg_reg[4] ),
        .I3(is_invalid_k),
        .O(\txd_out[7]_i_1__4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[0]_i_1_n_0 ),
        .Q(txd_filtered[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[1]_i_1_n_0 ),
        .Q(txd_filtered[1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[39] [2]),
        .Q(txd_filtered[2]),
        .S(\txd_out[7]_i_1__4_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[39] [3]),
        .Q(txd_filtered[3]),
        .S(\txd_out[7]_i_1__4_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[39] [4]),
        .Q(txd_filtered[4]),
        .S(\txd_out[7]_i_1__4_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[39] [5]),
        .Q(txd_filtered[5]),
        .S(\txd_out[7]_i_1__4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[6]_i_1_n_0 ),
        .Q(txd_filtered[6]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[39] [7]),
        .Q(txd_filtered[7]),
        .S(\txd_out[7]_i_1__4_n_0 ));
endmodule

(* ORIG_REF_NAME = "tx_filter" *) 
module rxaui_0_tx_filter_12
   (txc_filtered,
    D,
    \last_qmsg_reg[14] ,
    \last_qmsg_reg[10] ,
    \last_qmsg_reg[11] ,
    \last_qmsg_reg[12] ,
    \last_qmsg_reg[13] ,
    \last_qmsg_reg[15] ,
    Q,
    usrclk,
    is_terminate,
    \txd_is_IDLE_reg_reg[5] ,
    is_invalid_k,
    txc_out_reg_0,
    data1,
    \txd_pipe_2_reg[47] );
  output [0:0]txc_filtered;
  output [7:0]D;
  output [2:0]\last_qmsg_reg[14] ;
  output \last_qmsg_reg[10] ;
  output \last_qmsg_reg[11] ;
  output \last_qmsg_reg[12] ;
  output \last_qmsg_reg[13] ;
  output \last_qmsg_reg[15] ;
  input [0:0]Q;
  input usrclk;
  input is_terminate;
  input [0:0]\txd_is_IDLE_reg_reg[5] ;
  input [0:0]is_invalid_k;
  input txc_out_reg_0;
  input [7:0]data1;
  input [7:0]\txd_pipe_2_reg[47] ;

  wire [7:0]D;
  wire [0:0]Q;
  wire [7:0]data1;
  wire [0:0]is_invalid_k;
  wire is_terminate;
  wire \last_qmsg_reg[10] ;
  wire \last_qmsg_reg[11] ;
  wire \last_qmsg_reg[12] ;
  wire \last_qmsg_reg[13] ;
  wire [2:0]\last_qmsg_reg[14] ;
  wire \last_qmsg_reg[15] ;
  wire [0:0]txc_filtered;
  wire txc_out_reg_0;
  wire [0:0]\txd_is_IDLE_reg_reg[5] ;
  wire \txd_out[0]_i_1_n_0 ;
  wire \txd_out[1]_i_1_n_0 ;
  wire \txd_out[6]_i_1_n_0 ;
  wire \txd_out[7]_i_1__5_n_0 ;
  wire [7:0]\txd_pipe_2_reg[47] ;
  wire usrclk;

  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[10]_i_1 
       (.I0(\last_qmsg_reg[10] ),
        .I1(txc_out_reg_0),
        .I2(data1[2]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[11]_i_1 
       (.I0(\last_qmsg_reg[11] ),
        .I1(txc_out_reg_0),
        .I2(data1[3]),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[12]_i_1 
       (.I0(\last_qmsg_reg[12] ),
        .I1(txc_out_reg_0),
        .I2(data1[4]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[13]_i_1 
       (.I0(\last_qmsg_reg[13] ),
        .I1(txc_out_reg_0),
        .I2(data1[5]),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[14]_i_1 
       (.I0(\last_qmsg_reg[14] [2]),
        .I1(txc_out_reg_0),
        .I2(data1[6]),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[15]_i_1 
       (.I0(\last_qmsg_reg[15] ),
        .I1(txc_out_reg_0),
        .I2(data1[7]),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[8]_i_1 
       (.I0(\last_qmsg_reg[14] [0]),
        .I1(txc_out_reg_0),
        .I2(data1[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[9]_i_1 
       (.I0(\last_qmsg_reg[14] [1]),
        .I1(txc_out_reg_0),
        .I2(data1[1]),
        .O(D[1]));
  FDRE #(
    .INIT(1'b1)) 
    txc_out_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(txc_filtered),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0222AAAA)) 
    \txd_out[0]_i_1 
       (.I0(\txd_pipe_2_reg[47] [0]),
        .I1(is_invalid_k),
        .I2(\txd_is_IDLE_reg_reg[5] ),
        .I3(is_terminate),
        .I4(Q),
        .O(\txd_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[1]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[47] [1]),
        .I3(is_terminate),
        .I4(\txd_is_IDLE_reg_reg[5] ),
        .O(\txd_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[6]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[47] [6]),
        .I3(is_terminate),
        .I4(\txd_is_IDLE_reg_reg[5] ),
        .O(\txd_out[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAA80)) 
    \txd_out[7]_i_1__5 
       (.I0(Q),
        .I1(is_terminate),
        .I2(\txd_is_IDLE_reg_reg[5] ),
        .I3(is_invalid_k),
        .O(\txd_out[7]_i_1__5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[0]_i_1_n_0 ),
        .Q(\last_qmsg_reg[14] [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[1]_i_1_n_0 ),
        .Q(\last_qmsg_reg[14] [1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[47] [2]),
        .Q(\last_qmsg_reg[10] ),
        .S(\txd_out[7]_i_1__5_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[47] [3]),
        .Q(\last_qmsg_reg[11] ),
        .S(\txd_out[7]_i_1__5_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[47] [4]),
        .Q(\last_qmsg_reg[12] ),
        .S(\txd_out[7]_i_1__5_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[47] [5]),
        .Q(\last_qmsg_reg[13] ),
        .S(\txd_out[7]_i_1__5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[6]_i_1_n_0 ),
        .Q(\last_qmsg_reg[14] [2]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[47] [7]),
        .Q(\last_qmsg_reg[15] ),
        .S(\txd_out[7]_i_1__5_n_0 ));
endmodule

(* ORIG_REF_NAME = "tx_filter" *) 
module rxaui_0_tx_filter_13
   (txc_filtered,
    D,
    \last_qmsg_reg[22] ,
    \last_qmsg_reg[18] ,
    \last_qmsg_reg[19] ,
    \last_qmsg_reg[20] ,
    \last_qmsg_reg[21] ,
    \last_qmsg_reg[23] ,
    Q,
    usrclk,
    is_terminate,
    \txd_is_IDLE_reg_reg[6] ,
    is_invalid_k,
    txc_out_reg_0,
    data1,
    \txd_pipe_2_reg[55] );
  output [0:0]txc_filtered;
  output [7:0]D;
  output [2:0]\last_qmsg_reg[22] ;
  output \last_qmsg_reg[18] ;
  output \last_qmsg_reg[19] ;
  output \last_qmsg_reg[20] ;
  output \last_qmsg_reg[21] ;
  output \last_qmsg_reg[23] ;
  input [0:0]Q;
  input usrclk;
  input is_terminate;
  input [0:0]\txd_is_IDLE_reg_reg[6] ;
  input [0:0]is_invalid_k;
  input txc_out_reg_0;
  input [7:0]data1;
  input [7:0]\txd_pipe_2_reg[55] ;

  wire [7:0]D;
  wire [0:0]Q;
  wire [7:0]data1;
  wire [0:0]is_invalid_k;
  wire is_terminate;
  wire \last_qmsg_reg[18] ;
  wire \last_qmsg_reg[19] ;
  wire \last_qmsg_reg[20] ;
  wire \last_qmsg_reg[21] ;
  wire [2:0]\last_qmsg_reg[22] ;
  wire \last_qmsg_reg[23] ;
  wire [0:0]txc_filtered;
  wire txc_out_reg_0;
  wire [0:0]\txd_is_IDLE_reg_reg[6] ;
  wire \txd_out[0]_i_1_n_0 ;
  wire \txd_out[1]_i_1_n_0 ;
  wire \txd_out[6]_i_1_n_0 ;
  wire \txd_out[7]_i_1__6_n_0 ;
  wire [7:0]\txd_pipe_2_reg[55] ;
  wire usrclk;

  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[16]_i_1 
       (.I0(\last_qmsg_reg[22] [0]),
        .I1(txc_out_reg_0),
        .I2(data1[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[17]_i_1 
       (.I0(\last_qmsg_reg[22] [1]),
        .I1(txc_out_reg_0),
        .I2(data1[1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[18]_i_1 
       (.I0(\last_qmsg_reg[18] ),
        .I1(txc_out_reg_0),
        .I2(data1[2]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[19]_i_1 
       (.I0(\last_qmsg_reg[19] ),
        .I1(txc_out_reg_0),
        .I2(data1[3]),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[20]_i_1 
       (.I0(\last_qmsg_reg[20] ),
        .I1(txc_out_reg_0),
        .I2(data1[4]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[21]_i_1 
       (.I0(\last_qmsg_reg[21] ),
        .I1(txc_out_reg_0),
        .I2(data1[5]),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[22]_i_1 
       (.I0(\last_qmsg_reg[22] [2]),
        .I1(txc_out_reg_0),
        .I2(data1[6]),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[23]_i_1 
       (.I0(\last_qmsg_reg[23] ),
        .I1(txc_out_reg_0),
        .I2(data1[7]),
        .O(D[7]));
  FDRE #(
    .INIT(1'b1)) 
    txc_out_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(txc_filtered),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0222AAAA)) 
    \txd_out[0]_i_1 
       (.I0(\txd_pipe_2_reg[55] [0]),
        .I1(is_invalid_k),
        .I2(\txd_is_IDLE_reg_reg[6] ),
        .I3(is_terminate),
        .I4(Q),
        .O(\txd_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[1]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[55] [1]),
        .I3(is_terminate),
        .I4(\txd_is_IDLE_reg_reg[6] ),
        .O(\txd_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[6]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[55] [6]),
        .I3(is_terminate),
        .I4(\txd_is_IDLE_reg_reg[6] ),
        .O(\txd_out[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAA80)) 
    \txd_out[7]_i_1__6 
       (.I0(Q),
        .I1(is_terminate),
        .I2(\txd_is_IDLE_reg_reg[6] ),
        .I3(is_invalid_k),
        .O(\txd_out[7]_i_1__6_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[0]_i_1_n_0 ),
        .Q(\last_qmsg_reg[22] [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[1]_i_1_n_0 ),
        .Q(\last_qmsg_reg[22] [1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[55] [2]),
        .Q(\last_qmsg_reg[18] ),
        .S(\txd_out[7]_i_1__6_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[55] [3]),
        .Q(\last_qmsg_reg[19] ),
        .S(\txd_out[7]_i_1__6_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[55] [4]),
        .Q(\last_qmsg_reg[20] ),
        .S(\txd_out[7]_i_1__6_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[55] [5]),
        .Q(\last_qmsg_reg[21] ),
        .S(\txd_out[7]_i_1__6_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[6]_i_1_n_0 ),
        .Q(\last_qmsg_reg[22] [2]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[55] [7]),
        .Q(\last_qmsg_reg[23] ),
        .S(\txd_out[7]_i_1__6_n_0 ));
endmodule

(* ORIG_REF_NAME = "tx_filter" *) 
module rxaui_0_tx_filter_14
   (txc_filtered,
    D,
    \last_qmsg_reg[30] ,
    \last_qmsg_reg[26] ,
    \last_qmsg_reg[27] ,
    \last_qmsg_reg[28] ,
    \last_qmsg_reg[29] ,
    \last_qmsg_reg[31] ,
    Q,
    usrclk,
    is_terminate,
    \txd_is_IDLE_reg_reg[7] ,
    is_invalid_k,
    txc_out_reg_0,
    data1,
    \txd_pipe_2_reg[63] );
  output [0:0]txc_filtered;
  output [7:0]D;
  output [2:0]\last_qmsg_reg[30] ;
  output \last_qmsg_reg[26] ;
  output \last_qmsg_reg[27] ;
  output \last_qmsg_reg[28] ;
  output \last_qmsg_reg[29] ;
  output \last_qmsg_reg[31] ;
  input [0:0]Q;
  input usrclk;
  input is_terminate;
  input [0:0]\txd_is_IDLE_reg_reg[7] ;
  input [0:0]is_invalid_k;
  input txc_out_reg_0;
  input [7:0]data1;
  input [7:0]\txd_pipe_2_reg[63] ;

  wire [7:0]D;
  wire [0:0]Q;
  wire [7:0]data1;
  wire [0:0]is_invalid_k;
  wire is_terminate;
  wire \last_qmsg_reg[26] ;
  wire \last_qmsg_reg[27] ;
  wire \last_qmsg_reg[28] ;
  wire \last_qmsg_reg[29] ;
  wire [2:0]\last_qmsg_reg[30] ;
  wire \last_qmsg_reg[31] ;
  wire [0:0]txc_filtered;
  wire txc_out_reg_0;
  wire [0:0]\txd_is_IDLE_reg_reg[7] ;
  wire \txd_out[0]_i_1_n_0 ;
  wire \txd_out[1]_i_1_n_0 ;
  wire \txd_out[6]_i_1_n_0 ;
  wire \txd_out[7]_i_1__7_n_0 ;
  wire [7:0]\txd_pipe_2_reg[63] ;
  wire usrclk;

  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[24]_i_1 
       (.I0(\last_qmsg_reg[30] [0]),
        .I1(txc_out_reg_0),
        .I2(data1[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[25]_i_1 
       (.I0(\last_qmsg_reg[30] [1]),
        .I1(txc_out_reg_0),
        .I2(data1[1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[26]_i_1 
       (.I0(\last_qmsg_reg[26] ),
        .I1(txc_out_reg_0),
        .I2(data1[2]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[27]_i_1 
       (.I0(\last_qmsg_reg[27] ),
        .I1(txc_out_reg_0),
        .I2(data1[3]),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[28]_i_1 
       (.I0(\last_qmsg_reg[28] ),
        .I1(txc_out_reg_0),
        .I2(data1[4]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[29]_i_1 
       (.I0(\last_qmsg_reg[29] ),
        .I1(txc_out_reg_0),
        .I2(data1[5]),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[30]_i_1 
       (.I0(\last_qmsg_reg[30] [2]),
        .I1(txc_out_reg_0),
        .I2(data1[6]),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \last_qmsg[31]_i_2 
       (.I0(\last_qmsg_reg[31] ),
        .I1(txc_out_reg_0),
        .I2(data1[7]),
        .O(D[7]));
  FDRE #(
    .INIT(1'b1)) 
    txc_out_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(txc_filtered),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0222AAAA)) 
    \txd_out[0]_i_1 
       (.I0(\txd_pipe_2_reg[63] [0]),
        .I1(is_invalid_k),
        .I2(\txd_is_IDLE_reg_reg[7] ),
        .I3(is_terminate),
        .I4(Q),
        .O(\txd_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[1]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[63] [1]),
        .I3(is_terminate),
        .I4(\txd_is_IDLE_reg_reg[7] ),
        .O(\txd_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[6]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[63] [6]),
        .I3(is_terminate),
        .I4(\txd_is_IDLE_reg_reg[7] ),
        .O(\txd_out[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAA80)) 
    \txd_out[7]_i_1__7 
       (.I0(Q),
        .I1(is_terminate),
        .I2(\txd_is_IDLE_reg_reg[7] ),
        .I3(is_invalid_k),
        .O(\txd_out[7]_i_1__7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[0]_i_1_n_0 ),
        .Q(\last_qmsg_reg[30] [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[1]_i_1_n_0 ),
        .Q(\last_qmsg_reg[30] [1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[63] [2]),
        .Q(\last_qmsg_reg[26] ),
        .S(\txd_out[7]_i_1__7_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[63] [3]),
        .Q(\last_qmsg_reg[27] ),
        .S(\txd_out[7]_i_1__7_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[63] [4]),
        .Q(\last_qmsg_reg[28] ),
        .S(\txd_out[7]_i_1__7_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[63] [5]),
        .Q(\last_qmsg_reg[29] ),
        .S(\txd_out[7]_i_1__7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[6]_i_1_n_0 ),
        .Q(\last_qmsg_reg[30] [2]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[63] [7]),
        .Q(\last_qmsg_reg[31] ),
        .S(\txd_out[7]_i_1__7_n_0 ));
endmodule

(* ORIG_REF_NAME = "tx_filter" *) 
module rxaui_0_tx_filter_15
   (\txc_out_reg[0] ,
    \last_qmsg_reg[31] ,
    txd_filtered,
    Q,
    usrclk,
    \is_terminate_reg[0] ,
    \txd_is_IDLE_reg_reg[0] ,
    is_invalid_k,
    txc_out_reg_0,
    txc_out_reg_1,
    \txd_pipe_2_reg[7] );
  output [0:0]\txc_out_reg[0] ;
  output \last_qmsg_reg[31] ;
  output [7:0]txd_filtered;
  input [0:0]Q;
  input usrclk;
  input \is_terminate_reg[0] ;
  input [0:0]\txd_is_IDLE_reg_reg[0] ;
  input [0:0]is_invalid_k;
  input [2:0]txc_out_reg_0;
  input txc_out_reg_1;
  input [7:0]\txd_pipe_2_reg[7] ;

  wire [0:0]Q;
  wire [0:0]is_invalid_k;
  wire \is_terminate_reg[0] ;
  wire \last_qmsg[31]_i_3_n_0 ;
  wire \last_qmsg[31]_i_5_n_0 ;
  wire \last_qmsg_reg[31] ;
  wire [0:0]\txc_out_reg[0] ;
  wire [2:0]txc_out_reg_0;
  wire txc_out_reg_1;
  wire [7:0]txd_filtered;
  wire [0:0]\txd_is_IDLE_reg_reg[0] ;
  wire \txd_out[0]_i_1_n_0 ;
  wire \txd_out[1]_i_1_n_0 ;
  wire \txd_out[6]_i_1_n_0 ;
  wire \txd_out[7]_i_1__0_n_0 ;
  wire [7:0]\txd_pipe_2_reg[7] ;
  wire usrclk;

  LUT6 #(
    .INIT(64'hFFFFFFFF00001000)) 
    \last_qmsg[31]_i_1 
       (.I0(\last_qmsg[31]_i_3_n_0 ),
        .I1(txc_out_reg_0[2]),
        .I2(txd_filtered[2]),
        .I3(txd_filtered[4]),
        .I4(txd_filtered[1]),
        .I5(txc_out_reg_1),
        .O(\last_qmsg_reg[31] ));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \last_qmsg[31]_i_3 
       (.I0(txd_filtered[0]),
        .I1(txd_filtered[6]),
        .I2(txc_out_reg_0[0]),
        .I3(txd_filtered[7]),
        .I4(\last_qmsg[31]_i_5_n_0 ),
        .O(\last_qmsg[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFDF)) 
    \last_qmsg[31]_i_5 
       (.I0(txd_filtered[3]),
        .I1(txd_filtered[5]),
        .I2(\txc_out_reg[0] ),
        .I3(txc_out_reg_0[1]),
        .O(\last_qmsg[31]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    txc_out_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(\txc_out_reg[0] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0222AAAA)) 
    \txd_out[0]_i_1 
       (.I0(\txd_pipe_2_reg[7] [0]),
        .I1(is_invalid_k),
        .I2(\txd_is_IDLE_reg_reg[0] ),
        .I3(\is_terminate_reg[0] ),
        .I4(Q),
        .O(\txd_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[1]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[7] [1]),
        .I3(\is_terminate_reg[0] ),
        .I4(\txd_is_IDLE_reg_reg[0] ),
        .O(\txd_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[6]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[7] [6]),
        .I3(\is_terminate_reg[0] ),
        .I4(\txd_is_IDLE_reg_reg[0] ),
        .O(\txd_out[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAA80)) 
    \txd_out[7]_i_1__0 
       (.I0(Q),
        .I1(\is_terminate_reg[0] ),
        .I2(\txd_is_IDLE_reg_reg[0] ),
        .I3(is_invalid_k),
        .O(\txd_out[7]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[0]_i_1_n_0 ),
        .Q(txd_filtered[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[1]_i_1_n_0 ),
        .Q(txd_filtered[1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[7] [2]),
        .Q(txd_filtered[2]),
        .S(\txd_out[7]_i_1__0_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[7] [3]),
        .Q(txd_filtered[3]),
        .S(\txd_out[7]_i_1__0_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[7] [4]),
        .Q(txd_filtered[4]),
        .S(\txd_out[7]_i_1__0_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[7] [5]),
        .Q(txd_filtered[5]),
        .S(\txd_out[7]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[6]_i_1_n_0 ),
        .Q(txd_filtered[6]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[7] [7]),
        .Q(txd_filtered[7]),
        .S(\txd_out[7]_i_1__0_n_0 ));
endmodule

(* ORIG_REF_NAME = "tx_filter" *) 
module rxaui_0_tx_filter_16
   (\txc_out_reg[1] ,
    data1,
    Q,
    usrclk,
    \is_terminate_reg[0] ,
    \txd_is_IDLE_reg_reg[1] ,
    is_invalid_k,
    \txd_pipe_2_reg[15] );
  output [0:0]\txc_out_reg[1] ;
  output [7:0]data1;
  input [0:0]Q;
  input usrclk;
  input \is_terminate_reg[0] ;
  input [0:0]\txd_is_IDLE_reg_reg[1] ;
  input [0:0]is_invalid_k;
  input [7:0]\txd_pipe_2_reg[15] ;

  wire [0:0]Q;
  wire [7:0]data1;
  wire [0:0]is_invalid_k;
  wire \is_terminate_reg[0] ;
  wire [0:0]\txc_out_reg[1] ;
  wire [0:0]\txd_is_IDLE_reg_reg[1] ;
  wire \txd_out[0]_i_1_n_0 ;
  wire \txd_out[1]_i_1_n_0 ;
  wire \txd_out[6]_i_1_n_0 ;
  wire \txd_out[7]_i_1__1_n_0 ;
  wire [7:0]\txd_pipe_2_reg[15] ;
  wire usrclk;

  FDRE #(
    .INIT(1'b1)) 
    txc_out_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(\txc_out_reg[1] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0222AAAA)) 
    \txd_out[0]_i_1 
       (.I0(\txd_pipe_2_reg[15] [0]),
        .I1(is_invalid_k),
        .I2(\txd_is_IDLE_reg_reg[1] ),
        .I3(\is_terminate_reg[0] ),
        .I4(Q),
        .O(\txd_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[1]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[15] [1]),
        .I3(\is_terminate_reg[0] ),
        .I4(\txd_is_IDLE_reg_reg[1] ),
        .O(\txd_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[6]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[15] [6]),
        .I3(\is_terminate_reg[0] ),
        .I4(\txd_is_IDLE_reg_reg[1] ),
        .O(\txd_out[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAA80)) 
    \txd_out[7]_i_1__1 
       (.I0(Q),
        .I1(\is_terminate_reg[0] ),
        .I2(\txd_is_IDLE_reg_reg[1] ),
        .I3(is_invalid_k),
        .O(\txd_out[7]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[0]_i_1_n_0 ),
        .Q(data1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[1]_i_1_n_0 ),
        .Q(data1[1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[15] [2]),
        .Q(data1[2]),
        .S(\txd_out[7]_i_1__1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[15] [3]),
        .Q(data1[3]),
        .S(\txd_out[7]_i_1__1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[15] [4]),
        .Q(data1[4]),
        .S(\txd_out[7]_i_1__1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[15] [5]),
        .Q(data1[5]),
        .S(\txd_out[7]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[6]_i_1_n_0 ),
        .Q(data1[6]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[15] [7]),
        .Q(data1[7]),
        .S(\txd_out[7]_i_1__1_n_0 ));
endmodule

(* ORIG_REF_NAME = "tx_filter" *) 
module rxaui_0_tx_filter_17
   (\txc_out_reg[2] ,
    data1,
    Q,
    usrclk,
    \is_terminate_reg[0] ,
    \txd_is_IDLE_reg_reg[2] ,
    is_invalid_k,
    \txd_pipe_2_reg[23] );
  output [0:0]\txc_out_reg[2] ;
  output [7:0]data1;
  input [0:0]Q;
  input usrclk;
  input \is_terminate_reg[0] ;
  input [0:0]\txd_is_IDLE_reg_reg[2] ;
  input [0:0]is_invalid_k;
  input [7:0]\txd_pipe_2_reg[23] ;

  wire [0:0]Q;
  wire [7:0]data1;
  wire [0:0]is_invalid_k;
  wire \is_terminate_reg[0] ;
  wire [0:0]\txc_out_reg[2] ;
  wire [0:0]\txd_is_IDLE_reg_reg[2] ;
  wire \txd_out[0]_i_1_n_0 ;
  wire \txd_out[1]_i_1_n_0 ;
  wire \txd_out[6]_i_1_n_0 ;
  wire \txd_out[7]_i_1__2_n_0 ;
  wire [7:0]\txd_pipe_2_reg[23] ;
  wire usrclk;

  FDRE #(
    .INIT(1'b1)) 
    txc_out_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(\txc_out_reg[2] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0222AAAA)) 
    \txd_out[0]_i_1 
       (.I0(\txd_pipe_2_reg[23] [0]),
        .I1(is_invalid_k),
        .I2(\txd_is_IDLE_reg_reg[2] ),
        .I3(\is_terminate_reg[0] ),
        .I4(Q),
        .O(\txd_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[1]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[23] [1]),
        .I3(\is_terminate_reg[0] ),
        .I4(\txd_is_IDLE_reg_reg[2] ),
        .O(\txd_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[6]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[23] [6]),
        .I3(\is_terminate_reg[0] ),
        .I4(\txd_is_IDLE_reg_reg[2] ),
        .O(\txd_out[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAA80)) 
    \txd_out[7]_i_1__2 
       (.I0(Q),
        .I1(\is_terminate_reg[0] ),
        .I2(\txd_is_IDLE_reg_reg[2] ),
        .I3(is_invalid_k),
        .O(\txd_out[7]_i_1__2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[0]_i_1_n_0 ),
        .Q(data1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[1]_i_1_n_0 ),
        .Q(data1[1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[23] [2]),
        .Q(data1[2]),
        .S(\txd_out[7]_i_1__2_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[23] [3]),
        .Q(data1[3]),
        .S(\txd_out[7]_i_1__2_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[23] [4]),
        .Q(data1[4]),
        .S(\txd_out[7]_i_1__2_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[23] [5]),
        .Q(data1[5]),
        .S(\txd_out[7]_i_1__2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[6]_i_1_n_0 ),
        .Q(data1[6]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[23] [7]),
        .Q(data1[7]),
        .S(\txd_out[7]_i_1__2_n_0 ));
endmodule

(* ORIG_REF_NAME = "tx_filter" *) 
module rxaui_0_tx_filter_18
   (\last_qmsg_reg[31] ,
    data1,
    Q,
    usrclk,
    \is_terminate_reg[0] ,
    \txd_is_IDLE_reg_reg[3] ,
    is_invalid_k,
    \txd_pipe_2_reg[31] );
  output [0:0]\last_qmsg_reg[31] ;
  output [7:0]data1;
  input [0:0]Q;
  input usrclk;
  input \is_terminate_reg[0] ;
  input [0:0]\txd_is_IDLE_reg_reg[3] ;
  input [0:0]is_invalid_k;
  input [7:0]\txd_pipe_2_reg[31] ;

  wire [0:0]Q;
  wire [7:0]data1;
  wire [0:0]is_invalid_k;
  wire \is_terminate_reg[0] ;
  wire [0:0]\last_qmsg_reg[31] ;
  wire [0:0]\txd_is_IDLE_reg_reg[3] ;
  wire \txd_out[0]_i_1_n_0 ;
  wire \txd_out[1]_i_1_n_0 ;
  wire \txd_out[6]_i_1_n_0 ;
  wire \txd_out[7]_i_1__3_n_0 ;
  wire [7:0]\txd_pipe_2_reg[31] ;
  wire usrclk;

  FDRE #(
    .INIT(1'b1)) 
    txc_out_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(Q),
        .Q(\last_qmsg_reg[31] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0222AAAA)) 
    \txd_out[0]_i_1 
       (.I0(\txd_pipe_2_reg[31] [0]),
        .I1(is_invalid_k),
        .I2(\txd_is_IDLE_reg_reg[3] ),
        .I3(\is_terminate_reg[0] ),
        .I4(Q),
        .O(\txd_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[1]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[31] [1]),
        .I3(\is_terminate_reg[0] ),
        .I4(\txd_is_IDLE_reg_reg[3] ),
        .O(\txd_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h50F8F8F8)) 
    \txd_out[6]_i_1 
       (.I0(Q),
        .I1(is_invalid_k),
        .I2(\txd_pipe_2_reg[31] [6]),
        .I3(\is_terminate_reg[0] ),
        .I4(\txd_is_IDLE_reg_reg[3] ),
        .O(\txd_out[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAA80)) 
    \txd_out[7]_i_1__3 
       (.I0(Q),
        .I1(\is_terminate_reg[0] ),
        .I2(\txd_is_IDLE_reg_reg[3] ),
        .I3(is_invalid_k),
        .O(\txd_out[7]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[0]_i_1_n_0 ),
        .Q(data1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[1]_i_1_n_0 ),
        .Q(data1[1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[31] [2]),
        .Q(data1[2]),
        .S(\txd_out[7]_i_1__3_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[31] [3]),
        .Q(data1[3]),
        .S(\txd_out[7]_i_1__3_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[31] [4]),
        .Q(data1[4]),
        .S(\txd_out[7]_i_1__3_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[31] [5]),
        .Q(data1[5]),
        .S(\txd_out[7]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_out[6]_i_1_n_0 ),
        .Q(data1[6]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \txd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\txd_pipe_2_reg[31] [7]),
        .Q(data1[7]),
        .S(\txd_out[7]_i_1__3_n_0 ));
endmodule

(* ORIG_REF_NAME = "tx_recoder" *) 
module rxaui_0_tx_recoder
   (mgt_txdata,
    mgt_txcharisk,
    D,
    usrclk,
    test_en_reg_reg,
    \last_qmsg_reg[30] ,
    \last_qmsg_reg[22] ,
    \last_qmsg_reg[14] ,
    \last_qmsg_reg[6] ,
    \last_qmsg_reg[30]_0 ,
    \last_qmsg_reg[22]_0 ,
    \last_qmsg_reg[14]_0 ,
    \last_qmsg_reg[6]_0 ,
    test_en_reg_reg_0,
    \state_reg[1][2] ,
    \state_reg[1][2]_0 ,
    \state_reg[1][2]_1 ,
    \state_reg[1][2]_2 ,
    \state_reg[0][2] ,
    \state_reg[0][2]_0 ,
    \state_reg[0][2]_1 ,
    \state_reg[0][2]_2 ,
    test_en_reg_reg_1,
    \last_qmsg_reg[24] ,
    \last_qmsg_reg[16] ,
    \last_qmsg_reg[8] ,
    \last_qmsg_reg[0] ,
    \last_qmsg_reg[24]_0 ,
    \last_qmsg_reg[16]_0 ,
    \last_qmsg_reg[8]_0 ,
    \last_qmsg_reg[0]_0 ,
    \state_reg[1][2]_3 ,
    \state_reg[1][2]_4 ,
    \state_reg[1][2]_5 ,
    \state_reg[1][2]_6 ,
    \state_reg[1][2]_7 ,
    \test_sel_reg_reg[1] ,
    \state_reg[1][2]_8 ,
    \state_reg[1][2]_9 ,
    \state_reg[1][2]_10 ,
    \state_reg[1][2]_11 ,
    \state_reg[1][2]_12 ,
    \state_reg[1][2]_13 ,
    \state_reg[1][2]_14 ,
    \state_reg[1][2]_15 ,
    \state_reg[1][2]_16 ,
    \state_reg[1][2]_17 ,
    \state_reg[1][2]_18 ,
    \state_reg[1][2]_19 ,
    \state_reg[1][2]_20 ,
    \state_reg[1][2]_21 ,
    \state_reg[1][2]_22 ,
    \state_reg[0][2]_3 ,
    \state_reg[0][2]_4 ,
    \state_reg[0][2]_5 ,
    \state_reg[0][0] ,
    \state_reg[0][0]_0 ,
    \state_reg[0][0]_1 ,
    \state_reg[0][0]_2 ,
    \test_sel_reg_reg[1]_0 ,
    \state_reg[0][2]_6 ,
    \last_qmsg_reg[21] ,
    \last_qmsg_reg[13] ,
    \last_qmsg_reg[5] ,
    \state_reg[0][0]_3 ,
    \state_reg[0][0]_4 ,
    \state_reg[0][0]_5 ,
    \state_reg[0][0]_6 ,
    \state_reg[0][0]_7 ,
    \state_reg[0][0]_8 ,
    \state_reg[0][0]_9 ,
    \state_reg[0][0]_10 ,
    \state_reg[0][1] ,
    \state_reg[0][2]_7 ,
    Q,
    test_pattern_sel,
    test_pattern_en,
    \test_sel_reg_reg[1]_1 ,
    txc_out_reg);
  output [63:0]mgt_txdata;
  output [7:0]mgt_txcharisk;
  input [2:0]D;
  input usrclk;
  input test_en_reg_reg;
  input \last_qmsg_reg[30] ;
  input \last_qmsg_reg[22] ;
  input \last_qmsg_reg[14] ;
  input \last_qmsg_reg[6] ;
  input \last_qmsg_reg[30]_0 ;
  input \last_qmsg_reg[22]_0 ;
  input \last_qmsg_reg[14]_0 ;
  input \last_qmsg_reg[6]_0 ;
  input test_en_reg_reg_0;
  input \state_reg[1][2] ;
  input \state_reg[1][2]_0 ;
  input \state_reg[1][2]_1 ;
  input \state_reg[1][2]_2 ;
  input \state_reg[0][2] ;
  input \state_reg[0][2]_0 ;
  input \state_reg[0][2]_1 ;
  input \state_reg[0][2]_2 ;
  input test_en_reg_reg_1;
  input \last_qmsg_reg[24] ;
  input \last_qmsg_reg[16] ;
  input \last_qmsg_reg[8] ;
  input \last_qmsg_reg[0] ;
  input \last_qmsg_reg[24]_0 ;
  input \last_qmsg_reg[16]_0 ;
  input \last_qmsg_reg[8]_0 ;
  input \last_qmsg_reg[0]_0 ;
  input \state_reg[1][2]_3 ;
  input \state_reg[1][2]_4 ;
  input \state_reg[1][2]_5 ;
  input \state_reg[1][2]_6 ;
  input \state_reg[1][2]_7 ;
  input \test_sel_reg_reg[1] ;
  input \state_reg[1][2]_8 ;
  input \state_reg[1][2]_9 ;
  input \state_reg[1][2]_10 ;
  input \state_reg[1][2]_11 ;
  input \state_reg[1][2]_12 ;
  input \state_reg[1][2]_13 ;
  input \state_reg[1][2]_14 ;
  input \state_reg[1][2]_15 ;
  input \state_reg[1][2]_16 ;
  input \state_reg[1][2]_17 ;
  input \state_reg[1][2]_18 ;
  input \state_reg[1][2]_19 ;
  input \state_reg[1][2]_20 ;
  input \state_reg[1][2]_21 ;
  input \state_reg[1][2]_22 ;
  input \state_reg[0][2]_3 ;
  input \state_reg[0][2]_4 ;
  input \state_reg[0][2]_5 ;
  input \state_reg[0][0] ;
  input \state_reg[0][0]_0 ;
  input \state_reg[0][0]_1 ;
  input \state_reg[0][0]_2 ;
  input \test_sel_reg_reg[1]_0 ;
  input \state_reg[0][2]_6 ;
  input \last_qmsg_reg[21] ;
  input \last_qmsg_reg[13] ;
  input \last_qmsg_reg[5] ;
  input \state_reg[0][0]_3 ;
  input \state_reg[0][0]_4 ;
  input \state_reg[0][0]_5 ;
  input \state_reg[0][0]_6 ;
  input \state_reg[0][0]_7 ;
  input \state_reg[0][0]_8 ;
  input \state_reg[0][0]_9 ;
  input \state_reg[0][0]_10 ;
  input \state_reg[0][1] ;
  input \state_reg[0][2]_7 ;
  input [0:0]Q;
  input [1:0]test_pattern_sel;
  input test_pattern_en;
  input \test_sel_reg_reg[1]_1 ;
  input [7:0]txc_out_reg;

  wire [2:0]D;
  wire [0:0]Q;
  wire \last_qmsg_reg[0] ;
  wire \last_qmsg_reg[0]_0 ;
  wire \last_qmsg_reg[13] ;
  wire \last_qmsg_reg[14] ;
  wire \last_qmsg_reg[14]_0 ;
  wire \last_qmsg_reg[16] ;
  wire \last_qmsg_reg[16]_0 ;
  wire \last_qmsg_reg[21] ;
  wire \last_qmsg_reg[22] ;
  wire \last_qmsg_reg[22]_0 ;
  wire \last_qmsg_reg[24] ;
  wire \last_qmsg_reg[24]_0 ;
  wire \last_qmsg_reg[30] ;
  wire \last_qmsg_reg[30]_0 ;
  wire \last_qmsg_reg[5] ;
  wire \last_qmsg_reg[6] ;
  wire \last_qmsg_reg[6]_0 ;
  wire \last_qmsg_reg[8] ;
  wire \last_qmsg_reg[8]_0 ;
  wire [7:0]mgt_txcharisk;
  wire [63:0]mgt_txdata;
  wire \state_reg[0][0] ;
  wire \state_reg[0][0]_0 ;
  wire \state_reg[0][0]_1 ;
  wire \state_reg[0][0]_10 ;
  wire \state_reg[0][0]_2 ;
  wire \state_reg[0][0]_3 ;
  wire \state_reg[0][0]_4 ;
  wire \state_reg[0][0]_5 ;
  wire \state_reg[0][0]_6 ;
  wire \state_reg[0][0]_7 ;
  wire \state_reg[0][0]_8 ;
  wire \state_reg[0][0]_9 ;
  wire \state_reg[0][1] ;
  wire \state_reg[0][2] ;
  wire \state_reg[0][2]_0 ;
  wire \state_reg[0][2]_1 ;
  wire \state_reg[0][2]_2 ;
  wire \state_reg[0][2]_3 ;
  wire \state_reg[0][2]_4 ;
  wire \state_reg[0][2]_5 ;
  wire \state_reg[0][2]_6 ;
  wire \state_reg[0][2]_7 ;
  wire \state_reg[1][2] ;
  wire \state_reg[1][2]_0 ;
  wire \state_reg[1][2]_1 ;
  wire \state_reg[1][2]_10 ;
  wire \state_reg[1][2]_11 ;
  wire \state_reg[1][2]_12 ;
  wire \state_reg[1][2]_13 ;
  wire \state_reg[1][2]_14 ;
  wire \state_reg[1][2]_15 ;
  wire \state_reg[1][2]_16 ;
  wire \state_reg[1][2]_17 ;
  wire \state_reg[1][2]_18 ;
  wire \state_reg[1][2]_19 ;
  wire \state_reg[1][2]_2 ;
  wire \state_reg[1][2]_20 ;
  wire \state_reg[1][2]_21 ;
  wire \state_reg[1][2]_22 ;
  wire \state_reg[1][2]_3 ;
  wire \state_reg[1][2]_4 ;
  wire \state_reg[1][2]_5 ;
  wire \state_reg[1][2]_6 ;
  wire \state_reg[1][2]_7 ;
  wire \state_reg[1][2]_8 ;
  wire \state_reg[1][2]_9 ;
  wire test_en_reg_reg;
  wire test_en_reg_reg_0;
  wire test_en_reg_reg_1;
  wire test_pattern_en;
  wire [1:0]test_pattern_sel;
  wire \test_sel_reg_reg[1] ;
  wire \test_sel_reg_reg[1]_0 ;
  wire \test_sel_reg_reg[1]_1 ;
  wire [7:0]txc_out_reg;
  wire \txd_out[28]_i_1_n_0 ;
  wire usrclk;

  FDSE \txc_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(txc_out_reg[0]),
        .Q(mgt_txcharisk[0]),
        .S(\test_sel_reg_reg[1]_1 ));
  FDSE \txc_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(txc_out_reg[1]),
        .Q(mgt_txcharisk[1]),
        .S(\test_sel_reg_reg[1]_1 ));
  FDSE \txc_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(txc_out_reg[2]),
        .Q(mgt_txcharisk[4]),
        .S(\test_sel_reg_reg[1]_1 ));
  FDSE \txc_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(txc_out_reg[3]),
        .Q(mgt_txcharisk[5]),
        .S(\test_sel_reg_reg[1]_1 ));
  FDSE \txc_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(txc_out_reg[4]),
        .Q(mgt_txcharisk[2]),
        .S(\test_sel_reg_reg[1]_1 ));
  FDSE \txc_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(txc_out_reg[5]),
        .Q(mgt_txcharisk[3]),
        .S(\test_sel_reg_reg[1]_1 ));
  FDSE \txc_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(txc_out_reg[6]),
        .Q(mgt_txcharisk[6]),
        .S(\test_sel_reg_reg[1]_1 ));
  FDSE \txc_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(txc_out_reg[7]),
        .Q(mgt_txcharisk[7]),
        .S(\test_sel_reg_reg[1]_1 ));
  LUT6 #(
    .INIT(64'h4FFFFFFF4F4F4F4F)) 
    \txd_out[28]_i_1 
       (.I0(\state_reg[0][1] ),
        .I1(\state_reg[0][2]_7 ),
        .I2(Q),
        .I3(test_pattern_sel[1]),
        .I4(test_pattern_sel[0]),
        .I5(test_pattern_en),
        .O(\txd_out[28]_i_1_n_0 ));
  FDRE \txd_out_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[0]_0 ),
        .Q(mgt_txdata[0]),
        .R(test_en_reg_reg_1));
  FDSE \txd_out_reg[10] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_8 ),
        .Q(mgt_txdata[10]),
        .S(\txd_out[28]_i_1_n_0 ));
  FDRE \txd_out_reg[11] 
       (.C(usrclk),
        .CE(1'b1),
        .D(D[0]),
        .Q(mgt_txdata[11]),
        .R(1'b0));
  FDSE \txd_out_reg[12] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_7 ),
        .Q(mgt_txdata[12]),
        .S(\txd_out[28]_i_1_n_0 ));
  FDSE \txd_out_reg[13] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[13] ),
        .Q(mgt_txdata[13]),
        .S(\test_sel_reg_reg[1]_0 ));
  FDRE \txd_out_reg[14] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[14]_0 ),
        .Q(mgt_txdata[14]),
        .R(test_en_reg_reg));
  FDSE \txd_out_reg[15] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_1 ),
        .Q(mgt_txdata[15]),
        .S(\state_reg[0][2]_5 ));
  FDRE \txd_out_reg[16] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[16]_0 ),
        .Q(mgt_txdata[32]),
        .R(test_en_reg_reg_1));
  FDRE \txd_out_reg[17] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][2]_0 ),
        .Q(mgt_txdata[33]),
        .R(test_en_reg_reg_0));
  FDSE \txd_out_reg[18] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_6 ),
        .Q(mgt_txdata[34]),
        .S(\txd_out[28]_i_1_n_0 ));
  FDSE \txd_out_reg[19] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][2]_3 ),
        .Q(mgt_txdata[35]),
        .S(test_en_reg_reg_1));
  FDRE \txd_out_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][2]_2 ),
        .Q(mgt_txdata[1]),
        .R(test_en_reg_reg_0));
  FDSE \txd_out_reg[20] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_5 ),
        .Q(mgt_txdata[36]),
        .S(\txd_out[28]_i_1_n_0 ));
  FDSE \txd_out_reg[21] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[21] ),
        .Q(mgt_txdata[37]),
        .S(\test_sel_reg_reg[1]_0 ));
  FDRE \txd_out_reg[22] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[22]_0 ),
        .Q(mgt_txdata[38]),
        .R(test_en_reg_reg));
  FDSE \txd_out_reg[23] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_0 ),
        .Q(mgt_txdata[39]),
        .S(\state_reg[0][2]_5 ));
  FDRE \txd_out_reg[24] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[24]_0 ),
        .Q(mgt_txdata[40]),
        .R(test_en_reg_reg_1));
  FDRE \txd_out_reg[25] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][2] ),
        .Q(mgt_txdata[41]),
        .R(test_en_reg_reg_0));
  FDSE \txd_out_reg[26] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_4 ),
        .Q(mgt_txdata[42]),
        .S(\txd_out[28]_i_1_n_0 ));
  FDRE \txd_out_reg[27] 
       (.C(usrclk),
        .CE(1'b1),
        .D(D[1]),
        .Q(mgt_txdata[43]),
        .R(1'b0));
  FDSE \txd_out_reg[28] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_3 ),
        .Q(mgt_txdata[44]),
        .S(\txd_out[28]_i_1_n_0 ));
  FDSE \txd_out_reg[29] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][2]_6 ),
        .Q(mgt_txdata[45]),
        .S(\test_sel_reg_reg[1]_0 ));
  FDSE \txd_out_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_10 ),
        .Q(mgt_txdata[2]),
        .S(\txd_out[28]_i_1_n_0 ));
  FDRE \txd_out_reg[30] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[30]_0 ),
        .Q(mgt_txdata[46]),
        .R(test_en_reg_reg));
  FDSE \txd_out_reg[31] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0] ),
        .Q(mgt_txdata[47]),
        .S(\state_reg[0][2]_5 ));
  FDRE \txd_out_reg[32] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[0] ),
        .Q(mgt_txdata[16]),
        .R(test_en_reg_reg_1));
  FDRE \txd_out_reg[33] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_2 ),
        .Q(mgt_txdata[17]),
        .R(test_en_reg_reg_0));
  FDSE \txd_out_reg[34] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_19 ),
        .Q(mgt_txdata[18]),
        .S(test_en_reg_reg_0));
  FDSE \txd_out_reg[35] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_22 ),
        .Q(mgt_txdata[19]),
        .S(test_en_reg_reg_1));
  FDSE \txd_out_reg[36] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_18 ),
        .Q(mgt_txdata[20]),
        .S(test_en_reg_reg_0));
  FDSE \txd_out_reg[37] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_11 ),
        .Q(mgt_txdata[21]),
        .S(\test_sel_reg_reg[1] ));
  FDRE \txd_out_reg[38] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[6] ),
        .Q(mgt_txdata[22]),
        .R(test_en_reg_reg));
  FDSE \txd_out_reg[39] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_7 ),
        .Q(mgt_txdata[23]),
        .S(\state_reg[1][2]_3 ));
  FDSE \txd_out_reg[3] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][2]_4 ),
        .Q(mgt_txdata[3]),
        .S(test_en_reg_reg_1));
  FDRE \txd_out_reg[40] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[8] ),
        .Q(mgt_txdata[24]),
        .R(test_en_reg_reg_1));
  FDRE \txd_out_reg[41] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_1 ),
        .Q(mgt_txdata[25]),
        .R(test_en_reg_reg_0));
  FDSE \txd_out_reg[42] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_17 ),
        .Q(mgt_txdata[26]),
        .S(test_en_reg_reg_0));
  FDSE \txd_out_reg[43] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_21 ),
        .Q(mgt_txdata[27]),
        .S(test_en_reg_reg_1));
  FDSE \txd_out_reg[44] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_16 ),
        .Q(mgt_txdata[28]),
        .S(test_en_reg_reg_0));
  FDSE \txd_out_reg[45] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_10 ),
        .Q(mgt_txdata[29]),
        .S(\test_sel_reg_reg[1] ));
  FDRE \txd_out_reg[46] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[14] ),
        .Q(mgt_txdata[30]),
        .R(test_en_reg_reg));
  FDSE \txd_out_reg[47] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_6 ),
        .Q(mgt_txdata[31]),
        .S(\state_reg[1][2]_3 ));
  FDRE \txd_out_reg[48] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[16] ),
        .Q(mgt_txdata[48]),
        .R(test_en_reg_reg_1));
  FDRE \txd_out_reg[49] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_0 ),
        .Q(mgt_txdata[49]),
        .R(test_en_reg_reg_0));
  FDSE \txd_out_reg[4] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_9 ),
        .Q(mgt_txdata[4]),
        .S(\txd_out[28]_i_1_n_0 ));
  FDSE \txd_out_reg[50] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_15 ),
        .Q(mgt_txdata[50]),
        .S(test_en_reg_reg_0));
  FDSE \txd_out_reg[51] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_20 ),
        .Q(mgt_txdata[51]),
        .S(test_en_reg_reg_1));
  FDSE \txd_out_reg[52] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_14 ),
        .Q(mgt_txdata[52]),
        .S(test_en_reg_reg_0));
  FDSE \txd_out_reg[53] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_9 ),
        .Q(mgt_txdata[53]),
        .S(\test_sel_reg_reg[1] ));
  FDRE \txd_out_reg[54] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[22] ),
        .Q(mgt_txdata[54]),
        .R(test_en_reg_reg));
  FDSE \txd_out_reg[55] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_5 ),
        .Q(mgt_txdata[55]),
        .S(\state_reg[1][2]_3 ));
  FDRE \txd_out_reg[56] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[24] ),
        .Q(mgt_txdata[56]),
        .R(test_en_reg_reg_1));
  FDRE \txd_out_reg[57] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2] ),
        .Q(mgt_txdata[57]),
        .R(test_en_reg_reg_0));
  FDSE \txd_out_reg[58] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_13 ),
        .Q(mgt_txdata[58]),
        .S(test_en_reg_reg_0));
  FDRE \txd_out_reg[59] 
       (.C(usrclk),
        .CE(1'b1),
        .D(D[2]),
        .Q(mgt_txdata[59]),
        .R(1'b0));
  FDSE \txd_out_reg[5] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[5] ),
        .Q(mgt_txdata[5]),
        .S(\test_sel_reg_reg[1]_0 ));
  FDSE \txd_out_reg[60] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_12 ),
        .Q(mgt_txdata[60]),
        .S(test_en_reg_reg_0));
  FDSE \txd_out_reg[61] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_8 ),
        .Q(mgt_txdata[61]),
        .S(\test_sel_reg_reg[1] ));
  FDRE \txd_out_reg[62] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[30] ),
        .Q(mgt_txdata[62]),
        .R(test_en_reg_reg));
  FDSE \txd_out_reg[63] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[1][2]_4 ),
        .Q(mgt_txdata[63]),
        .S(\state_reg[1][2]_3 ));
  FDRE \txd_out_reg[6] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[6]_0 ),
        .Q(mgt_txdata[6]),
        .R(test_en_reg_reg));
  FDSE \txd_out_reg[7] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][0]_2 ),
        .Q(mgt_txdata[7]),
        .S(\state_reg[0][2]_5 ));
  FDRE \txd_out_reg[8] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\last_qmsg_reg[8]_0 ),
        .Q(mgt_txdata[8]),
        .R(test_en_reg_reg_1));
  FDRE \txd_out_reg[9] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state_reg[0][2]_1 ),
        .Q(mgt_txdata[9]),
        .R(test_en_reg_reg_0));
endmodule

(* ORIG_REF_NAME = "tx_state_machine" *) 
module rxaui_0_tx_state_machine
   (next_ifg_is_a_reg_0,
    \txd_out_reg[6] ,
    Q,
    \txd_out_reg[31] ,
    \txd_out_reg[31]_0 ,
    \txd_out_reg[14] ,
    \txd_out_reg[22] ,
    \txd_out_reg[54] ,
    \txd_out_reg[54]_0 ,
    \txd_out_reg[54]_1 ,
    \txd_out_reg[46] ,
    \txd_out_reg[62] ,
    \txd_out_reg[39] ,
    \txd_out_reg[0] ,
    \txd_out_reg[1] ,
    \txd_out_reg[2] ,
    \txd_out_reg[4] ,
    \txd_out_reg[8] ,
    \txd_out_reg[9] ,
    \txd_out_reg[10] ,
    \txd_out_reg[12] ,
    \txd_out_reg[16] ,
    \txd_out_reg[17] ,
    \txd_out_reg[18] ,
    \txd_out_reg[20] ,
    \txd_out_reg[24] ,
    \txd_out_reg[25] ,
    \txd_out_reg[26] ,
    \txd_out_reg[28] ,
    \txd_out_reg[30] ,
    \txd_out_reg[7] ,
    \txd_out_reg[32] ,
    \txd_out_reg[33] ,
    \txd_out_reg[40] ,
    \txd_out_reg[41] ,
    \txd_out_reg[48] ,
    \txd_out_reg[49] ,
    \txd_out_reg[56] ,
    \txd_out_reg[57] ,
    \txd_out_reg[38] ,
    \txd_out_reg[54]_2 ,
    \txc_out_reg[7] ,
    D,
    E,
    \state_reg[1][1]_0 ,
    \count_reg[4] ,
    \txd_out_reg[7]_0 ,
    \txd_out_reg[15] ,
    \txd_out_reg[23] ,
    \txd_out_reg[3] ,
    \txd_out_reg[5] ,
    \txd_out_reg[59] ,
    \txd_out_reg[27] ,
    \txd_out_reg[13] ,
    \txd_out_reg[19] ,
    \txd_out_reg[21] ,
    \txd_out_reg[29] ,
    \txd_out_reg[31]_1 ,
    \txc_out_reg[1] ,
    \txd_out_reg[34] ,
    \txd_out_reg[35] ,
    \txd_out_reg[36] ,
    \txd_out_reg[37] ,
    \txd_out_reg[39]_0 ,
    \txd_out_reg[42] ,
    \txd_out_reg[43] ,
    \txd_out_reg[44] ,
    \txd_out_reg[45] ,
    \txd_out_reg[47] ,
    \txd_out_reg[50] ,
    \txd_out_reg[51] ,
    \txd_out_reg[52] ,
    \txd_out_reg[53] ,
    \txd_out_reg[55] ,
    \txd_out_reg[58] ,
    \txc_out_reg[7]_0 ,
    \txd_out_reg[60] ,
    \txd_out_reg[61] ,
    \txd_out_reg[63] ,
    usrclk_reset,
    usrclk,
    last_qmsg,
    test_en_reg_reg,
    txd_filtered,
    data1,
    q_det,
    \prbs_reg[8] ,
    \txd_out_reg[6]_0 ,
    \txd_out_reg[6]_1 ,
    test_pattern_en,
    test_pattern_sel,
    test_en_reg_reg_0,
    \test_sel_reg_reg[1] ,
    \txd_out_reg[6]_2 ,
    \tx_is_q_reg[1] ,
    \tx_is_idle_reg[1] ,
    extra_a_reg,
    \count_reg[4]_0 ,
    \prbs_reg[2] ,
    \count_reg[1] ,
    \test_sel_reg_reg[0] ,
    test_en_reg_reg_1,
    \txd_out_reg[2]_0 ,
    \txd_out_reg[3]_0 ,
    \txd_out_reg[4]_0 ,
    \txd_out_reg[5]_0 ,
    \txd_out_reg[7]_1 ,
    \txd_out_reg[2]_1 ,
    \txd_out_reg[3]_1 ,
    \txd_out_reg[4]_1 ,
    \txd_out_reg[5]_1 ,
    \txd_out_reg[7]_2 ,
    \txd_out_reg[2]_2 ,
    \test_sel_reg_reg[1]_0 ,
    \txd_out_reg[3]_2 ,
    txc_filtered,
    \txd_out_reg[4]_2 ,
    \txd_out_reg[5]_2 ,
    \txd_out_reg[7]_3 ,
    \tx_is_idle_reg[0] );
  output next_ifg_is_a_reg_0;
  output \txd_out_reg[6] ;
  output [0:0]Q;
  output \txd_out_reg[31] ;
  output \txd_out_reg[31]_0 ;
  output \txd_out_reg[14] ;
  output \txd_out_reg[22] ;
  output [0:0]\txd_out_reg[54] ;
  output \txd_out_reg[54]_0 ;
  output \txd_out_reg[54]_1 ;
  output \txd_out_reg[46] ;
  output \txd_out_reg[62] ;
  output \txd_out_reg[39] ;
  output \txd_out_reg[0] ;
  output \txd_out_reg[1] ;
  output \txd_out_reg[2] ;
  output \txd_out_reg[4] ;
  output \txd_out_reg[8] ;
  output \txd_out_reg[9] ;
  output \txd_out_reg[10] ;
  output \txd_out_reg[12] ;
  output \txd_out_reg[16] ;
  output \txd_out_reg[17] ;
  output \txd_out_reg[18] ;
  output \txd_out_reg[20] ;
  output \txd_out_reg[24] ;
  output \txd_out_reg[25] ;
  output \txd_out_reg[26] ;
  output \txd_out_reg[28] ;
  output \txd_out_reg[30] ;
  output \txd_out_reg[7] ;
  output \txd_out_reg[32] ;
  output \txd_out_reg[33] ;
  output \txd_out_reg[40] ;
  output \txd_out_reg[41] ;
  output \txd_out_reg[48] ;
  output \txd_out_reg[49] ;
  output \txd_out_reg[56] ;
  output \txd_out_reg[57] ;
  output \txd_out_reg[38] ;
  output \txd_out_reg[54]_2 ;
  output \txc_out_reg[7] ;
  output [1:0]D;
  output [0:0]E;
  output \state_reg[1][1]_0 ;
  output \count_reg[4] ;
  output \txd_out_reg[7]_0 ;
  output \txd_out_reg[15] ;
  output \txd_out_reg[23] ;
  output \txd_out_reg[3] ;
  output \txd_out_reg[5] ;
  output [2:0]\txd_out_reg[59] ;
  output \txd_out_reg[27] ;
  output \txd_out_reg[13] ;
  output \txd_out_reg[19] ;
  output \txd_out_reg[21] ;
  output \txd_out_reg[29] ;
  output \txd_out_reg[31]_1 ;
  output \txc_out_reg[1] ;
  output \txd_out_reg[34] ;
  output \txd_out_reg[35] ;
  output \txd_out_reg[36] ;
  output \txd_out_reg[37] ;
  output \txd_out_reg[39]_0 ;
  output \txd_out_reg[42] ;
  output \txd_out_reg[43] ;
  output \txd_out_reg[44] ;
  output \txd_out_reg[45] ;
  output \txd_out_reg[47] ;
  output \txd_out_reg[50] ;
  output \txd_out_reg[51] ;
  output \txd_out_reg[52] ;
  output \txd_out_reg[53] ;
  output \txd_out_reg[55] ;
  output \txd_out_reg[58] ;
  output [2:0]\txc_out_reg[7]_0 ;
  output \txd_out_reg[60] ;
  output \txd_out_reg[61] ;
  output \txd_out_reg[63] ;
  input usrclk_reset;
  input usrclk;
  input [31:0]last_qmsg;
  input test_en_reg_reg;
  input [15:0]txd_filtered;
  input [23:0]data1;
  input q_det;
  input [1:0]\prbs_reg[8] ;
  input [2:0]\txd_out_reg[6]_0 ;
  input [2:0]\txd_out_reg[6]_1 ;
  input test_pattern_en;
  input [1:0]test_pattern_sel;
  input test_en_reg_reg_0;
  input \test_sel_reg_reg[1] ;
  input [2:0]\txd_out_reg[6]_2 ;
  input [1:0]\tx_is_q_reg[1] ;
  input [1:0]\tx_is_idle_reg[1] ;
  input extra_a_reg;
  input [3:0]\count_reg[4]_0 ;
  input [1:0]\prbs_reg[2] ;
  input \count_reg[1] ;
  input \test_sel_reg_reg[0] ;
  input test_en_reg_reg_1;
  input \txd_out_reg[2]_0 ;
  input \txd_out_reg[3]_0 ;
  input \txd_out_reg[4]_0 ;
  input \txd_out_reg[5]_0 ;
  input \txd_out_reg[7]_1 ;
  input \txd_out_reg[2]_1 ;
  input \txd_out_reg[3]_1 ;
  input \txd_out_reg[4]_1 ;
  input \txd_out_reg[5]_1 ;
  input \txd_out_reg[7]_2 ;
  input \txd_out_reg[2]_2 ;
  input \test_sel_reg_reg[1]_0 ;
  input \txd_out_reg[3]_2 ;
  input [2:0]txc_filtered;
  input \txd_out_reg[4]_2 ;
  input \txd_out_reg[5]_2 ;
  input \txd_out_reg[7]_3 ;
  input \tx_is_idle_reg[0] ;

  wire [1:0]D;
  wire [0:0]E;
  wire [0:0]Q;
  wire \count_reg[1] ;
  wire \count_reg[4] ;
  wire [3:0]\count_reg[4]_0 ;
  wire [23:0]data1;
  wire extra_a_reg;
  wire [0:0]\get_next_state[1]_0 ;
  wire [31:0]last_qmsg;
  wire next_ifg_is_a_i_1_n_0;
  wire next_ifg_is_a_i_2_n_0;
  wire next_ifg_is_a_reg_0;
  wire \next_state[1]16_out ;
  wire [1:0]\prbs_reg[2] ;
  wire [1:0]\prbs_reg[8] ;
  wire q_det;
  wire \state[0][0]_i_1_n_0 ;
  wire \state[0][0]_i_2_n_0 ;
  wire \state[0][0]_i_3_n_0 ;
  wire \state[0][0]_i_4_n_0 ;
  wire \state[0][1]_i_1_n_0 ;
  wire \state[0][1]_i_2_n_0 ;
  wire \state[0][1]_i_3_n_0 ;
  wire \state[0][1]_i_4_n_0 ;
  wire \state[0][2]_i_1_n_0 ;
  wire \state[0][2]_i_2_n_0 ;
  wire \state[1][0]_i_3_n_0 ;
  wire \state[1][0]_i_4_n_0 ;
  wire \state[1][1]_i_1_n_0 ;
  wire \state[1][1]_i_3_n_0 ;
  wire \state[1][1]_i_4_n_0 ;
  wire \state[1][1]_i_5_n_0 ;
  wire \state[1][2]_i_1_n_0 ;
  wire \state[1][2]_i_2_n_0 ;
  wire \state_reg[1][1]_0 ;
  wire test_en_reg_reg;
  wire test_en_reg_reg_0;
  wire test_en_reg_reg_1;
  wire test_pattern_en;
  wire [1:0]test_pattern_sel;
  wire \test_sel_reg_reg[0] ;
  wire \test_sel_reg_reg[1] ;
  wire \test_sel_reg_reg[1]_0 ;
  wire \tx_is_idle_reg[0] ;
  wire [1:0]\tx_is_idle_reg[1] ;
  wire [1:0]\tx_is_q_reg[1] ;
  wire [2:0]txc_filtered;
  wire \txc_out_reg[1] ;
  wire \txc_out_reg[7] ;
  wire [2:0]\txc_out_reg[7]_0 ;
  wire [15:0]txd_filtered;
  wire \txd_out[59]_i_4_n_0 ;
  wire \txd_out_reg[0] ;
  wire \txd_out_reg[10] ;
  wire \txd_out_reg[12] ;
  wire \txd_out_reg[13] ;
  wire \txd_out_reg[14] ;
  wire \txd_out_reg[15] ;
  wire \txd_out_reg[16] ;
  wire \txd_out_reg[17] ;
  wire \txd_out_reg[18] ;
  wire \txd_out_reg[19] ;
  wire \txd_out_reg[1] ;
  wire \txd_out_reg[20] ;
  wire \txd_out_reg[21] ;
  wire \txd_out_reg[22] ;
  wire \txd_out_reg[23] ;
  wire \txd_out_reg[24] ;
  wire \txd_out_reg[25] ;
  wire \txd_out_reg[26] ;
  wire \txd_out_reg[27] ;
  wire \txd_out_reg[28] ;
  wire \txd_out_reg[29] ;
  wire \txd_out_reg[2] ;
  wire \txd_out_reg[2]_0 ;
  wire \txd_out_reg[2]_1 ;
  wire \txd_out_reg[2]_2 ;
  wire \txd_out_reg[30] ;
  wire \txd_out_reg[31] ;
  wire \txd_out_reg[31]_0 ;
  wire \txd_out_reg[31]_1 ;
  wire \txd_out_reg[32] ;
  wire \txd_out_reg[33] ;
  wire \txd_out_reg[34] ;
  wire \txd_out_reg[35] ;
  wire \txd_out_reg[36] ;
  wire \txd_out_reg[37] ;
  wire \txd_out_reg[38] ;
  wire \txd_out_reg[39] ;
  wire \txd_out_reg[39]_0 ;
  wire \txd_out_reg[3] ;
  wire \txd_out_reg[3]_0 ;
  wire \txd_out_reg[3]_1 ;
  wire \txd_out_reg[3]_2 ;
  wire \txd_out_reg[40] ;
  wire \txd_out_reg[41] ;
  wire \txd_out_reg[42] ;
  wire \txd_out_reg[43] ;
  wire \txd_out_reg[44] ;
  wire \txd_out_reg[45] ;
  wire \txd_out_reg[46] ;
  wire \txd_out_reg[47] ;
  wire \txd_out_reg[48] ;
  wire \txd_out_reg[49] ;
  wire \txd_out_reg[4] ;
  wire \txd_out_reg[4]_0 ;
  wire \txd_out_reg[4]_1 ;
  wire \txd_out_reg[4]_2 ;
  wire \txd_out_reg[50] ;
  wire \txd_out_reg[51] ;
  wire \txd_out_reg[52] ;
  wire \txd_out_reg[53] ;
  wire [0:0]\txd_out_reg[54] ;
  wire \txd_out_reg[54]_0 ;
  wire \txd_out_reg[54]_1 ;
  wire \txd_out_reg[54]_2 ;
  wire \txd_out_reg[55] ;
  wire \txd_out_reg[56] ;
  wire \txd_out_reg[57] ;
  wire \txd_out_reg[58] ;
  wire [2:0]\txd_out_reg[59] ;
  wire \txd_out_reg[5] ;
  wire \txd_out_reg[5]_0 ;
  wire \txd_out_reg[5]_1 ;
  wire \txd_out_reg[5]_2 ;
  wire \txd_out_reg[60] ;
  wire \txd_out_reg[61] ;
  wire \txd_out_reg[62] ;
  wire \txd_out_reg[63] ;
  wire \txd_out_reg[6] ;
  wire [2:0]\txd_out_reg[6]_0 ;
  wire [2:0]\txd_out_reg[6]_1 ;
  wire [2:0]\txd_out_reg[6]_2 ;
  wire \txd_out_reg[7] ;
  wire \txd_out_reg[7]_0 ;
  wire \txd_out_reg[7]_1 ;
  wire \txd_out_reg[7]_2 ;
  wire \txd_out_reg[7]_3 ;
  wire \txd_out_reg[8] ;
  wire \txd_out_reg[9] ;
  wire usrclk;
  wire usrclk_reset;

  LUT6 #(
    .INIT(64'hBAFFBABA8A008A8A)) 
    \count[0]_i_1 
       (.I0(\count_reg[4]_0 [0]),
        .I1(\txd_out_reg[54] ),
        .I2(\txd_out_reg[54]_0 ),
        .I3(Q),
        .I4(\txd_out_reg[31]_0 ),
        .I5(\prbs_reg[2] [0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h75FF757545004545)) 
    \count[1]_i_1 
       (.I0(\count_reg[4]_0 [1]),
        .I1(\txd_out_reg[54] ),
        .I2(\txd_out_reg[54]_0 ),
        .I3(Q),
        .I4(\txd_out_reg[31]_0 ),
        .I5(\prbs_reg[2] [1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT4 #(
    .INIT(16'hB0BB)) 
    \count[3]_i_3 
       (.I0(\txd_out_reg[54] ),
        .I1(\txd_out_reg[54]_0 ),
        .I2(Q),
        .I3(\txd_out_reg[31]_0 ),
        .O(\count_reg[4] ));
  LUT6 #(
    .INIT(64'hDDD0DDDD1110111D)) 
    next_ifg_is_a_i_1
       (.I0(\txd_out_reg[54]_0 ),
        .I1(next_ifg_is_a_i_2_n_0),
        .I2(Q),
        .I3(\txd_out_reg[31] ),
        .I4(\txd_out_reg[31]_0 ),
        .I5(next_ifg_is_a_reg_0),
        .O(next_ifg_is_a_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT2 #(
    .INIT(4'hE)) 
    next_ifg_is_a_i_2
       (.I0(\txd_out_reg[54] ),
        .I1(\txd_out_reg[54]_1 ),
        .O(next_ifg_is_a_i_2_n_0));
  FDSE next_ifg_is_a_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(next_ifg_is_a_i_1_n_0),
        .Q(next_ifg_is_a_reg_0),
        .S(usrclk_reset));
  LUT4 #(
    .INIT(16'h22F2)) 
    \prbs[7]_i_1 
       (.I0(\txd_out_reg[31]_0 ),
        .I1(Q),
        .I2(\txd_out_reg[54]_0 ),
        .I3(\txd_out_reg[54] ),
        .O(E));
  LUT6 #(
    .INIT(64'hFF0DFF0DFF0DFFFF)) 
    \state[0][0]_i_1 
       (.I0(\state[0][0]_i_2_n_0 ),
        .I1(\txd_out_reg[54]_0 ),
        .I2(\state[0][0]_i_3_n_0 ),
        .I3(\state[0][0]_i_4_n_0 ),
        .I4(\tx_is_q_reg[1] [0]),
        .I5(\tx_is_idle_reg[1] [0]),
        .O(\state[0][0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT5 #(
    .INIT(32'h0000DD0D)) 
    \state[0][0]_i_2 
       (.I0(\txd_out_reg[31]_0 ),
        .I1(Q),
        .I2(\txd_out_reg[54]_0 ),
        .I3(\txd_out_reg[54] ),
        .I4(extra_a_reg),
        .O(\state[0][0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT5 #(
    .INIT(32'hBF00FFFF)) 
    \state[0][0]_i_3 
       (.I0(\txd_out_reg[54] ),
        .I1(\txd_out_reg[54]_0 ),
        .I2(q_det),
        .I3(\prbs_reg[8] [1]),
        .I4(\txd_out_reg[54]_1 ),
        .O(\state[0][0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT5 #(
    .INIT(32'h00005400)) 
    \state[0][0]_i_4 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\tx_is_q_reg[1] [0]),
        .I2(q_det),
        .I3(\txd_out_reg[54]_0 ),
        .I4(\txd_out_reg[54] ),
        .O(\state[0][0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000EAAA)) 
    \state[0][1]_i_1 
       (.I0(\state[0][1]_i_2_n_0 ),
        .I1(\state[0][1]_i_3_n_0 ),
        .I2(q_det),
        .I3(\txd_out_reg[54]_1 ),
        .I4(\next_state[1]16_out ),
        .I5(usrclk_reset),
        .O(\state[0][1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00A833BB008833BB)) 
    \state[0][1]_i_2 
       (.I0(\state[0][0]_i_2_n_0 ),
        .I1(\txd_out_reg[54]_1 ),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(\state[0][1]_i_4_n_0 ),
        .I5(next_ifg_is_a_reg_0),
        .O(\state[0][1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \state[0][1]_i_3 
       (.I0(\txd_out_reg[54]_0 ),
        .I1(\txd_out_reg[54] ),
        .O(\state[0][1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT4 #(
    .INIT(16'hBBBF)) 
    \state[0][1]_i_4 
       (.I0(\txd_out_reg[54] ),
        .I1(\txd_out_reg[54]_0 ),
        .I2(q_det),
        .I3(\tx_is_q_reg[1] [0]),
        .O(\state[0][1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFFF999D)) 
    \state[0][2]_i_1 
       (.I0(\txd_out_reg[54] ),
        .I1(\txd_out_reg[54]_0 ),
        .I2(q_det),
        .I3(\tx_is_q_reg[1] [0]),
        .I4(\txd_out_reg[54]_1 ),
        .I5(\state[0][2]_i_2_n_0 ),
        .O(\state[0][2]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hAB)) 
    \state[0][2]_i_2 
       (.I0(usrclk_reset),
        .I1(\tx_is_q_reg[1] [0]),
        .I2(\tx_is_idle_reg[1] [0]),
        .O(\state[0][2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h111F11111F1F1F1F)) 
    \state[1][0]_i_1 
       (.I0(\tx_is_q_reg[1] [1]),
        .I1(\tx_is_idle_reg[1] [1]),
        .I2(\next_state[1]16_out ),
        .I3(\prbs_reg[8] [0]),
        .I4(\state[1][0]_i_3_n_0 ),
        .I5(\state[1][0]_i_4_n_0 ),
        .O(\get_next_state[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \state[1][0]_i_2 
       (.I0(\tx_is_idle_reg[1] [0]),
        .I1(\tx_is_q_reg[1] [0]),
        .O(\next_state[1]16_out ));
  LUT6 #(
    .INIT(64'h110011001100FFCF)) 
    \state[1][0]_i_3 
       (.I0(extra_a_reg),
        .I1(\txd_out_reg[54]_0 ),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_1 ),
        .I4(\state[0][0]_i_4_n_0 ),
        .I5(\count_reg[1] ),
        .O(\state[1][0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4055555555555555)) 
    \state[1][0]_i_4 
       (.I0(\state[1][2]_i_2_n_0 ),
        .I1(\txd_out_reg[54]_0 ),
        .I2(\txd_out_reg[54] ),
        .I3(q_det),
        .I4(\txd_out_reg[54]_1 ),
        .I5(\state[0][0]_i_2_n_0 ),
        .O(\state[1][0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hEEFEEEFEFFFFEEFE)) 
    \state[1][0]_i_7 
       (.I0(\count_reg[4]_0 [3]),
        .I1(\count_reg[4]_0 [2]),
        .I2(\txd_out_reg[31]_0 ),
        .I3(Q),
        .I4(\txd_out_reg[54]_0 ),
        .I5(\txd_out_reg[54] ),
        .O(\state_reg[1][1]_0 ));
  LUT6 #(
    .INIT(64'h0000000000AAAAEF)) 
    \state[1][1]_i_1 
       (.I0(\tx_is_idle_reg[0] ),
        .I1(\state[1][1]_i_3_n_0 ),
        .I2(\state[1][0]_i_4_n_0 ),
        .I3(\next_state[1]16_out ),
        .I4(\state[1][1]_i_4_n_0 ),
        .I5(usrclk_reset),
        .O(\state[1][1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222A222A222A222)) 
    \state[1][1]_i_3 
       (.I0(\count_reg[1] ),
        .I1(\state[1][1]_i_5_n_0 ),
        .I2(extra_a_reg),
        .I3(\txd_out_reg[54]_1 ),
        .I4(\txd_out_reg[54] ),
        .I5(\txd_out_reg[54]_0 ),
        .O(\state[1][1]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \state[1][1]_i_4 
       (.I0(\tx_is_idle_reg[1] [1]),
        .I1(\tx_is_q_reg[1] [1]),
        .O(\state[1][1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT5 #(
    .INIT(32'h00FEFFF0)) 
    \state[1][1]_i_5 
       (.I0(q_det),
        .I1(\tx_is_q_reg[1] [0]),
        .I2(\txd_out_reg[54]_1 ),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .O(\state[1][1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000054545400)) 
    \state[1][2]_i_1 
       (.I0(\state[1][2]_i_2_n_0 ),
        .I1(\tx_is_q_reg[1] [1]),
        .I2(\tx_is_idle_reg[1] [1]),
        .I3(\tx_is_idle_reg[1] [0]),
        .I4(\tx_is_q_reg[1] [0]),
        .I5(usrclk_reset),
        .O(\state[1][2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h88800000)) 
    \state[1][2]_i_2 
       (.I0(\state[0][0]_i_2_n_0 ),
        .I1(next_ifg_is_a_reg_0),
        .I2(q_det),
        .I3(\tx_is_q_reg[1] [1]),
        .I4(\txc_out_reg[7] ),
        .O(\state[1][2]_i_2_n_0 ));
  (* FSM_ENCODING = "one-hot" *) 
  FDRE \state_reg[0][0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state[0][0]_i_1_n_0 ),
        .Q(Q),
        .R(usrclk_reset));
  (* FSM_ENCODING = "one-hot" *) 
  FDRE \state_reg[0][1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state[0][1]_i_1_n_0 ),
        .Q(\txd_out_reg[31]_0 ),
        .R(1'b0));
  (* FSM_ENCODING = "one-hot" *) 
  FDRE \state_reg[0][2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state[0][2]_i_1_n_0 ),
        .Q(\txd_out_reg[31] ),
        .R(1'b0));
  (* FSM_ENCODING = "one-hot" *) 
  FDRE \state_reg[1][0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\get_next_state[1]_0 ),
        .Q(\txd_out_reg[54] ),
        .R(usrclk_reset));
  (* FSM_ENCODING = "one-hot" *) 
  FDRE \state_reg[1][1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state[1][1]_i_1_n_0 ),
        .Q(\txd_out_reg[54]_0 ),
        .R(1'b0));
  (* FSM_ENCODING = "one-hot" *) 
  FDRE \state_reg[1][2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\state[1][2]_i_1_n_0 ),
        .Q(\txd_out_reg[54]_1 ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \txc_out[3]_i_2 
       (.I0(Q),
        .I1(\txd_out_reg[31]_0 ),
        .O(\txc_out_reg[1] ));
  LUT6 #(
    .INIT(64'h8B8BFF8B8B8B008B)) 
    \txc_out[5]_i_1 
       (.I0(txc_filtered[0]),
        .I1(\txc_out_reg[7] ),
        .I2(\txd_out[59]_i_4_n_0 ),
        .I3(test_pattern_en),
        .I4(test_pattern_sel[0]),
        .I5(test_pattern_sel[1]),
        .O(\txc_out_reg[7]_0 [0]));
  LUT6 #(
    .INIT(64'h8B8BFF8B8B8B008B)) 
    \txc_out[6]_i_1 
       (.I0(txc_filtered[1]),
        .I1(\txc_out_reg[7] ),
        .I2(\txd_out[59]_i_4_n_0 ),
        .I3(test_pattern_en),
        .I4(test_pattern_sel[0]),
        .I5(test_pattern_sel[1]),
        .O(\txc_out_reg[7]_0 [1]));
  LUT6 #(
    .INIT(64'h8B8BFF8B8B8B008B)) 
    \txc_out[7]_i_2 
       (.I0(txc_filtered[2]),
        .I1(\txc_out_reg[7] ),
        .I2(\txd_out[59]_i_4_n_0 ),
        .I3(test_pattern_en),
        .I4(test_pattern_sel[0]),
        .I5(test_pattern_sel[1]),
        .O(\txc_out_reg[7]_0 [2]));
  LUT6 #(
    .INIT(64'hECECCCFCECECCCCC)) 
    \txd_out[0]_i_1 
       (.I0(last_qmsg[0]),
        .I1(test_en_reg_reg_0),
        .I2(Q),
        .I3(\txd_out_reg[31] ),
        .I4(\txd_out_reg[31]_0 ),
        .I5(txd_filtered[0]),
        .O(\txd_out_reg[0] ));
  LUT5 #(
    .INIT(32'hFF002020)) 
    \txd_out[10]_i_1 
       (.I0(Q),
        .I1(\txd_out_reg[31] ),
        .I2(data1[2]),
        .I3(last_qmsg[10]),
        .I4(\txd_out_reg[31]_0 ),
        .O(\txd_out_reg[10] ));
  LUT6 #(
    .INIT(64'hAA808080AAAAAAAA)) 
    \txd_out[11]_i_1 
       (.I0(\test_sel_reg_reg[0] ),
        .I1(\txd_out_reg[27] ),
        .I2(data1[3]),
        .I3(last_qmsg[11]),
        .I4(\txd_out_reg[31]_0 ),
        .I5(test_en_reg_reg_1),
        .O(\txd_out_reg[59] [0]));
  LUT5 #(
    .INIT(32'hFF002020)) 
    \txd_out[12]_i_1 
       (.I0(Q),
        .I1(\txd_out_reg[31] ),
        .I2(data1[4]),
        .I3(last_qmsg[12]),
        .I4(\txd_out_reg[31]_0 ),
        .O(\txd_out_reg[12] ));
  LUT5 #(
    .INIT(32'h880C8800)) 
    \txd_out[13]_i_1 
       (.I0(last_qmsg[13]),
        .I1(Q),
        .I2(\txd_out_reg[31] ),
        .I3(\txd_out_reg[31]_0 ),
        .I4(data1[5]),
        .O(\txd_out_reg[13] ));
  LUT6 #(
    .INIT(64'hEFEFCCFCEFEFCCCC)) 
    \txd_out[14]_i_1 
       (.I0(last_qmsg[14]),
        .I1(test_en_reg_reg),
        .I2(Q),
        .I3(\txd_out_reg[31] ),
        .I4(\txd_out_reg[31]_0 ),
        .I5(data1[6]),
        .O(\txd_out_reg[14] ));
  LUT5 #(
    .INIT(32'h808F8080)) 
    \txd_out[15]_i_1 
       (.I0(Q),
        .I1(last_qmsg[15]),
        .I2(\txd_out_reg[31]_0 ),
        .I3(\txd_out_reg[31] ),
        .I4(data1[7]),
        .O(\txd_out_reg[15] ));
  LUT6 #(
    .INIT(64'hECECCCFCECECCCCC)) 
    \txd_out[16]_i_1 
       (.I0(last_qmsg[16]),
        .I1(test_en_reg_reg_0),
        .I2(Q),
        .I3(\txd_out_reg[31] ),
        .I4(\txd_out_reg[31]_0 ),
        .I5(data1[8]),
        .O(\txd_out_reg[16] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[17]_i_1 
       (.I0(\txd_out_reg[31] ),
        .I1(data1[9]),
        .I2(Q),
        .I3(\txd_out_reg[31]_0 ),
        .I4(last_qmsg[17]),
        .O(\txd_out_reg[17] ));
  LUT5 #(
    .INIT(32'hFF002020)) 
    \txd_out[18]_i_1 
       (.I0(Q),
        .I1(\txd_out_reg[31] ),
        .I2(data1[10]),
        .I3(last_qmsg[18]),
        .I4(\txd_out_reg[31]_0 ),
        .O(\txd_out_reg[18] ));
  LUT6 #(
    .INIT(64'h00000000FEFF0EFF)) 
    \txd_out[19]_i_1 
       (.I0(\txd_out_reg[31] ),
        .I1(data1[11]),
        .I2(\txd_out_reg[31]_0 ),
        .I3(Q),
        .I4(last_qmsg[19]),
        .I5(test_en_reg_reg_0),
        .O(\txd_out_reg[19] ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[1]_i_1 
       (.I0(\txd_out_reg[31] ),
        .I1(txd_filtered[1]),
        .I2(Q),
        .I3(\txd_out_reg[31]_0 ),
        .I4(last_qmsg[1]),
        .O(\txd_out_reg[1] ));
  LUT5 #(
    .INIT(32'hFF002020)) 
    \txd_out[20]_i_1 
       (.I0(Q),
        .I1(\txd_out_reg[31] ),
        .I2(data1[12]),
        .I3(last_qmsg[20]),
        .I4(\txd_out_reg[31]_0 ),
        .O(\txd_out_reg[20] ));
  LUT5 #(
    .INIT(32'h880C8800)) 
    \txd_out[21]_i_1 
       (.I0(last_qmsg[21]),
        .I1(Q),
        .I2(\txd_out_reg[31] ),
        .I3(\txd_out_reg[31]_0 ),
        .I4(data1[13]),
        .O(\txd_out_reg[21] ));
  LUT6 #(
    .INIT(64'hEFEFAAFAEFEFAAAA)) 
    \txd_out[22]_i_1 
       (.I0(test_en_reg_reg),
        .I1(last_qmsg[22]),
        .I2(Q),
        .I3(\txd_out_reg[31] ),
        .I4(\txd_out_reg[31]_0 ),
        .I5(data1[14]),
        .O(\txd_out_reg[22] ));
  LUT5 #(
    .INIT(32'h808F8080)) 
    \txd_out[23]_i_1 
       (.I0(Q),
        .I1(last_qmsg[23]),
        .I2(\txd_out_reg[31]_0 ),
        .I3(\txd_out_reg[31] ),
        .I4(data1[15]),
        .O(\txd_out_reg[23] ));
  LUT6 #(
    .INIT(64'hECECCCFCECECCCCC)) 
    \txd_out[24]_i_1 
       (.I0(last_qmsg[24]),
        .I1(test_en_reg_reg_0),
        .I2(Q),
        .I3(\txd_out_reg[31] ),
        .I4(\txd_out_reg[31]_0 ),
        .I5(data1[16]),
        .O(\txd_out_reg[24] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[25]_i_1 
       (.I0(\txd_out_reg[31] ),
        .I1(data1[17]),
        .I2(Q),
        .I3(\txd_out_reg[31]_0 ),
        .I4(last_qmsg[25]),
        .O(\txd_out_reg[25] ));
  LUT5 #(
    .INIT(32'hFF002020)) 
    \txd_out[26]_i_1 
       (.I0(Q),
        .I1(\txd_out_reg[31] ),
        .I2(data1[18]),
        .I3(last_qmsg[26]),
        .I4(\txd_out_reg[31]_0 ),
        .O(\txd_out_reg[26] ));
  LUT6 #(
    .INIT(64'hAA808080AAAAAAAA)) 
    \txd_out[27]_i_1 
       (.I0(\test_sel_reg_reg[0] ),
        .I1(\txd_out_reg[27] ),
        .I2(data1[19]),
        .I3(last_qmsg[27]),
        .I4(\txd_out_reg[31]_0 ),
        .I5(test_en_reg_reg_1),
        .O(\txd_out_reg[59] [1]));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \txd_out[27]_i_2 
       (.I0(Q),
        .I1(\txd_out_reg[31] ),
        .I2(\txd_out_reg[31]_0 ),
        .O(\txd_out_reg[27] ));
  LUT5 #(
    .INIT(32'hFF002020)) 
    \txd_out[28]_i_2 
       (.I0(Q),
        .I1(\txd_out_reg[31] ),
        .I2(data1[20]),
        .I3(last_qmsg[28]),
        .I4(\txd_out_reg[31]_0 ),
        .O(\txd_out_reg[28] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[29]_i_2 
       (.I0(\txd_out_reg[31] ),
        .I1(data1[21]),
        .I2(Q),
        .I3(\txd_out_reg[31]_0 ),
        .I4(last_qmsg[29]),
        .O(\txd_out_reg[29] ));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT5 #(
    .INIT(32'hFF002020)) 
    \txd_out[2]_i_1 
       (.I0(Q),
        .I1(\txd_out_reg[31] ),
        .I2(txd_filtered[2]),
        .I3(last_qmsg[2]),
        .I4(\txd_out_reg[31]_0 ),
        .O(\txd_out_reg[2] ));
  LUT6 #(
    .INIT(64'hEFEFCCFCEFEFCCCC)) 
    \txd_out[30]_i_1 
       (.I0(last_qmsg[30]),
        .I1(\test_sel_reg_reg[1] ),
        .I2(Q),
        .I3(\txd_out_reg[31] ),
        .I4(\txd_out_reg[31]_0 ),
        .I5(data1[22]),
        .O(\txd_out_reg[30] ));
  LUT6 #(
    .INIT(64'h2121FF21FF21FF21)) 
    \txd_out[31]_i_1 
       (.I0(\txd_out_reg[31] ),
        .I1(\txd_out_reg[31]_0 ),
        .I2(Q),
        .I3(test_pattern_en),
        .I4(test_pattern_sel[0]),
        .I5(test_pattern_sel[1]),
        .O(\txd_out_reg[7] ));
  LUT5 #(
    .INIT(32'h808F8080)) 
    \txd_out[31]_i_2 
       (.I0(Q),
        .I1(last_qmsg[31]),
        .I2(\txd_out_reg[31]_0 ),
        .I3(\txd_out_reg[31] ),
        .I4(data1[23]),
        .O(\txd_out_reg[31]_1 ));
  LUT6 #(
    .INIT(64'hEECCCFCCEECCCCCC)) 
    \txd_out[32]_i_1 
       (.I0(last_qmsg[0]),
        .I1(test_en_reg_reg_0),
        .I2(\txd_out_reg[54]_1 ),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .I5(txd_filtered[8]),
        .O(\txd_out_reg[32] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[33]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(txd_filtered[9]),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[1]),
        .O(\txd_out_reg[33] ));
  LUT5 #(
    .INIT(32'hF0FFEEFF)) 
    \txd_out[34]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(txd_filtered[10]),
        .I2(last_qmsg[2]),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .O(\txd_out_reg[34] ));
  LUT6 #(
    .INIT(64'h00000000FEFF0EFF)) 
    \txd_out[35]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(txd_filtered[11]),
        .I2(\txd_out_reg[54]_0 ),
        .I3(\txd_out_reg[54] ),
        .I4(last_qmsg[3]),
        .I5(test_en_reg_reg_0),
        .O(\txd_out_reg[35] ));
  LUT5 #(
    .INIT(32'hF0FFEEFF)) 
    \txd_out[36]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(txd_filtered[12]),
        .I2(last_qmsg[4]),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .O(\txd_out_reg[36] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[37]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(txd_filtered[13]),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[5]),
        .O(\txd_out_reg[37] ));
  LUT6 #(
    .INIT(64'hEEFFCFCCEEFFCCCC)) 
    \txd_out[38]_i_1 
       (.I0(last_qmsg[6]),
        .I1(\test_sel_reg_reg[1] ),
        .I2(\txd_out_reg[54]_1 ),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .I5(txd_filtered[14]),
        .O(\txd_out_reg[38] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[39]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(txd_filtered[15]),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[7]),
        .O(\txd_out_reg[39]_0 ));
  LUT6 #(
    .INIT(64'h00000000FEFF0EFF)) 
    \txd_out[3]_i_1 
       (.I0(\txd_out_reg[31] ),
        .I1(txd_filtered[3]),
        .I2(\txd_out_reg[31]_0 ),
        .I3(Q),
        .I4(last_qmsg[3]),
        .I5(test_en_reg_reg_0),
        .O(\txd_out_reg[3] ));
  LUT6 #(
    .INIT(64'hEECCCFCCEECCCCCC)) 
    \txd_out[40]_i_1 
       (.I0(last_qmsg[8]),
        .I1(test_en_reg_reg_0),
        .I2(\txd_out_reg[54]_1 ),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .I5(\txd_out_reg[6]_0 [0]),
        .O(\txd_out_reg[40] ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[41]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[6]_0 [1]),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[9]),
        .O(\txd_out_reg[41] ));
  LUT5 #(
    .INIT(32'hF0FFEEFF)) 
    \txd_out[42]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[2]_0 ),
        .I2(last_qmsg[10]),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .O(\txd_out_reg[42] ));
  LUT6 #(
    .INIT(64'h00000000FEFF0EFF)) 
    \txd_out[43]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[3]_0 ),
        .I2(\txd_out_reg[54]_0 ),
        .I3(\txd_out_reg[54] ),
        .I4(last_qmsg[11]),
        .I5(test_en_reg_reg_0),
        .O(\txd_out_reg[43] ));
  LUT5 #(
    .INIT(32'hF0FFEEFF)) 
    \txd_out[44]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[4]_0 ),
        .I2(last_qmsg[12]),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .O(\txd_out_reg[44] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[45]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[5]_0 ),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[13]),
        .O(\txd_out_reg[45] ));
  LUT6 #(
    .INIT(64'hEEFFCFCCEEFFCCCC)) 
    \txd_out[46]_i_1 
       (.I0(last_qmsg[14]),
        .I1(test_en_reg_reg),
        .I2(\txd_out_reg[54]_1 ),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .I5(\txd_out_reg[6]_0 [2]),
        .O(\txd_out_reg[46] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[47]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[7]_1 ),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[15]),
        .O(\txd_out_reg[47] ));
  LUT6 #(
    .INIT(64'hEECCCFCCEECCCCCC)) 
    \txd_out[48]_i_1 
       (.I0(last_qmsg[16]),
        .I1(test_en_reg_reg_0),
        .I2(\txd_out_reg[54]_1 ),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .I5(\txd_out_reg[6]_2 [0]),
        .O(\txd_out_reg[48] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[49]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[6]_2 [1]),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[17]),
        .O(\txd_out_reg[49] ));
  LUT5 #(
    .INIT(32'hFF002020)) 
    \txd_out[4]_i_1 
       (.I0(Q),
        .I1(\txd_out_reg[31] ),
        .I2(txd_filtered[4]),
        .I3(last_qmsg[4]),
        .I4(\txd_out_reg[31]_0 ),
        .O(\txd_out_reg[4] ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT5 #(
    .INIT(32'hF0FFEEFF)) 
    \txd_out[50]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[2]_1 ),
        .I2(last_qmsg[18]),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .O(\txd_out_reg[50] ));
  LUT6 #(
    .INIT(64'h00000000FEFF0EFF)) 
    \txd_out[51]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[3]_1 ),
        .I2(\txd_out_reg[54]_0 ),
        .I3(\txd_out_reg[54] ),
        .I4(last_qmsg[19]),
        .I5(test_en_reg_reg_0),
        .O(\txd_out_reg[51] ));
  LUT5 #(
    .INIT(32'hF0FFEEFF)) 
    \txd_out[52]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[4]_1 ),
        .I2(last_qmsg[20]),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .O(\txd_out_reg[52] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[53]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[5]_1 ),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[21]),
        .O(\txd_out_reg[53] ));
  LUT6 #(
    .INIT(64'hEEFFCFCCEEFFCCCC)) 
    \txd_out[54]_i_1 
       (.I0(last_qmsg[22]),
        .I1(\test_sel_reg_reg[1] ),
        .I2(\txd_out_reg[54]_1 ),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .I5(\txd_out_reg[6]_2 [2]),
        .O(\txd_out_reg[54]_2 ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[55]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[7]_2 ),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[23]),
        .O(\txd_out_reg[55] ));
  LUT6 #(
    .INIT(64'hEECCCFCCEECCCCCC)) 
    \txd_out[56]_i_2 
       (.I0(last_qmsg[24]),
        .I1(test_en_reg_reg_0),
        .I2(\txd_out_reg[54]_1 ),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .I5(\txd_out_reg[6]_1 [0]),
        .O(\txd_out_reg[56] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[57]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[6]_1 [1]),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[25]),
        .O(\txd_out_reg[57] ));
  LUT5 #(
    .INIT(32'hF0FFEEFF)) 
    \txd_out[58]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[2]_2 ),
        .I2(last_qmsg[26]),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .O(\txd_out_reg[58] ));
  LUT6 #(
    .INIT(64'hAAAA8AAA0A0A8AAA)) 
    \txd_out[59]_i_1 
       (.I0(\test_sel_reg_reg[0] ),
        .I1(last_qmsg[27]),
        .I2(\test_sel_reg_reg[1]_0 ),
        .I3(\txd_out[59]_i_4_n_0 ),
        .I4(\txc_out_reg[7] ),
        .I5(\txd_out_reg[3]_2 ),
        .O(\txd_out_reg[59] [2]));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \txd_out[59]_i_4 
       (.I0(\txd_out_reg[54] ),
        .I1(\txd_out_reg[54]_0 ),
        .O(\txd_out[59]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \txd_out[59]_i_5 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[54] ),
        .I2(\txd_out_reg[54]_0 ),
        .O(\txc_out_reg[7] ));
  LUT5 #(
    .INIT(32'h880C8800)) 
    \txd_out[5]_i_1 
       (.I0(last_qmsg[5]),
        .I1(Q),
        .I2(\txd_out_reg[31] ),
        .I3(\txd_out_reg[31]_0 ),
        .I4(txd_filtered[5]),
        .O(\txd_out_reg[5] ));
  LUT5 #(
    .INIT(32'hF0FFEEFF)) 
    \txd_out[60]_i_2 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[4]_2 ),
        .I2(last_qmsg[28]),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .O(\txd_out_reg[60] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[61]_i_2 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[5]_2 ),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[29]),
        .O(\txd_out_reg[61] ));
  LUT6 #(
    .INIT(64'hEEFFAFAAEEFFAAAA)) 
    \txd_out[62]_i_2 
       (.I0(test_en_reg_reg),
        .I1(last_qmsg[30]),
        .I2(\txd_out_reg[54]_1 ),
        .I3(\txd_out_reg[54] ),
        .I4(\txd_out_reg[54]_0 ),
        .I5(\txd_out_reg[6]_1 [2]),
        .O(\txd_out_reg[62] ));
  LUT6 #(
    .INIT(64'h0909FF09FF09FF09)) 
    \txd_out[63]_i_1 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[54] ),
        .I2(\txd_out_reg[54]_0 ),
        .I3(test_pattern_en),
        .I4(test_pattern_sel[0]),
        .I5(test_pattern_sel[1]),
        .O(\txd_out_reg[39] ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[63]_i_2 
       (.I0(\txd_out_reg[54]_1 ),
        .I1(\txd_out_reg[7]_3 ),
        .I2(\txd_out_reg[54] ),
        .I3(\txd_out_reg[54]_0 ),
        .I4(last_qmsg[31]),
        .O(\txd_out_reg[63] ));
  LUT6 #(
    .INIT(64'hEFEFCCFCEFEFCCCC)) 
    \txd_out[6]_i_1 
       (.I0(last_qmsg[6]),
        .I1(test_en_reg_reg),
        .I2(Q),
        .I3(\txd_out_reg[31] ),
        .I4(\txd_out_reg[31]_0 ),
        .I5(txd_filtered[6]),
        .O(\txd_out_reg[6] ));
  LUT5 #(
    .INIT(32'h808F8080)) 
    \txd_out[7]_i_1 
       (.I0(Q),
        .I1(last_qmsg[7]),
        .I2(\txd_out_reg[31]_0 ),
        .I3(\txd_out_reg[31] ),
        .I4(txd_filtered[7]),
        .O(\txd_out_reg[7]_0 ));
  LUT6 #(
    .INIT(64'hECECCCFCECECCCCC)) 
    \txd_out[8]_i_1 
       (.I0(last_qmsg[8]),
        .I1(test_en_reg_reg_0),
        .I2(Q),
        .I3(\txd_out_reg[31] ),
        .I4(\txd_out_reg[31]_0 ),
        .I5(data1[0]),
        .O(\txd_out_reg[8] ));
  LUT5 #(
    .INIT(32'hF0400040)) 
    \txd_out[9]_i_1 
       (.I0(\txd_out_reg[31] ),
        .I1(data1[1]),
        .I2(Q),
        .I3(\txd_out_reg[31]_0 ),
        .I4(last_qmsg[9]),
        .O(\txd_out_reg[9] ));
endmodule

(* ORIG_REF_NAME = "xaui_gen" *) 
module rxaui_0_xaui_gen
   (mgt_enchansync,
    out,
    mdio_out,
    mdio_tri,
    sync_status,
    mgt_enable_align_i,
    mgt_loopback_r_reg,
    mgt_powerdown_r_reg,
    reset_reg_reg,
    mgt_txdata,
    mgt_txcharisk,
    xgmii_rxd,
    xgmii_rxc,
    usrclk,
    prtad,
    reset,
    \core_mgt_rx_reset_reg[0] ,
    mgt_rxlock,
    \core_mgt_rx_reset_reg[1] ,
    signal_detect,
    mgt_tx_reset,
    rx_local_fault0,
    xgmii_txd,
    xgmii_txc,
    D,
    \mgt_rxdata_reg_reg[24] ,
    \mgt_rxdata_reg_reg[31] ,
    \mgt_rxcharisk_reg_reg[3] ,
    \mgt_rxdata_reg_reg[24]_0 ,
    \mgt_rxdata_reg_reg[30] ,
    \mgt_rxdata_reg_reg[14] ,
    \mgt_codecomma_reg_reg[3] ,
    \mgt_codevalid_reg_reg[3] ,
    \mgt_codecomma_reg_reg[3]_0 ,
    \mgt_codevalid_reg_reg[3]_0 ,
    mdc,
    mdio_in,
    type_sel);
  output mgt_enchansync;
  output out;
  output mdio_out;
  output mdio_tri;
  output [3:0]sync_status;
  output [3:0]mgt_enable_align_i;
  output mgt_loopback_r_reg;
  output mgt_powerdown_r_reg;
  output reset_reg_reg;
  output [63:0]mgt_txdata;
  output [7:0]mgt_txcharisk;
  output [63:0]xgmii_rxd;
  output [7:0]xgmii_rxc;
  input usrclk;
  input [4:0]prtad;
  input reset;
  input \core_mgt_rx_reset_reg[0] ;
  input [0:0]mgt_rxlock;
  input \core_mgt_rx_reset_reg[1] ;
  input [1:0]signal_detect;
  input [0:0]mgt_tx_reset;
  input rx_local_fault0;
  input [63:0]xgmii_txd;
  input [7:0]xgmii_txc;
  input [1:0]D;
  input [1:0]\mgt_rxdata_reg_reg[24] ;
  input [63:0]\mgt_rxdata_reg_reg[31] ;
  input [7:0]\mgt_rxcharisk_reg_reg[3] ;
  input [7:0]\mgt_rxdata_reg_reg[24]_0 ;
  input [7:0]\mgt_rxdata_reg_reg[30] ;
  input [3:0]\mgt_rxdata_reg_reg[14] ;
  input [3:0]\mgt_codecomma_reg_reg[3] ;
  input [3:0]\mgt_codevalid_reg_reg[3] ;
  input [3:0]\mgt_codecomma_reg_reg[3]_0 ;
  input [3:0]\mgt_codevalid_reg_reg[3]_0 ;
  input mdc;
  input mdio_in;
  input [1:0]type_sel;

  wire [1:0]D;
  wire \G_HAS_MDIO.management_1_n_10 ;
  wire \G_HAS_MDIO.management_1_n_11 ;
  wire \G_HAS_MDIO.management_1_n_12 ;
  wire \G_HAS_MDIO.management_1_n_13 ;
  wire \G_HAS_MDIO.management_1_n_14 ;
  wire \G_HAS_MDIO.management_1_n_15 ;
  wire \G_HAS_MDIO.management_1_n_16 ;
  wire \G_HAS_MDIO.management_1_n_17 ;
  wire \G_HAS_MDIO.management_1_n_18 ;
  wire \G_HAS_MDIO.management_1_n_19 ;
  wire \G_HAS_MDIO.management_1_n_20 ;
  wire \G_HAS_MDIO.management_1_n_21 ;
  wire \G_HAS_MDIO.management_1_n_24 ;
  wire \G_HAS_MDIO.management_1_n_25 ;
  wire \G_HAS_MDIO.management_1_n_26 ;
  wire \G_HAS_MDIO.management_1_n_27 ;
  wire \G_HAS_MDIO.management_1_n_28 ;
  wire \G_HAS_MDIO.management_1_n_7 ;
  (* RTL_KEEP = "true" *) wire align_status_int;
  wire aligned_sticky;
  wire aligned_sticky_i_1_n_0;
  wire clear_aligned;
  wire clear_aligned_edge;
  wire clear_local_fault;
  wire clear_local_fault_edge;
  wire \core_mgt_rx_reset_reg[0] ;
  wire \core_mgt_rx_reset_reg[1] ;
  wire last_value;
  wire last_value_reg__0_n_0;
  wire mdc;
  wire mdio_in;
  wire mdio_out;
  wire mdio_tri;
  wire [3:0]\mgt_codecomma_reg_reg[3] ;
  wire [3:0]\mgt_codecomma_reg_reg[3]_0 ;
  wire [3:0]\mgt_codevalid_reg_reg[3] ;
  wire [3:0]\mgt_codevalid_reg_reg[3]_0 ;
  wire [3:0]mgt_enable_align_i;
  wire mgt_enchansync;
  wire mgt_loopback_r_reg;
  wire mgt_powerdown_r_reg;
  wire [7:0]\mgt_rxcharisk_reg_reg[3] ;
  wire [3:0]\mgt_rxdata_reg_reg[14] ;
  wire [1:0]\mgt_rxdata_reg_reg[24] ;
  wire [7:0]\mgt_rxdata_reg_reg[24]_0 ;
  wire [7:0]\mgt_rxdata_reg_reg[30] ;
  wire [63:0]\mgt_rxdata_reg_reg[31] ;
  wire [0:0]mgt_rxlock;
  wire [0:0]mgt_tx_reset;
  wire [7:0]mgt_txcharisk;
  wire [63:0]mgt_txdata;
  wire p_0_in;
  wire [4:0]prtad;
  wire reset;
  wire reset_int;
  wire reset_reg_reg;
  wire rx_local_fault;
  wire rx_local_fault0;
  wire rx_local_fault_i_1_n_0;
  wire [1:0]signal_detect;
  wire \signal_detect_int[0]_i_1_n_0 ;
  wire \signal_detect_int[2]_i_1_n_0 ;
  wire \signal_detect_int_reg_n_0_[0] ;
  wire \signal_detect_int_reg_n_0_[2] ;
  (* RTL_KEEP = "true" *) wire [3:0]sync;
  wire test_pattern_en;
  wire [1:0]test_pattern_sel;
  wire transmitter_n_10;
  wire transmitter_n_11;
  wire transmitter_n_12;
  wire transmitter_n_13;
  wire transmitter_n_5;
  wire transmitter_n_6;
  wire transmitter_n_7;
  wire transmitter_n_8;
  wire transmitter_n_9;
  wire tx_local_fault;
  wire tx_local_fault_i_1_n_0;
  wire [4:0]txc_filtered;
  wire [1:0]type_sel;
  wire \type_sel_reg[0]_i_1_n_0 ;
  wire \type_sel_reg[1]_i_1_n_0 ;
  wire type_sel_reg_done;
  wire \type_sel_reg_reg_n_0_[0] ;
  wire usrclk;
  wire usrclk_reset;
  wire usrclk_reset_pipe;
  wire [7:0]xgmii_rxc;
  wire [63:0]xgmii_rxd;
  wire [7:0]xgmii_txc;
  wire [63:0]xgmii_txd;

  assign out = align_status_int;
  assign sync_status[3:0] = sync;
  rxaui_0_management \G_HAS_MDIO.management_1 
       (.D({\G_HAS_MDIO.management_1_n_16 ,\G_HAS_MDIO.management_1_n_17 ,\G_HAS_MDIO.management_1_n_18 ,\G_HAS_MDIO.management_1_n_19 ,\G_HAS_MDIO.management_1_n_20 }),
        .Q(transmitter_n_5),
        .aligned_sticky(aligned_sticky),
        .clear_aligned(clear_aligned),
        .clear_aligned_edge_reg(\G_HAS_MDIO.management_1_n_13 ),
        .clear_local_fault(clear_local_fault),
        .clear_local_fault_edge_reg(\G_HAS_MDIO.management_1_n_12 ),
        .last_value(last_value),
        .last_value_reg__0(last_value_reg__0_n_0),
        .mdc(mdc),
        .mdio_in(mdio_in),
        .mdio_out(mdio_out),
        .mdio_tri(mdio_tri),
        .mgt_loopback_r_reg(mgt_loopback_r_reg),
        .mgt_powerdown_r_reg(mgt_powerdown_r_reg),
        .out(align_status_int),
        .p_0_in(p_0_in),
        .prtad(prtad),
        .reset(reset),
        .reset_int(reset_int),
        .reset_reg_reg_0(reset_reg_reg),
        .rx_local_fault(rx_local_fault),
        .\signal_detect_int_reg[2] ({\signal_detect_int_reg_n_0_[2] ,\signal_detect_int_reg_n_0_[0] }),
        .\state_reg[0][0] (transmitter_n_12),
        .\state_reg[0][0]_0 (transmitter_n_13),
        .\state_reg[0][1] (transmitter_n_7),
        .\state_reg[0][2] (transmitter_n_6),
        .\state_reg[1][0] (transmitter_n_8),
        .\state_reg[1][1] (transmitter_n_9),
        .\state_reg[1][2] (transmitter_n_10),
        .\state_reg[1][2]_0 (transmitter_n_11),
        .sync_status(sync),
        .test_pattern_en(test_pattern_en),
        .test_pattern_sel(test_pattern_sel),
        .tx_local_fault(tx_local_fault),
        .txc_filtered(txc_filtered),
        .\txc_out_reg[7] (\G_HAS_MDIO.management_1_n_10 ),
        .\txd_out_reg[27] (\G_HAS_MDIO.management_1_n_21 ),
        .\txd_out_reg[34] (\G_HAS_MDIO.management_1_n_24 ),
        .\txd_out_reg[37] (\G_HAS_MDIO.management_1_n_15 ),
        .\txd_out_reg[56] (\G_HAS_MDIO.management_1_n_7 ),
        .\txd_out_reg[59] (\G_HAS_MDIO.management_1_n_11 ),
        .\txd_out_reg[59]_0 (\G_HAS_MDIO.management_1_n_25 ),
        .\txd_out_reg[5] (\G_HAS_MDIO.management_1_n_14 ),
        .\txd_out_reg[62] (\G_HAS_MDIO.management_1_n_26 ),
        .\txd_out_reg[6] (\G_HAS_MDIO.management_1_n_27 ),
        .\type_sel_reg_reg[0] (\type_sel_reg_reg_n_0_[0] ),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset),
        .usrclk_reset_pipe(usrclk_reset_pipe),
        .usrclk_reset_reg(\G_HAS_MDIO.management_1_n_28 ));
  LUT4 #(
    .INIT(16'h00E0)) 
    aligned_sticky_i_1
       (.I0(aligned_sticky),
        .I1(clear_aligned_edge),
        .I2(align_status_int),
        .I3(usrclk_reset),
        .O(aligned_sticky_i_1_n_0));
  FDRE aligned_sticky_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(aligned_sticky_i_1_n_0),
        .Q(aligned_sticky),
        .R(1'b0));
  FDRE clear_aligned_edge_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(\G_HAS_MDIO.management_1_n_13 ),
        .Q(clear_aligned_edge),
        .R(usrclk_reset));
  FDRE clear_local_fault_edge_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(\G_HAS_MDIO.management_1_n_12 ),
        .Q(clear_local_fault_edge),
        .R(usrclk_reset));
  FDRE last_value_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(clear_local_fault),
        .Q(last_value),
        .R(usrclk_reset));
  FDRE last_value_reg__0
       (.C(usrclk),
        .CE(1'b1),
        .D(clear_aligned),
        .Q(last_value_reg__0_n_0),
        .R(usrclk_reset));
  rxaui_0_rx receiver
       (.D(D),
        .Q({\signal_detect_int_reg_n_0_[2] ,\signal_detect_int_reg_n_0_[0] }),
        .align_status_int(align_status_int),
        .\core_mgt_rx_reset_reg[0] (\core_mgt_rx_reset_reg[0] ),
        .\core_mgt_rx_reset_reg[1] (\core_mgt_rx_reset_reg[1] ),
        .in0(sync),
        .\mgt_codecomma_reg_reg[3] (\mgt_codecomma_reg_reg[3] ),
        .\mgt_codecomma_reg_reg[3]_0 (\mgt_codecomma_reg_reg[3]_0 ),
        .\mgt_codevalid_reg_reg[3] (\mgt_codevalid_reg_reg[3] ),
        .\mgt_codevalid_reg_reg[3]_0 (\mgt_codevalid_reg_reg[3]_0 ),
        .mgt_enable_align_i(mgt_enable_align_i),
        .mgt_enchansync(mgt_enchansync),
        .\mgt_rxcharisk_reg_reg[3] (\mgt_rxcharisk_reg_reg[3] ),
        .\mgt_rxdata_reg_reg[14] (\mgt_rxdata_reg_reg[14] ),
        .\mgt_rxdata_reg_reg[24] (\mgt_rxdata_reg_reg[24] ),
        .\mgt_rxdata_reg_reg[24]_0 (\mgt_rxdata_reg_reg[24]_0 ),
        .\mgt_rxdata_reg_reg[30] (\mgt_rxdata_reg_reg[30] ),
        .\mgt_rxdata_reg_reg[31] (\mgt_rxdata_reg_reg[31] ),
        .mgt_rxlock(mgt_rxlock),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset),
        .xgmii_rxc(xgmii_rxc),
        .xgmii_rxd(xgmii_rxd));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT3 #(
    .INIT(8'hDC)) 
    rx_local_fault_i_1
       (.I0(clear_local_fault_edge),
        .I1(rx_local_fault0),
        .I2(rx_local_fault),
        .O(rx_local_fault_i_1_n_0));
  FDRE rx_local_fault_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(rx_local_fault_i_1_n_0),
        .Q(rx_local_fault),
        .R(usrclk_reset));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \signal_detect_int[0]_i_1 
       (.I0(p_0_in),
        .I1(signal_detect[0]),
        .O(\signal_detect_int[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \signal_detect_int[2]_i_1 
       (.I0(p_0_in),
        .I1(signal_detect[1]),
        .O(\signal_detect_int[2]_i_1_n_0 ));
  FDRE \signal_detect_int_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\signal_detect_int[0]_i_1_n_0 ),
        .Q(\signal_detect_int_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \signal_detect_int_reg[2] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\signal_detect_int[2]_i_1_n_0 ),
        .Q(\signal_detect_int_reg_n_0_[2] ),
        .R(1'b0));
  rxaui_0_tx transmitter
       (.D({\G_HAS_MDIO.management_1_n_16 ,\G_HAS_MDIO.management_1_n_17 ,\G_HAS_MDIO.management_1_n_18 ,\G_HAS_MDIO.management_1_n_19 ,\G_HAS_MDIO.management_1_n_20 }),
        .Q(transmitter_n_5),
        .mgt_txcharisk(mgt_txcharisk),
        .mgt_txdata(mgt_txdata),
        .test_en_reg_reg(\G_HAS_MDIO.management_1_n_26 ),
        .test_en_reg_reg_0(\G_HAS_MDIO.management_1_n_27 ),
        .test_en_reg_reg_1(\G_HAS_MDIO.management_1_n_21 ),
        .test_en_reg_reg_2(\G_HAS_MDIO.management_1_n_24 ),
        .test_en_reg_reg_3(\G_HAS_MDIO.management_1_n_7 ),
        .test_pattern_en(test_pattern_en),
        .test_pattern_sel(test_pattern_sel),
        .\test_sel_reg_reg[0] (\G_HAS_MDIO.management_1_n_11 ),
        .\test_sel_reg_reg[1] (\G_HAS_MDIO.management_1_n_10 ),
        .\test_sel_reg_reg[1]_0 (\G_HAS_MDIO.management_1_n_25 ),
        .\test_sel_reg_reg[1]_1 (\G_HAS_MDIO.management_1_n_15 ),
        .\test_sel_reg_reg[1]_2 (\G_HAS_MDIO.management_1_n_14 ),
        .\txc_out_reg[1] (transmitter_n_13),
        .\txc_out_reg[4] (txc_filtered),
        .\txc_out_reg[7] (transmitter_n_11),
        .\txd_out_reg[27] (transmitter_n_12),
        .\txd_out_reg[31] (transmitter_n_6),
        .\txd_out_reg[31]_0 (transmitter_n_7),
        .\txd_out_reg[54] (transmitter_n_8),
        .\txd_out_reg[54]_0 (transmitter_n_9),
        .\txd_out_reg[54]_1 (transmitter_n_10),
        .usrclk(usrclk),
        .usrclk_reset(usrclk_reset),
        .xgmii_txc(xgmii_txc),
        .xgmii_txd(xgmii_txd));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    tx_local_fault_i_1
       (.I0(mgt_tx_reset),
        .I1(clear_local_fault_edge),
        .I2(tx_local_fault),
        .O(tx_local_fault_i_1_n_0));
  FDRE tx_local_fault_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(tx_local_fault_i_1_n_0),
        .Q(tx_local_fault),
        .R(usrclk_reset));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \type_sel_reg[0]_i_1 
       (.I0(type_sel[0]),
        .I1(type_sel_reg_done),
        .I2(\type_sel_reg_reg_n_0_[0] ),
        .O(\type_sel_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \type_sel_reg[1]_i_1 
       (.I0(type_sel[1]),
        .I1(type_sel_reg_done),
        .I2(p_0_in),
        .O(\type_sel_reg[1]_i_1_n_0 ));
  FDRE type_sel_reg_done_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(1'b1),
        .Q(type_sel_reg_done),
        .R(usrclk_reset));
  FDRE \type_sel_reg_reg[0] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\type_sel_reg[0]_i_1_n_0 ),
        .Q(\type_sel_reg_reg_n_0_[0] ),
        .R(usrclk_reset));
  FDRE \type_sel_reg_reg[1] 
       (.C(usrclk),
        .CE(1'b1),
        .D(\type_sel_reg[1]_i_1_n_0 ),
        .Q(p_0_in),
        .R(usrclk_reset));
  FDRE #(
    .INIT(1'b1)) 
    usrclk_reset_pipe_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(reset_int),
        .Q(usrclk_reset_pipe),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    usrclk_reset_reg
       (.C(usrclk),
        .CE(1'b1),
        .D(\G_HAS_MDIO.management_1_n_28 ),
        .Q(usrclk_reset),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
