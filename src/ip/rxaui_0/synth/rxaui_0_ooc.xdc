# This constraints file contains default clock frequencies to be used during creation of a
# Synthesis Design Checkpoint (DCP). For best results the frequencies should be modified
# to match the target frequencies. 
# This constraints file is not used in top-down/global synthesis (not the default flow of Vivado).
create_clock -period 10.00 [get_ports dclk]
set_property HD.CLK_SRC BUFGCTRL_X0Y0 [get_ports dclk] 
