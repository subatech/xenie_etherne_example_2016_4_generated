#######################################################################
# Clock frequencies and clock management                              #
#######################################################################

create_clock -period 6.40 [get_ports refclk_p]

create_clock -period 6.40 [get_pins -of_objects [get_cells * -hierarchical -filter {REF_NAME=~ GTXE2_CHANNEL && NAME =~ *gt0*}] -filter {NAME =~ *TXOUTCLK}]

set_false_path -to   [get_cells -hierarchical -filter {NAME =~ */rxaui_block_i/reset_count_done_sync_i/sync_r_reg[0]}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ */rxaui_block_i/plllocked_sync_i/sync_r_reg[0]}]


##################################################################
# MDIO-related constraints                                       #
##################################################################
# set a false path from the mdc_in and mdc_in inputs to the first
# stage of the sychronizer register
set_false_path -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdc_reg_reg[0]}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_in_reg_reg[0]}]

# Constrain the MDIO to 2.5MHz. If you wish to overclock the MDIO
# port you must alter this
set_max_delay 400.000 -from [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg[*]}] -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/*_reg[*]}]
set_max_delay 400.000 -from [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg[*]}] -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/*_reg}]
set_max_delay 400.000 -from [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg}] -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/*_reg[*]}]
set_max_delay 400.000 -from [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg}] -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/*_reg}]

set_max_delay 400.000 -from [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg[*]}] -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg[*]}]
set_max_delay 400.000 -from [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg[*]}] -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg}]
set_max_delay 400.000 -from [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg}] -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg[*]}]
set_max_delay 400.000 -from [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg}] -to [get_cells -hierarchical -filter {NAME =~ */rxaui_0_core/*management_1/mdio_interface_1/*_reg}]

