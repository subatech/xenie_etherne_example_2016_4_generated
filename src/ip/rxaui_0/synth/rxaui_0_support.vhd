-------------------------------------------------------------------------------
-- Title      : Support Module
-- Project    : RXAUI
-------------------------------------------------------------------------------
-- File       : rxaui_0_support.vhd
-------------------------------------------------------------------------------
-- Description: This module holds the support level for the RXAUI core
--              This can be used as-is in a single core design, or adapted
--              for use with multi-core implementations
-------------------------------------------------------------------------------
-- (c) Copyright 2009 - 2013 Xilinx, Inc. All rights reserved. 
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.

library ieee;
use ieee.std_logic_1164.all;

entity rxaui_0_support is
    port (
      reset                    : in  std_logic;                     -- Asynchronous Reset
      dclk                     : in  std_logic;                     -- Stable Clock
      clk156_out               : out std_logic;                     -- 156.25MHz output clock derived from the GT
      clk156_lock              : out  std_logic;                    -- 156.25MHz clock ready for use
      qplloutclk_out           : out std_logic;                     -- Clock from common PLL
      qplllock_out             : out std_logic;                     -- Lock from common PLL
      qplloutrefclk_out        : out std_logic;                     -- Refclk from common PLL
      refclk_out               : out std_logic;                     -- Refclk from differential GT clock buffer
      refclk_p                 : in  std_logic;                     -- Refclk P - to differential GT clock buffer
      refclk_n                 : in  std_logic;                     -- Refclk N - to differential GT clock buffer
      xgmii_txd                : in  std_logic_vector(63 downto 0); -- XGMII Tx Data
      xgmii_txc                : in  std_logic_vector(7 downto 0);  -- XGMII Tx Control
      xgmii_rxd                : out std_logic_vector(63 downto 0); -- XGMII Rx Data
      xgmii_rxc                : out std_logic_vector(7 downto 0);  -- XGMII Rx Control
      rxaui_tx_l0_p            : out std_logic;                     -- GT0 Tx P
      rxaui_tx_l0_n            : out std_logic;                     -- GT0 Tx N
      rxaui_tx_l1_p            : out std_logic;                     -- GT1 Tx P
      rxaui_tx_l1_n            : out std_logic;                     -- GT1 Tx P
      rxaui_rx_l0_p            : in  std_logic;                     -- GT0 Rx P
      rxaui_rx_l0_n            : in  std_logic;                     -- GT0 Rx N
      rxaui_rx_l1_p            : in  std_logic;                     -- GT1 Rx P
      rxaui_rx_l1_n            : in  std_logic;                     -- GT1 Rx N
      signal_detect            : in  std_logic_vector(1 downto 0);  -- Signal detect from optics
      debug                    : out std_logic_vector(5 downto 0);  -- Debug vector
   -- GT Control Ports
   -- The following signals are connected directly to/from the transceiver
   -- DRP
      gt0_drpaddr              : in  std_logic_vector(8 downto 0);
      gt0_drpen                : in  std_logic;
      gt0_drpdi                : in  std_logic_vector(15 downto 0);
      gt0_drpdo                : out std_logic_vector(15 downto 0);
      gt0_drprdy               : out std_logic;
      gt0_drpwe                : in  std_logic;
   -- TX Reset and Initialisation
      gt0_txpmareset_in        : in std_logic;
      gt0_txpcsreset_in        : in std_logic;
      gt0_txresetdone_out      : out std_logic;
   -- RX Reset and Initialisation
      gt0_rxpmareset_in        : in std_logic;
      gt0_rxpcsreset_in        : in std_logic;
      gt0_rxresetdone_out      : out std_logic;
   -- Clocking
      gt0_rxbufstatus_out      : out std_logic_vector(2 downto 0);
      gt0_txphaligndone_out    : out std_logic;
      gt0_txphinitdone_out     : out std_logic;
      gt0_txdlysresetdone_out  : out std_logic;
      gt_qplllock_out                : out std_logic;
   -- Signal Integrity adn Functionality
   -- Eye Scan
      gt0_eyescantrigger_in    : in  std_logic;
      gt0_eyescanreset_in      : in  std_logic;
      gt0_eyescandataerror_out : out std_logic;
      gt0_rxrate_in            : in  std_logic_vector(2 downto 0);
   -- Loopback
      gt0_loopback_in          : in  std_logic_vector(2 downto 0);
   -- Polarity
      gt0_rxpolarity_in        : in  std_logic;
      gt0_txpolarity_in        : in  std_logic;
   -- RX Decision Feedback Equalizer(DFE)
      gt0_rxlpmen_in           : in  std_logic;
      gt0_rxdfelpmreset_in     : in  std_logic;
      gt0_rxmonitorsel_in      : in  std_logic_vector(1 downto 0);
      gt0_rxmonitorout_out     : out std_logic_vector(6 downto 0);
   -- TX Driver
      gt0_txpostcursor_in      : in  std_logic_vector(4 downto 0);
      gt0_txprecursor_in       : in  std_logic_vector(4 downto 0);
      gt0_txdiffctrl_in        : in  std_logic_vector(3 downto 0);
      gt0_txinhibit_in         : in  std_logic;
   -- PRBS
      gt0_rxprbscntreset_in    : in  std_logic;
      gt0_rxprbserr_out        : out std_logic;
      gt0_rxprbssel_in         : in  std_logic_vector(2 downto 0);
      gt0_txprbssel_in         : in  std_logic_vector(2 downto 0);
      gt0_txprbsforceerr_in    : in  std_logic;

      gt0_rxcdrhold_in         : in  std_logic;
      gt0_dmonitorout_out      : out std_logic_vector(7 downto 0);

   -- Status
      gt0_rxdisperr_out        : out std_logic_vector(3 downto 0);
      gt0_rxnotintable_out     : out std_logic_vector(3 downto 0);
      gt0_rxcommadet_out       : out std_logic;
   -- DRP
      gt1_drpaddr              : in  std_logic_vector(8 downto 0);
      gt1_drpen                : in  std_logic;
      gt1_drpdi                : in  std_logic_vector(15 downto 0);
      gt1_drpdo                : out std_logic_vector(15 downto 0);
      gt1_drprdy               : out std_logic;
      gt1_drpwe                : in  std_logic;
   -- TX Reset and Initialisation
      gt1_txpmareset_in        : in std_logic;
      gt1_txpcsreset_in        : in std_logic;
      gt1_txresetdone_out      : out std_logic;
   -- RX Reset and Initialisation
      gt1_rxpmareset_in        : in std_logic;
      gt1_rxpcsreset_in        : in std_logic;
      gt1_rxresetdone_out      : out std_logic;
   -- Clocking
      gt1_rxbufstatus_out      : out std_logic_vector(2 downto 0);
      gt1_txphaligndone_out    : out std_logic;
      gt1_txphinitdone_out     : out std_logic;
      gt1_txdlysresetdone_out  : out std_logic;
   -- Signal Integrity adn Functionality
   -- Eye Scan
      gt1_eyescantrigger_in    : in  std_logic;
      gt1_eyescanreset_in      : in  std_logic;
      gt1_eyescandataerror_out : out std_logic;
      gt1_rxrate_in            : in  std_logic_vector(2 downto 0);
   -- Loopback
      gt1_loopback_in          : in  std_logic_vector(2 downto 0);
   -- Polarity
      gt1_rxpolarity_in        : in  std_logic;
      gt1_txpolarity_in        : in  std_logic;
   -- RX Decision Feedback Equalizer(DFE)
      gt1_rxlpmen_in           : in  std_logic;
      gt1_rxdfelpmreset_in     : in  std_logic;
      gt1_rxmonitorsel_in      : in  std_logic_vector(1 downto 0);
      gt1_rxmonitorout_out     : out std_logic_vector(6 downto 0);
   -- TX Driver
      gt1_txpostcursor_in      : in  std_logic_vector(4 downto 0);
      gt1_txprecursor_in       : in  std_logic_vector(4 downto 0);
      gt1_txdiffctrl_in        : in  std_logic_vector(3 downto 0);
      gt1_txinhibit_in         : in  std_logic;
   -- PRBS
      gt1_rxprbscntreset_in    : in  std_logic;
      gt1_rxprbserr_out        : out std_logic;
      gt1_rxprbssel_in         : in  std_logic_vector(2 downto 0);
      gt1_txprbssel_in         : in  std_logic_vector(2 downto 0);
      gt1_txprbsforceerr_in    : in  std_logic;

      gt1_rxcdrhold_in         : in  std_logic;
      gt1_dmonitorout_out      : out std_logic_vector(7 downto 0);

   -- Status
      gt1_rxdisperr_out        : out std_logic_vector(3 downto 0);
      gt1_rxnotintable_out     : out std_logic_vector(3 downto 0);
      gt1_rxcommadet_out       : out std_logic;
      mdc                      : in  std_logic;                    -- MDIO Clock
      mdio_in                  : in  std_logic;                    -- MDIO input
      mdio_out                 : out std_logic;                    -- MDIO Output
      mdio_tri                 : out std_logic;                    -- MDIO tri-state enable
      prtad                    : in  std_logic_vector(4 downto 0); -- MDIO PRTAD
      type_sel                 : in  std_logic_vector(1 downto 0)  -- type_sel control
);
end rxaui_0_support;

library ieee;
use ieee.numeric_std.all;

architecture wrapper of rxaui_0_support is

----------------------------------------------------------------------------
-- Component Declarations
----------------------------------------------------------------------------

 component rxaui_0_support_clocking is
    port (
      refclk_p                 : in  std_logic;
      refclk_n                 : in  std_logic;
      refclk                   : out std_logic
      );
 end component;

 component rxaui_0_support_resets is
    port (
      reset                    : in  std_logic;
      dclk                     : in  std_logic;
      common_pll_reset         : out std_logic
      );
 end component;

 component rxaui_0_gt_common_wrapper is
   generic (
    SIM_RESET_SPEEDUP          : string := "false" -- Set to "true" to speed up sim reset
   );
   port
  (
    GTREFCLK0_IN               : in std_logic;
    QPLLLOCK_OUT               : out std_logic;
    QPLLLOCKDETCLK_IN          : in std_logic;
    QPLLOUTCLK_OUT             : out std_logic;
    QPLLOUTREFCLK_OUT          : out std_logic;
    QPLLREFCLKLOST_OUT         : out std_logic;
    QPLLRESET_IN               : in std_logic
  );
 end component;

 component rxaui_0_block is
    port (
      reset                    : in  std_logic;
      dclk                     : in  std_logic;
      clk156_out               : out std_logic;
      clk156_lock              : out  std_logic;
      refclk                   : in  std_logic;
      qplloutclk               : in  std_logic;
      qplllock                 : in  std_logic;
      qplloutrefclk            : in  std_logic;
      xgmii_txd                : in  std_logic_vector(63 downto 0);
      xgmii_txc                : in  std_logic_vector(7 downto 0);
      xgmii_rxd                : out std_logic_vector(63 downto 0);
      xgmii_rxc                : out std_logic_vector(7 downto 0);
      rxaui_tx_l0_p            : out std_logic;
      rxaui_tx_l0_n            : out std_logic;
      rxaui_tx_l1_p            : out std_logic;
      rxaui_tx_l1_n            : out std_logic;
      rxaui_rx_l0_p            : in  std_logic;
      rxaui_rx_l0_n            : in  std_logic;
      rxaui_rx_l1_p            : in  std_logic;
      rxaui_rx_l1_n            : in  std_logic;
      signal_detect            : in  std_logic_vector(1 downto 0);
      debug                    : out std_logic_vector(5 downto 0);
   -- GT Control Ports
   -- DRP
      gt0_drpaddr              : in  std_logic_vector(8 downto 0);
      gt0_drpen                : in  std_logic;
      gt0_drpdi                : in  std_logic_vector(15 downto 0);
      gt0_drpdo                : out std_logic_vector(15 downto 0);
      gt0_drprdy               : out std_logic;
      gt0_drpwe                : in  std_logic;
   -- TX Reset and Initialisation
      gt0_txpmareset_in        : in std_logic;
      gt0_txpcsreset_in        : in std_logic;
      gt0_txresetdone_out      : out std_logic;
   -- RX Reset and Initialisation
      gt0_rxpmareset_in        : in std_logic;
      gt0_rxpcsreset_in        : in std_logic;
      gt0_rxresetdone_out      : out std_logic;
   -- Clocking
      gt0_rxbufstatus_out      : out std_logic_vector(2 downto 0);
      gt0_txphaligndone_out    : out std_logic;
      gt0_txphinitdone_out     : out std_logic;
      gt0_txdlysresetdone_out  : out std_logic;
      gt_qplllock_out                : out std_logic;
   -- Signal Integrity adn Functionality
   -- Eye Scan
      gt0_eyescantrigger_in    : in  std_logic;
      gt0_eyescanreset_in      : in  std_logic;
      gt0_eyescandataerror_out : out std_logic;
      gt0_rxrate_in            : in  std_logic_vector(2 downto 0);
   -- Loopback
      gt0_loopback_in          : in  std_logic_vector(2 downto 0);
   -- Polarity
      gt0_rxpolarity_in        : in  std_logic;
      gt0_txpolarity_in        : in  std_logic;
   -- RX Decision Feedback Equalizer(DFE)
      gt0_rxlpmen_in           : in  std_logic;
      gt0_rxdfelpmreset_in     : in  std_logic;
      gt0_rxmonitorsel_in      : in  std_logic_vector(1 downto 0);
      gt0_rxmonitorout_out     : out std_logic_vector(6 downto 0);
   -- TX Driver
      gt0_txpostcursor_in      : in  std_logic_vector(4 downto 0);
      gt0_txprecursor_in       : in  std_logic_vector(4 downto 0);
      gt0_txdiffctrl_in        : in  std_logic_vector(3 downto 0);
      gt0_txinhibit_in         : in  std_logic;
   -- PRBS
      gt0_rxprbscntreset_in    : in  std_logic;
      gt0_rxprbserr_out        : out std_logic;
      gt0_rxprbssel_in         : in  std_logic_vector(2 downto 0);
      gt0_txprbssel_in         : in  std_logic_vector(2 downto 0);
      gt0_txprbsforceerr_in    : in  std_logic;

      gt0_rxcdrhold_in         : in  std_logic;
      gt0_dmonitorout_out      : out std_logic_vector(7 downto 0);

   -- Status
      gt0_rxdisperr_out        : out std_logic_vector(3 downto 0);
      gt0_rxnotintable_out     : out std_logic_vector(3 downto 0);
      gt0_rxcommadet_out       : out std_logic;
   -- DRP
      gt1_drpaddr              : in  std_logic_vector(8 downto 0);
      gt1_drpen                : in  std_logic;
      gt1_drpdi                : in  std_logic_vector(15 downto 0);
      gt1_drpdo                : out std_logic_vector(15 downto 0);
      gt1_drprdy               : out std_logic;
      gt1_drpwe                : in  std_logic;
   -- TX Reset and Initialisation
      gt1_txpmareset_in        : in std_logic;
      gt1_txpcsreset_in        : in std_logic;
      gt1_txresetdone_out      : out std_logic;
   -- RX Reset and Initialisation
      gt1_rxpmareset_in        : in std_logic;
      gt1_rxpcsreset_in        : in std_logic;
      gt1_rxresetdone_out      : out std_logic;
   -- Clocking
      gt1_rxbufstatus_out      : out std_logic_vector(2 downto 0);
      gt1_txphaligndone_out    : out std_logic;
      gt1_txphinitdone_out     : out std_logic;
      gt1_txdlysresetdone_out  : out std_logic;
   -- Signal Integrity adn Functionality
   -- Eye Scan
      gt1_eyescantrigger_in    : in  std_logic;
      gt1_eyescanreset_in      : in  std_logic;
      gt1_eyescandataerror_out : out std_logic;
      gt1_rxrate_in            : in  std_logic_vector(2 downto 0);
   -- Loopback
      gt1_loopback_in          : in  std_logic_vector(2 downto 0);
   -- Polarity
      gt1_rxpolarity_in        : in  std_logic;
      gt1_txpolarity_in        : in  std_logic;
   -- RX Decision Feedback Equalizer(DFE)
      gt1_rxlpmen_in           : in  std_logic;
      gt1_rxdfelpmreset_in     : in  std_logic;
      gt1_rxmonitorsel_in      : in  std_logic_vector(1 downto 0);
      gt1_rxmonitorout_out     : out std_logic_vector(6 downto 0);
   -- TX Driver
      gt1_txpostcursor_in      : in  std_logic_vector(4 downto 0);
      gt1_txprecursor_in       : in  std_logic_vector(4 downto 0);
      gt1_txdiffctrl_in        : in  std_logic_vector(3 downto 0);
      gt1_txinhibit_in         : in  std_logic;
   -- PRBS
      gt1_rxprbscntreset_in    : in  std_logic;
      gt1_rxprbserr_out        : out std_logic;
      gt1_rxprbssel_in         : in  std_logic_vector(2 downto 0);
      gt1_txprbssel_in         : in  std_logic_vector(2 downto 0);
      gt1_txprbsforceerr_in    : in  std_logic;

      gt1_rxcdrhold_in         : in  std_logic;
      gt1_dmonitorout_out      : out std_logic_vector(7 downto 0);

   -- Status
      gt1_rxdisperr_out        : out std_logic_vector(3 downto 0);
      gt1_rxnotintable_out     : out std_logic_vector(3 downto 0);
      gt1_rxcommadet_out       : out std_logic;
      mdc                      : in  std_logic;
      mdio_in                  : in  std_logic;
      mdio_out                 : out std_logic;
      mdio_tri                 : out std_logic;
      prtad                    : in  std_logic_vector(4 downto 0);
      type_sel                 : in  std_logic_vector(1 downto 0)
);
end component;

----------------------------------------------------------------------------
-- Signal declarations.
----------------------------------------------------------------------------
  signal refclk                : std_logic;

  signal qplloutclk_i          : std_logic;
  signal qplllock_i            : std_logic;
  signal qplloutrefclk_i       : std_logic;
  signal common_pll_reset_i    : std_logic;

begin

  rxaui_block_i : rxaui_0_block
    port map (
      reset                    => reset,
      dclk                     => dclk,
      clk156_out               => clk156_out,
      clk156_lock              => clk156_lock,
      refclk                   => refclk,
      qplloutclk               => qplloutclk_i,
      qplllock                 => qplllock_i,
      qplloutrefclk            => qplloutrefclk_i,
      xgmii_txd                => xgmii_txd,
      xgmii_txc                => xgmii_txc,
      xgmii_rxd                => xgmii_rxd,
      xgmii_rxc                => xgmii_rxc,
      rxaui_tx_l0_p            => rxaui_tx_l0_p,
      rxaui_tx_l0_n            => rxaui_tx_l0_n,
      rxaui_tx_l1_p            => rxaui_tx_l1_p,
      rxaui_tx_l1_n            => rxaui_tx_l1_n,
      rxaui_rx_l0_p            => rxaui_rx_l0_p,
      rxaui_rx_l0_n            => rxaui_rx_l0_n,
      rxaui_rx_l1_p            => rxaui_rx_l1_p,
      rxaui_rx_l1_n            => rxaui_rx_l1_n,
      signal_detect            => signal_detect,
      debug                    => debug,
   -- GT Control Ports
   -- DRP
      gt0_drpaddr              => gt0_drpaddr,
      gt0_drpen                => gt0_drpen,
      gt0_drpdi                => gt0_drpdi,
      gt0_drpdo                => gt0_drpdo,
      gt0_drprdy               => gt0_drprdy,
      gt0_drpwe                => gt0_drpwe,
   -- TX Reset and Initialisation
      gt0_txpmareset_in        => gt0_txpmareset_in,
      gt0_txpcsreset_in        => gt0_txpcsreset_in,
      gt0_txresetdone_out      => gt0_txresetdone_out,
   -- RX Reset and Initialisation
      gt0_rxpmareset_in        => gt0_rxpmareset_in,
      gt0_rxpcsreset_in        => gt0_rxpcsreset_in,
      gt0_rxresetdone_out      => gt0_rxresetdone_out,
   -- Clocking
      gt0_rxbufstatus_out      => gt0_rxbufstatus_out,
      gt0_txphaligndone_out    => gt0_txphaligndone_out,
      gt0_txphinitdone_out     => gt0_txphinitdone_out,
      gt0_txdlysresetdone_out  => gt0_txdlysresetdone_out,
      gt_qplllock_out                => gt_qplllock_out,
   -- Signal Integrity adn Functionality
   -- Eye Scan
      gt0_eyescantrigger_in    => gt0_eyescantrigger_in,
      gt0_eyescanreset_in      => gt0_eyescanreset_in,
      gt0_eyescandataerror_out => gt0_eyescandataerror_out,
      gt0_rxrate_in            => gt0_rxrate_in,
   -- Loopback
      gt0_loopback_in          => gt0_loopback_in,
   -- Polarity
      gt0_rxpolarity_in        => gt0_rxpolarity_in,
      gt0_txpolarity_in        => gt0_txpolarity_in,
   -- RX Decision Feedback Equalizer(DFE)
      gt0_rxlpmen_in           => gt0_rxlpmen_in,
      gt0_rxdfelpmreset_in     => gt0_rxdfelpmreset_in,
      gt0_rxmonitorsel_in      => gt0_rxmonitorsel_in,
      gt0_rxmonitorout_out     => gt0_rxmonitorout_out,
   -- TX Driver
      gt0_txpostcursor_in      => gt0_txpostcursor_in,
      gt0_txprecursor_in       => gt0_txprecursor_in,
      gt0_txdiffctrl_in        => gt0_txdiffctrl_in,
      gt0_txinhibit_in         => gt0_txinhibit_in,
   -- PRBS
      gt0_rxprbscntreset_in    => gt0_rxprbscntreset_in,
      gt0_rxprbserr_out        => gt0_rxprbserr_out,
      gt0_rxprbssel_in         => gt0_rxprbssel_in,
      gt0_txprbssel_in         => gt0_txprbssel_in,
      gt0_txprbsforceerr_in    => gt0_txprbsforceerr_in,

      gt0_rxcdrhold_in         => gt0_rxcdrhold_in,
      gt0_dmonitorout_out      => gt0_dmonitorout_out,

   -- Status
      gt0_rxdisperr_out        => gt0_rxdisperr_out,
      gt0_rxnotintable_out     => gt0_rxnotintable_out,
      gt0_rxcommadet_out       => gt0_rxcommadet_out,
   -- DRP
      gt1_drpaddr              => gt1_drpaddr,
      gt1_drpen                => gt1_drpen,
      gt1_drpdi                => gt1_drpdi,
      gt1_drpdo                => gt1_drpdo,
      gt1_drprdy               => gt1_drprdy,
      gt1_drpwe                => gt1_drpwe,
   -- TX Reset and Initialisation
      gt1_txpmareset_in        => gt1_txpmareset_in,
      gt1_txpcsreset_in        => gt1_txpcsreset_in,
      gt1_txresetdone_out      => gt1_txresetdone_out,
   -- RX Reset and Initialisation
      gt1_rxpmareset_in        => gt1_rxpmareset_in,
      gt1_rxpcsreset_in        => gt1_rxpcsreset_in,
      gt1_rxresetdone_out      => gt1_rxresetdone_out,
   -- Clocking
      gt1_rxbufstatus_out      => gt1_rxbufstatus_out,
      gt1_txphaligndone_out    => gt1_txphaligndone_out,
      gt1_txphinitdone_out     => gt1_txphinitdone_out,
      gt1_txdlysresetdone_out  => gt1_txdlysresetdone_out,
   -- Signal Integrity adn Functionality
   -- Eye Scan
      gt1_eyescantrigger_in    => gt1_eyescantrigger_in,
      gt1_eyescanreset_in      => gt1_eyescanreset_in,
      gt1_eyescandataerror_out => gt1_eyescandataerror_out,
      gt1_rxrate_in            => gt1_rxrate_in,
   -- Loopback
      gt1_loopback_in          => gt1_loopback_in,
   -- Polarity
      gt1_rxpolarity_in        => gt1_rxpolarity_in,
      gt1_txpolarity_in        => gt1_txpolarity_in,
   -- RX Decision Feedback Equalizer(DFE)
      gt1_rxlpmen_in           => gt1_rxlpmen_in,
      gt1_rxdfelpmreset_in     => gt1_rxdfelpmreset_in,
      gt1_rxmonitorsel_in      => gt1_rxmonitorsel_in,
      gt1_rxmonitorout_out     => gt1_rxmonitorout_out,
   -- TX Driver
      gt1_txpostcursor_in      => gt1_txpostcursor_in,
      gt1_txprecursor_in       => gt1_txprecursor_in,
      gt1_txdiffctrl_in        => gt1_txdiffctrl_in,
      gt1_txinhibit_in         => gt1_txinhibit_in,
   -- PRBS
      gt1_rxprbscntreset_in    => gt1_rxprbscntreset_in,
      gt1_rxprbserr_out        => gt1_rxprbserr_out,
      gt1_rxprbssel_in         => gt1_rxprbssel_in,
      gt1_txprbssel_in         => gt1_txprbssel_in,
      gt1_txprbsforceerr_in    => gt1_txprbsforceerr_in,

      gt1_rxcdrhold_in         => gt1_rxcdrhold_in,
      gt1_dmonitorout_out      => gt1_dmonitorout_out,

   -- Status
      gt1_rxdisperr_out        => gt1_rxdisperr_out,
      gt1_rxnotintable_out     => gt1_rxnotintable_out,
      gt1_rxcommadet_out       => gt1_rxcommadet_out,
      mdc                      => mdc,
      mdio_in                  => mdio_in,
      mdio_out                 => mdio_out,
      mdio_tri                 => mdio_tri,
      prtad                    => prtad,
      type_sel                 => type_sel);

  rxaui_support_clocking_i : rxaui_0_support_clocking
    port map (
      refclk_p                 => refclk_p,
      refclk_n                 => refclk_n,
      refclk                   => refclk
      );

  rxaui_support_resets_i : rxaui_0_support_resets
    port map (
      reset                    => reset,
      dclk                     => dclk,
      common_pll_reset         => common_pll_reset_i
      );

  rxaui_gt_common_i : rxaui_0_gt_common_wrapper
    generic map(
     SIM_RESET_SPEEDUP         => "TRUE")
    port map(
     GTREFCLK0_IN => refclk,
     QPLLLOCK_OUT              => qplllock_i,
     QPLLLOCKDETCLK_IN         => dclk,
     QPLLOUTCLK_OUT            => qplloutclk_i,
     QPLLOUTREFCLK_OUT         => qplloutrefclk_i,
     QPLLREFCLKLOST_OUT        => open,
     QPLLRESET_IN              => common_pll_reset_i
  );

  qplllock_out      <= qplllock_i;
  qplloutclk_out    <= qplloutclk_i;
  qplloutrefclk_out <= qplloutrefclk_i;
  refclk_out        <= refclk;

end wrapper;
