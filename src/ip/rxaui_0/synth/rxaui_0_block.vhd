-------------------------------------------------------------------------------
-- Title      : Block level
-- Project    : RXAUI
-------------------------------------------------------------------------------
-- File       : rxaui_0_block.vhd
-------------------------------------------------------------------------------
-- Description: This file is a wrapper for the RXAUI core. It contains the
-- RXAUI core, the transceivers and some transceiver logic.
-------------------------------------------------------------------------------
-- (c) Copyright 2009 - 2013 Xilinx, Inc. All rights reserved. 
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rxaui_0_block is
    port (
      reset                                 : in  std_logic; -- Asynchronous Reset
      dclk                                  : in  std_logic; -- Stable Clock
      clk156_out                            : out std_logic; -- 156.25MHz output clock derived from the GT
      clk156_lock                           : out std_logic; -- 156.25MHz clock ready for use
      refclk                                : in  std_logic; -- Refclk from IBUDS_GTE2
      qplloutclk                            : in  std_logic; -- output clock from common PLL
      qplllock                              : in  std_logic; -- locked indicator from common PLL
      qplloutrefclk                         : in  std_logic; -- refclk from common PLL
      xgmii_txd                             : in  std_logic_vector(63 downto 0); -- XGMII Tx Data
      xgmii_txc                             : in  std_logic_vector(7 downto 0);  -- XGMII Tx Control
      xgmii_rxd                             : out std_logic_vector(63 downto 0); -- XGMII Rx Data
      xgmii_rxc                             : out std_logic_vector(7 downto 0);  -- XGMII Rx Control
      rxaui_tx_l0_p                         : out std_logic; -- GT0 TXP
      rxaui_tx_l0_n                         : out std_logic; -- GT0 TXN
      rxaui_tx_l1_p                         : out std_logic; -- GT1 TXP
      rxaui_tx_l1_n                         : out std_logic; -- GT1 TXN
      rxaui_rx_l0_p                         : in  std_logic; -- GT0 RXP
      rxaui_rx_l0_n                         : in  std_logic; -- GT0 RXN
      rxaui_rx_l1_p                         : in  std_logic; -- GT1 RXP
      rxaui_rx_l1_n                         : in  std_logic; -- GT1 RXN
      signal_detect                         : in  std_logic_vector(1 downto 0); -- Signal Detect from optical interface
      debug                                 : out std_logic_vector(5 downto 0); -- Debug vector
   -- GT Control Ports
   -- The following signals are connected directly to/from the transceiver
   -- DRP
      gt0_drpaddr                           : in  std_logic_vector(8 downto 0);
      gt0_drpen                             : in  std_logic;
      gt0_drpdi                             : in  std_logic_vector(15 downto 0);
      gt0_drpdo                             : out std_logic_vector(15 downto 0);
      gt0_drprdy                            : out std_logic;
      gt0_drpwe                             : in  std_logic;
   -- TX Reset and Initialisation
      gt0_txpmareset_in                     : in std_logic;
      gt0_txpcsreset_in                     : in std_logic;
      gt0_txresetdone_out                   : out std_logic;
   -- RX Reset and Initialisation
      gt0_rxpmareset_in                     : in std_logic;
      gt0_rxpcsreset_in                     : in std_logic;
      gt0_rxresetdone_out                   : out std_logic;
   -- Clocking
      gt0_rxbufstatus_out                   : out std_logic_vector(2 downto 0);
      gt0_txphaligndone_out                 : out std_logic;
      gt0_txphinitdone_out                  : out std_logic;
      gt0_txdlysresetdone_out               : out std_logic;
      gt_qplllock_out                             : out std_logic;
   -- Signal Integrity adn Functionality
   -- Eye Scan
      gt0_eyescantrigger_in                 : in  std_logic;
      gt0_eyescanreset_in                   : in  std_logic;
      gt0_eyescandataerror_out              : out std_logic;
      gt0_rxrate_in                         : in  std_logic_vector(2 downto 0);
   -- Loopback
      gt0_loopback_in                       : in  std_logic_vector(2 downto 0);
   -- Polarity
      gt0_rxpolarity_in                     : in  std_logic;
      gt0_txpolarity_in                     : in  std_logic;
   -- RX Decision Feedback Equalizer(DFE)
      gt0_rxlpmen_in                        : in  std_logic;
      gt0_rxdfelpmreset_in                  : in  std_logic;
      gt0_rxmonitorsel_in                   : in  std_logic_vector(1 downto 0);
      gt0_rxmonitorout_out                  : out std_logic_vector(6 downto 0);
   -- TX Driver
      gt0_txpostcursor_in                   : in  std_logic_vector(4 downto 0);
      gt0_txprecursor_in                    : in  std_logic_vector(4 downto 0);
      gt0_txdiffctrl_in                     : in  std_logic_vector(3 downto 0);
      gt0_txinhibit_in                      : in  std_logic;
   -- PRBS
      gt0_rxprbscntreset_in                 : in  std_logic;
      gt0_rxprbserr_out                     : out std_logic;
      gt0_rxprbssel_in                      : in  std_logic_vector(2 downto 0);
      gt0_txprbssel_in                      : in  std_logic_vector(2 downto 0);
      gt0_txprbsforceerr_in                 : in  std_logic;

      gt0_rxcdrhold_in                      : in  std_logic;
      gt0_dmonitorout_out                   : out std_logic_vector(7 downto 0);

   -- Status
      gt0_rxdisperr_out                     : out std_logic_vector(3 downto 0);
      gt0_rxnotintable_out                  : out std_logic_vector(3 downto 0);
      gt0_rxcommadet_out                    : out std_logic;
   -- DRP
      gt1_drpaddr                           : in  std_logic_vector(8 downto 0);
      gt1_drpen                             : in  std_logic;
      gt1_drpdi                             : in  std_logic_vector(15 downto 0);
      gt1_drpdo                             : out std_logic_vector(15 downto 0);
      gt1_drprdy                            : out std_logic;
      gt1_drpwe                             : in  std_logic;
   -- TX Reset and Initialisation
      gt1_txpmareset_in                     : in std_logic;
      gt1_txpcsreset_in                     : in std_logic;
      gt1_txresetdone_out                   : out std_logic;
   -- RX Reset and Initialisation
      gt1_rxpmareset_in                     : in std_logic;
      gt1_rxpcsreset_in                     : in std_logic;
      gt1_rxresetdone_out                   : out std_logic;
   -- Clocking
      gt1_rxbufstatus_out                   : out std_logic_vector(2 downto 0);
      gt1_txphaligndone_out                 : out std_logic;
      gt1_txphinitdone_out                  : out std_logic;
      gt1_txdlysresetdone_out               : out std_logic;
   -- Signal Integrity adn Functionality
   -- Eye Scan
      gt1_eyescantrigger_in                 : in  std_logic;
      gt1_eyescanreset_in                   : in  std_logic;
      gt1_eyescandataerror_out              : out std_logic;
      gt1_rxrate_in                         : in  std_logic_vector(2 downto 0);
   -- Loopback
      gt1_loopback_in                       : in  std_logic_vector(2 downto 0);
   -- Polarity
      gt1_rxpolarity_in                     : in  std_logic;
      gt1_txpolarity_in                     : in  std_logic;
   -- RX Decision Feedback Equalizer(DFE)
      gt1_rxlpmen_in                        : in  std_logic;
      gt1_rxdfelpmreset_in                  : in  std_logic;
      gt1_rxmonitorsel_in                   : in  std_logic_vector(1 downto 0);
      gt1_rxmonitorout_out                  : out std_logic_vector(6 downto 0);
   -- TX Driver
      gt1_txpostcursor_in                   : in  std_logic_vector(4 downto 0);
      gt1_txprecursor_in                    : in  std_logic_vector(4 downto 0);
      gt1_txdiffctrl_in                     : in  std_logic_vector(3 downto 0);
      gt1_txinhibit_in                      : in  std_logic;
   -- PRBS
      gt1_rxprbscntreset_in                 : in  std_logic;
      gt1_rxprbserr_out                     : out std_logic;
      gt1_rxprbssel_in                      : in  std_logic_vector(2 downto 0);
      gt1_txprbssel_in                      : in  std_logic_vector(2 downto 0);
      gt1_txprbsforceerr_in                 : in  std_logic;

      gt1_rxcdrhold_in                      : in  std_logic;
      gt1_dmonitorout_out                   : out std_logic_vector(7 downto 0);

   -- Status
      gt1_rxdisperr_out                     : out std_logic_vector(3 downto 0);
      gt1_rxnotintable_out                  : out std_logic_vector(3 downto 0);
      gt1_rxcommadet_out                    : out std_logic;
   -- MDIO Interface
      mdc                                   : in  std_logic;                    -- MDIO Clock
      mdio_in                               : in  std_logic;                    -- MDIO Input
      mdio_out                              : out std_logic;                    -- MDIO Output
      mdio_tri                              : out std_logic;                    -- MDIO Tri-state enable
      prtad                                 : in  std_logic_vector(4 downto 0); -- MDIO PRTAD
      type_sel                              : in  std_logic_vector(1 downto 0)  -- type_sel control
);
end rxaui_0_block;

library ieee;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library rxaui_v4_3_7;
use rxaui_v4_3_7.all;

architecture wrapper of rxaui_0_block is

----------------------------------------------------------------------------
-- Component Declaration for the RXAUI core.
----------------------------------------------------------------------------

   component rxaui_v4_3_7_top
      generic (
      c_family                              : string  := "virtex7";
      c_rxaui_mode                          : integer := 0;
      c_txdata_width                        : integer := 0;
      c_rxdata_width                        : integer := 0;
      c_has_mdio                            : boolean := true
      );
      port (
      reset                                 : in  std_logic;
      xgmii_txd                             : in  std_logic_vector(63 downto 0);
      xgmii_txc                             : in  std_logic_vector(7 downto 0);
      xgmii_rxd                             : out std_logic_vector(63 downto 0);
      xgmii_rxc                             : out std_logic_vector(7 downto 0);
      usrclk                                : in  std_logic;
      rxclk                                 : in  std_logic;
      mgt_txdata                            : out std_logic_vector(c_txdata_width - 1 downto 0);
      mgt_txcharisk                         : out std_logic_vector(7 downto 0);
      mgt_rxdata                            : in  std_logic_vector(c_rxdata_width - 1 downto 0);
      mgt_rxcharisk                         : in std_logic_vector(7 downto 0);
      mgt_codevalid                         : in std_logic_vector(7 downto 0);
      mgt_codecomma                         : in std_logic_vector(7 downto 0);
      mgt_enchansync                        : out std_logic;
      mgt_enable_align                      : out std_logic_vector(1 downto 0);
      mgt_rxlock                            : in  std_logic_vector(1 downto 0);
      mgt_loopback                          : out std_logic;
      mgt_powerdown                         : out std_logic;
      mgt_tx_reset                          : in  std_logic_vector(1 downto 0);
      mgt_rx_reset                          : in  std_logic_vector(1 downto 0);
      soft_reset                            : out std_logic;
      signal_detect                         : in  std_logic_vector(1 downto 0);
      align_status                          : out std_logic;
      sync_status                           : out std_logic_vector(3 downto 0);
      mdc                                   : in  std_logic;
      mdio_in                               : in  std_logic;
      mdio_out                              : out std_logic;
      mdio_tri                              : out std_logic;
      prtad                                 : in  std_logic_vector(4 downto 0);
      type_sel                              : in  std_logic_vector(1 downto 0);
      configuration_vector                  : in  std_logic_vector(6 downto 0);
      status_vector                         : out std_logic_vector(7 downto 0));
  end component;

  component rxaui_0_gt_wrapper_GT
  generic
  (
    -- Simulation attributes
    GT_SIM_GTRESET_SPEEDUP                  : string     := "true";
    RX_DFE_KL_CFG2_IN                       : bit_vector := X"3010D90C";
    SIM_CPLLREFCLK_SEL                      : bit_vector :=   "001";
    PMA_RSV_IN                              : bit_vector := X"00000000";
    PCS_RSVD_ATTR_IN                        : bit_vector := X"000000000000"
  );
  port
  (
    cpllrefclksel_in : in std_logic_vector(2 downto 0);
    ---------------------------------- Channel ---------------------------------
    qpllclk_in                              : in   std_logic;
    qpllrefclk_in                           : in   std_logic;
    ---------------- Channel - Dynamic Reconfiguration Port (DRP) --------------
    drpaddr_in                              : in   std_logic_vector(8 downto 0);
    drpclk_in                               : in   std_logic;
    drpdi_in                                : in   std_logic_vector(15 downto 0);
    drpdo_out                               : out  std_logic_vector(15 downto 0);
    drpen_in                                : in   std_logic;
    drprdy_out                              : out  std_logic;
    drpwe_in                                : in   std_logic;
    ------------------------- Digital Monitor Ports --------------------------
    dmonitorout_out                         : out  std_logic_vector(7 downto 0);
    ------------------------------- Eye Scan Ports -----------------------------
    eyescandataerror_out                    : out  std_logic;
    eyescanreset_in                         : in   std_logic;
    eyescantrigger_in                       : in   std_logic;
    rxrate_in                               : in   std_logic_vector(2 downto 0);
    ------------------------- Receive Ports - CDR Ports ------------------------
    rxcdrhold_in                            : in   std_logic;
    ------------------------ Loopback and Powerdown Ports ----------------------
    loopback_in                             : in   std_logic_vector(2 downto 0);
    rxpd_in                                 : in   std_logic_vector(1 downto 0);
    txpd_in                                 : in   std_logic_vector(1 downto 0);
    ------------------------------- Receive Ports ------------------------------
    rxuserrdy_in                            : in   std_logic;
    ----------------------- Receive Ports - 8b10b Decoder ----------------------
    rxchariscomma_out                       : out  std_logic_vector(3 downto 0);
    rxcharisk_out                           : out  std_logic_vector(3 downto 0);
    rxdisperr_out                           : out  std_logic_vector(3 downto 0);
    rxnotintable_out                        : out  std_logic_vector(3 downto 0);
    ------------------- Receive Ports - Channel Bonding Ports ------------------
    rxchanbondseq_out                       : out  std_logic;
    rxchbonden_in                           : in   std_logic;
    rxchbondi_in                            : in   std_logic_vector(4 downto 0);
    rxchbondlevel_in                        : in   std_logic_vector(2 downto 0);
    rxchbondmaster_in                       : in   std_logic;
    rxchbondo_out                           : out  std_logic_vector(4 downto 0);
    rxchbondslave_in                        : in   std_logic;
    ------------------- Receive Ports - Channel Bonding Ports  -----------------
    rxchanisaligned_out                     : out  std_logic;
    rxchanrealign_out                       : out  std_logic;
    ------------------- Receive Ports - Clock Correction Ports -----------------
    rxclkcorcnt_out                         : out  std_logic_vector(1 downto 0);
    --------------- Receive Ports - Comma Detection and Alignment --------------
    rxbyteisaligned_out                     : out  std_logic;
    rxbyterealign_out                       : out  std_logic;
    rxcommadet_out                          : out  std_logic;
    rxmcommaalignen_in                      : in   std_logic;
    rxpcommaalignen_in                      : in   std_logic;
    ----------------------- Receive Ports - PRBS Detection ---------------------
    rxprbscntreset_in                       : in   std_logic;
    rxprbserr_out                           : out  std_logic;
    rxprbssel_in                            : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - RX Data Path interface -----------------
    gtrxreset_in                            : in   std_logic;
    rxpcsreset_in                           : in   std_logic;
    rxpmareset_in                           : in   std_logic;
    rxdata_out                              : out  std_logic_vector(31 downto 0);
    rxoutclk_out                            : out  std_logic;
    rxusrclk_in                             : in   std_logic;
    rxusrclk2_in                            : in   std_logic;
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
    rxpolarity_in                           : in   std_logic;
    ------------ Receive Ports - RX Decision Feedback Equalizer(DFE) -----------
    rxlpmhfhold_in                          : in   std_logic;
    rxdfelpmreset_in                        : in   std_logic;
    rxmonitorout_out                        : out  std_logic_vector(6 downto 0);
    rxmonitorsel_in                         : in   std_logic_vector(1 downto 0);
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    gtxrxn_in                               : in   std_logic;
    gtxrxp_in                               : in   std_logic;
    rxlpmlfhold_in                          : in   std_logic;
    rxlpmen_in                              : in   std_logic;
    -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
    rxbufreset_in                           : in   std_logic;
    rxbufstatus_out                         : out  std_logic_vector(2 downto 0);
    ------------------------ Receive Ports - RX PLL Ports ----------------------
    rxresetdone_out                         : out  std_logic;
    ------------------------------- Transmit Ports -----------------------------
    txpostcursor_in                         : in   std_logic_vector(4 downto 0);
    txprecursor_in                          : in   std_logic_vector(4 downto 0);
    txuserrdy_in                            : in   std_logic;
    ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
    txcharisk_in                            : in   std_logic_vector(3 downto 0);
    ------------ Transmit Ports - TX Buffer and Phase Alignment Ports ----------
    txdlyen_in                              : in   std_logic;
    txdlysreset_in                          : in   std_logic;
    txdlysresetdone_out                     : out  std_logic;
    txphalign_in                            : in   std_logic;
    txphaligndone_out                       : out  std_logic;
    txphalignen_in                          : in   std_logic;
    txphdlyreset_in                         : in   std_logic;
    txphinit_in                             : in   std_logic;
    txphinitdone_out                        : out  std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gttxreset_in                            : in   std_logic;
    txdata_in                               : in   std_logic_vector(31 downto 0);
    txoutclk_out                            : out  std_logic;
    txoutclkfabric_out                      : out  std_logic;
    txoutclkpcs_out                         : out  std_logic;
    rxratedone_out                          : out  std_logic;
    txusrclk_in                             : in   std_logic;
    txusrclk2_in                            : in   std_logic;
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    gtxtxn_out                              : out  std_logic;
    gtxtxp_out                              : out  std_logic;
    txdiffctrl_in                           : in   std_logic_vector(3 downto 0);
    txinhibit_in                            : in   std_logic;
    ----------------------- Transmit Ports - TX PLL Ports ----------------------
    txpcsreset_in                           : in   std_logic;
    txpmareset_in                           : in   std_logic;
    txresetdone_out                         : out  std_logic;
    ----------------- Transmit Ports - TX Polarity Control Ports ---------------
    txpolarity_in                           : in   std_logic;
    --------------------- Transmit Ports - TX PRBS Generator -------------------
    txprbsforceerr_in                       : in   std_logic;
    txprbssel_in                            : in   std_logic_vector(2 downto 0);
    ----------------- Transmit Ports - TX Ports for PCI Express ----------------
    txelecidle_in                           : in   std_logic
  );
  end component;

  component rxaui_0_cl_clocking
  port (
    txoutclk             : in std_logic;
    clk156               : out std_logic
  );
  end component;

  component rxaui_0_cl_resets
  port (
    reset                : in  std_logic;
    clk156               : in  std_logic;
    uclk_txlock          : in  std_logic;
    reset156             : out std_logic
  );
  end component;

  component rxaui_0_gt_wrapper_tx_sync_manual
  Generic (
    NUMBER_OF_LANES      : integer range 1 to 32:= 4;  -- Number of lanes that are controlled using this FSM.
    MASTER_LANE_ID       : integer range 0 to 31:= 0   -- Number of the lane which is considered the master in manual phase-alignment
  );

  Port (
    STABLE_CLOCK         : in  std_logic;              --Stable Clock, either a stable clock from the PCB
                                                       --or reference-clock present at startup.
    RESET_PHALIGNMENT    : in  std_logic;
    RUN_PHALIGNMENT      : in  std_logic;
    PHASE_ALIGNMENT_DONE : out std_logic := '0';       -- Manual phase-alignment performed sucessfully
    TXDLYSRESET          : out std_logic_vector(NUMBER_OF_LANES-1 downto 0) := (others=> '0');
    TXDLYSRESETDONE      : in  std_logic_vector(NUMBER_OF_LANES-1 downto 0);
    TXPHINIT             : out std_logic_vector(NUMBER_OF_LANES-1 downto 0) := (others=> '0');
    TXPHINITDONE         : in  std_logic_vector(NUMBER_OF_LANES-1 downto 0);
    TXPHALIGN            : out std_logic_vector(NUMBER_OF_LANES-1 downto 0) := (others=> '0');
    TXPHALIGNDONE        : in  std_logic_vector(NUMBER_OF_LANES-1 downto 0);
    TXDLYEN              : out std_logic_vector(NUMBER_OF_LANES-1 downto 0) := (others=> '0')
   );
   end component;

  component rxaui_0_reset_counter
    port
    (
      clk                : in  std_logic;
      done               : out std_logic
    );
  end component;

  component rxaui_0_ff_synchronizer
    generic
    (
      C_NUM_SYNC_REGS    : integer := 3
    );
    port
    (
      clk                : in  std_logic;
      data_in            : in std_logic;
      data_out           : out std_logic
    );
  end component;

  component rxaui_0_pulse_stretcher
    generic
    (
      C_NUM_SYNC_REGS    : integer := 3
    );
    port
    (
      clk                : in  std_logic;
      data_in            : in std_logic;
      data_out           : out std_logic
    );
  end component;

  component rxaui_0_gt_wrapper_tx_sync_manual_done_stretch
  Port (
  -- User Interface
      USER_DONE          : out STD_LOGIC := '0';
  -- GT Interface
      GT_DONE            : in STD_LOGIC;
  -- Clock and Reset
      CLK                : in STD_LOGIC
  );
  end component;

  constant SYNC_COUNT_LENGTH   : integer := 16;
  constant CHBOND_COUNT_LENGTH : integer := 16;

----------------------------------------------------------------------------
-- Signal declarations.
---------------------------------------------------------------------------
  attribute ASYNC_REG : string;
  attribute shreg_extract : string;
  signal uclk_signal_detect                : std_logic_vector(1 downto 0);
  signal core_mgt_rx_reset                 : std_logic_vector(1 downto 0);
  signal mgt_txdata                        : std_logic_vector(63 downto 0);
  signal mgt_txcharisk                     : std_logic_vector(7 downto 0);
  signal mgt_rxdata                        : std_logic_vector(63 downto 0);
  signal mgt_rxcharisk                     : std_logic_vector(7 downto 0);
  signal mgt_enable_align                  : std_logic_vector(1 downto 0);
  signal mgt_enchansync                    : std_logic;
  signal mgt_rxdisperr                     : std_logic_vector(7 downto 0);
  signal mgt_rxnotintable                  : std_logic_vector(7 downto 0);
  signal uclk_mgt_rx_reset                 : std_logic;
  signal uclk_mgt_tx_reset                 : std_logic;
  signal mgt_codevalid                     : std_logic_vector(7 downto 0);
  signal mgt_rxchariscomma                 : std_logic_vector(7 downto 0);
  signal mgt_rxdata_reg                    : std_logic_vector(63 downto 0) := X"0000000000000000";
  signal mgt_rxcharisk_reg                 : std_logic_vector(7 downto 0) := X"00";
  signal mgt_rxnotintable_reg              : std_logic_vector(7 downto 0) := X"00";
  signal mgt_rxdisperr_reg                 : std_logic_vector(7 downto 0) := X"00";
  signal mgt_codecomma_reg                 : std_logic_vector(7 downto 0) := X"00";
  signal uclk_mgt_rxbuf_reset              : std_logic_vector(1 downto 0);
  signal mgt_tx_fault                      : std_logic_vector(1 downto 0);
  signal mgt_loopback                      : std_logic;
  signal mgt_loopback_r                    : std_logic;
  signal uclk_mgt_loopback_falling         : std_logic;
  signal mgt_powerdown                     : std_logic;
  signal mgt_powerdown_2                   : std_logic_vector(1 downto 0);
  signal mgt_powerdown_r                   : std_logic := '0';
  signal uclk_mgt_powerdown_falling        : std_logic := '0';
  signal uclk_plllocked                    : std_logic;
  signal uclk_rxlock                       : std_logic_vector(1 downto 0);
  signal uclk_txlock                       : std_logic;
  signal uclk_mgt_rxbuferr                 : std_logic_vector(1 downto 0);
  signal uclk_mgt_rxbufstatus              : std_logic_vector(5 downto 0);
  signal uclk_mgt_rxbufstatus_reg          : std_logic_vector(5 downto 0) := (others => '0');
  signal uclk_txresetdone_reg              : std_logic_vector(1 downto 0) := (others => '0');
  signal loopback_int                      : std_logic_vector(2 downto 0);
  signal mgt_txuserrdy                     : std_logic;
  signal mgt_rxuserrdy                     : std_logic;
  signal uclk_reset_count_done             : std_logic;
  signal uclk_soft_reset                   : std_logic;
  signal gt0_txoutclk_i                    : std_logic;
  -- GT Control
  signal gt0_loopback                      : std_logic_vector(2 downto 0);

  signal uclk_gt0_rxresetdone              : std_logic;
  signal uclk_gt0_txresetdone              : std_logic;
  -- Debug Wires

  -- TX Reset and Initialisation
  signal gt0_gttxreset                     : std_logic;
  signal gt0_txpmareset                    : std_logic;
  signal gt0_txpcsreset                    : std_logic;
  signal gt0_txuserrdy                     : std_logic;
  -- RX Reset and Initialisation
  signal gt0_gtrxreset                     : std_logic;
  signal gt0_rxpmareset                    : std_logic;
  signal gt0_rxpcsreset                    : std_logic;
  signal gt0_rxbufreset                    : std_logic;
  signal gt0_rxuserrdy                     : std_logic;

  signal gt0_rxchanbondseq                 : std_logic;
  signal gt0_rxchanisaligned               : std_logic;
  signal gt0_rxchanrealign                 : std_logic;
  signal gt0_rxclkcorcnt                   : std_logic_vector(1 downto 0);
  signal gt0_rxbyteisaligned               : std_logic;
  signal gt0_rxbyterealign                 : std_logic;
  signal gt0_rxcommadet                    : std_logic;
  -------------------------- Channel Bonding Wires ---------------------------
  signal gt0_rxchbondo_i                   : std_logic_vector(4 downto 0);
  signal gt1_loopback                      : std_logic_vector(2 downto 0);

  signal uclk_gt1_rxresetdone              : std_logic;
  signal uclk_gt1_txresetdone              : std_logic;
  -- Debug Wires

  -- TX Reset and Initialisation
  signal gt1_gttxreset                     : std_logic;
  signal gt1_txpmareset                    : std_logic;
  signal gt1_txpcsreset                    : std_logic;
  signal gt1_txuserrdy                     : std_logic;
  -- RX Reset and Initialisation
  signal gt1_gtrxreset                     : std_logic;
  signal gt1_rxpmareset                    : std_logic;
  signal gt1_rxpcsreset                    : std_logic;
  signal gt1_rxbufreset                    : std_logic;
  signal gt1_rxuserrdy                     : std_logic;

  signal gt1_rxchanbondseq                 : std_logic;
  signal gt1_rxchanisaligned               : std_logic;
  signal gt1_rxchanrealign                 : std_logic;
  signal gt1_rxclkcorcnt                   : std_logic_vector(1 downto 0);
  signal gt1_rxbyteisaligned               : std_logic;
  signal gt1_rxbyterealign                 : std_logic;
  signal gt1_rxcommadet                    : std_logic;
  -------------------------- Channel Bonding Wires ---------------------------
  signal gt1_rxchbondo_i                   : std_logic_vector(4 downto 0);

  signal uclk_sync_status                  : std_logic_vector(3 downto 0);
  signal uclk_align_status                 : std_logic;
  signal gt_txdlysreset                    : std_logic_vector(1 downto 0);
  signal gt_txdlysresetdone                : std_logic_vector(1 downto 0);
  signal gt_txphaligndone                  : std_logic_vector(1 downto 0);

  signal  gt_txphinit                      : std_logic_vector(1 downto 0);
  signal  gt_txphinitdone                  : std_logic_vector(1 downto 0);
  signal  gt_txphalign                     : std_logic_vector(1 downto 0);
  signal  gt_txdlyen                       : std_logic_vector(1 downto 0);
  signal  uclk_txsync_start_phase_align    : std_logic := '0';
  signal  uclk_phase_align_complete        : std_logic;

  signal uclk_chbond_counter               : unsigned(CHBOND_COUNT_LENGTH -1 downto 0) := (others => '0');
  signal uclk_sync_counter                 : unsigned(SYNC_COUNT_LENGTH -1 downto 0)   := (others => '0');
  signal dclk_reset_count_done             : std_logic := '0';
  signal dclk_initial_reset                : std_logic := '0';
  signal clk156                            : std_logic;
  signal uclk_reset156                     : std_logic;

  signal rxprbs_in_use                     : std_logic := '0';

----------------------------------------------------------------------------
-- Function declarations.
---------------------------------------------------------------------------
function IsBufError (bufStatus:std_logic_vector(2 downto 0)) return std_logic is
  variable result : std_logic;
begin
  if bufStatus = "101" or bufStatus = "110" then
    result := '1';
  else
    result := '0';
  end if;
  return result;
end;

begin

  -- Assign output port from interal signals
  debug <= uclk_align_status & uclk_sync_status & uclk_phase_align_complete;
  clk156_out <= clk156;
  clk156_lock <= uclk_txlock;

   rxaui_cl_clocking_i : rxaui_0_cl_clocking
    port map(
      txoutclk    => gt0_txoutclk_i,
      clk156      => clk156
    );

  rxaui_cl_resets_i : rxaui_0_cl_resets
    port map(
      reset       => reset,
      clk156      => clk156,
      uclk_txlock => uclk_txlock,
      reset156    => uclk_reset156
    );


  -- Synchronize signal_detect to clk156
  signal_detect_0_sync_i : rxaui_0_ff_synchronizer
    generic map (
      C_NUM_SYNC_REGS => 5)
    port map (
     clk      => clk156,
     data_in  => signal_detect(0),
     data_out => uclk_signal_detect(0)
    );
  signal_detect_1_sync_i : rxaui_0_ff_synchronizer
    generic map (
      C_NUM_SYNC_REGS => 5)
    port map (
     clk      => clk156,
     data_in  => signal_detect(1),
     data_out => uclk_signal_detect(1)
    );

  rxaui_0_core : rxaui_v4_3_7_top
    generic map (
      c_family             => "kintex7",
      c_rxaui_mode         => 0,
      c_txdata_width       => 64,
      c_rxdata_width       => 64,
      c_has_mdio           => true
    )
    port map (
      reset                => uclk_reset156,
      xgmii_txd            => xgmii_txd,
      xgmii_txc            => xgmii_txc,
      xgmii_rxd            => xgmii_rxd,
      xgmii_rxc            => xgmii_rxc,
      usrclk               => clk156,
      rxclk                => '0',
      mgt_txdata           => mgt_txdata,
      mgt_txcharisk        => mgt_txcharisk,
      mgt_rxdata           => mgt_rxdata_reg,
      mgt_rxcharisk        => mgt_rxcharisk_reg,
      mgt_codevalid        => mgt_codevalid,
      mgt_codecomma        => mgt_codecomma_reg,
      mgt_enable_align     => mgt_enable_align,
      mgt_enchansync       => mgt_enchansync,
      mgt_rxlock           => uclk_rxlock,
      mgt_loopback         => mgt_loopback,
      mgt_powerdown        => mgt_powerdown,
      mgt_tx_reset         => mgt_tx_fault,
      mgt_rx_reset         => core_mgt_rx_reset,
      soft_reset           => uclk_soft_reset,
      signal_detect        => uclk_signal_detect,
      align_status         => uclk_align_status,
      sync_status          => uclk_sync_status,
      mdc                  => mdc,
      mdio_in              => mdio_in,
      mdio_out             => mdio_out,
      mdio_tri             => mdio_tri,
      prtad                => prtad,
      type_sel             => type_sel,
      configuration_vector => (others => '0'),
      status_vector        => open);

  ----------------------------------------------------------------------
   -- Transceiver instances
   gt0_wrapper_i : rxaui_0_gt_wrapper_GT
    generic map (
        GT_SIM_GTRESET_SPEEDUP      =>      "TRUE",
        RX_DFE_KL_CFG2_IN           =>      X"3010D90C",
        SIM_CPLLREFCLK_SEL          =>      "001",
        PCS_RSVD_ATTR_IN            =>      X"000000000002",
        PMA_RSV_IN                  =>      X"00018480"
    )
    port map (
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0
        cpllrefclksel_in            =>      "001",
        ---------------------------------- Channel ---------------------------------
        qpllclk_in                  =>      qplloutclk,
        qpllrefclk_in               =>      qplloutrefclk,
        ---------------- Channel - Dynamic Reconfiguration Port (DRP) --------------
        drpaddr_in                  =>      gt0_drpaddr,
        drpclk_in                   =>      dclk,
        drpdi_in                    =>      gt0_drpdi,
        drpdo_out                   =>      gt0_drpdo,
        drpen_in                    =>      gt0_drpen,
        drprdy_out                  =>      gt0_drprdy,
        drpwe_in                    =>      gt0_drpwe,
        ------------------------- Digital Monitor Ports --------------------------
        dmonitorout_out             =>      gt0_dmonitorout_out,
        ------------------------------- Eye Scan Ports -----------------------------
        eyescandataerror_out        =>      gt0_eyescandataerror_out,
        eyescanreset_in             =>      gt0_eyescanreset_in,
        eyescantrigger_in           =>      gt0_eyescantrigger_in,
        ------------------------- Receive Ports - CDR Ports ------------------------
        rxcdrhold_in                =>      gt0_rxcdrhold_in,
        ------------------------ Loopback and Powerdown Ports ----------------------
        loopback_in                 =>      gt0_loopback,
        rxpd_in                     =>      mgt_powerdown_2,
        txpd_in                     =>      mgt_powerdown_2,
        ------------------------------- Receive Ports ------------------------------
        rxuserrdy_in                =>      mgt_rxuserrdy,
        ----------------------- Receive Ports - 8b10b Decoder ----------------------
        rxchariscomma_out           =>      mgt_rxchariscomma(3 downto 0),
        rxcharisk_out               =>      mgt_rxcharisk(3 downto 0),
        rxdisperr_out               =>      mgt_rxdisperr(3 downto 0),
        rxnotintable_out            =>      mgt_rxnotintable(3 downto 0),
        ------------------- Receive Ports - Channel Bonding Ports ------------------
        rxchanbondseq_out           =>      gt0_rxchanbondseq,
        rxchbonden_in               =>      mgt_enchansync,
        rxchbondi_in                =>      gt1_rxchbondo_i,
        rxchbondlevel_in            =>      "000",
        rxchbondmaster_in           =>      '0',
        rxchbondo_out               =>      gt0_rxchbondo_i,
        rxchbondslave_in            =>      '1',
        ------------------- Receive Ports - Channel Bonding Ports  -----------------
        rxchanisaligned_out         =>      gt0_rxchanisaligned,
        rxchanrealign_out           =>      gt0_rxchanrealign,
        ------------------- Receive Ports - Clock Correction Ports -----------------
        rxclkcorcnt_out             =>      gt0_rxclkcorcnt,
        --------------- Receive Ports - Comma Detection and Alignment --------------
        rxbyteisaligned_out         =>      gt0_rxbyteisaligned,
        rxbyterealign_out           =>      gt0_rxbyterealign,
        rxcommadet_out              =>      gt0_rxcommadet,
        rxmcommaalignen_in          =>      mgt_enable_align(0),
        rxpcommaalignen_in          =>      mgt_enable_align(0),
        ----------------------- Receive Ports - PRBS Detection ---------------------
        rxprbscntreset_in           =>      gt0_rxprbscntreset_in,
        rxprbserr_out               =>      gt0_rxprbserr_out,
        rxprbssel_in                =>      gt0_rxprbssel_in,
        ------------------- Receive Ports - RX Data Path interface -----------------
        gtrxreset_in                =>      gt0_gtrxreset,
        rxpcsreset_in               =>      gt0_rxpcsreset_in,
        rxpmareset_in               =>      gt0_rxpmareset,
        rxdata_out                  =>      mgt_rxdata(31 downto 0),
        rxoutclk_out                =>      open,
        rxusrclk_in                 =>      clk156,
        rxusrclk2_in                =>      clk156,
        ----------------- Receive Ports - RX Polarity Control Ports ----------------
        rxpolarity_in               =>      gt0_rxpolarity_in,
        ------------ Receive Ports - RX Decision Feedback Equalizer(DFE) -----------
        rxlpmhfhold_in              =>      '0',
        rxdfelpmreset_in            =>      gt0_rxdfelpmreset_in,
        rxmonitorout_out            =>      gt0_rxmonitorout_out,
        rxmonitorsel_in             =>      gt0_rxmonitorsel_in,
        ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        gtxrxn_in                   =>      rxaui_rx_l0_n,
        gtxrxp_in                   =>      rxaui_rx_l0_p,
        rxlpmen_in                  =>      gt0_rxlpmen_in,
        rxlpmlfhold_in              =>      '0',
        -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
        rxbufreset_in               =>      gt0_rxbufreset,
        rxbufstatus_out             =>      uclk_mgt_rxbufstatus(2 downto 0),
        ------------------------ Receive Ports - RX PLL Ports ----------------------
        rxresetdone_out             =>      uclk_gt0_rxresetdone,
        ------------------------------- Transmit Ports -----------------------------
        txprecursor_in              =>      gt0_txprecursor_in,
        txpostcursor_in             =>      gt0_txpostcursor_in,
        txuserrdy_in                =>      mgt_txuserrdy,
        ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
        txcharisk_in                =>      mgt_txcharisk(3 downto 0),
        ------------ Transmit Ports - TX Buffer and Phase Alignment Ports ----------

        txdlyen_in                  =>      gt_txdlyen(0),
        txdlysreset_in              =>      gt_txdlysreset(0),
        txdlysresetdone_out         =>      gt_txdlysresetdone(0),
        txphalign_in                =>      gt_txphalign(0),
        txphaligndone_out           =>      gt_txphaligndone(0),
        txphalignen_in              =>      '1',
        txphdlyreset_in             =>      '0',
        txphinit_in                 =>      gt_txphinit(0),
        txphinitdone_out            =>      gt_txphinitdone(0),

        ------------------ Transmit Ports - TX Data Path interface -----------------
        gttxreset_in                =>      uclk_mgt_tx_reset,
        txdata_in                   =>      mgt_txdata(31 downto 0),
        txoutclk_out                =>      gt0_txoutclk_i,
        txoutclkfabric_out          =>      open,
        txoutclkpcs_out             =>      open,
        rxratedone_out              =>      open,
        txusrclk_in                 =>      clk156,
        txusrclk2_in                =>      clk156,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gtxtxn_out                  =>      rxaui_tx_l0_n,
        gtxtxp_out                  =>      rxaui_tx_l0_p,
        txdiffctrl_in               =>      gt0_txdiffctrl_in,
        txinhibit_in                =>      gt0_txinhibit_in,
        ----------------------- Transmit Ports - TX PLL Ports ----------------------
        txpcsreset_in               =>      gt0_txpcsreset_in,
        txpmareset_in               =>      gt0_txpmareset_in,
        txresetdone_out             =>      uclk_gt0_txresetdone,
        ----------------- Transmit Ports - TX Polarity Control Ports ---------------
        txpolarity_in              =>       gt0_txpolarity_in,
        --------------------- Transmit Ports - TX PRBS Generator -------------------
        txprbsforceerr_in           =>      gt0_txprbsforceerr_in,
        txprbssel_in                =>      gt0_txprbssel_in,
        ----------------- Transmit Ports - TX Ports for PCI Express ----------------
        rxrate_in                   =>      gt0_rxrate_in,
        txelecidle_in               =>      mgt_powerdown_r
    );

   gt1_wrapper_i : rxaui_0_gt_wrapper_GT
    generic map (
        GT_SIM_GTRESET_SPEEDUP      =>      "TRUE",
        RX_DFE_KL_CFG2_IN           =>      X"3010D90C",
        SIM_CPLLREFCLK_SEL          =>      "001",
        PCS_RSVD_ATTR_IN            =>      X"000000000002",
        PMA_RSV_IN                  =>      X"00018480"
    )
    port map (
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT1
        cpllrefclksel_in            =>      "001",
        ---------------------------------- Channel ---------------------------------
        qpllclk_in                  =>      qplloutclk,
        qpllrefclk_in               =>      qplloutrefclk,
        ---------------- Channel - Dynamic Reconfiguration Port (DRP) --------------
        drpaddr_in                  =>      gt1_drpaddr,
        drpclk_in                   =>      dclk,
        drpdi_in                    =>      gt1_drpdi,
        drpdo_out                   =>      gt1_drpdo,
        drpen_in                    =>      gt1_drpen,
        drprdy_out                  =>      gt1_drprdy,
        drpwe_in                    =>      gt1_drpwe,
        ------------------------- Digital Monitor Ports --------------------------
        dmonitorout_out             =>      gt1_dmonitorout_out,
        ------------------------------- Eye Scan Ports -----------------------------
        eyescandataerror_out        =>      gt1_eyescandataerror_out,
        eyescanreset_in             =>      gt1_eyescanreset_in,
        eyescantrigger_in           =>      gt1_eyescantrigger_in,
        ------------------------- Receive Ports - CDR Ports ------------------------
        rxcdrhold_in                =>      gt1_rxcdrhold_in,
        ------------------------ Loopback and Powerdown Ports ----------------------
        loopback_in                 =>      gt1_loopback,
        rxpd_in                     =>      mgt_powerdown_2,
        txpd_in                     =>      mgt_powerdown_2,
        ------------------------------- Receive Ports ------------------------------
        rxuserrdy_in                =>      mgt_rxuserrdy,
        ----------------------- Receive Ports - 8b10b Decoder ----------------------
        rxchariscomma_out           =>      mgt_rxchariscomma(7 downto 4),
        rxcharisk_out               =>      mgt_rxcharisk(7 downto 4),
        rxdisperr_out               =>      mgt_rxdisperr(7 downto 4),
        rxnotintable_out            =>      mgt_rxnotintable(7 downto 4),
        ------------------- Receive Ports - Channel Bonding Ports ------------------
        rxchanbondseq_out           =>      gt1_rxchanbondseq,
        rxchbonden_in               =>      mgt_enchansync,
        rxchbondi_in                =>      (others => '0'),
        rxchbondlevel_in            =>      "001",
        rxchbondmaster_in           =>      '1',
        rxchbondo_out               =>      gt1_rxchbondo_i,
        rxchbondslave_in            =>      '0',
        ------------------- Receive Ports - Channel Bonding Ports  -----------------
        rxchanisaligned_out         =>      gt1_rxchanisaligned,
        rxchanrealign_out           =>      gt1_rxchanrealign,
        ------------------- Receive Ports - Clock Correction Ports -----------------
        rxclkcorcnt_out             =>      gt1_rxclkcorcnt,
        --------------- Receive Ports - Comma Detection and Alignment --------------
        rxbyteisaligned_out         =>      gt1_rxbyteisaligned,
        rxbyterealign_out           =>      gt1_rxbyterealign,
        rxcommadet_out              =>      gt1_rxcommadet,
        rxmcommaalignen_in          =>      mgt_enable_align(1),
        rxpcommaalignen_in          =>      mgt_enable_align(1),
        ----------------------- Receive Ports - PRBS Detection ---------------------
        rxprbscntreset_in           =>      gt1_rxprbscntreset_in,
        rxprbserr_out               =>      gt1_rxprbserr_out,
        rxprbssel_in                =>      gt1_rxprbssel_in,
        ------------------- Receive Ports - RX Data Path interface -----------------
        gtrxreset_in                =>      gt1_gtrxreset,
        rxpcsreset_in               =>      gt1_rxpcsreset_in,
        rxpmareset_in               =>      gt1_rxpmareset,
        rxdata_out                  =>      mgt_rxdata(63 downto 32),
        rxoutclk_out                =>      open,
        rxusrclk_in                 =>      clk156,
        rxusrclk2_in                =>      clk156,
        ----------------- Receive Ports - RX Polarity Control Ports ----------------
        rxpolarity_in               =>      gt1_rxpolarity_in,
        ------------ Receive Ports - RX Decision Feedback Equalizer(DFE) -----------
        rxlpmhfhold_in              =>      '0',
        rxdfelpmreset_in            =>      gt1_rxdfelpmreset_in,
        rxmonitorout_out            =>      gt1_rxmonitorout_out,
        rxmonitorsel_in             =>      gt1_rxmonitorsel_in,
        ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        gtxrxn_in                   =>      rxaui_rx_l1_n,
        gtxrxp_in                   =>      rxaui_rx_l1_p,

        rxlpmen_in                  =>      gt1_rxlpmen_in,
        rxlpmlfhold_in              =>      '0',
        -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
        rxbufreset_in               =>      gt1_rxbufreset,
        rxbufstatus_out             =>      uclk_mgt_rxbufstatus(5 downto 3),
        ------------------------ Receive Ports - RX PLL Ports ----------------------
        rxresetdone_out             =>      uclk_gt1_rxresetdone,
        ------------------------------- Transmit Ports -----------------------------
        txprecursor_in              =>      gt1_txprecursor_in,
        txpostcursor_in             =>      gt1_txpostcursor_in,
        txuserrdy_in                =>      mgt_txuserrdy,
        ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
        txcharisk_in                =>      mgt_txcharisk(7 downto 4),
        ------------ Transmit Ports - TX Buffer and Phase Alignment Ports ----------
        txdlyen_in                  =>      gt_txdlyen(1),
        txdlysreset_in              =>      gt_txdlysreset(1),
        txdlysresetdone_out         =>      gt_txdlysresetdone(1),
        txphalign_in                =>      gt_txphalign(1),
        txphaligndone_out           =>      gt_txphaligndone(1),
        txphalignen_in              =>      '1',
        txphdlyreset_in             =>      '0',
        txphinit_in                 =>      gt_txphinit(1),
        txphinitdone_out            =>      gt_txphinitdone(1),

        ------------------ Transmit Ports - TX Data Path interface -----------------
        gttxreset_in                =>      uclk_mgt_tx_reset,
        txdata_in                   =>      mgt_txdata(63 downto 32),
        txoutclk_out                =>      open,
        txoutclkfabric_out          =>      open,
        txoutclkpcs_out             =>      open,
        rxratedone_out              =>      open,
        txusrclk_in                 =>      clk156,
        txusrclk2_in                =>      clk156,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gtxtxn_out                  =>      rxaui_tx_l1_n,
        gtxtxp_out                  =>      rxaui_tx_l1_p,
        txdiffctrl_in               =>      gt1_txdiffctrl_in,
        txinhibit_in                =>      gt1_txinhibit_in,
        ----------------------- Transmit Ports - TX PLL Ports ----------------------
        txpcsreset_in               =>      gt1_txpcsreset_in,
        txpmareset_in               =>      gt1_txpmareset_in,
        txresetdone_out             =>      uclk_gt1_txresetdone,
        ----------------- Transmit Ports - TX Polarity Control Ports ---------------
        txpolarity_in               =>      gt1_txpolarity_in,
        --------------------- Transmit Ports - TX PRBS Generator -------------------
        txprbsforceerr_in           =>      gt1_txprbsforceerr_in,
        txprbssel_in                =>      gt1_txprbssel_in,
        ----------------- Transmit Ports - TX Ports for PCI Express ----------------
        rxrate_in                   =>      gt1_rxrate_in,
        txelecidle_in               =>      mgt_powerdown_r
  );

  gt0_loopback             <= loopback_int or gt0_loopback_in;
  gt0_rxresetdone_out      <= uclk_gt0_rxresetdone;
  gt0_txresetdone_out      <= uclk_gt0_txresetdone;
  gt0_rxdisperr_out        <= mgt_rxdisperr(3 downto 0);
  gt0_rxnotintable_out     <= mgt_rxnotintable(3 downto 0);
  gt0_rxcommadet_out       <= gt0_rxcommadet;
  gt0_rxbufstatus_out      <= uclk_mgt_rxbufstatus(2 downto 0);

  gt0_txphaligndone_out   <= gt_txphaligndone(0);
  gt0_txphinitdone_out    <= gt_txphinitdone(0);
  gt0_txdlysresetdone_out <= gt_txdlysresetdone(0);

  gt_qplllock_out               <= uclk_plllocked;

  gt0_gttxreset  <= uclk_mgt_tx_reset;
  gt0_txpmareset <= gt0_txpmareset_in;
  gt0_txpcsreset <= gt0_txpcsreset_in;
  gt0_txuserrdy  <= mgt_txuserrdy;

  gt0_gtrxreset  <= uclk_mgt_rx_reset;
  gt0_rxbufreset <= uclk_mgt_rxbuf_reset(0);

  -- Synchronize the rxpmareset signal
  rxpmareset_sync0_i : rxaui_0_ff_synchronizer
    generic map (
      C_NUM_SYNC_REGS => 5)
    port map (
      clk      => dclk,
      data_in  => gt0_rxpmareset_in,
      data_out => gt0_rxpmareset
    );

  gt0_rxpcsreset <= gt0_rxpcsreset_in;
  gt0_rxuserrdy  <= mgt_rxuserrdy;
  gt1_loopback             <= loopback_int or gt1_loopback_in;
  gt1_rxresetdone_out      <= uclk_gt1_rxresetdone;
  gt1_txresetdone_out      <= uclk_gt1_txresetdone;
  gt1_rxdisperr_out        <= mgt_rxdisperr(7 downto 4);
  gt1_rxnotintable_out     <= mgt_rxnotintable(7 downto 4);
  gt1_rxcommadet_out       <= gt1_rxcommadet;
  gt1_rxbufstatus_out      <= uclk_mgt_rxbufstatus(5 downto 3);

  gt1_txphaligndone_out   <= gt_txphaligndone(1);
  gt1_txphinitdone_out    <= gt_txphinitdone(1);
  gt1_txdlysresetdone_out <= gt_txdlysresetdone(1);

  gt_qplllock_out               <= uclk_plllocked;

  gt1_gttxreset  <= uclk_mgt_tx_reset;
  gt1_txpmareset <= gt1_txpmareset_in;
  gt1_txpcsreset <= gt1_txpcsreset_in;
  gt1_txuserrdy  <= mgt_txuserrdy;

  gt1_gtrxreset  <= uclk_mgt_rx_reset;
  gt1_rxbufreset <= uclk_mgt_rxbuf_reset(1);

  -- Synchronize the rxpmareset signal
  rxpmareset_sync1_i : rxaui_0_ff_synchronizer
    generic map (
      C_NUM_SYNC_REGS => 5)
    port map (
      clk      => dclk,
      data_in  => gt1_rxpmareset_in,
      data_out => gt1_rxpmareset
    );

  gt1_rxpcsreset <= gt1_rxpcsreset_in;
  gt1_rxuserrdy  <= mgt_rxuserrdy;

  mgt_codevalid   <= not (mgt_rxnotintable_reg or mgt_rxdisperr_reg);

  -- The Actual GT Loopback is can be set by the external gt_control port
  -- logical OR this. The user should not drive both the XAUI Loopback
  -- and gt_control ports simultaneously
  loopback_int    <= "010" when mgt_loopback_r = '1' else "000";

  mgt_powerdown_2 <= mgt_powerdown & mgt_powerdown;
  mgt_txuserrdy   <= uclk_txlock;
  mgt_rxuserrdy   <= uclk_txlock;


  -- Synchronize the PLL Locked signal
  plllocked_sync_i : rxaui_0_ff_synchronizer
    generic map (
      C_NUM_SYNC_REGS => 5)
    port map (
     clk      => clk156,
     data_in  => qplllock,
     data_out => uclk_plllocked
    );


 -- Synchronize the dclk_reset_count_done signal to clk156
  reset_count_done_sync_i : rxaui_0_ff_synchronizer
    generic map (
      C_NUM_SYNC_REGS => 5)
    port map (
     clk      => clk156,
     data_in  => dclk_reset_count_done,
     data_out => uclk_reset_count_done
    );


  process(clk156) begin
    if rising_edge(clk156) then
      core_mgt_rx_reset(0) <= not uclk_gt0_rxresetdone;
      core_mgt_rx_reset(1) <= not uclk_gt1_rxresetdone;
    end if;
  end process;

  -- Detect falling edge of mgt_powerdown and mgt_loopback
  p_gt_r : process(clk156)
  begin
    if rising_edge(clk156) then
      mgt_powerdown_r <= mgt_powerdown;
      mgt_loopback_r  <= mgt_loopback;
    end if;
  end process;

  p_powerdown_falling : process(clk156)
  begin
    if rising_edge(clk156) then
      if mgt_powerdown_r = '1' and mgt_powerdown = '0' then
        uclk_mgt_powerdown_falling <= '1';
      else
        uclk_mgt_powerdown_falling <= '0';
      end if;
    end if;
  end process;

  p_loopback_falling : process(clk156)
  begin
    if rising_edge(clk156) then
      if mgt_loopback_r = '1' and mgt_loopback = '0' then
        uclk_mgt_loopback_falling <= '1';
      else
        uclk_mgt_loopback_falling <= '0';
      end if;
    end if;
  end process;

  RXBUFERR_P: process (uclk_mgt_rxbufstatus_reg)
  begin
    for i in 0 to 1 loop
      uclk_mgt_rxbuferr(i) <= IsBufError(uclk_mgt_rxbufstatus_reg(i*3+2 downto i*3));
    end loop;
  end process;

  -- chbond counter. Resets the GTX RX Buffers if the core fails to align due
  -- to extra skew introduced by the buffers.
  process (clk156) begin
    if rising_edge(clk156) then
      if ((uclk_chbond_counter(CHBOND_COUNT_LENGTH-1) = '1') or (uclk_align_status = '1') or (rxprbs_in_use = '1')) then
        uclk_chbond_counter <= (others => '0');
      elsif (uclk_sync_status = "1111") then
        uclk_chbond_counter <= uclk_chbond_counter + 1;
      else
        uclk_chbond_counter <= (others => '0');
      end if;
    end if;
  end process;

  -- Detect when the Rx PRBS is in use.  When it is, auto-generated periodic uclk_sync_counter
  -- and uclk_cbm_rx_reset resets will be inhibited
  process (clk156) begin
    if rising_edge(clk156) then
      if (gt0_rxprbssel_in  /= "000" or gt1_rxprbssel_in  /= "000") then
        rxprbs_in_use <= '1';
      else
        rxprbs_in_use <= '0';
      end if;
    end if;
  end process;

  -- sync timeout counter. GTX requires a reset if the far end powers down.
  process (clk156) begin
    if rising_edge(clk156) then
      if (uclk_sync_counter(SYNC_COUNT_LENGTH-1) = '1' or mgt_powerdown = '1' or rxprbs_in_use = '1') then
        uclk_sync_counter <= (others => '0');
      elsif (uclk_sync_status /= "1111") then
        uclk_sync_counter <= uclk_sync_counter + 1;
      else
        uclk_sync_counter <= (others => '0');
      end if;
    end if;
  end process;

   -- reset logic
  uclk_txlock <= uclk_plllocked;
  uclk_rxlock <= "11" when uclk_txlock = '1' else "00";
  mgt_tx_fault <= "11" when uclk_phase_align_complete = '0' else "00";

  process (clk156)
  begin
    if rising_edge(clk156) then
    uclk_mgt_rx_reset <= (uclk_soft_reset or uclk_reset156 or (not uclk_txlock) or uclk_mgt_powerdown_falling or
                          uclk_mgt_loopback_falling or uclk_sync_counter(SYNC_COUNT_LENGTH-1)) and uclk_reset_count_done;
    uclk_mgt_tx_reset <= (uclk_soft_reset or uclk_reset156 or (not uclk_txlock) or uclk_mgt_powerdown_falling) and
                          uclk_reset_count_done;
    end if;
  end process;

  -- reset the rx side when the buffer overflows / underflows
  process (clk156)
  begin
    if rising_edge(clk156) then
      if (uclk_mgt_rxbuferr /= "00") or (uclk_chbond_counter(CHBOND_COUNT_LENGTH-1) = '1') then
        uclk_mgt_rxbuf_reset <= "11";
      else
        uclk_mgt_rxbuf_reset <= "00";
      end if;
    end if;
  end process;

  p_mgt_reg : process(clk156)
  begin
    if rising_edge(clk156) then
        mgt_rxdata_reg          <= mgt_rxdata;
        mgt_rxcharisk_reg       <= mgt_rxcharisk;
        mgt_rxnotintable_reg    <= mgt_rxnotintable;
        mgt_rxdisperr_reg       <= mgt_rxdisperr;
        mgt_codecomma_reg       <= mgt_rxchariscomma;
        uclk_mgt_rxbufstatus_reg <= uclk_mgt_rxbufstatus;
        uclk_txresetdone_reg(0) <= uclk_gt0_txresetdone;
        uclk_txresetdone_reg(1) <= uclk_gt1_txresetdone;
    end if;
  end process p_mgt_reg;

  reset_counter_i : rxaui_0_reset_counter
  port map (
    clk           => dclk,
    done          => dclk_reset_count_done);

    --------------------------- TX Buffer Bypass Logic --------------------
    -- The TX SYNC Module drives the ports needed to Bypass the TX Buffer.

  process (clk156) begin
    if (rising_edge(clk156)) then
      if (uclk_reset_count_done = '1') then
        if (uclk_txresetdone_reg = "11") then
          uclk_txsync_start_phase_align <= '1';
        else
          uclk_txsync_start_phase_align <= '0';
        end if;
      end if;
    end if;
  end process;

  txsync_i : rxaui_0_gt_wrapper_tx_sync_manual
  generic map
  ( NUMBER_OF_LANES      => 2,
    MASTER_LANE_ID       => 0
   )
  port map
  (
    STABLE_CLOCK         => clk156,
    RESET_PHALIGNMENT    => uclk_mgt_tx_reset,
    RUN_PHALIGNMENT      => uclk_txsync_start_phase_align,
    PHASE_ALIGNMENT_DONE => uclk_phase_align_complete,
    TXDLYSRESET          => gt_txdlysreset,
    TXDLYSRESETDONE      => gt_txdlysresetdone,
    TXPHINIT             => gt_txphinit,
    TXPHINITDONE         => gt_txphinitdone,
    TXPHALIGN            => gt_txphalign,
    TXPHALIGNDONE        => gt_txphaligndone,
    TXDLYEN              => gt_txdlyen
  );


end wrapper;
