// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
// Date        : Wed Mar 29 09:06:36 2017
// Host        : PCKVAS running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               C:/projects/dfc/xenie/Eth_example/trunk/src/ip/rxaui_0/rxaui_0_stub.v
// Design      : rxaui_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k70tfbg676-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "rxaui_v4_3_7,Vivado 2016.4" *)
module rxaui_0(reset, dclk, clk156_out, clk156_lock, refclk_p, 
  refclk_n, qplloutclk_out, qplllock_out, qplloutrefclk_out, refclk_out, xgmii_txd, xgmii_txc, 
  xgmii_rxd, xgmii_rxc, rxaui_tx_l0_p, rxaui_tx_l0_n, rxaui_tx_l1_p, rxaui_tx_l1_n, 
  rxaui_rx_l0_p, rxaui_rx_l0_n, rxaui_rx_l1_p, rxaui_rx_l1_n, signal_detect, debug, mdc, mdio_in, 
  mdio_out, mdio_tri, prtad, type_sel)
/* synthesis syn_black_box black_box_pad_pin="reset,dclk,clk156_out,clk156_lock,refclk_p,refclk_n,qplloutclk_out,qplllock_out,qplloutrefclk_out,refclk_out,xgmii_txd[63:0],xgmii_txc[7:0],xgmii_rxd[63:0],xgmii_rxc[7:0],rxaui_tx_l0_p,rxaui_tx_l0_n,rxaui_tx_l1_p,rxaui_tx_l1_n,rxaui_rx_l0_p,rxaui_rx_l0_n,rxaui_rx_l1_p,rxaui_rx_l1_n,signal_detect[1:0],debug[5:0],mdc,mdio_in,mdio_out,mdio_tri,prtad[4:0],type_sel[1:0]" */;
  input reset;
  input dclk;
  output clk156_out;
  output clk156_lock;
  input refclk_p;
  input refclk_n;
  output qplloutclk_out;
  output qplllock_out;
  output qplloutrefclk_out;
  output refclk_out;
  input [63:0]xgmii_txd;
  input [7:0]xgmii_txc;
  output [63:0]xgmii_rxd;
  output [7:0]xgmii_rxc;
  output rxaui_tx_l0_p;
  output rxaui_tx_l0_n;
  output rxaui_tx_l1_p;
  output rxaui_tx_l1_n;
  input rxaui_rx_l0_p;
  input rxaui_rx_l0_n;
  input rxaui_rx_l1_p;
  input rxaui_rx_l1_n;
  input [1:0]signal_detect;
  output [5:0]debug;
  input mdc;
  input mdio_in;
  output mdio_out;
  output mdio_tri;
  input [4:0]prtad;
  input [1:0]type_sel;
endmodule
