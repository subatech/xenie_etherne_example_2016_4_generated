-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
-- Date        : Wed Mar 29 09:06:36 2017
-- Host        : PCKVAS running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/projects/dfc/xenie/Eth_example/trunk/src/ip/rxaui_0/rxaui_0_stub.vhdl
-- Design      : rxaui_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k70tfbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity rxaui_0 is
  Port ( 
    reset : in STD_LOGIC;
    dclk : in STD_LOGIC;
    clk156_out : out STD_LOGIC;
    clk156_lock : out STD_LOGIC;
    refclk_p : in STD_LOGIC;
    refclk_n : in STD_LOGIC;
    qplloutclk_out : out STD_LOGIC;
    qplllock_out : out STD_LOGIC;
    qplloutrefclk_out : out STD_LOGIC;
    refclk_out : out STD_LOGIC;
    xgmii_txd : in STD_LOGIC_VECTOR ( 63 downto 0 );
    xgmii_txc : in STD_LOGIC_VECTOR ( 7 downto 0 );
    xgmii_rxd : out STD_LOGIC_VECTOR ( 63 downto 0 );
    xgmii_rxc : out STD_LOGIC_VECTOR ( 7 downto 0 );
    rxaui_tx_l0_p : out STD_LOGIC;
    rxaui_tx_l0_n : out STD_LOGIC;
    rxaui_tx_l1_p : out STD_LOGIC;
    rxaui_tx_l1_n : out STD_LOGIC;
    rxaui_rx_l0_p : in STD_LOGIC;
    rxaui_rx_l0_n : in STD_LOGIC;
    rxaui_rx_l1_p : in STD_LOGIC;
    rxaui_rx_l1_n : in STD_LOGIC;
    signal_detect : in STD_LOGIC_VECTOR ( 1 downto 0 );
    debug : out STD_LOGIC_VECTOR ( 5 downto 0 );
    mdc : in STD_LOGIC;
    mdio_in : in STD_LOGIC;
    mdio_out : out STD_LOGIC;
    mdio_tri : out STD_LOGIC;
    prtad : in STD_LOGIC_VECTOR ( 4 downto 0 );
    type_sel : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );

end rxaui_0;

architecture stub of rxaui_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "reset,dclk,clk156_out,clk156_lock,refclk_p,refclk_n,qplloutclk_out,qplllock_out,qplloutrefclk_out,refclk_out,xgmii_txd[63:0],xgmii_txc[7:0],xgmii_rxd[63:0],xgmii_rxc[7:0],rxaui_tx_l0_p,rxaui_tx_l0_n,rxaui_tx_l1_p,rxaui_tx_l1_n,rxaui_rx_l0_p,rxaui_rx_l0_n,rxaui_rx_l1_p,rxaui_rx_l1_n,signal_detect[1:0],debug[5:0],mdc,mdio_in,mdio_out,mdio_tri,prtad[4:0],type_sel[1:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "rxaui_v4_3_7,Vivado 2016.4";
begin
end;
