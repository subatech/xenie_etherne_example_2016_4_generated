-- (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: dfcdesign.cz:dfc:udp_ip_10g:1.0
-- IP Revision: 3

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY udp_ip_10g_0 IS
  PORT (
    RST : IN STD_LOGIC;
    CLK : IN STD_LOGIC;
    LINK_SPEED : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    HOST_MAC : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    HOST_IP : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    HOST_IP_NETMASK : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    TX_DST_MAC : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    TX_DST_IP : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    TX_SRC_UDP : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    TX_DST_UDP : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    TX_FRAME_VALID : IN STD_LOGIC;
    TX_FRAME_RDY : OUT STD_LOGIC;
    TX_FRAME_LAST : IN STD_LOGIC;
    TX_FRAME_BE : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    TX_FRAME_DATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    RX_SRC_MAC : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    RX_SRC_IP : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    RX_SRC_UDP : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    RX_DST_UDP : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    RX_FRAME_VALID : OUT STD_LOGIC;
    RX_FRAME_RDY : IN STD_LOGIC;
    RX_FRAME_LAST : OUT STD_LOGIC;
    RX_FRAME_BE : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    RX_FRAME_DATA : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    RX_FRAME_LENGTH : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    XGMII_TXC : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    XGMII_TXD : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    XGMII_RXC : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    XGMII_RXD : IN STD_LOGIC_VECTOR(63 DOWNTO 0)
  );
END udp_ip_10g_0;

ARCHITECTURE udp_ip_10g_0_arch OF udp_ip_10g_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF udp_ip_10g_0_arch: ARCHITECTURE IS "yes";
  COMPONENT udp_ip_10g IS
    GENERIC (
      g_tx_dfifo_depth : INTEGER;
      g_tx_tfifo_depth : INTEGER;
      g_rx_dfifo_depth : INTEGER;
      g_rx_tfifo_depth : INTEGER;
      g_tx_dfifo_type : STRING;
      g_tx_tfifo_type : STRING;
      g_rx_dfifo_type : STRING;
      g_rx_tfifo_type : STRING
    );
    PORT (
      RST : IN STD_LOGIC;
      CLK : IN STD_LOGIC;
      LINK_SPEED : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      HOST_MAC : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
      HOST_IP : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      HOST_IP_NETMASK : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      TX_DST_MAC : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
      TX_DST_IP : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      TX_SRC_UDP : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      TX_DST_UDP : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      TX_FRAME_VALID : IN STD_LOGIC;
      TX_FRAME_RDY : OUT STD_LOGIC;
      TX_FRAME_LAST : IN STD_LOGIC;
      TX_FRAME_BE : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      TX_FRAME_DATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      RX_SRC_MAC : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
      RX_SRC_IP : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      RX_SRC_UDP : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      RX_DST_UDP : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      RX_FRAME_VALID : OUT STD_LOGIC;
      RX_FRAME_RDY : IN STD_LOGIC;
      RX_FRAME_LAST : OUT STD_LOGIC;
      RX_FRAME_BE : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      RX_FRAME_DATA : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      RX_FRAME_LENGTH : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      XGMII_TXC : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      XGMII_TXD : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      XGMII_RXC : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      XGMII_RXD : IN STD_LOGIC_VECTOR(63 DOWNTO 0)
    );
  END COMPONENT udp_ip_10g;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF udp_ip_10g_0_arch: ARCHITECTURE IS "udp_ip_10g,Vivado 2016.4";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF udp_ip_10g_0_arch : ARCHITECTURE IS "udp_ip_10g_0,udp_ip_10g,{}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF RST: SIGNAL IS "xilinx.com:signal:reset:1.0 RST RST";
  ATTRIBUTE X_INTERFACE_INFO OF CLK: SIGNAL IS "xilinx.com:signal:clock:1.0 CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF XGMII_TXC: SIGNAL IS "xilinx.com:interface:xgmii:1.0 xgmii TXC";
  ATTRIBUTE X_INTERFACE_INFO OF XGMII_TXD: SIGNAL IS "xilinx.com:interface:xgmii:1.0 xgmii TXD";
  ATTRIBUTE X_INTERFACE_INFO OF XGMII_RXC: SIGNAL IS "xilinx.com:interface:xgmii:1.0 xgmii RXC";
  ATTRIBUTE X_INTERFACE_INFO OF XGMII_RXD: SIGNAL IS "xilinx.com:interface:xgmii:1.0 xgmii RXD";
BEGIN
  U0 : udp_ip_10g
    GENERIC MAP (
      g_tx_dfifo_depth => 2048,
      g_tx_tfifo_depth => 128,
      g_rx_dfifo_depth => 2048,
      g_rx_tfifo_depth => 128,
      g_tx_dfifo_type => "block",
      g_tx_tfifo_type => "block",
      g_rx_dfifo_type => "block",
      g_rx_tfifo_type => "block"
    )
    PORT MAP (
      RST => RST,
      CLK => CLK,
      LINK_SPEED => LINK_SPEED,
      HOST_MAC => HOST_MAC,
      HOST_IP => HOST_IP,
      HOST_IP_NETMASK => HOST_IP_NETMASK,
      TX_DST_MAC => TX_DST_MAC,
      TX_DST_IP => TX_DST_IP,
      TX_SRC_UDP => TX_SRC_UDP,
      TX_DST_UDP => TX_DST_UDP,
      TX_FRAME_VALID => TX_FRAME_VALID,
      TX_FRAME_RDY => TX_FRAME_RDY,
      TX_FRAME_LAST => TX_FRAME_LAST,
      TX_FRAME_BE => TX_FRAME_BE,
      TX_FRAME_DATA => TX_FRAME_DATA,
      RX_SRC_MAC => RX_SRC_MAC,
      RX_SRC_IP => RX_SRC_IP,
      RX_SRC_UDP => RX_SRC_UDP,
      RX_DST_UDP => RX_DST_UDP,
      RX_FRAME_VALID => RX_FRAME_VALID,
      RX_FRAME_RDY => RX_FRAME_RDY,
      RX_FRAME_LAST => RX_FRAME_LAST,
      RX_FRAME_BE => RX_FRAME_BE,
      RX_FRAME_DATA => RX_FRAME_DATA,
      RX_FRAME_LENGTH => RX_FRAME_LENGTH,
      XGMII_TXC => XGMII_TXC,
      XGMII_TXD => XGMII_TXD,
      XGMII_RXC => XGMII_RXC,
      XGMII_RXD => XGMII_RXD
    );
END udp_ip_10g_0_arch;
