-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
-- Date        : Thu May 03 10:13:29 2018
-- Host        : PCKVAS running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/projects/dfc/xenie/Eth_example/trunk/src/ip/udp_ip_10g_0/udp_ip_10g_0_stub.vhdl
-- Design      : udp_ip_10g_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k160tffg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity udp_ip_10g_0 is
  Port ( 
    RST : in STD_LOGIC;
    CLK : in STD_LOGIC;
    LINK_SPEED : in STD_LOGIC_VECTOR ( 2 downto 0 );
    HOST_MAC : in STD_LOGIC_VECTOR ( 47 downto 0 );
    HOST_IP : in STD_LOGIC_VECTOR ( 31 downto 0 );
    HOST_IP_NETMASK : in STD_LOGIC_VECTOR ( 31 downto 0 );
    TX_DST_MAC : in STD_LOGIC_VECTOR ( 47 downto 0 );
    TX_DST_IP : in STD_LOGIC_VECTOR ( 31 downto 0 );
    TX_SRC_UDP : in STD_LOGIC_VECTOR ( 15 downto 0 );
    TX_DST_UDP : in STD_LOGIC_VECTOR ( 15 downto 0 );
    TX_FRAME_VALID : in STD_LOGIC;
    TX_FRAME_RDY : out STD_LOGIC;
    TX_FRAME_LAST : in STD_LOGIC;
    TX_FRAME_BE : in STD_LOGIC_VECTOR ( 7 downto 0 );
    TX_FRAME_DATA : in STD_LOGIC_VECTOR ( 63 downto 0 );
    RX_SRC_MAC : out STD_LOGIC_VECTOR ( 47 downto 0 );
    RX_SRC_IP : out STD_LOGIC_VECTOR ( 31 downto 0 );
    RX_SRC_UDP : out STD_LOGIC_VECTOR ( 15 downto 0 );
    RX_DST_UDP : out STD_LOGIC_VECTOR ( 15 downto 0 );
    RX_FRAME_VALID : out STD_LOGIC;
    RX_FRAME_RDY : in STD_LOGIC;
    RX_FRAME_LAST : out STD_LOGIC;
    RX_FRAME_BE : out STD_LOGIC_VECTOR ( 7 downto 0 );
    RX_FRAME_DATA : out STD_LOGIC_VECTOR ( 63 downto 0 );
    RX_FRAME_LENGTH : out STD_LOGIC_VECTOR ( 15 downto 0 );
    XGMII_TXC : out STD_LOGIC_VECTOR ( 7 downto 0 );
    XGMII_TXD : out STD_LOGIC_VECTOR ( 63 downto 0 );
    XGMII_RXC : in STD_LOGIC_VECTOR ( 7 downto 0 );
    XGMII_RXD : in STD_LOGIC_VECTOR ( 63 downto 0 )
  );

end udp_ip_10g_0;

architecture stub of udp_ip_10g_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "RST,CLK,LINK_SPEED[2:0],HOST_MAC[47:0],HOST_IP[31:0],HOST_IP_NETMASK[31:0],TX_DST_MAC[47:0],TX_DST_IP[31:0],TX_SRC_UDP[15:0],TX_DST_UDP[15:0],TX_FRAME_VALID,TX_FRAME_RDY,TX_FRAME_LAST,TX_FRAME_BE[7:0],TX_FRAME_DATA[63:0],RX_SRC_MAC[47:0],RX_SRC_IP[31:0],RX_SRC_UDP[15:0],RX_DST_UDP[15:0],RX_FRAME_VALID,RX_FRAME_RDY,RX_FRAME_LAST,RX_FRAME_BE[7:0],RX_FRAME_DATA[63:0],RX_FRAME_LENGTH[15:0],XGMII_TXC[7:0],XGMII_TXD[63:0],XGMII_RXC[7:0],XGMII_RXD[63:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "udp_ip_10g,Vivado 2016.4";
begin
end;
