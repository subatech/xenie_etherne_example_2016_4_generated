// (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: dfcdesign.cz:dfc:udp_ip_10g:1.0
// IP Revision: 3

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
udp_ip_10g_0 your_instance_name (
  .RST(RST),                          // input wire RST
  .CLK(CLK),                          // input wire CLK
  .LINK_SPEED(LINK_SPEED),            // input wire [2 : 0] LINK_SPEED
  .HOST_MAC(HOST_MAC),                // input wire [47 : 0] HOST_MAC
  .HOST_IP(HOST_IP),                  // input wire [31 : 0] HOST_IP
  .HOST_IP_NETMASK(HOST_IP_NETMASK),  // input wire [31 : 0] HOST_IP_NETMASK
  .TX_DST_MAC(TX_DST_MAC),            // input wire [47 : 0] TX_DST_MAC
  .TX_DST_IP(TX_DST_IP),              // input wire [31 : 0] TX_DST_IP
  .TX_SRC_UDP(TX_SRC_UDP),            // input wire [15 : 0] TX_SRC_UDP
  .TX_DST_UDP(TX_DST_UDP),            // input wire [15 : 0] TX_DST_UDP
  .TX_FRAME_VALID(TX_FRAME_VALID),    // input wire TX_FRAME_VALID
  .TX_FRAME_RDY(TX_FRAME_RDY),        // output wire TX_FRAME_RDY
  .TX_FRAME_LAST(TX_FRAME_LAST),      // input wire TX_FRAME_LAST
  .TX_FRAME_BE(TX_FRAME_BE),          // input wire [7 : 0] TX_FRAME_BE
  .TX_FRAME_DATA(TX_FRAME_DATA),      // input wire [63 : 0] TX_FRAME_DATA
  .RX_SRC_MAC(RX_SRC_MAC),            // output wire [47 : 0] RX_SRC_MAC
  .RX_SRC_IP(RX_SRC_IP),              // output wire [31 : 0] RX_SRC_IP
  .RX_SRC_UDP(RX_SRC_UDP),            // output wire [15 : 0] RX_SRC_UDP
  .RX_DST_UDP(RX_DST_UDP),            // output wire [15 : 0] RX_DST_UDP
  .RX_FRAME_VALID(RX_FRAME_VALID),    // output wire RX_FRAME_VALID
  .RX_FRAME_RDY(RX_FRAME_RDY),        // input wire RX_FRAME_RDY
  .RX_FRAME_LAST(RX_FRAME_LAST),      // output wire RX_FRAME_LAST
  .RX_FRAME_BE(RX_FRAME_BE),          // output wire [7 : 0] RX_FRAME_BE
  .RX_FRAME_DATA(RX_FRAME_DATA),      // output wire [63 : 0] RX_FRAME_DATA
  .RX_FRAME_LENGTH(RX_FRAME_LENGTH),  // output wire [15 : 0] RX_FRAME_LENGTH
  .XGMII_TXC(XGMII_TXC),              // output wire [7 : 0] XGMII_TXC
  .XGMII_TXD(XGMII_TXD),              // output wire [63 : 0] XGMII_TXD
  .XGMII_RXC(XGMII_RXC),              // input wire [7 : 0] XGMII_RXC
  .XGMII_RXD(XGMII_RXD)              // input wire [63 : 0] XGMII_RXD
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

