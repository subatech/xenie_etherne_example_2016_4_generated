// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
// Date        : Thu May 03 10:13:29 2018
// Host        : PCKVAS running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               C:/projects/dfc/xenie/Eth_example/trunk/src/ip/udp_ip_10g_0/udp_ip_10g_0_stub.v
// Design      : udp_ip_10g_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k160tffg676-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "udp_ip_10g,Vivado 2016.4" *)
module udp_ip_10g_0(RST, CLK, LINK_SPEED, HOST_MAC, HOST_IP, 
  HOST_IP_NETMASK, TX_DST_MAC, TX_DST_IP, TX_SRC_UDP, TX_DST_UDP, TX_FRAME_VALID, TX_FRAME_RDY, 
  TX_FRAME_LAST, TX_FRAME_BE, TX_FRAME_DATA, RX_SRC_MAC, RX_SRC_IP, RX_SRC_UDP, RX_DST_UDP, 
  RX_FRAME_VALID, RX_FRAME_RDY, RX_FRAME_LAST, RX_FRAME_BE, RX_FRAME_DATA, RX_FRAME_LENGTH, 
  XGMII_TXC, XGMII_TXD, XGMII_RXC, XGMII_RXD)
/* synthesis syn_black_box black_box_pad_pin="RST,CLK,LINK_SPEED[2:0],HOST_MAC[47:0],HOST_IP[31:0],HOST_IP_NETMASK[31:0],TX_DST_MAC[47:0],TX_DST_IP[31:0],TX_SRC_UDP[15:0],TX_DST_UDP[15:0],TX_FRAME_VALID,TX_FRAME_RDY,TX_FRAME_LAST,TX_FRAME_BE[7:0],TX_FRAME_DATA[63:0],RX_SRC_MAC[47:0],RX_SRC_IP[31:0],RX_SRC_UDP[15:0],RX_DST_UDP[15:0],RX_FRAME_VALID,RX_FRAME_RDY,RX_FRAME_LAST,RX_FRAME_BE[7:0],RX_FRAME_DATA[63:0],RX_FRAME_LENGTH[15:0],XGMII_TXC[7:0],XGMII_TXD[63:0],XGMII_RXC[7:0],XGMII_RXD[63:0]" */;
  input RST;
  input CLK;
  input [2:0]LINK_SPEED;
  input [47:0]HOST_MAC;
  input [31:0]HOST_IP;
  input [31:0]HOST_IP_NETMASK;
  input [47:0]TX_DST_MAC;
  input [31:0]TX_DST_IP;
  input [15:0]TX_SRC_UDP;
  input [15:0]TX_DST_UDP;
  input TX_FRAME_VALID;
  output TX_FRAME_RDY;
  input TX_FRAME_LAST;
  input [7:0]TX_FRAME_BE;
  input [63:0]TX_FRAME_DATA;
  output [47:0]RX_SRC_MAC;
  output [31:0]RX_SRC_IP;
  output [15:0]RX_SRC_UDP;
  output [15:0]RX_DST_UDP;
  output RX_FRAME_VALID;
  input RX_FRAME_RDY;
  output RX_FRAME_LAST;
  output [7:0]RX_FRAME_BE;
  output [63:0]RX_FRAME_DATA;
  output [15:0]RX_FRAME_LENGTH;
  output [7:0]XGMII_TXC;
  output [63:0]XGMII_TXD;
  input [7:0]XGMII_RXC;
  input [63:0]XGMII_RXD;
endmodule
