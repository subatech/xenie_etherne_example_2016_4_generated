-------------------------------------------------------------------------------
--
-- (C) Copyright 2017 DFC Design, s.r.o., Brno, Czech Republic
-- Author: Marek Kvas (m.kvas@dspfpga.com)
--
-------------------------------------------------------------------------------
-- This file is part of Xenia Ethernet Example project.
-- 
-- Xenia Ethernet Example project is free software: you can 
-- redistribute it and/or modify it under the terms of 
-- the GNU Lesser General Public License as published by the Free 
-- Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- Xenia Ethernet Example project is distributed in the hope that 
-- it will be useful, but WITHOUT ANY WARRANTY; without even 
-- the implied warranty of MERCHANTABILITY or FITNESS FOR A 
-- PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
-- for more details.
-- 
-- You should have received a copy of the GNU Lesser General Public 
-- License along with Xenia Ethernet Example project.  If not, 
-- see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
--
-- This is a top-level entity of Xenie Ethernet test and demo design.
--
--
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.Vcomponents.all;

entity xenie_eth_example is
   generic (
      -- Arbitrary constant defined here
      -- 15 downto 12 - reserved; 7 downto 4 major; 3 downto 0 minor
      -- 11 downto 8 - target board :
      --    1 - Xenie 1.0
      -- !! careful board version may be used in generate statements anywhere!!
      g_version            : std_logic_vector(15 downto 0) := x"0110";
      -- Date and time is passed from synthesis
      g_compilation_date   : std_logic_vector(47 downto 0) := x"000000000000"
           );
   port (
      -- Free running clock
      CLK_200M_IN_P              : in  std_logic;
      CLK_200M_IN_N              : in  std_logic;

      -- UART for debugging
      MB_UART_TX                 : out std_logic;
      MB_UART_RX                 : in  std_logic;

      -- MDIO towards 10G ETH PHY
      ETH_MDIO_MDC               : out std_logic;
      ETH_MDIO_MDIO              : inout std_logic;

      -- RXAUI - Reference clock and transceivers
      ETH_RXAUI_REFCLK_P         : in  std_logic;
      ETH_RXAUI_REFCLK_N         : in  std_logic;
      ETH_RXAUI_TX_P             : out std_logic_vector(1 downto 0);
      ETH_RXAUI_TX_N             : out std_logic_vector(1 downto 0);
      ETH_RXAUI_RX_P             : in  std_logic_vector(1 downto 0);
      ETH_RXAUI_RX_N             : in  std_logic_vector(1 downto 0);

      ETH_PHY_RESETN             : out std_logic;
      ETH_PHY_RCLK1              : in  std_logic;
      ETH_PHY_INTN               : in  std_logic;
      ETH_PHY_GPIO               : in  std_logic_vector(5 downto 0);

      -- select between on-board oscillator 156.25 MHz('1')or external clock('0')
      ETH_PHY_CLK_SRC_SEL        : out std_logic;


      -- I2C towards both on-board(UID EEPROM) and off-board peripherals
      I2C_SCL                    : inout std_logic;
      I2C_SDA                    : inout std_logic;

      -- QSPI to on-board configuration SPI flash
      -- As flash is connected to cfg interface, SPI clock is connected
      -- to dedicated CCLK pin. It is driven using STARTUP primitive
      -- that is instantiated somewhere else (e.g. in AXI_QSPI core).
      CFG_QSPI_IO                : inout std_logic_vector(3 downto 0);
      CFG_QSPI_SS                : inout std_logic;

      -- Xenia LEDs
      LEDS                       : out std_logic_vector(1 downto 0);
      -- Power good signal
      ALL_SRC_PG                 : in  std_logic;

      ddr3_addr                  : out std_logic_vector ( 14 downto 0 );
      ddr3_ba                    : out std_logic_vector ( 2 downto 0 );
      ddr3_cas_n                 : out std_logic;
      ddr3_ck_n                  : out std_logic_vector ( 0 to 0 );
      ddr3_ck_p                  : out std_logic_vector ( 0 to 0 );
      ddr3_cke                   : out std_logic_vector ( 0 to 0 );
      ddr3_cs_n                  : out std_logic_vector ( 0 to 0 );
      ddr3_dm                    : out std_logic_vector ( 3 downto 0 );
      ddr3_dq                    : inout std_logic_vector ( 31 downto 0 );
      ddr3_dqs_n                 : inout std_logic_vector ( 3 downto 0 );
      ddr3_dqs_p                 : inout std_logic_vector ( 3 downto 0 );
      ddr3_odt                   : out std_logic_vector ( 0 to 0 );
      ddr3_ras_n                 : out std_logic;
      ddr3_reset_n               : out std_logic;
      ddr3_we_n                  : out std_logic;
      
      DBG_PORT                   : out std_logic_vector(12 downto 0)
      
        );
end entity;





architecture synthesis of xenie_eth_example is

  component main_bd is
  port (
      MB_MDIO_mdio_t : out STD_LOGIC;
      MB_MDIO_mdio_o : out STD_LOGIC;
      MB_MDIO_mdc : out STD_LOGIC;
      MB_MDIO_mdio_i : in STD_LOGIC;
      SPI_RTL_io0_i : in STD_LOGIC;
      SPI_RTL_io0_o : out STD_LOGIC;
      SPI_RTL_io0_t : out STD_LOGIC;
      SPI_RTL_io1_i : in STD_LOGIC;
      SPI_RTL_io1_o : out STD_LOGIC;
      SPI_RTL_io1_t : out STD_LOGIC;
      SPI_RTL_io2_i : in STD_LOGIC;
      SPI_RTL_io2_o : out STD_LOGIC;
      SPI_RTL_io2_t : out STD_LOGIC;
      SPI_RTL_io3_i : in STD_LOGIC;
      SPI_RTL_io3_o : out STD_LOGIC;
      SPI_RTL_io3_t : out STD_LOGIC;
      SPI_RTL_ss_i : in STD_LOGIC_VECTOR ( 0 to 0 );
      SPI_RTL_ss_o : out STD_LOGIC_VECTOR ( 0 to 0 );
      SPI_RTL_ss_t : out STD_LOGIC;
      MB_UART_rxd : in STD_LOGIC;
      MB_UART_txd : out STD_LOGIC;
      IIC_RTL_scl_i : in STD_LOGIC;
      IIC_RTL_scl_o : out STD_LOGIC;
      IIC_RTL_scl_t : out STD_LOGIC;
      IIC_RTL_sda_i : in STD_LOGIC;
      IIC_RTL_sda_o : out STD_LOGIC;
      IIC_RTL_sda_t : out STD_LOGIC;
      DDR3_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
      DDR3_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR3_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR3_addr : out STD_LOGIC_VECTOR ( 14 downto 0 );
      DDR3_ba : out STD_LOGIC_VECTOR ( 2 downto 0 );
      DDR3_ras_n : out STD_LOGIC;
      DDR3_cas_n : out STD_LOGIC;
      DDR3_we_n : out STD_LOGIC;
      DDR3_reset_n : out STD_LOGIC;
      DDR3_ck_p : out STD_LOGIC_VECTOR ( 0 to 0 );
      DDR3_ck_n : out STD_LOGIC_VECTOR ( 0 to 0 );
      DDR3_cke : out STD_LOGIC_VECTOR ( 0 to 0 );
      DDR3_cs_n : out STD_LOGIC_VECTOR ( 0 to 0 );
      DDR3_dm : out STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR3_odt : out STD_LOGIC_VECTOR ( 0 to 0 );
      GPIO_IO_I : in STD_LOGIC_VECTOR ( 31 downto 0 );
      GPIO_IO_O : out STD_LOGIC_VECTOR ( 31 downto 0 );
      GPIO_IO_T : out STD_LOGIC_VECTOR ( 31 downto 0 );
      GPIO2_IO_I : in STD_LOGIC_VECTOR ( 31 downto 0 );
      GPIO2_IO_O : out STD_LOGIC_VECTOR ( 31 downto 0 );
      GPIO2_IO_T : out STD_LOGIC_VECTOR ( 31 downto 0 );
      VERSION_GPIO : in STD_LOGIC_VECTOR ( 63 downto 0 );
      AXI_CLK : out STD_LOGIC;
      SYS_CLK_I : in STD_LOGIC;
      HOST_MAC0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
      HOST_MAC1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
      HOST_IP : out STD_LOGIC_VECTOR ( 31 downto 0 );
      HOST_IP_NETMASK : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component main_bd;

  component rxaui_0 is
  port (
      reset : IN STD_LOGIC;
      dclk : IN STD_LOGIC;
      clk156_out : OUT STD_LOGIC;
      clk156_lock : OUT STD_LOGIC;
      refclk_out : OUT STD_LOGIC;
      refclk_p : IN STD_LOGIC;
      refclk_n : IN STD_LOGIC;
      qplloutclk_out : OUT STD_LOGIC;
      qplllock_out : OUT STD_LOGIC;
      qplloutrefclk_out : OUT STD_LOGIC;
      xgmii_txd : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      xgmii_txc : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      xgmii_rxd : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      xgmii_rxc : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      rxaui_tx_l0_p : OUT STD_LOGIC;
      rxaui_tx_l0_n : OUT STD_LOGIC;
      rxaui_tx_l1_p : OUT STD_LOGIC;
      rxaui_tx_l1_n : OUT STD_LOGIC;
      rxaui_rx_l0_p : IN STD_LOGIC;
      rxaui_rx_l0_n : IN STD_LOGIC;
      rxaui_rx_l1_p : IN STD_LOGIC;
      rxaui_rx_l1_n : IN STD_LOGIC;
      signal_detect : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      debug : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
      mdc : IN STD_LOGIC;
      mdio_in : IN STD_LOGIC;
      mdio_out : OUT STD_LOGIC;
      mdio_tri : OUT STD_LOGIC;
      prtad : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      type_sel : IN STD_LOGIC_VECTOR(1 DOWNTO 0)
   );
   end component;

   component udp_ip_10g_0 is
   port (
      RST : IN STD_LOGIC;
      CLK : IN STD_LOGIC;
      LINK_SPEED : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      HOST_MAC : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
      HOST_IP : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      HOST_IP_NETMASK : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      TX_DST_MAC : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
      TX_DST_IP : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      TX_SRC_UDP : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      TX_DST_UDP : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      TX_FRAME_VALID : IN STD_LOGIC;
      TX_FRAME_RDY : OUT STD_LOGIC;
      TX_FRAME_LAST : IN STD_LOGIC;
      TX_FRAME_BE : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      TX_FRAME_DATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      RX_SRC_MAC : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
      RX_SRC_IP : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      RX_SRC_UDP : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      RX_DST_UDP : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      RX_FRAME_VALID : OUT STD_LOGIC;
      RX_FRAME_RDY : IN STD_LOGIC;
      RX_FRAME_LAST : OUT STD_LOGIC;
      RX_FRAME_BE : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      RX_FRAME_DATA : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      RX_FRAME_LENGTH : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      XGMII_TXC : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      XGMII_TXD : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      XGMII_RXC : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      XGMII_RXD : IN STD_LOGIC_VECTOR(63 DOWNTO 0)
   );
   end component;


   signal sys_clk                   : std_logic;

   signal clk_200_mhz_in_clk_n      : std_logic;
   signal clk_200_mhz_in_clk_p      : std_logic;
   signal mb_mdio_mdc               : std_logic;
   signal mb_mdio_mdio_i            : std_logic;
   signal mb_mdio_mdio_o            : std_logic;
   signal mb_mdio_mdio_t            : std_logic;
   signal mb_uart_rxd               : std_logic;
   signal mb_uart_txd               : std_logic;
   signal rxaui_clk156_lock         : std_logic;
   signal rxaui_clk156_out          : std_logic;
   signal rxaui_debug               : std_logic_vector ( 5 downto 0 );
   signal rxaui_mdio_mdc            : std_logic;
   signal rxaui_mdio_mdio_i         : std_logic;
   signal rxaui_mdio_mdio_o         : std_logic;
   signal rxaui_mdio_mdio_t         : std_logic;
   signal rxaui_qplllock_out        : std_logic;
   signal rxaui_refclk_clk_n        : std_logic;
   signal rxaui_refclk_clk_p        : std_logic;
   signal rxaui_rst                 : std_logic;
   signal rxaui_xgmii_rxc           : std_logic_vector ( 7 downto 0 );
   signal rxaui_xgmii_rxd           : std_logic_vector ( 63 downto 0 );
   signal rxaui_xgmii_txc           : std_logic_vector ( 7 downto 0 );
   signal rxaui_xgmii_txd           : std_logic_vector ( 63 downto 0 );
   signal version_gpio              : std_logic_vector ( 63 downto 0 );
   signal gpio2_io_i                : std_logic_vector ( 31 downto 0 );
   signal gpio2_io_o                : std_logic_vector ( 31 downto 0 );
   signal gpio2_io_t                : std_logic_vector ( 31 downto 0 );
   signal gpio_io_i                 : std_logic_vector ( 31 downto 0 );
   signal gpio_io_o                 : std_logic_vector ( 31 downto 0 );
   signal gpio_io_t                 : std_logic_vector ( 31 downto 0 );
   signal iic_rtl_scl_i             : std_logic;
   signal iic_rtl_scl_o             : std_logic;
   signal iic_rtl_scl_t             : std_logic;
   signal iic_rtl_sda_i             : std_logic;
   signal iic_rtl_sda_o             : std_logic;
   signal iic_rtl_sda_t             : std_logic;
   signal rxaui_rx_l0_n             : std_logic;
   signal rxaui_rx_l0_p             : std_logic;
   signal rxaui_rx_l1_n             : std_logic;
   signal rxaui_rx_l1_p             : std_logic;
   signal rxaui_tx_l0_n             : std_logic;
   signal rxaui_tx_l0_p             : std_logic;
   signal rxaui_tx_l1_n             : std_logic;
   signal rxaui_tx_l1_p             : std_logic;
   signal spi_rtl_io0_i             : std_logic;
   signal spi_rtl_io0_o             : std_logic;
   signal spi_rtl_io0_t             : std_logic;
   signal spi_rtl_io1_i             : std_logic;
   signal spi_rtl_io1_o             : std_logic;
   signal spi_rtl_io1_t             : std_logic;
   signal spi_rtl_io2_i             : std_logic;
   signal spi_rtl_io2_o             : std_logic;
   signal spi_rtl_io2_t             : std_logic;
   signal spi_rtl_io3_i             : std_logic;
   signal spi_rtl_io3_o             : std_logic;
   signal spi_rtl_io3_t             : std_logic;
   signal spi_rtl_ss_i              : std_logic_vector ( 0 to 0 );
   signal spi_rtl_ss_o              : std_logic_vector ( 0 to 0 );
   signal spi_rtl_ss_t              : std_logic;

   -- Alive counter 
   signal alive_cnt_sys_clk         : std_logic_vector(25 downto 0);
   signal alive_cnt_rxaui_clk       : std_logic_vector(25 downto 0);

   signal clk_200m_buffered         : std_logic;

   -- Frame generator signals
   signal fg_reset_mb               : std_logic;
   signal fg_reset_meta             : std_logic;
   signal fg_reset_sync             : std_logic;

   signal xgmii_loopback_mb         : std_logic;
   signal xgmii_loopback_meta       : std_logic;
   signal xgmii_loopback_sync       : std_logic;

   signal rxaui_xgmii_txc_fg        : std_logic_vector(7 downto 0);
   signal rxaui_xgmii_txd_fg        : std_logic_vector(63 downto 0);

   signal mb_host_mac1              : std_logic_vector(31 downto 0);

   -- General control signals
   signal udp_link_speed            : std_logic_vector(2 downto 0);

   signal udp_host_mac              : std_logic_vector(47 downto 0);
   signal udp_host_ip               : std_logic_vector(31 downto 0);
   signal udp_host_ip_netmask       : std_logic_vector(31 downto 0);

   signal udp_tx_dst_mac            : std_logic_vector(47 downto 0);
   signal udp_tx_dst_ip             : std_logic_vector(31 downto 0);
   signal udp_tx_src_udp            : std_logic_vector(15 downto 0);
   signal udp_tx_dst_udp            : std_logic_vector(15 downto 0);

   signal udp_tx_frame_valid        : std_logic;
   signal udp_tx_frame_rdy          : std_logic;
   signal udp_tx_frame_last         : std_logic;
   signal udp_tx_frame_be           : std_logic_vector(7 downto 0);
   signal udp_tx_frame_data         : std_logic_vector(63 downto 0);

   signal udp_rx_src_mac            : std_logic_vector(47 downto 0);
   signal udp_rx_src_ip             : std_logic_vector(31 downto 0);
   signal udp_rx_src_udp            : std_logic_vector(15 downto 0);
   signal udp_rx_dst_udp            : std_logic_vector(15 downto 0);

   signal udp_rx_frame_valid        : std_logic;
   signal udp_rx_frame_rdy          : std_logic;
   signal udp_rx_frame_last         : std_logic;
   signal udp_rx_frame_be           : std_logic_vector(7 downto 0);
   signal udp_rx_frame_data         : std_logic_vector(63 downto 0);
   signal udp_rx_frame_length       : std_logic_vector(15 downto 0);


   -- Helpers
   signal gpio_io_t_1_d             : std_logic;
   signal eth_phy_resetn_i          : std_logic := '0';
   

   attribute ASYNC_REG     : string;
   attribute ASYNC_REG of fg_reset_meta: signal is "TRUE";
   attribute ASYNC_REG of xgmii_loopback_meta: signal is "TRUE";

begin

   -- Synchronize control signals to XGMII domain
   xgmii_sync_proc : process(rxaui_clk156_out)
   begin
      if rising_edge(rxaui_clk156_out) then
         xgmii_loopback_meta <= xgmii_loopback_mb;
         xgmii_loopback_sync <= xgmii_loopback_meta;

         fg_reset_meta <= fg_reset_mb;
         fg_reset_sync <= fg_reset_meta;
      end if;
   end process;

   -- Loopback on XGMII
   xgmii_loopback_proc : process(rxaui_clk156_out)
   begin
      if rising_edge(rxaui_clk156_out) then
         if xgmii_loopback_sync = '1' then
            rxaui_xgmii_txc <= rxaui_xgmii_rxc;
            rxaui_xgmii_txd <= rxaui_xgmii_rxd;
         else
            rxaui_xgmii_txc <= rxaui_xgmii_txc_fg;
            rxaui_xgmii_txd <= rxaui_xgmii_txd_fg;
         end if;
      end if;
   end process;



   udp_ip_10g_0_inst : udp_ip_10g_0
   port map (
      RST               => fg_reset_sync,
      CLK               => rxaui_clk156_out,

      LINK_SPEED        => udp_link_speed,
      
      HOST_MAC          => udp_host_mac,
      HOST_IP           => udp_host_ip,
      HOST_IP_NETMASK   => udp_host_ip_netmask,

      TX_DST_MAC        => udp_tx_dst_mac,
      TX_DST_IP         => udp_tx_dst_ip,
      TX_SRC_UDP        => udp_tx_src_udp,
      TX_DST_UDP        => udp_tx_dst_udp,

      TX_FRAME_VALID    => udp_tx_frame_valid,
      TX_FRAME_RDY      => udp_tx_frame_rdy,
      TX_FRAME_LAST     => udp_tx_frame_last,
      TX_FRAME_BE       => udp_tx_frame_be,
      TX_FRAME_DATA     => udp_tx_frame_data,

      RX_SRC_MAC        => udp_rx_src_mac,
      RX_SRC_IP         => udp_rx_src_ip,
      RX_SRC_UDP        => udp_rx_src_udp,
      RX_DST_UDP        => udp_rx_dst_udp,

      RX_FRAME_VALID    => udp_rx_frame_valid,
      RX_FRAME_RDY      => udp_rx_frame_rdy,
      RX_FRAME_LAST     => udp_rx_frame_last,
      RX_FRAME_BE       => udp_rx_frame_be,
      RX_FRAME_DATA     => udp_rx_frame_data,
      RX_FRAME_LENGTH   => udp_rx_frame_length,
      
      XGMII_TXC         => rxaui_xgmii_txc_fg,
      XGMII_TXD         => rxaui_xgmii_txd_fg,
      XGMII_RXC         => rxaui_xgmii_rxc,
      XGMII_RXD         => rxaui_xgmii_rxd
      

        );

   udp_ip_10g_test_app_inst : entity work.udp_ip_10g_test_app
   port map (
      RST               => fg_reset_sync,
      CLK               => rxaui_clk156_out,

      HOST_MAC          => udp_host_mac,
      HOST_IP           => udp_host_ip,
      HOST_IP_NETMASK   => udp_host_ip_netmask,

      TX_DST_MAC        => udp_tx_dst_mac,
      TX_DST_IP         => udp_tx_dst_ip,
      TX_SRC_UDP        => udp_tx_src_udp,
      TX_DST_UDP        => udp_tx_dst_udp,

      TX_FRAME_VALID    => udp_tx_frame_valid,
      TX_FRAME_RDY      => udp_tx_frame_rdy,
      TX_FRAME_LAST     => udp_tx_frame_last,
      TX_FRAME_BE       => udp_tx_frame_be,
      TX_FRAME_DATA     => udp_tx_frame_data,

      RX_SRC_MAC        => udp_rx_src_mac,
      RX_SRC_IP         => udp_rx_src_ip,
      RX_SRC_UDP        => udp_rx_src_udp,
      RX_DST_UDP        => udp_rx_dst_udp,

      RX_FRAME_VALID    => udp_rx_frame_valid,
      RX_FRAME_RDY      => udp_rx_frame_rdy,
      RX_FRAME_LAST     => udp_rx_frame_last,
      RX_FRAME_BE       => udp_rx_frame_be,
      RX_FRAME_DATA     => udp_rx_frame_data,
      RX_FRAME_LENGTH   => udp_rx_frame_length
        );        





   -- Map GPIO signals
   -- False path should be applied on rxaui reset even though it is internally
   -- synchronized
   rxaui_rst <= gpio_io_o(0);
   gpio_io_i(0) <= gpio_io_o(0);

   -- Change PHY reset state only when direction register falling edge
   phy_rst_proc : process(sys_clk)
   begin
      if rising_edge(sys_clk) then
         gpio_io_t_1_d <= gpio_io_t(1);
         if gpio_io_t(1) = '0' and gpio_io_t_1_d = '1' then
            eth_phy_resetn_i <= gpio_io_o(1);
         end if;
      end if;
   end process;
   ETH_PHY_RESETN <= eth_phy_resetn_i;
   gpio_io_i(1) <= eth_phy_resetn_i;

   gpio_io_i(2) <= rxaui_clk156_lock;
   gpio_io_i(3) <= rxaui_qplllock_out;

   gpio_io_i(9 downto 4) <= rxaui_debug;

   ETH_PHY_CLK_SRC_SEL <= gpio_io_o(10) when gpio_io_t(10) = '0' else 'Z';
   gpio_io_i(10) <= gpio_io_o(10);

   gpio_io_i(11) <= ALL_SRC_PG;

   gpio_io_i(12) <= '0';
   fg_reset_mb <= not gpio_io_o(13);
   
   xgmii_loopback_mb <= gpio_io_o(14);

   gpio_io_i(31 downto 13) <= gpio_io_o(31 downto 13);
   gpio2_io_i <= gpio2_io_o;



   -- Alive counter demonstrating free running clocks are operational
   -- with connection to LED
   free_clk_alive_proc : process(sys_clk)
   begin
      if rising_edge(sys_clk) then
         alive_cnt_sys_clk <= std_logic_vector(unsigned(alive_cnt_sys_clk) + 1);
      end if;
   end process;
   LEDS(0) <= alive_cnt_sys_clk(alive_cnt_sys_clk'left);


   -- Alive counter demonstrating PHY clocks are operational
   -- with connection to LED
   rxaui_clk_alive_proc : process(rxaui_clk156_out)
   begin
      if rising_edge(rxaui_clk156_out) then
         alive_cnt_rxaui_clk <= std_logic_vector(unsigned(alive_cnt_rxaui_clk) + 1);
      end if;
   end process;
   LEDS(1) <= alive_cnt_rxaui_clk(alive_cnt_rxaui_clk'left);

   -- Version GPIO is combination of generics
   version_gpio <= g_version & g_compilation_date;



   -- Create internal connection of external MDIO and
   -- RXAUI core control mdio
   ETH_MDIO_MDC <= mb_mdio_mdc;
   ETH_MDIO_MDIO <= 'Z' when mb_mdio_mdio_t = '1' else mb_mdio_mdio_o;
   mb_mdio_mdio_i <= ETH_MDIO_MDIO and (rxaui_mdio_mdio_o or rxaui_mdio_mdio_t);
   rxaui_mdio_mdio_i <= mb_mdio_mdio_o;
   rxaui_mdio_mdc <= mb_mdio_mdc;


   DBG_PORT <= "0000000" &
               udp_rx_frame_last & udp_rx_frame_rdy & udp_rx_frame_valid &
               udp_tx_frame_last & udp_tx_frame_rdy & udp_tx_frame_valid;

   -- Buffer input clock
   IBUFGDS_inst : IBUFGDS
   generic map (
      DIFF_TERM      => FALSE, -- Differential Termination
      IBUF_LOW_PWR   => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD     => "DEFAULT"
               )
   port map (
      O  => clk_200m_buffered,
      I  => CLK_200M_IN_P,
      IB => CLK_200M_IN_N
   );

   main_bd_inst : main_bd
   port map (
      sys_clk_i                 => clk_200m_buffered,
      AXI_CLK                   => sys_clk,
      MB_MDIO_MDC               => mb_mdio_mdc,
      MB_MDIO_MDIO_I            => mb_mdio_mdio_i,
      MB_MDIO_MDIO_O            => mb_mdio_mdio_o,
      MB_MDIO_MDIO_T            => mb_mdio_mdio_t,
      MB_UART_RXD               => MB_UART_RX,
      MB_UART_TXD               => MB_UART_TX,
      VERSION_GPIO              => version_gpio,
      GPIO2_IO_I                => gpio2_io_i,
      GPIO2_IO_O                => gpio2_io_o,
      GPIO2_IO_T                => gpio2_io_t,
      GPIO_IO_I                 => gpio_io_i,
      GPIO_IO_O                 => gpio_io_o,
      GPIO_IO_T                 => gpio_io_t,
      IIC_RTL_SCL_I             => iic_rtl_scl_i,
      IIC_RTL_SCL_O             => iic_rtl_scl_o,
      IIC_RTL_SCL_T             => iic_rtl_scl_t,
      IIC_RTL_SDA_I             => iic_rtl_sda_i,
      IIC_RTL_SDA_O             => iic_rtl_sda_o,
      IIC_RTL_SDA_T             => iic_rtl_sda_t,
      SPI_RTL_IO0_I             => spi_rtl_io0_i,
      SPI_RTL_IO0_O             => spi_rtl_io0_o,
      SPI_RTL_IO0_T             => spi_rtl_io0_t,
      SPI_RTL_IO1_I             => spi_rtl_io1_i,
      SPI_RTL_IO1_O             => spi_rtl_io1_o,
      SPI_RTL_IO1_T             => spi_rtl_io1_t,
      SPI_RTL_IO2_I             => spi_rtl_io2_i,
      SPI_RTL_IO2_O             => spi_rtl_io2_o,
      SPI_RTL_IO2_T             => spi_rtl_io2_t,
      SPI_RTL_IO3_I             => spi_rtl_io3_i,
      SPI_RTL_IO3_O             => spi_rtl_io3_o,
      SPI_RTL_IO3_T             => spi_rtl_io3_t,
      SPI_RTL_SS_I              => spi_rtl_ss_i,
      SPI_RTL_SS_O              => spi_rtl_ss_o,
      SPI_RTL_SS_T              => spi_rtl_ss_t,

      HOST_IP                   => udp_host_ip,
      HOST_IP_NETMASK           => udp_host_ip_netmask,
      HOST_MAC0                 => udp_host_mac(31 downto 0),
      HOST_MAC1                 => mb_host_mac1,

      DDR3_dq                   => ddr3_dq,
      DDR3_dqs_p                => ddr3_dqs_p,
      DDR3_dqs_n                => ddr3_dqs_n,
      DDR3_addr                 => ddr3_addr,
      DDR3_ba                   => ddr3_ba,
      DDR3_ras_n                => ddr3_ras_n,
      DDR3_cas_n                => ddr3_cas_n,
      DDR3_we_n                 => ddr3_we_n,
      DDR3_reset_n              => ddr3_reset_n,
      DDR3_ck_p                 => ddr3_ck_p,
      DDR3_ck_n                 => ddr3_ck_n,
      DDR3_cke                  => ddr3_cke,
      DDR3_cs_n                 => ddr3_cs_n,
      DDR3_dm                   => ddr3_dm,
      DDR3_odt                  => ddr3_odt

   );
   udp_host_mac(47 downto 32) <= mb_host_mac1(15 downto 0);
   udp_link_speed <= mb_host_mac1(18 downto 16);

  

  rxaui_inst : rxaui_0
  port map (
    reset               => rxaui_rst,
    dclk                => sys_clk,
    clk156_out          => rxaui_clk156_out,
    clk156_lock         => rxaui_clk156_lock,
    refclk_out          => open,
    refclk_p            => ETH_RXAUI_REFCLK_P,
    refclk_n            => ETH_RXAUI_REFCLK_N,
    qplloutclk_out      => open,
    qplllock_out        => rxaui_qplllock_out,
    qplloutrefclk_out   => open,
    xgmii_txd           => rxaui_xgmii_txd,
    xgmii_txc           => rxaui_xgmii_txc,
    xgmii_rxd           => rxaui_xgmii_rxd,
    xgmii_rxc           => rxaui_xgmii_rxc,
    rxaui_tx_l0_p       => ETH_RXAUI_TX_P(0),
    rxaui_tx_l0_n       => ETH_RXAUI_TX_N(0),
    rxaui_tx_l1_p       => ETH_RXAUI_TX_P(1),
    rxaui_tx_l1_n       => ETH_RXAUI_TX_N(1),
    rxaui_rx_l0_p       => ETH_RXAUI_RX_P(0),
    rxaui_rx_l0_n       => ETH_RXAUI_RX_N(0),
    rxaui_rx_l1_p       => ETH_RXAUI_RX_P(1),
    rxaui_rx_l1_n       => ETH_RXAUI_RX_N(1),
    signal_detect       => "11", -- signal always present
    debug               => rxaui_debug,
    mdc                 => rxaui_mdio_mdc,
    mdio_in             => rxaui_mdio_mdio_i,
    mdio_out            => rxaui_mdio_mdio_o,
    mdio_tri            => rxaui_mdio_mdio_t,
    prtad               => "01000",
    type_sel            => "11" -- PHY XGXS
  );

   -- IO Bufs for bidirectional signals
   -- I2C bus
   iic_rtl_scl_iobuf: component IOBUF
   port map (
      I => iic_rtl_scl_o,
      IO => I2C_SCL,
      O => iic_rtl_scl_i,
      T => iic_rtl_scl_t
   );
   iic_rtl_sda_iobuf: component IOBUF
   port map (
      I => iic_rtl_sda_o,
      IO => I2C_SDA,
      O => iic_rtl_sda_i,
      T => iic_rtl_sda_t
   );

   -- QSPI for configuration flash
   spi_rtl_io0_iobuf: component IOBUF
   port map (
      I => spi_rtl_io0_o,
      IO => CFG_QSPI_IO(0),
      O => spi_rtl_io0_i,
      T => spi_rtl_io0_t
   );
   spi_rtl_io1_iobuf: component IOBUF
   port map (
      I => spi_rtl_io1_o,
      IO => CFG_QSPI_IO(1),
      O => spi_rtl_io1_i,
      T => spi_rtl_io1_t
   );
   spi_rtl_io2_iobuf: component IOBUF
   port map (
      I => spi_rtl_io2_o,
      IO => CFG_QSPI_IO(2),
      O => spi_rtl_io2_i,
      T => spi_rtl_io2_t
   );
   spi_rtl_io3_iobuf: component IOBUF
   port map (
      I => spi_rtl_io3_o,
      IO => CFG_QSPI_IO(3),
      O => spi_rtl_io3_i,
      T => spi_rtl_io3_t
   );
   spi_rtl_ss_iobuf_0: component IOBUF
   port map (
      I => spi_rtl_ss_o(0),
      IO => CFG_QSPI_SS,
      O => spi_rtl_ss_i(0),
      T => spi_rtl_ss_t
   );

end architecture;









