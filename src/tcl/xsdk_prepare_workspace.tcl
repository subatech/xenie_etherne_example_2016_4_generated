#
# This script prepares workspace for SDK and builds testing app.
#




# Set the reference directory to where the script is
set origin_dir [file normalize [file dirname [info script]]]

# Set SDK workspace directory
set ws_dir [file normalize $origin_dir/../../mb_fw]

# Set path to HW specification file
set hw_spec [file normalize $origin_dir/../../vivado/eth_example.sdk/xenie_eth_example.hdf]

# Set workspace (it is supposed to be directory with projects)
setws $ws_dir

# Create HW project
createhw -name xenie_eth_example_hw_platform_0 -hwspec $hw_spec

# Set repository with modified IIC driver
repo -set $ws_dir/drivers
repo -scan

# Create BSP projects based on mss
createbsp -name mb_basic_test_bsp -mss $ws_dir/mb_basic_test_bsp_tmp/system.mss -hwproject xenie_eth_example_hw_platform_0


# Import application project
importprojects $ws_dir

# Set release build
configapp -app xenie_eth_test_womtd build-config release

# Clean projects and rebuild it
projects -clean
projects -build


