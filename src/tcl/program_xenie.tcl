
# This script loads resulting MCSs (both Marvell FW and FPGA bitstream)
# to serial configuration flash on Xenie 1.0 via JTAG cable.

# Open project if necessary, or use the opened one
if {[llength [current_project -quiet]] == 0} {
   # Set the reference directory to where the script is
   set origin_dir [file normalize [file dirname [info script]]]

   # Set project directory path
   set proj_dir [file normalize $origin_dir/../../vivado]

   # open project
   open_project $proj_dir/eth_example.xpr
}

# Set the reference directory to where the script is
set origin_dir [file normalize [file dirname [info script]]]

# open HW manager
open_hw

# Open target
if {[llength [get_hw_servers -quiet]] == 0} {
   connect_hw_server
}

open_hw_target


current_hw_device [lindex [get_hw_devices -regexp xc7k.*] 0]
refresh_hw_device -update_hw_probes false [current_hw_device]



# Add spi flash
if {[llength [get_property PROGRAM.HW_CFGMEM [current_hw_device]]] == 0} {
   create_hw_cfgmem -hw_device [current_hw_device] -mem_dev  [lindex [get_cfgmem_parts {s25fl256sxxxxxx0-spi-x1_x2_x4}] 0]
}

#current_hw_cfgmem [get_property PROGRAM.HW_CFGMEM [current_hw_device]]

set_property PROGRAM.BLANK_CHECK  0 [current_hw_cfgmem]
set_property PROGRAM.ERASE  1 [current_hw_cfgmem]
set_property PROGRAM.CFG_PROGRAM  1 [current_hw_cfgmem]
set_property PROGRAM.VERIFY  1 [current_hw_cfgmem]
set_property PROGRAM.CHECKSUM  0 [current_hw_cfgmem]
set_property PROGRAM.UNUSED_PIN_TERMINATION {pull-none} [current_hw_cfgmem]
refresh_hw_device [current_hw_device]






if {![string equal \
         [get_property PROGRAM.HW_CFGMEM_TYPE  [current_hw_device]] \
         [get_property MEM_TYPE [get_property CFGMEM_PART [current_hw_cfgmem]]] \
     ] } { 
      create_hw_bitstream -hw_device [current_hw_device] [get_property PROGRAM.HW_CFGMEM_BITFILE [current_hw_device]]; 
      program_hw_devices [current_hw_device]; 
   };


set_property PROGRAM.ADDRESS_RANGE  {use_file} [current_hw_cfgmem]

# Program Marvell PHY FW mcs
set prog_file "$origin_dir/../../outputs/res/marvell_only.mcs"
set_property PROGRAM.FILES $prog_file [current_hw_cfgmem]
program_hw_cfgmem -hw_cfgmem [current_hw_cfgmem]

# Program Microblaze MCS
set prog_file "$origin_dir/../../outputs/res/xenie_eth_example_mb.mcs"
set_property PROGRAM.FILES $prog_file [current_hw_cfgmem]

program_hw_cfgmem -hw_cfgmem [current_hw_cfgmem]



