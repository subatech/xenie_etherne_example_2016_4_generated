
# 
# This script run all necessary scripts to get final MCSs to be loaded to
# Xenie 1.0.
#
#

# Recreate Vivado project for example design
source recreate_project_xc7k160t.tcl

# Run synthesis, implementation, generate bitstream and HW description for SDK
source build_project.tcl

# Recreate SDK workspace, create HW platform project, rebuild SW
exec xsdk -batch -source xsdk_prepare_workspace.tcl

# Create MCS with Marvell FW
source ../../outputs/create_mcs_marvell_only.tcl

# Gather all generated files needed to create MCS with Microblaze
source ../../outputs/update_src_data.tcl

# Create MCS with Microblaze with test application loaded to BRAM
source ../../outputs/create_mcs_microblaze.tcl

#
# All needed to program flash is available. When ready run program_xenie.bat
#
