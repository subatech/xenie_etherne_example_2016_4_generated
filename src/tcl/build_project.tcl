
# Set the reference directory to where the script is
set origin_dir [file normalize [file dirname [info script]]]

# Set project directory path
set proj_dir [file normalize $origin_dir/../../vivado]

# Open project if necessary, or use the opened one
if {[llength [current_project -quiet]] == 0} {
   open_project $proj_dir/eth_example.xpr
}
# Launch all steps necssary to build bitstream
launch_runs impl_1 -to_step write_bitstream -jobs 4

# Wait for run to finish
wait_on_run impl_1

# We are done so export HW with bitstream for SDK
file mkdir $proj_dir/eth_example.sdk
file copy -force $proj_dir/eth_example.runs/impl_1/xenie_eth_example.sysdef \
   $proj_dir/eth_example.sdk/xenie_eth_example.hdf
