
# Refreshes recreate scripts
# Block design must be open when this script is called.
# TODO: ensure block design is opened automatically


set proj_cur_dir [get_property DIRECTORY [current_project]]
set tcl_dir $proj_cur_dir/../src/tcl

# Create recreate script for BD
write_bd_tcl -force "$tcl_dir/recreate_bd.tcl"

# Create recreate script for project
write_project_tcl -all_properties -no_copy_sources -force "$tcl_dir/recreate_project.tcl"
